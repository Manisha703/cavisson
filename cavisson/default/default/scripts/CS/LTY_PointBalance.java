// Converted from Acc_Services_End_To_End_C/LTY_PointBalance.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class LTY_PointBalance implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
    int status = 0;
    nsApi.ns_start_transaction("LTY_PointBalance");
    nsApi.ns_web_url ("LTY_PointBalance",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/{LPFLTY}/balance",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
         "HEADER=X-SESSIONID: {LPFSessionID1}",
        "HEADER=X-PROFILEID: {LPFProfileID1}",
);
    nsApi.ns_end_transaction("LTY_PointBalance", NS_AUTO_STATUS);
	return status;

}


}
