// Converted from Acc_Services_End_To_End_C/GetProfile.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class GetProfile implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{	
	int status = 0;
	nsApi.ns_start_transaction("GetProfile");
     nsApi.ns_web_url ("GetProfile",
         "URL=https://{AccServiceHostUrl}/v1/profile",
         "METHOD=GET",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=userId: {LPFEMail}",
         "HEADER=X-SESSIONID:{LPFSessionID}",
         "HEADER=X-PROFILEID:{LPFProfileID}",
         "HEADER=correlation-id: GetProfile_{CorrChar}{CorrID}",
);
    nsApi.ns_end_transaction("GetProfile", NS_AUTO_STATUS);
    nsApi.ns_save_data_eval("/home/netstorm/work/data/Stress2/ProfileID_New_APR22.txt",NS_APPEND_FILE,"{EMailID},{GetID}");


	if(nsApi.ns_eval_string("{Payload}").equals("payload"))
	{
	status = nsApi.ns_save_data_eval("/home/netstorm/work/data/AMServices/test.txt", NS_APPEND_FILE,
                "{EMail},{SessionID},{ProfileID},{Profileid},{profileid},{FltyId}");
	}
	return status;	
}

}
