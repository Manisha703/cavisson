//*****************************Common Host URL File******************************************
nsl_static_var(UIHostUrl:1,SnBServiceHostUrl:2,AccServiceHostUrl:3,CnCServiceHostUrl:4,SolrHost:5,WcsHostURL:6, DATADIR=Stress, File=Common/Host.txt.seq, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All);

//*******************StressProj Email ids 

nsl_static_var(FP_Email_ID:1,FP_Password:2,FP_DynID:3,FP_XprofileId:4, DATADIR=Stress, File=Platform_E2E/REG_EmaiIds.unq, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_static_var(EmailID_Wallet:1,Password_Wallet:2,DynID_Wallet:3,XprofileId_Wallet:4, DATADIR=Stress, File=Platform_E2E/CreateAccount_CSM_YLW_31_Jan.txt.use, Refresh=USE, Mode=USE_ONCE, EncodeMode=All, CopyFileToTR=No, OnUseOnceError=ABORTSESSION, UseOnceWithinTest=Yes);
nsl_static_var(SL_FP_Email_ID:1,SL_FP_Password:2,SL_FP_DynID:3,SL_FP_XProfileID:4, DATADIR=Stress, File=Platform_E2E/SL_EmaiIds_Set2.txt, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_static_var(Bopus_FP_Email_ID:1,Bopus_FP_Password:2,Bopus_FP_DynID:3,Bopus_FP_XProfileID:4, DATADIR=Stress, File=Platform_E2E/Bopus_EmaiIds.unq, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_static_var(VGC_FP_Email_ID:1,VGC_FP_Password:2,VGC_FP_DynID:3,VGC_FP_XProfileID:4, DATADIR=Stress, File=Platform_E2E/Boss_EmaiIds.unq, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_static_var(Boss_FP_Email_ID:1,Boss_FP_Password:2,Boss_FP_DynID:3,Boss_FP_XProfileID:4, DATADIR=Stress, File=Platform_E2E/VGC_EmaiIds.unq, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
//Create profile Input file to handle email Already exist scenario
nsl_static_var(CP_Email_ID:1,CP_Password:2,CP_DynID:3,CP_XprofileId:4, DATADIR=Stress, File=Platform_E2E/REG_EmaiIds.seq, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_search_var(SP_UserExist, PAGE=CA_AccountPage, LB="errorKey\":\"", RB="\"}]", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(deviceId:1, DATADIR=Stress, File=Platform_E2E/SessionIDs.seq, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_static_var(FP_Email_NonLoyalty_KCC:1,NonLoyalty_KCC_LoyaltyID:2, DATADIR=Stress, File=Platform_E2E/REG_NonLoaylty_KCC.unq, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);

nsl_decl_var(DV_EmailID);
nsl_decl_var(DV_Password);
nsl_decl_var(DV_DynID);
nsl_decl_var(DV_XProfileID);

nsl_decl_var(DV_OrderType);
nsl_decl_var(DV_kls_sbp);
nsl_decl_var(DV_UserType);

nsl_decl_var(UnReg_EMailID);
nsl_decl_var(DV_PostalCode);
nsl_decl_var(DV_StoreID);

nsl_decl_var(DV_Environment);
nsl_decl_var(DV_PIDJSP);
nsl_decl_var(DV_EDE_SearchKeyword);
nsl_decl_var(DV_EDE_pgid);

nsl_decl_var(DV_Del_CartItemID);
nsl_static_var(GiftCardFP:1,GiftCardPinFP:2, DATADIR=Stress, File=Platform_E2E/Wallet_Giftcard.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(PromocodeFP, DATADIR=Stress, File=Platform_E2E/Offer.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All, CopyFileToTR=No);

nsl_static_var(FP_Add1:1,FP_Add2:2,FP_ACity:3,FP_AState:4,FP_ApostalCode:5,DATADIR=Stress, File=Platform_E2E/AddAddress.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(SearchKeyword:1, DATADIR=Stress, File=Platform_E2E/solr-new-search_Keywords.txt, Refresh=USE, Mode=RANDOM, FirstDataLine=2, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(EDE_SearchKeyword:1, DATADIR=Stress, File=Platform_E2E/EDE_SerachKeyword.txt, Refresh=USE, Mode=RANDOM, FirstDataLine=2, EncodeMode=Specified,CopyFileToTR=No);
nsl_static_var(EDE_ProdcutId:1,EDE_StoreID:2,DATADIR=Stress, File=Platform_E2E/EDE_ProductIds.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);


nsl_static_var(PromoCodes:1, DATADIR=Stress, File=Platform_E2E/Stress_PromoCode.txt, Refresh=USE, Mode=RANDOM, FirstDataLine=2, EncodeMode=None, CopyFileToTR=No);
nsl_static_var(PromoCodesSUPC:1, DATADIR=Stress, File=Platform_E2E/Stress_SUPCOffer.use, Refresh=USE, Mode=USE_ONCE, EncodeMode=None, CopyFileToTR=No, OnUseOnceError=ABORTSESSION, UseOnceWithinTest=No);
nsl_static_var(FP_kohlsCashNum:1,FP_Pin:2, DATADIR=Stress, File=Platform_E2E/kohlsCashNo.txt.use, Refresh=USE, Mode=USE_ONCE, FirstDataLine=2, EncodeMode=None, CopyFileToTR=No, OnUseOnceError=ABORTPAGE, UseOnceWithinTest=Yes);


nsl_static_var(SP_Search_categoryName:1,SP_Search_departmentName:2,DATADIR=Stress, File=Platform_E2E/Dept_categaroy.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);



nsl_decl_var(DV_PlaceOrder);
nsl_decl_var(DV_AddCardBody);
nsl_decl_var(DV_PaymentCard);
nsl_decl_var(DV_paymentId);
nsl_decl_var(DV_cvv);
nsl_decl_var(DV_ccp);



nsl_random_number_var(RN_Number, Min=1, Max=100, Format=%01lu, Refresh=SESSION);
nsl_random_string_var(RN_First_Name, Min=1, Max=5, CharSet="a-zA-Z0-9", Refresh=SESSION);
nsl_date_var(CurrentDate, Format="%r/%m-%d-%y", Refresh=USE);

nsl_decl_var(DV_pgid);

nsl_static_var(FP_skuId:1,FP_productID:2,DATADIR=Stress, File=Platform_E2E/SKU_ProductId.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(Bopus_ProdcutId:1,Bopus_StoreID:2,Bopus_SkuIDFP:3,DATADIR=Stress, File=Platform_E2E/Stress_Bopus_ProductIds.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(Boss_ProdcutId:1, DATADIR=Stress, File=Platform_E2E/Boss_Prod_ProductIds.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);

nsl_static_var(Boss_Sku:1,Boss_StoreID:2,Boss_zipcode:3,DATADIR=Stress, File=Platform_E2E/Stress_Boss_Pid.txt, Refresh=USE, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(Boss_SkuFP:1,Boss_StoreIDFP:2,Boss_ProductIdFP:4,DATADIR=Stress, File=Platform_E2E/Stress_Boss_Pid.txt, Refresh=USE, Mode=RANDOM, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);


nsl_static_var(FP_PID_1_50:1, DATADIR=Stress, File=Platform_E2E/PID_1_50_ProductID_New.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(FP_PID_51_100:1, DATADIR=Stress, File=Platform_E2E/PID_51_100_ProductID_New.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(FP_PID_101_200:1, DATADIR=Stress, File=Platform_E2E/PID_101_200_ProductID_New.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(FP_PID_201_500:1, DATADIR=Stress, File=Platform_E2E/PID_201-500-productids_New.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(FP_PID_501_1000:1, DATADIR=Stress, File=Platform_E2E/PID_501-1000-productids_New.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(FP_PID_1001:1, DATADIR=Stress, File=Platform_E2E/PID_1000-productids_New.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All, CopyFileToTR=No);




// --------------Anonymous Personalisation --------------------------//
nsl_static_var(VisIdFP:1, MCMID_FP:2, DATADIR=Stress, File=Platform_E2E/MCMID.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2);
nsl_decl_var(DV_KLS_SBP);

//-------------------------------Home page ----------------------------------
nsl_web_find(TEXT="sessionStatus", PAGE=Home_Session, FAIL=NOTFOUND, ActionOnFail=CONTINUE);

//-----------------------------------Create Account ---------------------
nsl_decl_var(DS_EMailID);
nsl_web_find(TEXT="Invalid value passed for Email", PAGE=CA_AccountPage, FAIL=FOUND, ID="Invalid value passed for Email", ActionOnFail=STOP);
nsl_search_var(SP_AC_ProfileId, PAGE=CA_AccountPage, LB="\"id\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_AC_xprofileid, PAGE=CA_AccountPage, LB="\"x-profileid\":\"", RB=".\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="Account Created Successfully", PAGE=CA_AccountPage, FAIL=NOTFOUND, ID="Invalid value passed for Email", ActionOnFail=STOP);
nsl_web_find(TEXT="My Info", PAGE=MyInfo, FAIL=NOTFOUND, ID="MyInfo page not found", ActionOnFail=CONTINUE);

nsl_web_find(TEXT="Please enter your email address", PAGE=ClickOnCreate_1, FAIL=NOTFOUND, ID="ClickOnCreate_1 page not found", ActionOnFail=STOP);


//------------------------------------RegLogin ---------------------------
nsl_random_string_var(Log_In_Successfull_Page, Min=5, Max=9, CharSet="a-zA-Z0-9", Refresh=SESSION);
nsl_web_find(TEXT="<title>Account</title>", PAGE=Login_Page_loyalty, FAIL=NOTFOUND, ActionOnFail=STOP);
nsl_search_var(SP_sessionid, PAGE=LoggedIn, PAGE=CA_AccountPage, PAGE=LoggedIn_Boss_VGC, LB="\"x-sessionid\":\"", RB="\",\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(SP_Email_ID);
//-----------------------------Add Ship Address
nsl_search_var(SP_addressKey, PAGE=Get_Ship_Address_Address, LB="\"ID\":\"", RB="\",\"", LBMATCH=FIRST,ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_ShipAddress, PAGE=Get_Ship_Address_ship, LB="\"addr1\":\"", RB="\",\"", LBMATCH=FIRST,ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_random_string_var(random, Min=6, Max=7, CharSet="a-zA-Z0-9", Refresh=USE);

nsl_search_var(SP_Address_Key, PAGE=Get_Ship_Address_ship, LB="\"ID\":\"", RB="\",\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(D_addressKey);

nsl_web_find(TEXT="Profile Updated Successfully", PAGE=Update_Shipping_Details, Page=Update_customer_info, FAIL=NOTFOUND, ID="Profile was Updated Successfully", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Profile Updated Successfully", PAGE=RemoveShippingAddress, FAIL=NOTFOUND, ID="Address Not Removed", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="addr1", PAGE=Edit_Ship_Addr_Page, FAIL=NOTFOUND, ID="No Address Found on this page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Profile Updated Successfully", PAGE=RemoveShippingAddress, FAIL=NOTFOUND, ID="Address not removed successfully", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Profile Updated Successfully", PAGE=Save_Shipping_detail_Info, FAIL=NOTFOUND, ID="New Address not saved successfully ", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Profile Updated Successfully", PAGE=Update_Shipping_Details, FAIL=NOTFOUND, ID="Edit Address not saved successfully ", ActionOnFail=CONTINUE);

//---------------------------------------------------------------------------------------------------
// ------------------ Add_Card ---------------------
nsl_web_find(TEXT="This Credit Card already exists", PAGE=SaveNewPaymentInfo_Visacard, FAIL=FOUND, ID="This Credit Card already exists", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="\"message\":\"Profile Updated Successfully\",\"id\":\"", PAGE=SaveNewPaymentInfo_Visacard, FAIL=NOTFOUND, ID="Visa card not added successfully", ActionOnFail=CONTINUE);
nsl_search_var(SP_cardNumVisa, PAGE=Customer_info_details1, LB=",\"cardNum\":\"", RB="\",\"type\":\"visa\"", LBMATCH=CLOSEST,SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_CardNumKC, PAGE=Customer_info_details,  LB=",\"cardNum\":\"", RB="\",\"type\":\"kohlsCharge\"", LBMATCH=CLOSEST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_decl_var(DV_cardID);
nsl_decl_var(DV_expyear);

//--------------------------- SNB_Typeahead -----------------------------------------------------
nsl_random_string_var(RS_typehead, Min=1, Max=2, CharSet="a-z", Refresh=USE);
nsl_search_var(SP_defSuggestion, PAGE=KeyStroke_slr, LB ="{\"Suggestions\":[\"", RB ="\"", LBMATCH=FIRST, SaveOffset=0, Convert=TextToUrl, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Suggestion_block, PAGE=KeyStroke_slr, LB ="{\"Suggestions\":[", RB ="]", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Suggestion_all, PAGE=KeyStroke_slr, LB=",\"", RB="\"", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, Convert=TextToUrl, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(SP_Suggestion);
nsl_search_var(SP_productURL, PAGE=Visultyehead, PAGE=VisualTypeaheadDefault, LB="\"productURL\":\"", RB="submit-search", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_searchKeyword, PAGE=Visultyehead, PAGE=VisualTypeaheadDefault, LB="\"searchKeyword\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_web_find(TEXT="We're sorry, the page you're looking for may be temporarily unavailable or no longer exist", PAGE=SelectVisualProdcut, FAIL=FOUND, ID="SelectVisualProdcut Page not found", ActionOnFail=CONTINUE);
nsl_decl_var(productItems);

//-------------------------------------SearchBykeyword--------
nsl_search_var(SP_prodSeoURL, PAGE=SearchBykeyword,PAGE=CatalogPage, PAGE=SaleEvent1, LB="\"prodSeoURL\":\"/product/", RB="\"}", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=Specified, EncodeSpaceBy=%20);
//nsl_search_var(SP_prodSeoURL, PAGE=SearchBykeyword,PAGE=CatalogPage, PAGE=SaleEvent1, LB="\"prodSeoURL\":\"/product/", RB="\"}", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_prodSeoURL_ALL, PAGE=SearchBykeyword,PAGE=CatalogPage, PAGE=SaleEvent1, LB="\"prodSeoURL\":\"/product/", RB="\"}", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=Specified, EncodeSpaceBy=%20);
nsl_search_var(SP_SearchKeyword_productID, PAGE=SearchPDP, PAGE=Search_BopusProduct, PAGE=Search_BossProduct, LB ="\"productID\":\"", RB ="\"," , LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=SearchBykeyword, FAIL=FOUND, ID="SearchBykeyword not found on PDP page",ActionOnFail=CONTINUE);
//---------------------------- Snb Browsing-----------------

nsl_search_var(SP_CatalogUrl, PAGE=SaleEvent1, LB="href=\"/catalog/", RB="\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(SP_SaleEventURL:1, DATADIR=Stress, File=Platform_E2E/Sale_Event.ran, Refresh=SESSION, Mode=RANDOM, EncodeMode=None, CopyFileToTR=No);
nsl_random_number_var(SortBy, Min=1, Max=7, Format=%01lu, Refresh=SESSION);
nsl_search_var(SP_totalRecordsCount, PAGE=Catalog_Refines, PAGE=CatalogPage, LB="\"totalRecordsCount\":", RB=",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_webID, PAGE=CatalogPage, LB="{\"webID\":\"", RB="\",\"productTitle\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_WS);
nsl_search_var(SP_CatPage_prodSeoURL_Block, PAGE=CatalogPage, PAGE=SaleEvent1, LB="\"prodType\":\"product\"", RB="\"webID\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=Specified, EncodeSpaceBy=%20);
nsl_search_var(SP_CatPage_prodSeoURL, PAGE=CatalogPage, PAGE=SaleEvent1,PAGE=SearchBykeyword, LB="\"prodSeoURL\":\"/product", RB="\"}", LBMATCH=FIRST, ORD=ANY, SaveOffset=0,Search=Variable, Var=SP_CatPage_prodSeoURL_Block, RETAINPREVALUE="NO", EncodeMode=Specified, EncodeSpaceBy=%20);
nsl_search_var(SP_Refine_URL, PAGE=CatalogPage, LB ="<a href=\"/catalog/", RB ="\" ", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_web_find(TEXT="sale-event", PAGE=SaleEvent1, FAIL=NOTFOUND, ID="Wrong Page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="We couldn't find a match", PAGE=Catalog_Pagination, FAIL=FOUND, ID="Catalog_Pagination not found", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Product_Not_Available", PAGE=Catalog_PDP, FAIL=FOUND, ID="PID not found on Catalog_PDP page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="The page you're looking for has been moved, deleted or can't be found", PAGE=CatalogPage, FAIL=FOUND, ID="No product found for Catalog URL on CatalogPage", ActionOnFail=CONTINUE);

//-------------------------  SEARCH BY PID 1-50---------------------
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=PDP_search_sku_50, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=pid_1_50_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(pid_1_50_notfound);
nsl_web_find(TEXT="skuCode", PAGE=PDP_search_sku_50, FAIL=NOTFOUND, ID="PID not found on PDP page", SaveCount=pid_1_50_sku_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(pid_1_50_sku_notfound);

nsl_search_var(SP_skuCode,PAGE=Search_BopusProduct, PAGE=Search_BossProduct,PAGE=PDP_search_sku_500,PAGE=PDP_search_sku_100,PAGE=PDP_search_sku_200, PAGE=PDP_search_sku_50,PAGE=PDP_search_sku_1000,PAGE=PDP_search_sku_1001, LB="\"skuCode\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//-------------------------  SEARCH BY PID 51-100---------------------
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=PDP_search_sku_100, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=pid_51_100_notfound, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="skuCode", PAGE=PDP_search_sku_100, FAIL=NOTFOUND, ID="PID not found on PDP page", SaveCount=pid_1_50_sku_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(pid_51_100_sku_notfound);
nsl_decl_var(pid_51_100_notfound);
nsl_search_var(SP_productID_100, PAGE=PDP_search_sku_100, LB ="\"productID\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//-------------------------  SEARCH BY PID 101-200---------------------
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=PDP_search_sku_200, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=pid_101_200_notFound, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="skuCode", PAGE=PDP_search_sku_200, FAIL=NOTFOUND, ID="PID not found on PDP page", SaveCount=pid_101_200_sku_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(pid_101_200_sku_notfound);
nsl_decl_var(pid_101_200_notFound);



//-------------------------  SEARCH BY PID 501-1000---------------------
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=PDP_search_sku_1000, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=pid_501_1000_notfound, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="skuCode", PAGE=PDP_search_sku_1000, FAIL=NOTFOUND, ID="PID not found on PDP page", SaveCount=pid_501_1000_sku_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(pid_501_1000_notfound);
nsl_decl_var(pid_501_1000_sku_notfound);

//-------------------------  SEARCH BY PID 1000+ Skus---------------------
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=PDP_search_sku_1001, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=pid_1001_notfound, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="skuCode", PAGE=PDP_search_sku_1001, FAIL=NOTFOUND, ID="PID not found on PDP page", SaveCount=pid_501_1000_sku_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(pid_1001_sku_notfound);
nsl_decl_var(pid_1001_notfound);

//----------------------------BigDatProdcutid
nsl_search_var(SP_bd_productID, PAGE=Search_BopusProduct,PAGE=Search_BopusProduct,PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,LB ="\"productID\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_bdRendering_Block, PAGE=Search_BopusProduct,PAGE=Search_BopusProduct,PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,LB ="\"bdRendering\":", RB =",\"videos\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_departmentName, PAGE=SelectVisualProdcut,PAGE=Catalog_PDP,PAGE=SearchPDP, LB="\"departmentName\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_bdRendering_Block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_categoryName, PAGE=SelectVisualProdcut,PAGE=Catalog_PDP,PAGE=SearchPDP, LB="\"categoryName\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_bdRendering_Block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_subcategoryName, PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP, LB="\"subcategoryName\":\"", RB="\"}", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_bdRendering_Block, RETAINPREVALUE="NO", EncodeMode=None);

//--------------------ZineOne-----------------------------------------------------
nsl_search_var(SP_originalPrice, PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,LB ="\"originalPrice\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);

nsl_search_var(SP_bossQty,PAGE=Bopus_productInventoryCheck,PAGE=Boss_productInventoryCheck,PAGE=Catalog_ProductInventoryPrice,PAGE=Search_ProductInventoryPrice, LB="\"bossQty\":", RB=",", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_bopusQty, PAGE=Bopus_productInventoryCheck, PAGE=Boss_productInventoryCheck, PAGE=Catalog_ProductInventoryPrice, PAGE=Search_ProductInventoryPrice, LB="\"bopusQty\":", RB="}", LBMATCH=FIRST, ORD=ALL, SaveLen=1, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_zinesku,PAGE=Bopus_productInventoryCheck,PAGE=Boss_productInventoryCheck,PAGE=Catalog_ProductInventoryPrice,PAGE=Search_ProductInventoryPrice, LB="\"skuCode\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_onlineAvailableQty,PAGE=Bopus_productInventoryCheck,PAGE=Boss_productInventoryCheck,PAGE=Catalog_ProductInventoryPrice,PAGE=Search_ProductInventoryPrice, LB="\"onlineAvailableQty\":", RB=",\"", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);

nsl_search_var(SP_Zine_prodSeoURL,PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,PAGE=Search_BossProduct,PAGE=Search_BopusProduct, LB="\"pageURL\":\"", RB=",", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=Specified, EncodeSpaceBy=%20);
nsl_search_var(SP_z1pagepath, PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,PAGE=Search_BossProduct,PAGE=Search_BopusProduct, LB="product/", RB=".jsp", LBMATCH=CLOSEST, SaveOffset=0, Search=Variable, Var=SP_Zine_prodSeoURL, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_prdPV, PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,PAGE=Search_BossProduct,PAGE=Search_BopusProduct, LB="prdPV=", RB="\"", LBMATCH=CLOSEST, SaveOffset=0, Search=Variable, Var=SP_Zine_prodSeoURL, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_z1pagename,PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,PAGE=Search_BossProduct,PAGE=Search_BopusProduct,LB="/", RB=".jsp", LBMATCH=CLOSEST, SaveOffset=0, Search=Variable, Var=SP_Zine_prodSeoURL, RETAINPREVALUE="NO", EncodeMode=None);


nsl_search_var(SP_Zine_Bagitem_Block, PAGE=AddtoBag,PAGE=Boss_AddtoBag,PAGE=Bopus_AddtoBag, LB="\"cartItems\":[{\"", RB="}]" , LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="Yes", EncodeMode=None);
nsl_search_var(SP_zine_cartItemId, PAGE=AddtoBag,PAGE=Boss_AddtoBag,PAGE=Bopus_AddtoBag, LB="\"cartItemId\":\"", RB="\"", LBMATCH=CLOSEST, ORD=ALL,SaveOffset=0, Search=Variable, Var=SP_Zine_Bagitem_Block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_zine_skuId, PAGE=AddtoBag,PAGE=Boss_AddtoBag,PAGE=Bopus_AddtoBag, LB="\"skuId\":\"", RB="\"", LBMATCH=CLOSEST, ORD=ALL,SaveOffset=0, Search=Variable, Var=SP_Zine_Bagitem_Block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_zine_productId, PAGE=AddtoBag,PAGE=Boss_AddtoBag,PAGE=Bopus_AddtoBag, LB="\"productId\":\"", RB="\"", LBMATCH=CLOSEST, ORD=ALL,SaveOffset=0, Search=Variable, Var=SP_Zine_Bagitem_Block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_zine_saleUnitprice, PAGE=AddtoBag,PAGE=Boss_AddtoBag,PAGE=Bopus_AddtoBag, LB="\"saleUnitprice\":", RB=",", LBMATCH=CLOSEST, ORD=ALL,SaveOffset=0, Search=Variable, Var=SP_Zine_Bagitem_Block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_zine_regularUnitPrice, PAGE=AddtoBag,PAGE=Boss_AddtoBag,PAGE=Bopus_AddtoBag, LB="\"regularUnitPrice\":", RB=",", LBMATCH=CLOSEST, ORD=ALL,SaveOffset=0, Search=Variable, Var=SP_Zine_Bagitem_Block, RETAINPREVALUE="NO", EncodeMode=None);


//------------------------------Normal AddToBag --------------------------------
nsl_search_var(SP_productID, PAGE=SelectVisualProdcut,PAGE=Catalog_PDP, PAGE=SearchPDP,PAGE=PDP_search_sku_50,PAGE=PDP_search_sku_100,PAGE=PDP_search_sku_200,PAGE=PDP_search_sku_1001,LB ="\"productID\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_sku_availabilty_block, PAGE=SelectVisualProdcut, PAGE=Catalog_PDP, PAGE=SearchPDP,PAGE=PDP_search_sku_50,PAGE=PDP_search_sku_100,PAGE=PDP_search_sku_200,PAGE=PDP_search_sku_1001,  LB="{\"skuCode\"", RB="\"availability\":\"In Stock\"", LBMATCH=CLOSEST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_skuId, PAGE=SelectVisualProdcut,PAGE=Catalog_PDP,PAGE=SearchPDP,PAGE=PDP_search_sku_50,PAGE=PDP_search_sku_100,PAGE=PDP_search_sku_200,PAGE=PDP_search_sku_1001, LB=":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_sku_availabilty_block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_Bagitem_all, PAGE=AddtoBag, LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_addtobagStatus, PAGE=AddtoBag, LB="\"status\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_statusMessage, PAGE=AddtoBag, LB="\"statusMessage\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_BagitemCount);
nsl_web_find(TEXT="Cart Retrieved Successfully", PAGE=AddtoBag,  FAIL=NOTFOUND, ID="Cart item id not not Retrieved Successfully", ActionOnFail=CONTINUE);

nsl_web_find(TEXT="You are only allowed 99 item", PAGE=AddtoBag, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=AddtoBag_99items,ActionOnFail=CONTINUE);
nsl_decl_var(AddtoBag_99items);

nsl_search_var(SP_available_pickup_block,PAGE=Bopus_productInventoryCheck,PAGE=Boss_productInventoryCheck,PAGE=Catalog_ProductInventoryPrice,PAGE=Search_ProductInventoryPrice, LB="\"skuCode\"", RB="\"UPC\"", LBMATCH=CLOSEST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_decl_var(DV_skuId);


//--------------------------ShopingCart -------------------------------------------
nsl_search_var(SP_skuId_all,PAGE=SoftLogin_SingedInCheckout, PAGE=ShoppingCart,PAGE=SingedInCheckout,LB="\"skuId\":\"", RB="\",", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_cartItemId_block_all,PAGE=SoftLogin_SingedInCheckout, PAGE=ShoppingCart,PAGE=SingedInCheckout,LB="\"cartItemId\"", RB="\"eGiftInfo\"", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_cartItemId_all,PAGE=SingedInCheckout, PAGE=SoftLogin_SingedInCheckout,PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems,LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_cartItemId_Any,PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=ShoppingCart, PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems, LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_del_cartItemId);

nsl_web_find(TEXT="cartItemId", PAGE=ShoppingCart, FAIL=NOTFOUND, ID="ShoppingCart not cart item found", ActionOnFail=STOP);

//----------------------------------Shipit faster andd update quantiy-------------------------- 
nsl_search_var(SP_CartItem_Update_Block,PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems,  LB="\"cartItemId\"", RB="\"eGiftInfo\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_update_cartItemId, PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems, LB=":\"", RB="\",\"skuId\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_CartItem_Update_Block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_update_skuid, PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems, LB="\"skuId\":\"", RB="\",\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_CartItem_Update_Block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_update_ProductId, PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems,  LB="\"productId\":\"", RB="\",\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_CartItem_Update_Block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_shipItFaster,PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems, LB="\"shipItFaster\":true", RB="},\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, Search=Variable, Var=SP_CartItem_Update_Block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(Sp_quantity, PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems,  LB=",\"quantity\":", RB=",\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_CartItem_Update_Block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_methodCode, PAGE=ShoppingCart,PAGE=CartItemRemove_OutOfStock,PAGE=CartItemRemove_Extraitems,  LB="\"methodCode\":\"", RB="\",\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, Search=Variable, Var=SP_CartItem_Update_Block, RETAINPREVALUE="YES", EncodeMode=None);

//---------------------------Checkout ---------------------------------
nsl_search_var(shipAddress_block, PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=Checkout,PAGE=AddAddress_OnCheckout, LB ="\"shipAddress\":", RB ="\"billingAddress\"" , LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Shiipadd_id_all,PAGE=SingedInCheckout ,PAGE=SoftLogin_SingedInCheckout,PAGE=Checkout,PAGE=AddAddress_OnCheckout, LB ="{\"id\":\"", RB ="\",\"addr1\"" , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, Search=Variable, Var=shipAddress_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Shiipadd_id, PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=Checkout,PAGE=AddAddress_OnCheckout, LB ="{\"id\":\"", RB ="\",\"addr1\"" , LBMATCH=FIRST, ORD=ANY, SaveOffset=0, Search=Variable, Var=shipAddress_block, RETAINPREVALUE="YES", EncodeMode=None);

nsl_search_var(SP_eligibleForExpeditedCheckout,PAGE=SingedInCheckout,PAGE=Checkout, LB="\"eligibleForExpeditedCheckout\":", RB=",\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_search_var(visa_paymentId_block,PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=Checkout,PAGE=ContinueToPayment,PAGE=AddPaymentCard_OnCheckout, LB="{\"paymentId", RB=",\"type\":\"visa\"," , LBMATCH=CLOSEST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_paymentId_visa,PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=Checkout,PAGE=ContinueToPayment,PAGE=AddPaymentCard_OnCheckout, LB="\":\"", RB="\",\"cvvRequired\"" , LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=visa_paymentId_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(kc_paymentId_block, PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=Checkout,PAGE=ContinueToPayment,PAGE=AddPaymentCard_OnCheckout, LB="{\"paymentId", RB=",\"type\":\"kohlsCharge\",", LBMATCH=CLOSEST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_paymentId_kc,PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=ContinueToPayment,PAGE=Checkout,PAGE=AddPaymentCard_OnCheckout, LB="\":\"", RB="\",\"cvvRequired\"" , LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=kc_paymentId_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_paymentId_all,PAGE=SingedInCheckout,PAGE=SoftLogin_SingedInCheckout,PAGE=AddPaymentCard_OnCheckout,PAGE=ContinueToPayment, LB="\"paymentId\":\"", RB="\",\"cardNum\"s", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_search_var(SP_billAddId,PAGE=Checkout,PAGE=ContinueToPayment,PAGE=AddBillingAddress_OnCheckout, LB="\"billingAddress\":[{\"id\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_search_var(SP_cartId, PAGE=Orderreview, PAGE=Checkout,PAGE=ShoppingCart, LB="\"cartId\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_version, PAGE=Orderreview,PAGE=Checkout,PAGE=ShoppingCart, LB="\"version\":", RB=",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_search_var(SP_UnReg_cartId, PAGE=UnReg_Bopus_Orderreview,PAGE=UnReg_Orderreview,PAGE=ShoppingCart,PAGE=ContinueToPayment,PAGE=SingedInCheckout, LB="\"cartId\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_UnReg_version,PAGE=UnReg_Bopus_Orderreview, PAGE=UnReg_Orderreview,PAGE=ShoppingCart, PAGE=ContinueToPayment,PAGE=SingedInCheckout,LB="\"version\":", RB=",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);


nsl_web_find(TEXT="Order placed successfully", PAGE=Reg_PlaceOrder,PAGE=UnReg_placeorder,PAGE=UnReg_placeorder_1, FAIL=NOTFOUND, ID="Order not placed successfully on placeorder Page", ActionOnFail=STOP);
nsl_web_find(TEXT="Failed to authorize payment", PAGE=Reg_PlaceOrder,PAGE=UnReg_placeorder,PAGE=UnReg_placeorder_1, FAIL=FOUND, ID="Please check the card details", ActionOnFail=STOP);
nsl_web_find(TEXT="Failed to submit order, as actual tax has not been calculated", PAGE=Reg_PlaceOrder,PAGE=UnReg_placeorder,PAGE=UnReg_placeorder_1, FAIL=FOUND, ID="Placeorder is failed due to tax not calculated", ActionOnFail=STOP);
nsl_web_find(TEXT="Order details retrieved successfully", PAGE=OrderConfirm,PAGE=UnReg_order_confirm, FAIL=NOTFOUND, ID="order_confirm_jsp page not found", ActionOnFail=CONTINUE);


//-----------------------------Save order Details Parameter -----------------------------------
nsl_search_var(SP_ordplaced_orderId, PAGE=OrderConfirm, PAGE=UnReg_order_confirm,  LB="\"orderId\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_OrderPlacedEmail,  PAGE=OrderConfirm, PAGE=UnReg_order_confirm, LB="\"email\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_ordplaced_paymentType, PAGE=OrderConfirm, PAGE=UnReg_order_confirm, LB="\"paymentType\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_ordplaced_shipmentItemsCount, PAGE=OrderConfirm, PAGE=UnReg_order_confirm, LB="\"shipmentItemsCount\":", RB=",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//--------------------Set My store ---------------------------------------
nsl_static_var(FP_Store:1,FP_city:2,FP_state:3,FP_ZipCode:4, DATADIR=Stress, File=Platform_E2E/SetMystore.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All);
nsl_search_var(SP_zipcode_block, PAGE=AllStoresAvailabilitySearch, LB="\"addressLine1\"", RB ="\"longitude\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_zipCode, PAGE=AllStoresAvailabilitySearch, LB ="\"zipCode\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_zipcode_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_city, PAGE=AllStoresAvailabilitySearch, LB ="\"city\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, Convert=TextToUrl, Search=Variable, Var=SP_zipcode_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_storeId, PAGE=AllStoresAvailabilitySearch, LB="\"shipNode\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_zipcode_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_StoresetCookie, PAGE=submitFavStore, LB="setCookie\":\"", RB="\"}", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="allAvailableStores", PAGE=AllStoresAvailabilitySearch, FAIL=NOTFOUND, ID="Page Not Working", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="selectedStore", PAGE=submitFavStore, FAIL=NOTFOUND, ID="Page Not Working", ActionOnFail=CONTINUE);


//---------------------------------Bopus Search and Addto Bag-----------------------------------
nsl_search_var(SP_Bopus_productID, PAGE=Search_BopusProduct, LB ="\"productID\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_BopusSkus_block, PAGE=Bopus_productInventoryCheck, LB="\"skuCode\"", RB="\"availability\":\"In Stock\",\"bossPickupDate\"", LBMATCH=CLOSEST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Bopus_Sku, PAGE=Bopus_productInventoryCheck, LB =":\"", RB ="\",", LBMATCH=CLOSEST, SaveOffset=0, Search=Variable, Var=SP_BopusSkus_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Bopus_StoreID, PAGE=Bopus_productInventoryCheck, LB="\"storeNum\":\"", RB="\",", LBMATCH=CLOSEST, SaveOffset=0, Search=Variable, Var=SP_BopusSkus_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_bopus_sku_availability, PAGE=Bopus_productInventoryCheck, LB="\"availability\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_BopusSkus_block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Bopus_Bagitem_all, PAGE=Bopus_AddtoBag, LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_Bopus_BagitemCount);
nsl_search_var(SP_Bopus_addtobagStatus, PAGE=Bopus_AddtoBag, LB="\"status\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Bopus_statusMessage, PAGE=Bopus_AddtoBag, LB="\"statusMessage\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="Cart Retrieved Successfully", PAGE=Bopus_AddtoBag,  FAIL=NOTFOUND, ID="Bopus_AddtoBag page not found", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="skuCode", PAGE=Search_BopusProduct, FAIL=NOTFOUND, ID="Search_BopusProduct PID not found on PDP page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=Search_BopusProduct, FAIL=FOUND, ID="PID not found on Reg_PDP page", SaveCount=Bopus_pid_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(Bopus_pid_notfound);
nsl_web_find(TEXT="skuCode", PAGE=Bopus_productInventoryCheck, FAIL=NOTFOUND, ID="sku id is not eligible for Bopus on Bopus_productInventoryCheck Page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="We couldn't find a match", PAGE=Search_BopusProduct, FAIL=FOUND, ID="Search_BopusProduct  not found", ActionOnFail=STOP);
nsl_web_find(TEXT="You are only allowed 99 item", PAGE=Bopus_AddtoBag, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=AddtoBag_Bopus_99items,ActionOnFail=CONTINUE);
nsl_decl_var(AddtoBag_Bopus_99items);

//----------------------------Search and Boss Add to bag -------------

nsl_search_var(SP_Boss_productID, PAGE=Search_BossProduct, LB ="\"productID\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_BossSkus_block_all, PAGE=Boss_productInventoryCheck, LB="\"skuCode\"", RB="\"UPC\"", LBMATCH=CLOSEST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_BossQty);
nsl_decl_var(DV_Boss_skuid);
nsl_decl_var(SP_Boss_StoreID);
nsl_decl_var(DV_Boss_BagitemCount);
nsl_search_var(SP_Boss_Bagitem_all, PAGE=Boss_AddtoBag, LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Boss_addtobagStatus, PAGE=Boss_AddtoBag, PAGE=Bopus_AddtoBag, LB="\"status\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Boss_statusMessage, PAGE=Boss_AddtoBag, PAGE=Bopus_AddtoBag, LB="\"statusMessage\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

nsl_web_find(TEXT="Cart Retrieved Successfully", PAGE=Boss_AddtoBag,  FAIL=NOTFOUND, ID="Bopus_AddtoBag1 page not found", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="skuCode", PAGE=Search_BossProduct, FAIL=NOTFOUND, ID="Search_BossProduct PID not found on PDP page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="did not match any products. Please try again.", PAGE=Search_BossProduct, FAIL=FOUND, ID="PID not found on Reg_PDP page", SaveCount=Bopus_pid_notfound, ActionOnFail=CONTINUE);
nsl_decl_var(Boss_pid_notfound);
nsl_web_find(TEXT="skuCode", PAGE=Boss_productInventoryCheck, FAIL=NOTFOUND, ID="sku id is not eligible for Bopus on Bopus_productInventoryCheck Page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="We couldn't find a match", PAGE=Search_BossProduct, FAIL=FOUND, ID="Search_BopusProduct  not found", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="isBossAvailable", PAGE=Boss_productInventoryCheck, FAIL=NOTFOUND, ID="Wrong Page", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="storeBossEligible", PAGE=Boss_Availability, FAIL=NOTFOUND, ID="Boss Not Working", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="You are only allowed 99 item", PAGE=Boss_AddtoBag, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=AddtoBag_Boss_99items,ActionOnFail=CONTINUE);
nsl_decl_var(AddtoBag_Boss_99items);

//------------------------- Virtual Gift Card (VGC) ----------------------------
nsl_search_var(SP_CatPageVGC_prodSeoURL, PAGE=VGC_CatalogPage, LB="\"prodSeoURL\":\"/product", RB="\"}", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=Specified, EncodeSpaceBy=%20);
nsl_search_var(SP_VGC_totalRecordsCount, PAGE=VGC_CatalogPage, LB="\"totalRecordsCount\":", RB=",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_VGC_productID, PAGE=VGC_PDP, LB ="\"productID\":\"", RB ="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_VGC_sku_availabilty_block, PAGE=VGC_PDP,  LB="{\"skuCode\"", RB="\"availability\":\"In Stock\"", LBMATCH=CLOSEST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_VGC_skuId, PAGE=VGC_PDP, LB=":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_VGC_sku_availabilty_block, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SP_VGC_addtobagStatus, PAGE=VGC_AddtoBag_1, LB="\"status\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_VGC_statusMessage, PAGE=VGC_AddtoBag_1, LB="\"statusMessage\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_VGC_BagitemCount);
nsl_search_var(SP_VGC_Bagitem_all, PAGE=VGC_AddtoBag_1, LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="You are only allowed 99 item", PAGE=VGC_AddtoBag_1, FAIL=FOUND, ID="PID not found on PDP page", SaveCount=AddtoBag_VGC_99items,ActionOnFail=CONTINUE);
nsl_decl_var(AddtoBag_VGC_99items);

//------------------------------------------Purchase History --------------------------
nsl_web_find(TEXT="usenow\":\"USE NOW", PAGE=kc_notification_wallet_PurchaseHistory, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Order list retrieval", PAGE=Purchase_History_json_purchaseHistory, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Yes2You Rewards", PAGE=Purchase_History_purchase_history, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="usenow\":\"USE NOW", PAGE=kc_notification_wallet_PurchaseHistory, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Cart Retrieved Successfully", PAGE=AddAddress_OnCheckout, PAGE=CartItem_ShipItFaster, FAIL=NOTFOUND, ID="CartItem_ShipItFaster Page not found", ActionOnFail=CONTINUE);

//--------------------------------Apply promo code---------- 
nsl_web_find(TEXT="Coupon applied successfully", PAGE=ApplyPromoCode, FAIL=NOTFOUND, ID="Cart item id not not Retrieved Successfully", ActionOnFail=CONTINUE);
//nsl_web_find(TEXT="Cart Retrieved Successfully", PAGE=RemovePromoCode,PAGE=ApplyPromoCode,PAGE=RemoveUnQualified_PromoCode,  FAIL=NOTFOUND, ID="Cart item id not not Retrieved Successfully", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Unable to remove promocode", PAGE=RemovePromoCode,PAGE=RemoveUnQualified_PromoCode,FAIL=FOUND, ID="Unable to remove promocode", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="The promo code entered doesn't match our records", PAGE=ApplyPromoCode,FAIL=FOUND, ID="Unable to remove promocode", ActionOnFail=CONTINUE);
nsl_search_var(SP_PromoCode_all, PAGE=PC_KohlsCashModalWindowOpen, LB="\"type\":\"offer\",\"code\":\"", RB="\",\"description\"", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_decl_var(DV_promoCode);
nsl_search_var(SP_Promocode_qualifiedItems, PAGE=ApplyPromoCode, LB="\"qualifiedItems\":[", RB="],", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_qualified_PromoCode, PAGE=ApplyPromoCode, LB="\"code\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_Promocode_qualifiedItems, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Promocode_unQualifiedItems, PAGE=ApplyPromoCode,PAGE=RemovePromoCode, LB="\"unQualifiedItems\":[", RB="],", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_unQualified_PromoCode, PAGE=ApplyPromoCode,PAGE=RemovePromoCode, LB="\"code\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_Promocode_unQualifiedItems, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_paymenttypes_Block, PAGE=Customer_info_details, PAGE=Customer_info_details1, LB="\"paymentTypes\":", RB="}]", LBMATCH=FIRST,SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Card_Block_all, PAGE=Customer_info_details, PAGE=Customer_info_details1, LB="\"nameOnCard\"", RB="}", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, Search=Variable, Var=SP_paymenttypes_Block, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_BillingAddress, PAGE=LoadBilling_Address, LB="\"addr1\":\"", RB="\",\"addr2\":\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//--------------------------------------------------Wallet Operations
nsl_web_find(TEXT="This promo code didn?t work", PAGE=Wallet_AddOffer, FAIL=FOUND, ID="PromoCode Not Valid", ActionOnFail=STOP);
nsl_web_find(TEXT="You have already entered this Gift Card", PAGE=Wallet_AddGiftCard, FAIL=FOUND, ID="Already Have This GIFT card", ActionOnFail=STOP);
nsl_web_find(TEXT="Service Unavailable", PAGE=*, PAGE=Wallet_AddKohlsCash, FAIL=FOUND, ID="Service Unavailable", ActionOnFail=STOP);
nsl_web_find(TEXT="OFFER IS VALID ON PURCHASES MADE WITHIN", PAGE=Wallet_AddGiftCard, FAIL=NOTFOUND, ID="GiftCardNot Valid", ActionOnFail=STOP);
nsl_web_find(TEXT="Promo Codes are typically printed in bold-faced", PAGE=getMyWalletDetails_jsp, FAIL=NOTFOUND, ID="Wrong_response_getwallet", ActionOnFail=STOP);
nsl_web_find(TEXT="Mini Shopping Bag Content Starts Here", PAGE=my_wallet_jsp, FAIL=NOTFOUND, ID="Wrong_Response_Mywallet", ActionOnFail=CONTINUE);
nsl_search_var(SP_csstoken, PAGE=SessionTokenJSP, PAGE=Get_Ship_Address_ship,PAGE=customer_info_details,PAGE=Edit_Ship_Addr_Page,LB="\"csstoken\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_UCity, PAGE=Edit_Ship_Addr_Page, LB="\"city\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Uaddr1, PAGE=Edit_Ship_Addr_Page, LB="\"addr1\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_Ustate, PAGE=Edit_Ship_Addr_Page, LB="\"state\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_UpostalCode, PAGE=Edit_Ship_Addr_Page, LB="\"postalCode\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//-----------------------------------------------YourPrice-------------------------
nsl_static_var(OfferID_FP:1,PromoCode_FP:2, DATADIR=Stress, File=Platform_E2E/YourPrice, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=1, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);
nsl_search_var(BrowseJSP_block, PAGE=CatalogPage, LB="pageSeoURL\":\"", RB=",\"", LBMATCH=FIRST, ORD=ANY_NONEMPTY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(BrowseJSP, PAGE=CatalogPage, LB="CN=", RB="\"", LBMATCH=FIRST, ORD=ANY_NONEMPTY, SaveOffset=0, Search=Variable, Var=BrowseJSP_block, RETAINPREVALUE="YES", EncodeMode=None);

nsl_search_var(PIDJSPSP,PAGE=SelectVisualProdcut, PAGE=SearchBykeyword,PAGE=PDPOnly, LB="prd-", RB="/", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=SP_prodSeoURL, RETAINPREVALUE="YES", EncodeMode=None);
// ---------------------------------------- AvailablilityPick UP

nsl_search_var(SP_SKU, PAGE=Search_ProductInventoryPrice, PAGE=Catalog_ProductInventoryPrice, PAGE=Search_AvailabilityPickup,  LB="{\"skuCode\":\"", RB="\",\"", LBMATCH=FIRST, ORD=ANY_NONEMPTY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

// ---------------------------  ADD_WALLET_MUPC Offer

nsl_static_var(FP_AddWallet_MUPC:1,DATADIR=Stress, File=Platform_E2E/Stress_MUPC_Wallet.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=1, EncodeMode=All, CopyFileToTR=No, RemoveDupRecords=Yes);                                                                                
nsl_static_var(FP_PDPURL:1, DATADIR=Stress, File=Platform_E2E/NewStressPDPurl.seq, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_search_var(PIDJSPSP_PDPOnly, PAGE=PDPOnly, LB="prd-", RB="/", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SP_LowQtyBlockALL, PAGE=Checkout, LB="{\"errors\":[{\"code\":\"ERR_CART", RB="\"eGiftInfo\":null", LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);


nsl_web_find(TEXT="\"message\":\"Invalid cart id\"", PAGE=Reg_PlaceOrder,PAGE=UnReg_placeorder,PAGE=UnReg_placeorder_1,PAGE=SL_placeorder, FAIL=FOUND, ID="Place Order not successful ", ActionOnFail=STOP);
nsl_web_find(TEXT="\"message\":\"There are no items in your shopping bag.", PAGE=SL_placeorder, FAIL=FOUND, ID="No items in bag.", ActionOnFail=STOP);
nsl_web_find(TEXT="Invalid JSON Provided.", PAGE=Wallet_AddOffer_SUPC, PAGE=Wallet_AddOffer_MUPC, PAGE=Wallet_AddOffer, FAIL=FOUND, ID="Promocodes are not working", ActionOnFail=STOP);

nsl_web_find(TEXT="The item in your cart cannot be shipped using the shipping method you selected. Please choose a different shipping method", PAGE=Bopus_AddtoBag,  FAIL=FOUND, ID="Shipping method not eligible for product ID selected", ActionOnFail=CONTINUE);

nsl_search_var(Session, PAGE=MyInfo, PAGE=My_Info_Page, LB="id=\"sessionId\" type=\"hidden\" value=", RB=">", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

// -------------------------------------------------------- SnB Service ------------------------------------//

//CatalogAPI
nsl_random_string_var(CorrID, Min=2, Max=2, CharSet="a-zA-Z0-9", Refresh=USE);
nsl_static_var(Keyword_FP, DATADIR=Stress, File=SnB/NewProdCatalog.txt, Refresh=USE, Mode=SEQUENTIAL);
nsl_static_var(catalogID_FP, DATADIR=Stress, File=SnB/CatalogId_FP_v2cat.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(key,skey, DATADIR=Stress, File=SnB/keys.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None );
nsl_static_var(ProfileId_New_FP, DATADIR=Stress, File=SnB/NewATGIds2.5k.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None);
//Keyword & dimension Block
nsl_static_var(CatalogDimValueId_FP,CatalogDimKeyword_FP, DATADIR=Stress, File=SnB/DimensionvalueID_AND_Keyword.txt_final, Refresh=SESSION, Mode=RANDOM, EncodeMode=None);
nsl_static_var(CatalogDimID_FP, DATADIR=Stress, File=SnB/Mobile_Dimension.txt_final, Refresh=SESSION, Mode=RANDOM, EncodeMode=None);
nsl_static_var(CatalogKeyword_FP, DATADIR=Stress, File=SnB/keyword.txt_final, Refresh=SESSION, Mode=RANDOM, EncodeMode=None);
nsl_static_var(CatalogWebstore_FP, DATADIR=Stress, File=SnB/webstore.txt_final, Refresh=SESSION, Mode=RANDOM, EncodeMode=None);
//YourPrice
nsl_static_var(OfferID_FP1:1,PromoCode_FP1:2, DATADIR=Stress, File=SnB/OfferCode.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=1, EncodeMode=All, RemoveDupRecords=Yes);
nsl_static_var(Product_WebID_FP, DATADIR=Stress, File=SnB/Web_ProductID.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(MCM_ID_FP, DATADIR=Stress, File=SnB/mcmids_500k.txt,Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(visId, DATADIR=Stress, File=SnB/vId.txt,Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Zipcode_FP:1, DATADIR=Stress, File=SnB/ZipCode_New.txt.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_5_FP, DATADIR=Stress, File=SnB/output1.txt.used, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_10_30_FP, DATADIR=Stress, File=SnB/WebId10_30.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_30_50_FP, DATADIR=Stress, File=SnB/WebID_30_50.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_5_10_FP, DATADIR=Stress, File=SnB/WebId5_10.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_1_50_FP, DATADIR=Stress, File=SnB/WebId1_50.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_51_100_1_FP, DATADIR=Stress, File=SnB/WebId51_100_BKP.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_101_200_1_FP, DATADIR=Stress, File=SnB/WebId101_200.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_201_500_1_FP, DATADIR=Stress, File=SnB/WebId201_500.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_501_1000_1_FP, DATADIR=Stress, File=SnB/WebId501_1000.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(Product_WebID_1000_1_FP, DATADIR=Stress, File=SnB/WebId1000.txt, Refresh=USE, Mode=SEQUENTIAL, EncodeMode=None);
nsl_static_var(StoreId_FP, DATADIR=Stress, File=SnB/StoreId.txt, Refresh=SESSION, Mode=SEQUENTIAL);
nsl_search_var(PinfoSP, PAGE=CatalogByKeyword_P, PAGE=CatalogByKeyword_P_Guest,LB="\"resultsPersonalized\":", RB=",", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_static_var(SKUIDFP, DATADIR=Stress, File=SnB/Prod_SkuIds.txt, Refresh=USE, Mode=SEQUENTIAL, FirstDataLine=1);
nsl_web_find(TEXT="\"No pricing information available.\"", PAGE=PriceBySKU_GET, FAIL=FOUND, ID="Price Information Not available", ActionOnFail=STOP);
nsl_web_find(TEXT="No pricing information available.", PAGE=PriceByWebId_GET, FAIL=FOUND, ID="No Price Information for WebID", ActionOnFail=STOP);
nsl_web_find(TEXT="In Stock", PAGE=Inventory_SKU, FAIL=NOTFOUND, ID="Inventory not found", ActionOnFail=STOP);
nsl_web_find(TEXT="In Stock", PAGE=Inventory_WebID, FAIL=NOTFOUND, ID="Out of Stock", ActionOnFail=STOP);
nsl_web_find(TEXT="\"totalRecordsCount\":\"0\"", PAGE=CatalogPersonalization_Browse, PAGE=CatalogPersonalization_Browse, FAIL=FOUND, ID="No records found", ActionOnFail=STOP); 
nsl_search_var(PInfoWebSP, PAGE=CatalogPersonalization_Browse_Guest, PAGE=CatalogPersonalization_Browse, PAGE=CatalogByKeyword_P, PAGE=CatalogByKeyword_P_Guest,LB="\"resultsPersonalized\":", RB=",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_web_find(TEXT="<productStatus>Out of Stock</productStatus>", PAGE=Product_WebID_10_30_v2, PAGE=Product_WebID_30_50_v2, PAGE=Product_WebID_5_10_v2, PAGE=Product_WebID_5_v2,PAGE=Product_WebID_51_100_1,PAGE= Product_WebID_201_500_1,PAGE=Product_WebID_501_1000_1,PAGE=Product_WebID_1000_1, FAIL=FOUND, ActionOnFail=STOP);
nsl_web_find(TEXT="<count>0</count>",  PAGE=CatalogByKeyword_Bopus, PAGE=CatalogByKeyword_Bopus_3, PAGE=CatalogByKeyword_Bopus_5, FAIL=FOUND, ID="Product count is very less,  Kindly run Full Synchronization", ActionOnFail=STOP);
nsl_web_find(TextPfx="bopusStockLevel", TextSfx="In Stock\",", PAGE=Availability, FAIL=NOTFOUND, ID="Out of stock", ActionOnFail=STOP);
nsl_search_var(SkuCodeSP,PAGE=Product_WebID_1000_1,  PAGE=Product_WebID_10_30_v2,PAGE=Product_WebID_201_500_1, PAGE=Product_WebID_30_50_v2, PAGE=Product_WebID_501_1000_1, PAGE=Product_WebID_51_100_1, PAGE=Product_WebID_5_10_v2, PAGE=Product_WebID_5_v2, LB/BINARY/RE="\"skuCode\":\"", RB/BINARY/RE="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_random_string_var(CorrChar, Min=1, Max=3, CharSet="a-zA-Z0-9", Refresh=USE);
nsl_static_var(channel:1, DATADIR=Stress, File=SnB/channels.txt.seq, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All);

//-----------------------------------------------------------------------Account Services----------------------------------------------------------------------
// Create Profile

nsl_decl_var(EMailID);
nsl_decl_var(SDP);
nsl_decl_var(DP);
nsl_decl_var(LTYDP);
nsl_decl_var(hash_dp);

//Random variable to generate first & last names
nsl_random_string_var(FirstNam, Min=3, Max=5, CharSet="A-Z", Refresh=USE);
nsl_random_string_var(LastNam, Min=3, Max=5, CharSet="A-Z", Refresh=USE);


nsl_static_var(LPFEMail1:1,LPFLTY:2,LPFSessionID1:4,LPFProfileID1:5,LPFprofileid1:3, DATADIR=Stress, File=tmp/LTY_Newdata_File.txt, Refresh=SESSION, Mode=SEQUENTIAL);
nsl_static_var(LPFEMail:1,LPFSessionID:2,LPFProfileID:4,LPFprofileid:3, DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=SEQUENTIAL);
nsl_static_var(SessionID11:2,ProfileID11:4,DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=SEQUENTIAL);
nsl_static_var(EMail12:1,session:2,Profile:4, DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=SEQUENTIAL);

//We can uncomment the below file for Update Profile after running Create Profile scenario

nsl_static_var(WallEMail:1,WallSessionID:2,WallProfileID:4,Wallprofileid:3, DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=UNIQUE);
nsl_static_var(Emailid:1,Sessionid:2,Profileid:4,profileId:3, DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=SEQUENTIAL);
nsl_static_var(SoftLoggedIn:1,SoftSessionID:2,SoftProfileID:4,SoftprofileId:3, DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=SEQUENTIAL);
nsl_static_var(EMail:1,SessionID:2,ProfileID:4,profileid:3, DATADIR=Stress, File=tmp/Normal_User_DataSet.txt, Refresh=SESSION, Mode=SEQUENTIAL);



//From CreateProfile
nsl_search_var(profile, PAGE=CreateProfile, LB="\"id\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(PassPhras, PAGE=CreateProfile, LB="passphrase: ", RB="\r", SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SessionId, PAGE=CreateProfile, LB="X-SESSIONID: ", RB="\r", SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ProfileId, PAGE=CreateProfile, LB="X-PROFILEID: ", RB=".", SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(Zipcode, DATADIR=Stress, File=Acc/Zipcode.txt, Refresh=SESSION, Mode=RANDOM);

nsl_search_var(LoyalytID, PAGE=LTY_CreateLoyality, LB="\"loyaltyId\":", RB="}", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_random_string_var(CorrId, Min=2, Max=2, CharSet="a-z", Refresh=USE);
//nsl_random_string_var(CorrChar, Min=4, Max=7, CharSet="a-z", Refresh=USE);
//nsl_unique_number_var(CorrID, Format=%01lu, Refresh=USE);
nsl_search_var(ShippingId, PAGE=AddShippingAdress, LB="\"id\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ShippingId2, PAGE=AddShippingAdress_2, LB="\"id\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(DeleteID, PAGE=AddShippingAdress_2, LB="\"id\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(DeletePaymentID, PAGE=UpdatePaymentType, LB="\"id\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(NotifyAddressID, PAGE=AddBillingAdress, LB="\"id\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
//nsl_decl_var(key);
nsl_decl_var(ChallangeCount);
nsl_decl_var(ChallangeCount1);


nsl_search_var(SerialNumber, PAGE=AW_GetLatestVersion, LB="\"serialNumber\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(AuthToken, PAGE=AW_GetLatestVersion, LB="\"authenticationToken\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);



nsl_web_find(TEXT="Account Created Successfully", PAGE=CreateProfile, FAIL=NOTFOUND, ActionOnFail=STOP);
nsl_web_find(TEXT="Wallet Item successfully added", PAGE=AddtoWallet, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="ANONYMOUS", PAGE=AnonymousGuest, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TextPfx="{\"message\":", TextSfx="false", PAGE=Refresh_Wallet, FAIL=FOUND, ActionOnFail=CONTINUE);



nsl_web_find(TEXT="Profile Updated Successfully", PAGE=UpdateBillingAdress, PAGE=AddBillingAdress, PAGE=AddShippingAdress, FAIL=NOTFOUND, ActionOnFail=STOP);
nsl_web_find(TEXT="payload", PAGE=GetProfile, FAIL=NOTFOUND, ActionOnFail=STOP);


nsl_web_find(TEXT="\"hasKCC\":true", PAGE=LTYTracker, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="\"offersCount\"", PAGE=AccountDropDown, FAIL=NOTFOUND, ActionOnFail=CONTINUE);
nsl_web_find(TEXT="\"rewardsPilot\"", PAGE=Segment, PAGE=Segment_EmailID, PAGE=Segment_KCC, FAIL=NOTFOUND, ActionOnFail=CONTINUE);

nsl_search_var(GetID, PAGE=GetProfile, LB="\"kpProvisioned\":false,\"ID\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(GETID, PAGE=GetProfile, LB/BINARY/RE="\"kpProvisioned\":false,\"ID\":\"", RB/BINARY/RE="\",\"isEligibleForExpeditedCheckout", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="{\"payload\":{\"profile\":{\"", PAGE=GetProfile, FAIL=NOTFOUND, ID="No response in body", ActionOnFail=STOP);
nsl_search_var(Payload, PAGE=GetProfile, LB="{\"", RB="\":{\"profile\":", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(FltyId, PAGE=GetProfile, LB=",\"loyaltyId\":\"", RB="\",\"updatePaymentId\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);


nsl_random_string_var(Type_1, Min=1, Max=20, CharSet="a-zA-Z0-9", Refresh=SESSION);
nsl_random_string_var(Type_2, Min=1, Max=20, CharSet="a-zA-Z0-9", Refresh=SESSION);
nsl_static_var(Address1_FP:1,Address2_FP:2,City_FP:3,State_FP:4,PostalCode_FP:5,PostalCode_Ext_FP:6,ValidationType_FP:7, DATADIR=Stress, File=Acc/Address.txt.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
//----------PayFone-----------------
nsl_web_find(TEXT="Unable to fetch information from partner services", PAGE=InitiateOtp, FAIL=FOUND, ID="HytrixTimeOUtIssue", ActionOnFail=STOP);
nsl_web_find(TEXT="phoneNumber", PAGE=InitiateOtp, FAIL=NOTFOUND, ID="wrongresponse", ActionOnFail=STOP);
nsl_search_var(TransactionID, PAGE=InitiateOtp, LB="transactionId\":\"", RB="\"}", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(walletofferFP:1, DATADIR=Stress, File=Acc/offer.txt.seq, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All);

//---------------------------------------------------------------CnC Services----------------------------------------------------------------------

nsl_decl_var(SessionIDDP);
nsl_decl_var(SessionStatusDP);
nsl_decl_var(NumDP);
nsl_decl_var(FName_DP);
nsl_decl_var(LName_DP);
nsl_decl_var(Email_DP);
nsl_decl_var(MonthDP);
nsl_decl_var(YearDP);
nsl_decl_var(QuantityDP);


nsl_static_var(KohlsCashFP:1,PinFP:2, DATADIR=Stress, File=CnC/KohlsCash.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=1, EncodeMode=All);
nsl_static_var(Addr1FP:1,Addr2FP:2,CityFP:3,StateFP:4,PostalCodeFP:5,PhoneNumberFP:6, DATADIR=Stress, File=CnC/AddShippingAddress.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=1, EncodeMode=All);
nsl_search_var(ShippingIdSP, PAGE=Reg_GetAll_AddressShipping, PAGE=Add_Address, PAGE=Get_Address, LB="\"id\":\"", RB="\"", ORD=ANY, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(BillingIdSP, PAGE=Reg_Get_BillingAddress, LB="\"id\":\"", RB="\"", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(typeFP:1,cardNumFP:2,expMonthFP:3,expYearFP:4, DATADIR=Stress, File=CnC/AddNewPaymentInfo.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=1, EncodeMode=All);
nsl_static_var(ProductIDFP:1,SkuIDFP:2, DATADIR=Stress, File=CnC/NewProductSKU.txt, Refresh=USE, Mode=RANDOM, FirstDataLine=1, EncodeMode=All);
nsl_static_var(UnRegSESSIONFP:3, UnRegEmailIdFP:4, DATADIR=Stress, File=CnC/CnC_Payfone_GuestIds.txt, Refresh=SESSION, Mode=RANDOM, EncodeMode=All);


nsl_static_var(SESSION_ID_FP:4,ProfileIdFP:4,EmailIdFP:1,Orig_ProfileID_FP:3, DATADIR=Stress, File=CnC/NewUserDataFile.txt.unq, Refresh=SESSION, Mode=UNIQUE, EncodeMode=All);


nsl_static_var(FnameFP:5,LnameFP:6, DATADIR=Stress, File=CnC/5000Data, Refresh=SESSION, Mode=RANDOM, EncodeMode=All);
nsl_search_var(PaymentIDSP, PAGE=Reg_Get_AllPaymentCard, PAGE=Reg_Add_PaymentCard, PAGE=Reg_Add_PaymentCardControl, LB="{\"id\":\"", RB="\"", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(CartIdSP, PAGE=Reg_Get_Cart,PAGE=Get_ShipmentCart,PAGE=UnReg_Get_Cart, LB="\"cartId\":\"", RB="\"", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(CartItemIdSP, PAGE=Reg_Get_Cart,PAGE=Get_ShipmentCart,PAGE=UnReg_Get_Cart, LB="\"cartItemId\":\"", RB="\"", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ProfileIDSP, PAGE=UnReg_Get_MiniCart, LB="profileId: ", RB="\r\n", LBMATCH=FIRST, SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(Billing_AddressSP, PAGE=Reg_Get_BillingAddress, LB="\"Billing\",\"id\":\"", RB="\",", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(BillAddressSP, PAGE=Reg_Get_BillingAddress, LB="\"Billing\",\"id\":\"", RB="\",", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(BillIdCountSP, PAGE=Reg_Get_Cart, LB="{\"id\":\"", RB="\",\"addr1\"", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, Search=Variable, Var=BillAddressSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(VersionIdSP, PAGE=Reg_Get_Cart,PAGE=UnReg_Get_Cart, LB="\"version\":", RB=",\"", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(PromoFP:1, DATADIR=Stress, File=CnC/PromoCode_csm3_prp.txt, Refresh=USE, Mode=RANDOM, EncodeMode=All);
nsl_search_var(CartItemIDBlockSP, PAGE=Reg_Get_Cart, PAGE=UnReg_Get_Cart, LB="\"cartItemId", RB="\"upcCode\"", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(CartItemID_SP, PAGE=Reg_Get_Cart, PAGE=UnReg_Get_Cart, LB="\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=CartItemIDBlockSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(SkuIDSP, PAGE=Reg_Get_Cart, PAGE=UnReg_Get_Cart, LB="\"skuId\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=CartItemIDBlockSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ProductIdSP, PAGE=Reg_Get_Cart, PAGE=UnReg_Get_Cart, LB="\"productId\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=CartItemIDBlockSP, RETAINPREVALUE="YES", EncodeMode=None);

nsl_web_find(TEXT="Address Not Inserted Successfully", PAGE=Reg_Add_ShippingAddress, FAIL=FOUND, ID="Address Not added Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Address Added Successfully", PAGE=Reg_Add_ShippingAddress, FAIL=NOTFOUND, ID="Address Not added Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Address Added Successfully", PAGE=Reg_Add_BillingAddress, FAIL=NOTFOUND, ID="Address Not added Successfully", ActionOnFail=CONTINUE);

nsl_web_find(TEXT="Get Address is Successful", PAGE=Reg_Get_AddressShippingId, FAIL=NOTFOUND, ID="A ddress Not retrived Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Address Updated Successfully", PAGE=Reg_Update_ShippingAddress, PAGE=RegUpdate_BillingAddresses, FAIL=NOTFOUND, ID="Address Not updated Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Get All Address is Successful", PAGE=Reg_GetAll_AddressShipping, FAIL=NOTFOUND, ID="Address Not retrived Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Address Deleted Successfully", PAGE=Reg_Delete_AddressShipping, FAIL=NOTFOUND, ID="Address Not deleted Successfully", ActionOnFail=STOP);

nsl_web_find(TEXT="Payment Not Inserted", PAGE=Reg_Add_PaymentCard, PAGE=Reg_Add_PaymentCardControl, FAIL=FOUND, ID="Payment Not added Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Payment Added Successfully", PAGE=Reg_Add_PaymentCard, PAGE=Reg_Add_PaymentCardControl, FAIL=NOTFOUND, ID="Payment Not added Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Payment Updated Successfully", PAGE=Reg_Update_PaymentCard, FAIL=NOTFOUND, ID="Payment Not updated Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Fetched payment details successfully", PAGE=Reg_Get_PaymentCard,PAGE=Reg_Get_AllPaymentCard, FAIL=NOTFOUND, ID="Payment Not fetched Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Payment Deleted Successfully", PAGE=Reg_Delete_PaymentCard, PAGE=Reg_Delete_PaymentCardControl, FAIL=NOTFOUND, ID="Payment Not deleted Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Items Successfully Added to Cart", PAGE=Reg_Add_Cart,PAGE=UnReg_Add_Cart, FAIL=NOTFOUND, ID="Item Not Added Successfully", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Cart Retrieved Successfully", PAGE=Reg_Get_Cart,PAGE=Get_ShipmentCart,PAGE=Reg_Get_MiniCart,PAGE=UnReg_Get_Cart, FAIL=NOTFOUND, ID="Item Not Retrieved Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="ERR_REVIEW_007", PAGE=Reg_Review_Cart,PAGE=UnReg_CartReview, FAIL=FOUND, ID="Payment information is missing", ActionOnFail=STOP);
nsl_web_find(TEXT="ERR_CART_028", PAGE=Reg_Get_Cart,PAGE=UnReg_Get_Cart, FAIL=FOUND, ID="Payment information is missing", ActionOnFail=STOP);
nsl_web_find(TEXT="Cart Items Updated Successfully", PAGE=Reg_Update_Cart,PAGE=UnReg_Update_Cart,PAGE=Update_CartShipMethod, FAIL=NOTFOUND, ID="Item Not Updated Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Cart Items Deleted Successfully", PAGE=Reg_Delete_Cart,PAGE=UnReg_Delete_Cart, FAIL=NOTFOUND, ID="Item Not Deleted Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Updated Cart Shipment Successfully", PAGE=UnReg_Add_CartShipment, PAGE=Updadte_Shipment, FAIL=NOTFOUND, ID="Shipment Not Updated Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Kohls Cash bulk balance inquiry successful", PAGE=Reg_Get_KohlsCash, FAIL=NOTFOUND, ID="Kohls Cash not retirieved Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Cart is validated successfully", PAGE=Reg_Review_Cart, FAIL=NOTFOUND, ID="Review cart is not Successful", ActionOnFail=STOP);
nsl_web_find(TEXT="Order placed successfully", PAGE=Reg_Place_Order, FAIL=NOTFOUND, ID="Place Order is not Successful", ActionOnFail=STOP);
nsl_web_find(TEXT="Gift Card Applied Successfully", PAGE=Reg_Apply_GiftCard, FAIL=NOTFOUND, ID="Gift Card not applied Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="CreditCard applied successfully", PAGE=UnReg_Add_CartPayment, FAIL=NOTFOUND, ID="Gift Card not applied Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="Order placed successfully", PAGE=Reg_Place_Order, PAGE=UnReg_Place_Order, FAIL=NOTFOUND, ID="Place Order failed", ActionOnFail=STOP);
nsl_web_find(TEXT="Order list retrieval is successful", PAGE=Reg_Purchase_History, FAIL=NOTFOUND, ID="Purchase history failed", ActionOnFail=STOP);
nsl_search_var(OrderSP, PAGE=Reg_Place_Order,PAGE=UnReg_Place_Order, LB="\"orderId\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, Search=Body, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(XSessionSP, PAGE=Authenticate1, LB="X-SESSIONID: ", RB="\r", LBMATCH=FIRST, SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(XProfileSP, PAGE=Authenticate1, LB="X-PROFILEID: ", RB=".", LBMATCH=FIRST, SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="ERR_REVIEW_003", PAGE=UnReg_CartReview, FAIL=FOUND, ID="Cart validation failed_Invalid cart id", ActionOnFail=STOP);
nsl_random_string_var(PlaceOrderRP, Min=10, Max=11, CharSet="a-zA-Z0-9", Refresh=SESSION);
//nsl_random_string_var(CorrChar, Min=4, Max=7, CharSet="a-z", Refresh=USE);
//nsl_unique_number_var(CorrID, Format=%01lu, Refresh=USE);

//---------------------------------------Payfone--------------------------------

nsl_search_var(RequestIdSP, PAGE=Initiate_payfone, LB="\"requestId\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, Search=Body, RETAINPREVALUE="YES", EncodeMode=None);
nsl_static_var(VerifyFingerPrintFP:1, DATADIR=Stress, File=CnC/VerificationFingerprint.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=1, EncodeMode=All);
nsl_web_find(TEXT="ERR_PAYFONE_001", PAGE=Initiate_payfone, FAIL=FOUND, ID="Unable to send SMS to the given mobile number", ActionOnFail=STOP);
nsl_web_find(TEXT="ERR_GEN_005", PAGE=Initiate_payfone, PAGE=PollingApi_Payfone, PAGE=FeedbackApi_Payfone, FAIL=FOUND, ID="Unable to send SMS to the given mobile number", ActionOnFail=STOP);
nsl_web_find(TEXT="ERR_GEN_001", PAGE=Initiate_payfone, PAGE=PollingApi_Payfone, PAGE=FeedbackApi_Payfone, FAIL=FOUND, ID="There was an error while processing the request", ActionOnFail=STOP);
nsl_web_find(TEXT="orderStatus", PAGE=PollingApi_Payfone, FAIL=NOTFOUND, ID="Polling servie Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="ERR_PAYFONE_002", PAGE=Initiate_payfone, FAIL=FOUND, ID="Unable to fetch intomation from partner services", ActionOnFail=STOP);
nsl_search_var(FeedbackSP, PAGE=FeedbackApi_Payfone, LB="\"orderStatus\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, Search=Body, RETAINPREVALUE="YES", EncodeMode=None);
nsl_web_find(TEXT="Order cancelled successfully", PAGE=Guest_Cancel_Order, FAIL=NOTFOUND, ID="Unable to Cancel Order", ActionOnFail=STOP);


//------------------------------------2020----------------------------------------
nsl_search_var(SP_Bagitemss_all, PAGE=UnReg_Get_Cart, PAGE=Reg_Get_Cart, LB="\"cartItemId\":\"", RB="\"," , LBMATCH=FIRST, ORD=ALL, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(ShipAddressSP, PAGE=Reg_GetAll_AddressShipping, LB="\"addresses\":[", RB="]}", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ShipIdCountSP, PAGE=Reg_GetAll_AddressShipping, LB="\"id\":\"", RB="\",\"preferredAddress", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, Search=Variable, Var=ShipAddressSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ShipDelUpAddressIdSP, PAGE=Reg_GetAll_AddressShipping, LB="\"id\":\"", RB="\",\"preferredAddress", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, Search=Variable, Var=ShipAddressSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(Postal_SP, PAGE=UnReg_Place_Order, PAGE=Reg_Place_Order, LB="\"postalCode\":\"", RB="\",", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(PaymentSP, PAGE=Reg_Get_AllPaymentCard, LB="\"payments\":[", RB="]}", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(PaymentIdCountSP, PAGE=Reg_Get_AllPaymentCard, LB="\"id\":\"", RB="\",\"nameOnCard", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, Search=Variable, Var=PaymentSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(PaymentIdSP, PAGE=Reg_Get_AllPaymentCard, LB="{\"id\":\"", RB="\",\"nameOnCard", LBMATCH=FIRST, SaveOffset=0, Search=Variable, Var=PaymentSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(PaymentIDDelUpSP, PAGE=Reg_Get_AllPaymentCard, LB="{\"id\":\"", RB="\"", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, Search=Variable, Var=PaymentSP, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(KCCEditPaymentCheckSP, PAGE=Reg_Get_PaymentCard, LB="\"type\":\"", RB="\",\"expMonth", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(RemovePromoSP, PAGE=Reg_Get_Coupon, PAGE=Reg_Get_CouponRemove, LB="\"code\":\"", RB="\",\"", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(RemovePromoCountSP, PAGE=Reg_Get_Coupon, PAGE=Reg_Get_CouponRemove, LB="\"code\":\"", RB="\",\"", LBMATCH=FIRST, SaveOffset=0, ORD=ALL, RETAINPREVALUE="YES", EncodeMode=None);

nsl_web_find(TEXT="Get All Address is Successful", PAGE=Reg_Get_BillingAddress, FAIL=NOTFOUND, ID="Address Not retrived Successfully", ActionOnFail=STOP);
nsl_web_find(TEXT="CreditCard applied successfully", PAGE=RegApplyCreditcardPayment, FAIL=NOTFOUND, ID="CreditCard applied failed", ActionOnFail=CONTINUE);
nsl_web_find(TEXT="Order details retrieved successfully", PAGE=UnRegOrder_Details, PAGE=RegOrderDetails, FAIL=NOTFOUND, ID="Order details retrieved Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="Failed To Update Items In Cart", PAGE=Update_CartShipMethod, FAIL=FOUND, ID="Update Item In cart Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="Coupon removed successfully", PAGE=Reg_Remove_Coupon, FAIL=NOTFOUND, ID="Coupon removed Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="Updated billing successfully", PAGE=UnReg_ApplyBillingAddr, FAIL=NOTFOUND, ID="Billing Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="Failed To Add Items To Cart", PAGE=Reg_Add_Cart, FAIL=FOUND, ID="Item Added Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="Coupon retrieved successfully", PAGE=Reg_Get_Coupon, PAGE=Reg_Get_CouponRemove, FAIL=NOTFOUND, ID="Coupon retrieve Failed", ActionOnFail=STOP);
nsl_web_find(TEXT="The promo code entered doesn", PAGE=Reg_Apply_Coupon, FAIL=FOUND, ID="Coupon not applied", ActionOnFail=CONTINUE);

//-----------------------------------------------------Solr------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
nsl_static_var(Profile_ID, DATADIR=Stress, File=Solr/NewATGIds.txt, Refresh=USE, Mode=SEQUENTIAL);

nsl_static_var(SolrKeyword_FP, DATADIR=Stress, File=Solr/Catalog_Keywords_FP.txt, Refresh=USE, Mode=SEQUENTIAL);
nsl_static_var(SearchKwr_FP:1, DATADIR=Stress, File=Solr/SearchKeywords_1.txt, Refresh=SESSION, Mode=SEQUENTIAL, CopyFileToTR=No);
nsl_static_var(AffinityURL_FP:1, DATADIR=Stress, File=Solr/AffinityURL_WO_Promo_sales.txt, Refresh=SESSION, Mode=SEQUENTIAL,EncodeMode=None,CopyFileToTR=No);
nsl_static_var(BrowseURL_FP:1, DATADIR=Stress, File=Solr/CatalogId_FP1_new.txt, Refresh=SESSION, Mode=SEQUENTIAL,EncodeMode=None,CopyFileToTR=No);
nsl_static_var(SaleEventQueryFP:1, DATADIR=Stress, File=Solr/SolrSaleEventURLs.txt, Refresh=SESSION, Mode=SEQUENTIAL, CopyFileToTR=No);
nsl_static_var(BOPUS_URL:1, DATADIR=Stress, File=Solr/BOPUS_URLs_1.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(VisualTypeahead_FP:1, DATADIR=Stress, File=Solr/VisualTypeahead_Query_unique.txt, Refresh=SESSION, Mode=SEQUENTIAL, CopyFileToTR=No);
//nsl_random_string_var(CorrId, Min=2, Max=2, CharSet="a-s", Refresh=USE);
//nsl_random_string_var(CorrChar, Min=4, Max=7, CharSet="a-s", Refresh=USE);
//nsl_unique_number_var(CorrID, Format=%01lu, Refresh=USE);
nsl_static_var(OAPI_KWFP, DATADIR=Stress, File=Solr/OAPI_Direct_Solr_SearchKeywords.txt, Refresh=SESSION, Mode=SEQUENTIAL, CopyFileToTR=No);
nsl_static_var(OAPI_BRFP, DATADIR=Stress, File=Solr/OAPI_Direct_Solr_DimensionIds.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None, CopyFileToTR=No);
nsl_static_var(ProductID_FP:1, DATADIR=Stress, File=Solr/Product_Ids.txt, Refresh=SESSION, Mode=UNIQUE, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(DimensionId_FP:1, DATADIR=Stress, File=Solr/Catalog_DimensionIds.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None, CopyFileToTR=No);
nsl_static_var(ProfileID_FP:1, DATADIR=Stress, File=Solr/ProfileIds.txt.seq, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);

nsl_static_var(TypeAheadQuery1FP, DATADIR=Stress, File=Solr/typeahead_1.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadQuery2FP, DATADIR=Stress, File=Solr/typeahead_2.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadQuery3FP, DATADIR=Stress, File=Solr/typeahead_3.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadQuery4FP, DATADIR=Stress, File=Solr/typeahead_4.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadQuery5FP, DATADIR=Stress, File=Solr/typeahead_5.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadQuery6FP, DATADIR=Stress, File=Solr/typeahead_6.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadQuery7FP, DATADIR=Stress, File=Solr/typeahead_7.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=Specified, EncodeSpaceBy=%20, CopyFileToTR=No);
nsl_static_var(TypeAheadGenderPropFP, DATADIR=Stress, File=Solr/GenderPropensity.txt, Refresh=USE, Mode=SEQUENTIAL);

nsl_static_var(SolrProfileId_New_FP, DATADIR=Stress, File=Solr/NewATGIds.txt, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=None);

//--------------------------------------------------------WCS----------------------------------------------------------------------------------------------------------------------------
nsl_static_var(UrlFP, DATADIR=Stress, File=Wcs/WCSTestData.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=1, EncodeMode=None);
nsl_static_var(UrlFP1, DATADIR=Stress, File=Wcs/BrandLandingURL_Stress1.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=1, EncodeMode=None);
nsl_static_var(GRUrlFP, DATADIR=Stress, File=Wcs/GiftRegistry_Stress1.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=1, EncodeMode=None);
nsl_static_var(SIUrlFP, DATADIR=Stress, File=Wcs/SpotlightImages.txt, Refresh=SESSION, Mode=RANDOM, FirstDataLine=1, EncodeMode=None);

//**************Check Point for Stress1 WCS*******************
nsl_web_find(TEXT="Kohl's | Shop Clothing", PAGE=WcsHomePage, FAIL=NOTFOUND, ID="Home Page not found", ActionOnFail=STOP);
nsl_web_find(TEXT="sale event landing", PAGE=SaleEventPage, FAIL=NOTFOUND, ID="SaleEventPage not found", ActionOnFail=STOP); 
nsl_web_find(TEXT="Error Page", PAGE=SaleEventPage, FAIL=FOUND, ID="SaleEventPage not found", ActionOnFail=STOP);
nsl_web_find(TEXT="SCTrackingBean.pageName", PAGE=WCSInternal_OmnitureAkamaiJspPage, FAIL=NOTFOUND, ID="OmnitureAkamaiJspPage not found", ActionOnFail=STOP);
nsl_web_find(TEXT="sale event landing", PAGE=SaleEventPage, FAIL=NOTFOUND, ID="TopNavigation Link not configured correctly. [{TopNavUrlFP}]", ActionOnFail=STOP);

nsl_static_var(spot_light_value:1, DATADIR=Stress, File=Wcs/spotlight_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(mcom_values:1, DATADIR=Stress, File=Wcs/mcom_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(sale_event_values:1, DATADIR=Stress, File=Wcs/sale_event_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(tablet_feature_values:1, DATADIR=Stress, File=Wcs/tablet_feature_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(tablet_sale_event_values:1, DATADIR=Stress, File=Wcs/tablet_sale_event_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(mcom_sale_event_values:1, DATADIR=Stress, File=Wcs/mcom_sale_event_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(resources_sub_category:1, DATADIR=Stress, File=Wcs/resources_subcategory.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(resources_values:1, DATADIR=Stress, File=Wcs/resources_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(flex_values:1, DATADIR=Stress, File=Wcs/flex_value.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);
nsl_static_var(nativeapp_feature_value:1, DATADIR=Stress, File=Wcs/nativeapp_values.txt, Refresh=SESSION, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All, CopyFileToTR=No);

