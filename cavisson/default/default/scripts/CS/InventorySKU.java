package CS;
import pacJnvmApi.NSApi;

public class InventorySKU implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("Inventory_SKU_NotAval");
		status = nsApi.ns_web_url("Inventory_SKU",
				"METHOD=POST",
				"URL=http://{SnBServiceHostUrl}/v1/inventory/sku",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_Inventory_SKU_{CorrChar}{CorrID}",
				"BODY=$CAVINCLUDE$=InventorySKU.json"
				);


		status = nsApi.ns_end_transaction_as("Inventory_SKU_NotAval", NS_AUTO_STATUS,"Inventory_SKU");
		status = nsApi.ns_save_data_eval("output/Stress_ValidSKU.txt",NS_APPEND_FILE,"{SKUSP}\n");

		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
