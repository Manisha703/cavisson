// Converted from Acc_Services_End_To_End_C/Geo_Code_validation.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class Geo_Code_validation implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{

	int status = 0;
    nsApi.ns_start_transaction("Geo_Code_validation");
    nsApi.ns_web_url("Geo_Code_validation",
       "URL=https://{AccServiceHostUrl}/v1/validation/address",
       "METHOD=POST",
       "HEADER=Content-Type: application/json",
       "HEADER=correlation-id: Geo_Code_validation_{CorrChar}{CorrID}",
      "BODY=$CAVINCLUDE$=Geo_Code_validation.json",
    );
    nsApi.ns_end_transaction("Geo_Code_validation", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(0);
    return status;
   }
}
