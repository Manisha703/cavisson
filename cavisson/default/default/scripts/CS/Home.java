// Converted from P2HR_With_Promo_Mixes_CM_0921/Home.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: Home
Recorded By: Manish
Date of recording: 07/09/2018 04:35:12
Flow details:
Build details: 4.1.10 (build# 27)
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class Home implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		if (debug) return status;
		// --------------------------------------------------------------------- Home Page --------------------------------------------------------------------------------------------
		int home_flag =1;

		nsApi.ns_start_transaction("HomePage");
		nsApi.ns_web_url ("HomePage",
				"URL=https://{UIHostUrl}/",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Proxy-Connection: Keep-Alive",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9",
				"REDIRECT=YES",
				"LOCATION=https://{UIHostUrl}/",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/javascript/deploy/environment.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/omniture/mbox.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/javascript/deploy/framework.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/javascript/deploy/homepageR51-debug.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/omniture/SkavaOmnitureCode.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/javascript/deploy/kohls_v1_m56577569839297458-debug.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/homepage1-debug.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/homepage-debug.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/fonts/hfjFonts.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/skava-custom.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/product-matrix_v1_m56577569836982752.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/monetization.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE,
				"URL=https://{UIHostUrl}/snb/media/css/easyzoom.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;K_favstore;__gads;s_fid;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;productnum;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;gpv_v9", END_INLINE
					);
		nsApi.ns_end_transaction("HomePage", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		return status;    	


	}

}
