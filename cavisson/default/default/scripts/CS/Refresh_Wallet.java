// Converted from Acc_Services_End_To_End_C/Refresh_Wallet.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class Refresh_Wallet implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
       int status = 0;
       nsApi.ns_start_transaction("Refresh_Wallet");
       nsApi.ns_web_url ("Refresh_Wallet",
        "URL=https://{AccServiceHostUrl}/v1/kohlswallet/refresh",
        "METHOD=PUT",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID11}",
        "HEADER=X-PROFILEID: {ProfileID11}",
);
    nsApi.ns_end_transaction("Refresh_Wallet", NS_AUTO_STATUS);
    return status;

}


}
