// Converted from Acc_Services_End_To_End_C/Add_PickUp.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: Add_PickUp
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class Add_PickUp implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{

	int status = 0;
	nsApi.ns_start_transaction("Add_PickUp");
     nsApi.ns_web_url ("Add_PickUp",
		"URL=https://{AccServiceHostUrl}/v1/profile/altpickup",
		"METHOD=POST",
		"HEADER=Accept: application/json",
		"HEADER=Content-Type: application/json",
		"HEADER=channel:web",
          "HEADER=correlation-id: Add_PickUp_{CorrChar}{CorrID}",
          "HEADER=X-SESSIONID: {SessionID11}",
          "HEADER=X-PROFILEID: {ProfileID11}",
		"BODY=$CAVINCLUDE$=Add_Alt_PickUp.json",
       
);
    nsApi.ns_end_transaction("Add_PickUp", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(1);
    return status;

}

}
