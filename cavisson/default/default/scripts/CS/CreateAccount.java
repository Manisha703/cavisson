// Converted from P2HR_With_Promo_Mixes_CM_0921/CreateAccount.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: CreateAccount
Recorded By: Tanmay
Date of recording: 07/30/2017 03:30:40
Flow details:
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;

//#include "Utility.c"


public class CreateAccount implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;

		nsApi.ns_start_transaction("ClickOnCreate");
		
		nsApi.ns_web_url ("ClickOnCreate_1",
				"URL=https://{UIHostUrl}/myaccount/kohls_login.jsp?action=createAccount",
				"HEADER=x-requested-with: XMLHttpRequest",
				"HEADER=Accept-Language: en-us",
				"HEADER=Proxy-Connection: Keep-Alive",
				"HEADER=correlation-id: ClickOnCreate_Pavan",
				"COOKIE=JSESSIONID;klsbrwcki:1001160121;VisitorId;mbox;s_cc;gpv_v9;s_sq;cookieSetting;",
				);
		nsApi.ns_end_transaction("ClickOnCreate", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		if (nsApi.ns_get_random_number_int(1,100) < 101)
		{

	
			nsApi.ns_save_string(nsApi.ns_get_guid(), "DS_EMailID");

			nsApi.ns_start_transaction("CreateAccount");
			nsApi.ns_web_url ("CA_AccountPage",
					"URL=https://{UIHostUrl}/myaccount/createAccount",
					"METHOD=POST",
					"HEADER=Accept-Language:en-us,en;q=0.5",
					"HEADER=Content-Length:1843",
					"HEADER=Content-Type:application/x-www-form-urlencoded",
					"REDIRECT=YES",
					"HEADER=correlation-id: CreateAccountPage_Pavan",
					"LOCATION=https://{UIHostUrl}",
					"PreSnapshot=webpage_1400496003026.png",
					"Snapshot=webpage_1400496015642.png",
					"BODY=$CAVINCLUDE$=CreateAccount.json"        
					);
			nsApi.ns_end_transaction("CreateAccount", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);


			nsApi.ns_advance_param("CurrentDate");
		


		}
		else
		{ 

			nsApi.ns_save_string(nsApi.ns_eval_string("{CP_Email_ID}"), "DS_EMailID");
			nsApi.ns_start_transaction("CreateAccountWithExistingUser");
			nsApi.ns_web_url ("CA_AccountPage_EX",
					"URL=https://{UIHostUrl}/myaccount/createAccount",
					"METHOD=POST",
					"HEADER=Accept-Language:en-us,en;q=0.5",
					"HEADER=Content-Length:1843",
					"HEADER=Content-Type:application/x-www-form-urlencoded",
					"REDIRECT=YES",
					"HEADER=correlation-id: CreateAccountPage_Pavan",
					"LOCATION=https://{UIHostUrl}",
					"PreSnapshot=webpage_1400496003026.png",
					"Snapshot=webpage_1400496015642.png",
					"BODY=$CAVINCLUDE$=CreateAccount.json"        
					);

			nsApi.ns_end_transaction("CreateAccountWithExistingUser", NS_AUTO_STATUS);


		}

		
		//Saving email to palce order
		nsApi.ns_save_string(nsApi.ns_eval_string("ws_{DS_EMailID}@bh.exacttarget.com"), "SP_Email_ID");

		nsApi.ns_start_transaction("MyInfo");
		nsApi.ns_web_url ("MyInfo",
				"URL=https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
				"HEADER=Accept-Language:en-us,en;q=0.5",
				"PreSnapshot=webpage_1463436012920.png",
				"Snapshot=webpage_1463436024307.png"            
				);
		nsApi.ns_end_transaction("MyInfo", NS_AUTO_STATUS);


		int percentage = nsApi.ns_get_random_number_int(1,10);
		 
		nsApi.ns_save_string("217", "DV_StoreID"); 
		nsApi.ns_page_think_time(0);

		return status;

	}

}
