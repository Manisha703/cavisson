package CS;
import pacJnvmApi.NSApi;

public class StoreAddress implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("StoreAddress");
		status = nsApi.ns_web_url ("StoreAddress",
				"URL=http://{SnBServiceHostUrl}/v1/stores?city=Milwuakee&state=WI&radius=10",
				"METHOD=GET",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: StoreAddress_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("StoreAddress", NS_AUTO_STATUS);
		return status;
	}
}


