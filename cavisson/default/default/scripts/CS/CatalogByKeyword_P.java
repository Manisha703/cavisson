package CS;
import pacJnvmApi.NSApi;

public class CatalogByKeyword_P implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("CatalogByKeyword_P");
		status = nsApi.ns_web_url("CatalogByKeyword_P",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?keyword={CatalogKeyword_FP}&storeNum={StoreId_FP}&isDefaultStore=true&pid={ProfileId_New_FP}&vid={MCM_ID_FP}&channel={channel}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"
				);

		if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P", 0, "CatalogByKeyword_PdataFalse");
		}
		else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("true"))
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P", 0, "CatalogByKeyword_PdataTrue");
		}
		else
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P", 0, "CatalogByKeyword_NoPData");

		}
		status = nsApi.ns_page_think_time(0);
		int randNum = nsApi.ns_get_random_number_int(1,100);
		if(randNum<=50)
		{
			status = nsApi.ns_start_transaction("CatalogByKeyword_Dimension_P");
			status = nsApi.ns_web_url("CatalogByKeyword_Dimension_P",
					"URL=http://{SnBServiceHostUrl}/v1/catalog?dimensionValueID={CatalogDimValueId_FP}&sortID=1&storeNum={StoreId_FP}&isDefaultStore=true&shipToStore=true&keyword={CatalogDimKeyword_FP}&includeStoreOnlyProducts=true&isVGC=false&offset=1&limit=72&pid={ProfileId_New_FP}&vid={MCM_ID_FP}&channel={channel}",
					"HEADER=Accept: application/json",
					"HEADER=Content-Type: application/json",
					"HEADER=bypass_cache: false",
					"HEADER=X-APP-API_KEY: {key}",
					"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"
					);

			if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
			{

				status = nsApi.ns_end_transaction_as("CatalogByKeyword_Dimension_P", 0, "CatalogByKeyword_PdataFalse");
			}

			else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("true"))
			{
				status = nsApi.ns_end_transaction_as("CatalogByKeyword_Dimension_P", 0, "CatalogByKeyword_PdataTrue");

			}
			else
			{
				status = nsApi.ns_end_transaction_as("CatalogByKeyword_P", 0, "CatalogByKeyword_NoPData");

			}


			status = nsApi.ns_page_think_time(0);
		}
		return status;
	}
}	
