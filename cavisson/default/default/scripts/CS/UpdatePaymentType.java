// Converted from Acc_Services_End_To_End_C/UpdatePaymentType.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class UpdatePaymentType implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;

	nsApi.ns_start_transaction("UpdatePaymentType");
     nsApi.ns_web_url ("UpdatePaymentType",
        "URL=https://{AccServiceHostUrl}/v1/profile/paymentType",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
        "HEADER=X-PROFILEID: {LPFProfileID}",
        "BODY=$CAVINCLUDE$=UpdatePaymentType.json",	
);		
    nsApi.ns_end_transaction("UpdatePaymentType", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(0);
 
	nsApi.ns_start_transaction("PreferredPaymenttype");
    nsApi.ns_web_url ("PreferredPaymenttype",
        "URL=https://{AccServiceHostUrl}/v1/profile/paymentType/preferred",
          "METHOD=PUT",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
        "HEADER=X-PROFILEID: {LPFProfileID}",
         "HEADER=correlation-id: UpdatePaymentType_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=PreferredPaymenttype.json",
);
    nsApi.ns_end_transaction("PreferredPaymenttype", NS_AUTO_STATUS);
    
nsApi.ns_start_transaction("DeletePayment");
    nsApi.ns_web_url ("DeletePayment",
        "URL=https://{AccServiceHostUrl}/v1/profile/paymentType/{DeletePaymentID}",
          "METHOD=DELETE",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
        "HEADER=x-profileid: {LPFProfileID}",
        "HEADER=correlation-id: UpdatePaymentType_{CorrChar}{CorrID}",

);
    nsApi.ns_end_transaction("DeletePayment", NS_AUTO_STATUS);
    return status;

}


}
