// Converted from Acc_Services_End_To_End_C/UpdatePassword.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class UpdatePassword implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("UpdatePassword");
     nsApi.ns_web_url ("UpdatePassword",
        "URL=https://{AccServiceHostUrl}/v1/profile/password",
          "METHOD=PUT",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {sessioni}",
        "HEADER=X-PROFILEID: {Profilei}",
        "HEADER=correlation-id: UpdatePassword_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=updatepassword.json",
);
    nsApi.ns_end_transaction("UpdatePassword", NS_AUTO_STATUS);
    return status;

}

}
