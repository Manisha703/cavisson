// Converted from Acc_Services_End_To_End_C/Signout.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class Signout implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("Signout");
     nsApi.ns_web_url ("Signout",
        "URL=https://{AccServiceHostUrl}/v1/signout",
          "METHOD=DELETE",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
        "HEADER=X-PROFILEID: {ProfileID}.",
);
    nsApi.ns_end_transaction("Signout", NS_AUTO_STATUS);
    return status;

}

}
