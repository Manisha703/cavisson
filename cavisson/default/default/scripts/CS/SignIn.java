// Converted from Acc_Services_End_To_End_C/SignIn.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class SignIn implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	int i;
	for(i = 1; i <= 4; i++)
  {
	nsApi.ns_start_transaction("SignIn");
    nsApi.ns_web_url ("SignIn",
        "URL=https://{AccServiceHostUrl}/v1/profile/session?info=true",
        "METHOD=GET",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID11}",
        "HEADER=X-PROFILEID: {ProfileID11}",
        "HEADER=correlation-id: SignIn_{CorrChar}{CorrID}",

);
    nsApi.ns_end_transaction("SignIn", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(2);
  }
	return status;

}

}
