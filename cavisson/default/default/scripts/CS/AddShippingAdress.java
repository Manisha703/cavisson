// Converted from Acc_Services_End_To_End_C/AddShippingAdress.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class AddShippingAdress implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	
	nsApi.ns_start_transaction("AddShippingAdress");
     nsApi.ns_web_url ("AddShippingAdress",
        "URL=https://{AccServiceHostUrl}/v1/profile/shipAddress",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}",
         "HEADER=correlation-id: AddShippingAdress_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=AddShippingAdress.json",
);
    nsApi.ns_end_transaction("AddShippingAdress", NS_AUTO_STATUS);


    nsApi.ns_start_transaction("AddShippingAdress_2");
    nsApi.ns_web_url ("AddShippingAdress_2",
        "URL=https://{AccServiceHostUrl}/v1/profile/shipAddress",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}",
         "HEADER=correlation-id: AddShippingAdress_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=AddShippingAdress_2.json",
);
    nsApi.ns_end_transaction("AddShippingAdress_2", NS_AUTO_STATUS);

nsApi.ns_start_transaction("UpdateShippingAdress");
    nsApi.ns_web_url ("UpdateShippingAdress",
        "URL=https://{AccServiceHostUrl}/v1/profile/shipAddress",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}",
        "HEADER=correlation-id: AddShippingAdress_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=UpdateShippingAdress.json",
);
     nsApi.ns_end_transaction("UpdateShippingAdress", NS_AUTO_STATUS);
   	nsApi.ns_start_transaction("DeleteAddress");
     nsApi.ns_web_url ("DeleteAddress",
        "URL=https://{AccServiceHostUrl}/v1/profile/shipAddress/{ShippingId2}",
          "METHOD=DELETE",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}",
        "HEADER=correlation-id: AddShippingAdress_{CorrChar}{CorrID}",

);
    nsApi.ns_end_transaction("DeleteAddress", NS_AUTO_STATUS);
    return status;
 
}


}
