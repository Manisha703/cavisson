// Converted from Mix_Solr_SPA1_R69_C//TypeAheadQuery_GenderProp_2.c on Tue May 18 13:11:11 2021
/*-----------------------------------------------------------------------------
Name: TypeAheadQuery
Recorded By: Sawant
Date of recording: 23/04/2019 05:02:32
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class TypeAheadQuery_GenderProp_2 implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		nsApi.ns_start_transaction("TypeAhead_GenderPropensity_taspa2");
		nsApi.ns_web_url ("TypeAheadQuery_GenderProp_2",
				"URL=http://{SolrHost}/typeahead/{TypeAheadGenderPropFP}.jsp?callback=typeaheadResult&ta_spa=2",
				"HEADER=Accept-Language: en-us,en;q=0.5",
				"HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
				"HEADER=Proxy-Connection: keep-alive",
				"HEADER=X-APP-API-PROFILEID:{SolrProfileId_New_FP}",
				INLINE_URLS,
				"URL=http://integration.richrelevance.com/rrserver/p13n_generated.js?a=648c894ab44bc04a&ts=1407237879155&pt=%7Chome_page.pushdown%7Chome_page.ad_160x600%7Chome_page.horizontal_scroller&u=1000450571&s=vvwLTg2HLTgNphQp1w7v8zn0GHj3kmvh1WjsWnmJnRwPwsnscdL2!932978008!1407237863564&cts=http%3A%2F%2F10.210.156.65%3A17010&flv=14.0.0&l=1", "HEADER=Accept-Language: en-us,en;q=0.5", "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7", "HEADER=Proxy-Connection: keep-alive", END_INLINE
				);
		nsApi.ns_end_transaction("TypeAhead_GenderPropensity_taspa2", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);   
		return 0;
	}

}
