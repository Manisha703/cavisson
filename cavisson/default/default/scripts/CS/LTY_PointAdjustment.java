// Converted from Acc_Services_End_To_End_C/LTY_PointAdjustment.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class LTY_PointAdjustment implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
    int status = 0;
    nsApi.ns_start_transaction("LTY_PointAdjustment");
    nsApi.ns_web_url ("LTY_PointAdjustment",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/pointAdjustment",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "BODY=$CAVINCLUDE$=LTY_PointAdjustment.json",
);
    nsApi.ns_end_transaction("LTY_PointAdjustment", NS_AUTO_STATUS);
	return status;

}


}
