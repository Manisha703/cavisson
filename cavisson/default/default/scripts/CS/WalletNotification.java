// Converted from Acc_Services_End_To_End_C/WalletNotification.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class WalletNotification implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	 int status = 0;
	 nsApi.ns_start_transaction("WalletNotification");
      nsApi.ns_web_url ("WalletNotification",
        "URL=https://{AccServiceHostUrl}/v1/kohlscash/digitalNotification",
        "METHOD=GET",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
        "HEADER=X-PROFILEID: {ProfileID}",
        "HEADER=correlation-id: WalletNotification_{CorrChar}{CorrID}",
        "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr7F5",
);
    nsApi.ns_end_transaction("WalletNotification", NS_AUTO_STATUS);
	return status;

	}


}
