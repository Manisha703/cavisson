// Converted from Acc_Services_End_To_End_C/PrequalInquiry_Web.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class PrequalInquiry_Web implements NsFlow
{
    private boolean debug = true;
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	if (debug) return status;


	nsApi.ns_start_transaction("PrequalInquiry_Web");
     nsApi.ns_web_url ("PrequalInquiry_Web",
        "URL=https://{AccServiceHostUrl}/v1/profile/prequal/inquiry",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=userId: {LPFEMail}",
         "HEADER=X-SESSIONID: {LPFSessionID}",
         "HEADER=X-PROFILEID: {LPFProfileID}",
         "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr7F5",
         "HEADER=correlation-id: PrequalInquiry_Web_{CorrChar}{CorrID}",
         "HEADER=channel: web",
         "BODY=$CAVINCLUDE$=PrequalInquiry_Web.json",
);
    nsApi.ns_end_transaction("PrequalInquiry_Web", NS_AUTO_STATUS);
    return status;
}

}
