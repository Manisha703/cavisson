package CS;
import pacJnvmApi.NSApi;

public class CatalogByMonetization implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("CatalogByMonetization");
		status = nsApi.ns_web_url("CatalogByMonetization",
				"URL=http://{SnBServiceHostUrl}/v1/catalog/category/monetization/{catalogID_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation-id: CatalogByMonetization_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("CatalogByMonetization", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
