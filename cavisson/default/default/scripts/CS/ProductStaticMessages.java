package CS;
import pacJnvmApi.NSApi;

public class ProductStaticMessages implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("Product_StaticMessages");
		status = nsApi.ns_web_url("Product_StaticMessages",

				"URL=http://{SnBServiceHostUrl}/v1/staticmessages/product",
				"HEADER=Accept: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=Content-Type: application/json"

				);
		status = nsApi.ns_end_transaction("Product_StaticMessages", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
