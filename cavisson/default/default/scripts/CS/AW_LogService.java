// Converted from Acc_Services_End_To_End_C/AW_LogService.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class AW_LogService implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("AW_LogService");
    	nsApi.ns_web_url ("AW_LogService",
         "URL=https://{AccServiceHostUrl}/v1/passthru/v1/log",
         "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
        "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr799087896",
        "BODY=$CAVINCLUDE$=AW_LogService.json",
);
    nsApi.ns_end_transaction("AW_LogService", NS_AUTO_STATUS);
    return status;
}
   


}
