// Converted from Acc_Services_End_To_End_C/Address_Geo_Code_validation.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;


public class Address_Geo_Code_validation implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{

	int status = 0;
    nsApi.ns_start_transaction("Address_Geo_Code_validation");
    nsApi.ns_web_url("Address_Geo_Code_validation",
       "URL=https://{AccServiceHostUrl}/v1/validation/address",
       "METHOD=POST",
       "HEADER=Accept: application/json",
       "HEADER=Content-Type: application/json",
       "HEADER=correlation-id: Address_Geo_Code_validation_{CorrChar}{CorrID}",
       "BODY=$CAVINCLUDE$=Address_Geo_Code_validation.json",
    );
    	nsApi.ns_end_transaction("Address_Geo_Code_validation", NS_AUTO_STATUS);
	nsApi.ns_page_think_time(0);
	return status;

   }
}
