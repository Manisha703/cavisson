// Converted from Acc_Services_End_To_End_C/Refresh_Wallet_Mobile.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class Refresh_Wallet_Mobile implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	int i = 1;
	for(i=1;i<=5;i++)
{
        nsApi.ns_start_transaction("Refresh_Wallet_Mobile");
        nsApi.ns_web_url ("Refresh_Wallet_Mobile",
        "URL=https://{AccServiceHostUrl}/v1/profile/refreshWallet",
        "METHOD=PUT",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
        "HEADER=X-PROFILEID: {LPFProfileID}",
        "HEADER=profileId: {LPFprofileid}",
        "HEADER=channel: mobile",
        "HEADER=correlation-id: Refresh_Wallet_Mobile_{CorrChar}{CorrID}",
);
    nsApi.ns_end_transaction("Refresh_Wallet_Mobile", NS_AUTO_STATUS);
}
	return status;

}


}
