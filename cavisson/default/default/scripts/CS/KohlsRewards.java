// Converted from P2HR_With_Promo_Mixes_CM_0921/KohlsRewards.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: KohlsRewards 
Recorded By: cavisson
Date of recording: 07/16/2018 01:54:27
Flow details:
Build details: 4.1.10 (build# 34)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class KohlsRewards implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
          if (debug) return status;
		nsApi.ns_start_transaction("KohlsRewards");

		nsApi.ns_web_url("KohlsRewards",
				"URL=https://{UIHostUrl}/myaccount/kohls_rewards.jsp",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("KohlsRewards", NS_AUTO_STATUS);    	


		nsApi.ns_start_transaction("RewardsCard");

		nsApi.ns_web_url("RewardsCard",
				"URL=https://{UIHostUrl}/myaccount/rewards_card.jsp",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"HEADER=Content-Type:application/json",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("RewardsCard", NS_AUTO_STATUS);    

		
		nsApi.ns_start_transaction("RightPointBalance");

		nsApi.ns_web_url("RightPointBalance",
				"URL=https://{UIHostUrl}/myaccount/kohls_rewards_right_point_balance.jsp",
				"METHOD=POST",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq"
				);

		nsApi.ns_end_transaction("RightPointBalance", NS_AUTO_STATUS);

		nsApi.ns_start_transaction("RightRecentActivity");

		nsApi.ns_web_url("RightRecentActivity",
				"URL=https://{UIHostUrl}/myaccount/kohls_rewards_right_recent_activity.jsp",
				"METHOD=POST",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("RightRecentActivity", NS_AUTO_STATUS);

          return status;
	}

}
