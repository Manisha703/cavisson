package CS;
import pacJnvmApi.NSApi;

public class StoreRadius implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("StoreRadius");
		status = nsApi.ns_web_url ("StoreRadius",
				"URL=http://{SnBServiceHostUrl}/v1/stores?postalCode={Zipcode_FP}&radius=25",
				"METHOD=GET",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: StoreRadius_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("StoreRadius", NS_AUTO_STATUS);
		return status;
	}
}
