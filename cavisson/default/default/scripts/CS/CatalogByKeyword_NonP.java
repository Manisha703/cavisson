package CS;
import pacJnvmApi.NSApi;

public class CatalogByKeyword_NonP implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("CatalogByKeyword_NonP");
		status = nsApi.ns_web_url("CatalogByKeyword_NonP",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?keyword={CatalogKeyword_FP}&limit=60&offset=1&isLTL=true&channel=web&storeNum={StoreId_FP}&isDefaultStore=true&includeStoreOnlyProducts=true",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"	
				);
		status = nsApi.ns_end_transaction("CatalogByKeyword_NonP", NS_AUTO_STATUS); 

		int randNumber = nsApi.ns_get_random_number_int(1,100);
		if(randNumber<=50)
		{
			status = nsApi.ns_start_transaction("CatalogByKeyword_Dimension_NonP");
			status = nsApi.ns_web_url("CatalogByKeyword_Dimension_NonP",
					"URL=http://{SnBServiceHostUrl}/v1/catalog?dimensionValueID={CatalogDimValueId_FP}&sortID=1&storeNum={StoreId_FP}&isDefaultStore=true&shipToStore=true&keyword={CatalogDimKeyword_FP}&includeStoreOnlyProducts=true&isVGC=false&offset=1&limit=72",
					"HEADER=Accept: application/json",
					"HEADER=Content-Type: application/json",
					"HEADER=bypass_cache: false",
					"HEADER=X-APP-API_KEY: {key}",
					"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"	
					);
			status = nsApi.ns_end_transaction("CatalogByKeyword_Dimension_NonP", NS_AUTO_STATUS); 

			status = nsApi.ns_page_think_time(0);
		}
		return status;
	}
}
