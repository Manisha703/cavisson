package CS;
import pacJnvmApi.NSApi;

public class Catalog_VisualTypeAhead implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("Catalog_VisualTypeAhead");
		status = nsApi.ns_web_url("Catalog_VisualTypeAhead",
				"URL=http://{SnBServiceHostUrl}/v1/catalog/suggestions/products/{Keyword_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation-id: Catalog_VisualTypeAhead_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("Catalog_VisualTypeAhead", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
