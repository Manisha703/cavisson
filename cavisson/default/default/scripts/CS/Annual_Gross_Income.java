// Converted from P2HR_With_Promo_Mixes_CM_0921/Annual_Gross_Income.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: Annual_Gross_Income
Recorded By: Manish
Date of recording: 07/16/2018 01:54:27
Flow details:
Build details: 4.1.10 (build# 34)
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class Annual_Gross_Income implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception

	{
		int status =0;
		
		nsApi.ns_start_transaction("Annual_Gross_Income");
		nsApi.ns_web_url("update_house_hold_income",
				"URL=https://{UIHostUrl}/myaccount/json/myinfo/house_hold_income_json.jsp",
				"METHOD=POST",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Origin:https://{UIHostUrl}",
				"HEADER=Accept:application/json, text/javascript, */*; q=0.01",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AAMC_kohls_0;fltk;aam_uuid;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore",
				BODY_BEGIN,
				"annualGrossIncome={R_annualGrossIncome}&nds-pmd=%7B%22jvqtrgQngn%22%3A%7B%22oq%22%3A%221366%3A607%3A0%3A0%3A1366%3A728%22%2C%22wfi%22%3A%22flap-98538%22%2C%22oc%22%3A%22r180p62r2on26oq2%22%2C%22fe%22%3A%220k0+0%22%2C%22qvqgm%22%3A%22-330%22%2C%22jxe%22%3A598218%2C%22syi%22%3A%22snyfr%22%2C%22si%22%3A%22si%2Cbtt%2Czc4%2Cjroz%22%2C%22sn%22%3A%22sn%2Czcrt%2Cbtt%2Cjni%22%2C%22us%22%3A%221o20o4p236p3q1pp%22%2C%22cy%22%3A%22Jva32%22%2C%22sg%22%3A%22%7B%5C%22zgc%5C%22%3A0%2C%5C%22gf%5C%22%3Asnyfr%2C%5C%22gr%5C%22%3Asnyfr%7D%22%2C%22sp%22%3A%22%7B%5C%22gp%5C%22%3Agehr%2C%5C%22ap%5C%22%3Agehr%7D%22%2C%22sf%22%3A%22gehr%22%2C%22jt%22%3A%22ss4n2rp5r9p7o3s0%22%2C%22sz%22%3A%221rqsq5761s4859n9%22%2C%22vce%22%3A%22apvc%2C0%2C5o4p54n0%2C2%2C1%3Bzp%2C0%2C270%2C262%2C%3Bzzf%2C1q53%2Crn63%2C1r%2CAnA+AnA%2CAnA+AnA%2CAnA%2CAnA%2CAnA%2CAnA%2CAnA%3Bzz%2C53r3%2C445%2C2p2%2C%3Bgf%2C0%2C7136%3Bzp%2C6sn%2C44o%2C209%2C%3Bzp%2C56p%2C243%2C287%2Cxyf_ubhfrUbyqVapbzr%3Bzz%2Co21%2C244%2C288%2Cxyf_ubhfrUbyqVapbzr%3B%22%2C%22ns%22%3A%22%22%7D%2C%22jg%22%3A%22%22%7D",
				BODY_END
				);
		nsApi.ns_end_transaction("Annual_Gross_Income", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0.0);

		return status;
	}

}
