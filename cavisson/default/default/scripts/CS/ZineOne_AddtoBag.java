// Converted from P2HR_With_Promo_Mixes_CM_0921/ZineOne_AddtoBag.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: ZineOne_AddtoBag
Recorded By: cavisson
Date of recording: 10/04/2018 01:18:37
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class ZineOne_AddtoBag implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
          if (debug) return status;

		if(nsApi.ns_eval_string("{DV_UserType}").equals("UnReg"))
		{
			nsApi.ns_save_string(nsApi.ns_eval_string("{deviceId}"), "DV_DynID");
		}



		nsApi.ns_start_transaction("Event_addedtobag");
		nsApi.ns_web_url ("event_3_AddtoBag",
				"URL=https://{UIHostUrl}/c3/api/v1/event",
				"METHOD=POST",
				"HEADER=Origin:https://www.kohls.com",
				"HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357",
				"HEADER=Accept-Language:en-in",
				"HEADER=Content-Type:text/plain",
				"PreSnapshot=webpage_1538639274864.png",
				"Snapshot=webpage_1538639274876.png",
				BODY_BEGIN,
				"[{"cartItems":"[{$CAVREPEAT_BLOCK_START,sep(,),Count(SP_zine_cartItemId)}{\"id\":\"{SP_zine_cartItemId}\",\"sku\":\"{SP_zine_skuId}\",\"webID\":\"{SP_zine_productId}\",\"saleUnitPrice\":\"{SP_zine_saleUnitprice}\",\"regularUnitPrice\":\"{SP_zine_regularUnitPrice}\"}{$CAVREPEAT_BLOCK_END}]","page":"PDP","event":"_ws_addedtobag","z1_profile":{"device_id":"{deviceId}","_z1_origin":"282de6e8-1e7c-4025-d58d-85406dcd7afc","firstTimeUser":"true","os":"html5","os_version":"8.1","platform_os":"Windows","language":"en","country":"US","tz":"IST","devicetype":"desktop","browser":"Chrome","browserversion":"Chrome:69.0.3497.100","resolution":"1366x768","_z1_pagename":"{SP_z1pagename}.jsp","_z1_pagepath":"/product/{SP_z1pagepath}.jsp"},"profileId":"{DV_DynID}","z1SDKVersion":"3.1.27","wstoken":"kohls_com:-970539763","color":"Black","prdPV":"{SP_prdPV}"}]",
				BODY_END
				);

		nsApi.ns_end_transaction("Event_addedtobag", NS_AUTO_STATUS);

          return status;


	}

}
