// Converted from Acc_Services_End_To_End_C/AW_GetLatestVersion.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class AW_GetLatestVersion implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
    int status = 0;

 	String hash;
     String lid = nsApi.ns_eval_string("{LID}");
     hash = Integer.toString(lid.hashCode());
     nsApi.ns_save_string(hash, "hash_dp");
   	nsApi.ns_start_transaction("AW_GetLatestVersion");
     nsApi.ns_web_url ("AW_GetLatestVersion",
        "URL=https://{AccServiceHostUrl}/v1/passthru/v1/passes/pass.com.kohls.y2y/Pqn66Eqq",
        "METHOD=GET",
        "HEADER=Accept: application/vnd.apple.pkpass",
        "HEADER=Content-Type: application/json",
        "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
);
    nsApi.ns_end_transaction("AW_GetLatestVersion", NS_AUTO_STATUS);
    
    nsApi.ns_start_transaction("AW_RegisterPush");
    nsApi.ns_web_url ("AW_RegisterPush",
       "URL=https://{AccServiceHostUrl}/v1/passthru/v1/devices/{AuthToken}/registrations/pass.com.kohls.y2y/{SerialNumber}",
        "METHOD=POST",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
        "HEADER=HTTP_AUTHORIZATION: ApplePass {AuthToken}",
        "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr799087896",
        "BODY=$CAVINCLUDE$=AW_RegisterPush.json",
);
    nsApi.ns_end_transaction("AW_RegisterPush", NS_AUTO_STATUS);
    
    nsApi.ns_start_transaction("AW_UnRegister");
    nsApi.ns_web_url ("AW_UnRegister",
       "URL=https://{AccServiceHostUrl}/v1/passthru/v1/devices/{AuthToken}/registrations/pass.com.kohls.y2y/{SerialNumber}",
        "METHOD=DELETE",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
        "HEADER=HTTP_AUTHORIZATION: ApplePass {AuthToken}",
        "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr799087896",
);
    nsApi.ns_end_transaction("AW_UnRegister", NS_AUTO_STATUS);
    
    nsApi.ns_start_transaction("AW_GetSerialNum");
    nsApi.ns_web_url ("AW_GetSerialNum",
         "URL=https://{AccServiceHostUrl}/v1/passthru/v1/devices/{AuthToken}/registrations/pass.com.kohls.y2y",
          "METHOD=GET",
          "HEADER=Accept: application/json",
          "HEADER=Content-Type: application/json",
          "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
          "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr799087896",

);
    nsApi.ns_end_transaction("AW_GetSerialNum", NS_AUTO_STATUS);
    return status;

}
   


}
