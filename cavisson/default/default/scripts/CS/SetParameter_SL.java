// Converted from P2HR_With_Promo_Mixes_CM_0921/SetParameter_SL.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: SetParameter_SL
Generated By: cavisson
Date of generation: 07/25/2018 09:22:29
Flow details:
Build details: 4.1.11 (build# 216)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;




public class SetParameter_SL implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;

		nsApi.ns_advance_param("SL_FP_Email_ID");
		nsApi.ns_save_string(nsApi.ns_eval_string("{SL_FP_Email_ID}"), "DV_EmailID");

		nsApi.ns_save_string(nsApi.ns_eval_string("{SL_FP_Password}"), "DV_Password");

		nsApi.ns_save_string(nsApi.ns_eval_string("{SL_FP_DynID}"), "DV_DynID");

		nsApi.ns_save_string(nsApi.ns_eval_string("{SL_FP_XProfileID}"), "DV_XProfileID");


		//nsApi.ns_save_data_ex("/home/cavisson/work/data/SoftLogin_Email.txt",NS_APPEND_FILE,"%s%s","SoftLogin,",nsApi.ns_eval_string("{DV_EmailID},{DV_Password},{DV_DynID},{DV_XProfileID}"));
		status = nsApi.ns_save_data_eval("/tmp/SoftLogin_Email.txt", NS_APPEND_FILE,"{DV_EmailID},{DV_Password},{DV_DynID},{DV_XProfileID}\n");
		
		nsApi.ns_save_string(nsApi.ns_eval_string("{DV_EmailID}"), "CancelOrder_Email_ID");




		nsApi.ns_save_string("ShiptoMe", "DV_OrderType");
		return status;



	}

}
