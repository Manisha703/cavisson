// Converted from Acc_Services_End_To_End_C/Authenticate.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class Authenticate implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("Authenticate");
     nsApi.ns_web_url ("Authenticate",
        "URL=https://{AccServiceHostUrl}/v1/authenticate",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=correlation-id: Authenticate_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=authenticate.json",
);
    nsApi.ns_end_transaction("Authenticate", NS_AUTO_STATUS);
    return status;

}

}
