// Converted from P2HR_With_Promo_Mixes_CM_0921/TypeHeadSolr.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: TypeHeadSolr
Recorded By: Manish
Date of recording: 07/10/2018 04:15:29
Flow details:
Build details: 4.1.10 (build# 34)
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class TypeHeadSolr implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;

		nsApi.ns_start_transaction("Typehead_LoggedInUser");
		nsApi.ns_web_url ("Typehead_LoggedInUser",
				"URL=https://{UIHostUrl}/typeahead/{RS_typehead}.jsp?callback=categoryGenderTypeaheadResult&ta_exp=d&svid=&vid=3000000082637001&pid=3000000082637001&_=1553505046542",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Referer:https://{UIHostUrl}/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=store_location;_abck;SL_Cookie;AKA_A2;bm_sz;mosaic;X-SESSIONID;akacd_www-kohls-com-mosaic-p2;check;kohls_wcs_cookie;kohls_klbd_cookie;dcroute;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;namogooTest;AAMC_kohls_0;fltk;aam_uuid;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;SignalSpring2016;SignalSpring2018;cto_lwid;btpdb.4DPyaxM.dGZjLjYyMTAxMDM;btpdb.4DPyaxM.dGZjLjYyMTAxMTA;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;staleStorage;__gads;_ga;_gid;IR_gbd;IR_PI;btpdb.4DPyaxM.X2J0X2djbXNfaWQ;SignalUniversalID;productnum;ndcd;s_stv;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;CavNV;TS01ada7dd;DCRouteS;LoginSuccess;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;fsr.s;VisitorBagSession;VisitorBagTotals;isPreQualEligible;_gat;rr_rcs;IR_5349;mbox;_scid;correlation-id;X-PROFILEID;CRTOABE;akavpau_www;hl_p;gpv_v9;_sctr;s_sq;RT;CavNVC;CavSF"
				);
		nsApi.ns_end_transaction("Typehead_LoggedInUser", NS_AUTO_STATUS);

		nsApi.ns_start_transaction("Typehead_AnonymousUser");
		nsApi.ns_web_url ("Typehead_AnonymousUser",
				"URL=https://{UIHostUrl}/typeahead/{RS_typehead}.jsp?callback=categoryGenderTypeaheadResult&ta_exp=d&svid=&vid=30956936758633277891888015041835422237&_=1553504647709",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Referer:https://{UIHostUrl}/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=store_location;_abck;SL_Cookie;AKA_A2;bm_sz;mosaic;X-SESSIONID;akacd_www-kohls-com-mosaic-p2;check;kohls_wcs_cookie;kohls_klbd_cookie;dcroute;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;namogooTest;AAMC_kohls_0;fltk;aam_uuid;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;SignalSpring2016;SignalSpring2018;cto_lwid;btpdb.4DPyaxM.dGZjLjYyMTAxMDM;btpdb.4DPyaxM.dGZjLjYyMTAxMTA;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;staleStorage;__gads;_ga;_gid;IR_gbd;IR_PI;btpdb.4DPyaxM.X2J0X2djbXNfaWQ;SignalUniversalID;productnum;ndcd;s_stv;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;CavNV;TS01ada7dd;DCRouteS;LoginSuccess;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;fsr.s;VisitorBagSession;VisitorBagTotals;isPreQualEligible;_gat;rr_rcs;IR_5349;mbox;_scid;correlation-id;X-PROFILEID;CRTOABE;akavpau_www;hl_p;gpv_v9;_sctr;s_sq;RT;CavNVC;CavSF"
				);
		nsApi.ns_end_transaction("Typehead_AnonymousUser", NS_AUTO_STATUS);

		nsApi.ns_start_transaction("KeyStroke_slr");
		nsApi.ns_web_url ("KeyStroke_slr",
				"URL=https://{UIHostUrl}/typeahead/{RS_typehead}.jsp?callback=typeaheadResult&ta_exp=d&_=1532359242556",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Referer:https://{UIHostUrl}/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=store_location;_abck;SL_Cookie;AKA_A2;bm_sz;mosaic;X-SESSIONID;akacd_www-kohls-com-mosaic-p2;check;kohls_wcs_cookie;kohls_klbd_cookie;dcroute;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;namogooTest;AAMC_kohls_0;fltk;aam_uuid;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;SignalSpring2016;SignalSpring2018;cto_lwid;btpdb.4DPyaxM.dGZjLjYyMTAxMDM;btpdb.4DPyaxM.dGZjLjYyMTAxMTA;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;staleStorage;__gads;_ga;_gid;IR_gbd;IR_PI;btpdb.4DPyaxM.X2J0X2djbXNfaWQ;SignalUniversalID;productnum;ndcd;s_stv;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;CavNV;TS01ada7dd;DCRouteS;LoginSuccess;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;fsr.s;VisitorBagSession;VisitorBagTotals;isPreQualEligible;_gat;rr_rcs;IR_5349;mbox;_scid;correlation-id;X-PROFILEID;CRTOABE;akavpau_www;hl_p;gpv_v9;_sctr;s_sq;RT;CavNVC;CavSF"
				);




		
		if(nsApi.ns_eval_string("{SP_Suggestion_block}").equals(""))
		{
			nsApi.ns_end_transaction_as("KeyStroke_slr", 0, "KeyStroke_slr_NoResult"); 

		}else{
			nsApi.ns_end_transaction("KeyStroke_slr", NS_AUTO_STATUS);       	
		}

		nsApi.ns_page_think_time(0);
		
		if(!nsApi.ns_eval_string("{SP_Suggestion_block}").equals(""))
		{
			nsApi.ns_start_transaction("VisualTypeaheadDefault");
			nsApi.ns_web_url ("VisualTypeaheadDefault",
					"URL=https://{UIHostUrl}/snb/catalog/suggestions/{SP_defSuggestion}?preview=true&callback=typeaheadProductPreview",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
					"HEADER=Upgrade-Insecure-Requests:1",
					"HEADER=Referer:https://{UIHostUrl}/sale-event/mens-clothing.jsp?cc=mens-TN1.0-S-men",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"COOKIE=store_location;_abck;SL_Cookie;AKA_A2;bm_sz;mosaic;X-SESSIONID;akacd_www-kohls-com-mosaic-p2;check;kohls_wcs_cookie;kohls_klbd_cookie;dcroute;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;namogooTest;AAMC_kohls_0;fltk;aam_uuid;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;SignalSpring2016;SignalSpring2018;cto_lwid;btpdb.4DPyaxM.dGZjLjYyMTAxMDM;btpdb.4DPyaxM.dGZjLjYyMTAxMTA;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;staleStorage;__gads;_ga;_gid;IR_gbd;IR_PI;btpdb.4DPyaxM.X2J0X2djbXNfaWQ;ndcd;s_stv;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;LoginSuccess;VisitorBagSession;VisitorBagTotals;isPreQualEligible;_scid;CRTOABE;hl_p;_sctr;fsr.s;SignalUniversalID;CavNV;TS01ada7dd;DCRouteS;productnum;gpv_v9;fsr.a;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;mbox;_gat;rr_rcs;CavSF;IR_5349;akavpau_www;correlation-id;X-PROFILEID;RT;s_sq;kls_p;CavNVC"
					);
			nsApi.ns_end_transaction("VisualTypeaheadDefault", NS_AUTO_STATUS);


			if ( nsApi.ns_get_int_val("productItems") > 0)
			{
				nsApi.ns_end_transaction_as("VisualTypeaheadDefault", 0, "VisualTypeaheadDefault_NoResults"); 
			}else{
				nsApi.ns_end_transaction("VisualTypeaheadDefault", NS_AUTO_STATUS);
			}

			nsApi.ns_page_think_time(0);

			int sugg_count = nsApi.ns_get_int_val("SP_Suggestion_all_count");
			int rand_sugg = nsApi.ns_get_random_number_int(1,nsApi.ns_get_int_val("SP_Suggestion_all_count"));

			if(rand_sugg > 2)
				rand_sugg=2;
			int sugg;
			String buffer;		
			for (sugg=1;sugg<=rand_sugg;sugg++)
			{			

				buffer = "\"{SP_Suggestion_all_" +  sugg+ "}\"";
				nsApi.ns_save_string(nsApi.ns_eval_string(buffer),"SP_Suggestion");

				nsApi.ns_start_transaction("VisualTypeahead");
				nsApi.ns_web_url ("Visultyehead",
						"URL=https://{UIHostUrl}/snb/catalog/suggestions/{SP_Suggestion}?preview=true&callback=typeaheadProductPreview",
						"HEADER=Upgrade-Insecure-Requests:1",
						"HEADER=Accept-Language:en-us",
						"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;productnum;check;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;SignalUniversalID;s_stv;mbox;LoginSuccess;s_cc;VisitorBagTotals;VisitorBagSession;isPreQualEligible;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_fid;gpv_v9;s_sq;kls_p",
						INLINE_URLS,
						"URL=https://{UIHostUrl}/pcs/activeview?xai=AKAOjstoJJTayul0wyeD1pvNBIEn-XEEfygQyMGWIj0L0Crai4iA4U3zB40zS8RGk6sK3qnCMXbpkxpvNP4Qg5pXqk766u_vKIKgPDo&sig=Cg0ArKJSzOagUCTDMMcMEAE&adk=4151307844&tt=59331&bs=1263%2C583&mtos=59295%2C59295%2C59295%2C59295%2C59295&tos=59295%2C0%2C0%2C0%2C0&p=321%2C120%2C571%2C420&mcvt=59295&rs=3&ht=0&tfs=119&tls=59414&mc=1&lte=1&bas=0&bac=0&avms=geo&bos=-12245933%2C-12245933&ps=1263%2C1335&ss=0%2C0&pt=86&deb=1-4-4-896-553-6-18917-545&tvt=59344&op=1&r=u&id=osdtos&ti=1&uc=6508&tgt=DIV&cl=1&cec=5&clc=1&cac=0&cd=300x250&v=r20180627", "HEADER=Cache-Control:max-age=0", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/pcs/activeview?xai=AKAOjsuCIGZFO5kM_UNnoTo_Kq898LSHCxUIEbcQ6IAJyBgls-7yu6_uvxfxKpSacbCU1NsKL_W0xHrwytlqd9vdppcRHXeLKS3xrJ8&sig=Cg0ArKJSzG9FgXel_g6lEAE&adk=3063740786&tt=59331&bs=1263%2C583&mtos=59297%2C59297%2C59297%2C59297%2C59297&tos=59297%2C0%2C0%2C0%2C0&p=321%2C482%2C571%2C782&mcvt=59297&rs=3&ht=0&tfs=117&tls=59414&mc=1&lte=1&bas=0&bac=0&avms=geo&bos=-12245933%2C-12245933&ps=1263%2C1335&ss=0%2C0&pt=86&deb=1-4-4-896-553-6-18917-545&tvt=59344&op=1&r=u&id=osdtos&ti=1&uc=6492&tgt=DIV&cl=1&cec=5&clc=1&cac=0&cd=300x250&v=r20180627", "HEADER=Cache-Control:max-age=0", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/ddm/activity/dc_oe=ChMIt--C-5WU3AIVEb9PCh06TQ8VEAEYACD1h_Au;met=1;&timestamp=1531213338072;eid1=2;ecn1=0;etm1=1;", "HEADER=Cache-Control:max-age=0", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/activeview?avi=Bnt0G23VEW_eVL5H-vgK6mr2oAQD36q-N_QYAABABOAHIAQngAgDgBAGgBhHSCAUIgGEQAcITGgoSGKDIiP0DIgoIAxABGAEgAFAAGNL4y-ID&cid=CAASFeRoyqY5yb-EZ-sgoq5qB5hoRredXg&adk=4276857337&tt=59331&bs=1263%2C583&mtos=59330%2C59330%2C59330%2C59330%2C59330&tos=59330%2C0%2C0%2C0%2C0&p=321%2C844%2C571%2C1144&mcvt=59330&rs=3&ht=0&tfs=84&tls=59414&mc=1&lte=1&bas=0&bac=0&avms=geo&bos=-12245933%2C-12245933&ps=1263%2C1335&ss=0%2C0&pt=86&deb=1-4-4-896-553-6-18917-545&tvt=59344&op=1&r=u&id=osdtos&ti=1&uc=5936&tgt=BODY&cl=1&lop=1&tslp=45248&cec=13&clc=0&cac=0&cd=300x254&v=r20180627", "HEADER=Cache-Control:max-age=0", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/activeview?avi=BbsZm23VEW_eVL5H-vgK6mr2oAQAAAAAQATgB4AQCiAWuttQJoAZH&adk=4276857337&tt=59331&bs=1263%2C583&mtos=59330%2C59330%2C59330%2C59330%2C59330&tos=59330%2C0%2C0%2C0%2C0&p=321%2C844%2C571%2C1144&mcvt=59330&rs=3&ht=0&tfs=84&tls=59414&mc=1&lte=1&bas=0&bac=0&avms=geo&bos=-12245933%2C-12245933&ps=1263%2C1335&ss=0%2C0&pt=86&deb=1-4-4-896-553-6-18917-545&tvt=59344&op=1&r=u&id=osdtos&ti=1&uc=5936&tgt=BODY&cl=1&lop=1&tslp=45249&cec=13&clc=0&cac=0&cd=300x254&v=r20180627", "HEADER=Cache-Control:max-age=0", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/snb/media/javascript/deploy/environment.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;productnum;check;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;SignalUniversalID;s_stv;mbox;LoginSuccess;s_cc;VisitorBagTotals;VisitorBagSession;isPreQualEligible;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_fid;gpv_v9;s_sq;kls_p", END_INLINE,
						"URL=https://{UIHostUrl}/snb/media/css/productlist.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;productnum;check;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;SignalUniversalID;s_stv;mbox;LoginSuccess;s_cc;VisitorBagTotals;VisitorBagSession;isPreQualEligible;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_fid;gpv_v9;s_sq;kls_p", END_INLINE,
						"URL=https://{UIHostUrl}/snb/media/css/productlist2.css", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;productnum;check;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;SignalUniversalID;s_stv;mbox;LoginSuccess;s_cc;VisitorBagTotals;VisitorBagSession;isPreQualEligible;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_fid;gpv_v9;s_sq;kls_p", END_INLINE,
						"URL=https://{UIHostUrl}/snb/media/javascript/deploy/framework2.js", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;productnum;check;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;SignalUniversalID;s_stv;mbox;LoginSuccess;s_cc;VisitorBagTotals;VisitorBagSession;isPreQualEligible;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_fid;gpv_v9;s_sq;kls_p", END_INLINE
						);

				if ( nsApi.ns_get_int_val("productItems") > 0)
				{
					nsApi.ns_end_transaction_as("VisualTypeahead", 0, "VisualTypeahead_NoResults"); 
				}else{
					nsApi.ns_end_transaction("VisualTypeahead", NS_AUTO_STATUS);
				}



			}

			
			if(!nsApi.ns_eval_string("{SP_productURL}").equals(""))
			{

				nsApi.ns_start_transaction("SelectVisualProdcut");
				nsApi.ns_web_url ("SelectVisualProdcut",
						"URL=https://{UIHostUrl}{SP_productURL}submit-search=web-ta-product&pfm=search-ta%20product&search={SP_searchKeyword}",
						"HEADER=X-Requested-With:XMLHttpRequest",
						"HEADER=Accept-Language:en-us",
						"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;evarRefinementValues;pdpUrl;productnum;s_cc;CRTOABE;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;AAMC_kohls_0;fltk;aam_uuid;fsr.s;bd_session;mbox;check;hl_p;rr_rcs;SignalUniversalID;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq;kls_p",
						INLINE_URLS,
						"URL=https://{UIHostUrl}/is/image/kohls/1028112_Dark_Obsidian?wid=240&hei=240&op_sharpen=1", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/is/image/kohls/1477621_Black?wid=240&hei=240&op_sharpen=1", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/b/ss/kohlsgnwebstoreqa/10/JS-2.0.0/s91094524426735", "METHOD=POST", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept-Language:en-us", "HEADER=Content-Type:text/plain;charset=UTF-8",
						BODY_BEGIN,
						"AQB=1&ndh=1&pf=1&et=1&t=10%2F6%2F2018%204%3A3%3A14%202%20300&cid.&atgid.&id=3002100005378344&as=1&.atgid&.cid&d.&nsid=0&jsonv=1&.d&sdid=4CA394CD763B59AC-19CF3837CEA8802F&mid=92013556980993099993160920543835275268&aamlh=9&ce=UTF-8&ns=kohls&pageName=d%3Eclothing%7Choodies%20%26%20sweatshirts%7Cmens%7Ctops&g=https%3A%2F%2F{UIHostUrl}%2Fcatalog%2Fmens-hoodies-sweatshirts-tops-clothing.jsp%3FCN%3DGender%3AMens%2BSilhouette%3AHoodies%2520%2526%2520Sweatshirts%2BCategory%3ATops%2BDepartment%3AClothing%26cc%3Dmens-TN3.0-S-hoodiessweatshirts&c.&k.&pageDomain={UIHostUrl}&.k&mcid.&version=2.1.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event38%2Cevent58&products=%3Bproductmerch2&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&v3=browse%20refine&c4=d%7Cpmp&c5=non-search&v8=non-search&c9=mens-hoodies-sweatshirts-tops-clothing%7Cpmp&v9=d%3Eclothing%7Choodies%20%26%20sweatshirts%7Cmens%7Ctops&c16=browse%20refine&c17=kohls%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20logged%20in%7Cloyalty%20not%20logged%20in&c18=tue%7Cweekday%7C04%3A00%20am&v18=tue%7Cweekday%7C04%3A00%20am&c22=2018-07-10&v22=desktop&v23=size%20range&v24=size%20range%3Aregular&v27=clothing%7Choodies%20%26%20sweatshirts%7Cmens%7Csize%20range%3Aregular%7Ctops%7Call&v28=clothing%7Choodies%20%26%20sweatshirts%7Cmens%7Ctops&v29=featured%3A1%3A60&v39=3002100005378344&c40=clothing%7Choodies%20%26%20sweatshirts%7Cmens%7Ctops%3Esize%20range%3Aregular&v40=cloud17&c42=D%3Dv28&v42=registered&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v71=klsbrwcki%7C3002100005378344&v96=browse%7C%7C%7C%7C%7C1&c.&a.&activitymap.&page=d%3Eclothing%7Choodies%20%26%20sweatshirts%7Cmens%7Ctops&link=Regular%20%28120%29&region=SizeRange&pageIDType=1&.activitymap&.a&.c&pid=d%3Eclothing%7Choodies%20%26%20sweatshirts%7Cmens%7Ctops&pidt=1&oid=https%3A%2F%2F{UIHostUrl}%2Fcatalog%2Fmens-regular-hoodies-sweatshirts-tops-clothing.jsp%3FCN%3DGe&ot=A&s=0x0&j=1.6&v=N&k=Y&bw=1280&bh=583&AQE=1",
						BODY_END,
						END_INLINE,
						"URL=https://{UIHostUrl}/mapfiles/api-3/images/powered-by-google-on-white3_hdpi.png", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/mapfiles/api-3/images/autocomplete-icons_hdpi.png", "HEADER=Accept-Language:en-us", END_INLINE,
						"URL=https://{UIHostUrl}/ads/user-lists/1018012790/?guid=ON&data=10170497&cdct=2&is_vtc=1&random=1367635302", END_INLINE,
						"URL=https://{UIHostUrl}/ads/user-lists/1018012790/?guid=ON&data=10170497&cdct=2&is_vtc=1&random=1367635302", "HEADER=Accept-Language:en-us", END_INLINE
						);
				nsApi.ns_end_transaction("SelectVisualProdcut", NS_AUTO_STATUS);

				//nsApi.ns_save_data_ex("/home/cavisson/work/data/P2HR/PDP_UrlHit.txt",NS_APPEND_FILE,"%s%s",nsApi.ns_eval_string("{SP_productURL},{SP_searchKeyword}"));
				status = nsApi.ns_save_data_eval("/tmp/PDP_UrlHit.txt", NS_APPEND_FILE,"{SP_productURL},{SP_searchKeyword}\n");
			}

			nsApi.ns_start_transaction("Web_PDPSessionTypeHead");
			nsApi.ns_web_url ("Web_PDPSessionTypeHead",
					"METHOD=POST",
					"URL=https://{UIHostUrl}/web/session.jsp?lpf=v2",
					"HEADER=Accept:application/json",
					"HEADER=Accept-Encoding:gzip, deflate, br",
					"HEADER=Accept-Language:en-US,en;q=0.5",
					"HEADER=Content-Type:application/json",
					BODY_BEGIN,
					"{"pageName":"PDP","pId":"{PIDJSPSP}"}",
					BODY_END,
					);
			nsApi.ns_end_transaction("Web_PDPSessionTypeHead", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);

		}

		return status;

	}

}
