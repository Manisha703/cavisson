// Converted from Acc_Services_End_To_End_C/PreferredPayment_CMDM.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class PreferredPayment_CMDM implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	 int status = 0;
	nsApi.ns_start_transaction("PreferredPayment_CMDM");
  	nsApi.ns_web_url ("PreferredPayment_CMDM",
        "URL=https://{AccServiceHostUrl}/v1/profile/paymentType/preferred",
          "METHOD=PUT",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
        "HEADER=X-PROFILEID: {LPFProfileID}.",
         "HEADER=correlation-id: PreferredPayment_CMDM_{CorrChar}{CorrID}",
//          "HEADER=X-APP-API_KEY: {key}",
//        "HEADER=Accept-Language: en-us,en;q=0.5",
  //      "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
    //    "HEADER=Proxy-Connection: keep-alive",
        "BODY=$CAVINCLUDE$=PreferredPayment_CMDM.json",
);
    nsApi.ns_end_transaction("PreferredPayment_CMDM", NS_AUTO_STATUS);
    
    //return;
	return status;


}


}
