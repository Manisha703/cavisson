package CS;
import pacJnvmApi.NSApi;

public class StoreLatitude implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("StoreLatitude");
		status = nsApi.ns_web_url ("StoreLatitude",
				"URL=http://{SnBServiceHostUrl}/v1/stores?latitude=41.920368&longitude=-87.670692",
				"METHOD=GET",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: StoreLatitude_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("StoreLatitude", NS_AUTO_STATUS);
		return status;
	}
}
