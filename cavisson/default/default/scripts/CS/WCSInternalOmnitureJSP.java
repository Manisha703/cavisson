// Converted from WCS_WithNewURL/WCSInternalOmnitureJSP.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name:WCSInternalOmnitureJSP
Recorded By: Jitendra
Date of recording: 03/08/2014 04:59:11
Flow details:
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class WCSInternalOmnitureJSP implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		nsApi.ns_start_transaction("WCSInternal_OmnitureAkamaiJspPage"); 

		nsApi.ns_web_url ("WCSInternal_OmnitureAkamaiJspPage",
				"URL=http://{WcsHostURL}/wcs-internal/OmnitureAkamai.jsp",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive",
				);

		nsApi.ns_end_transaction("WCSInternal_OmnitureAkamaiJspPage", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);
		return status;

	}

}
