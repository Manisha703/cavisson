package CS;
import pacJnvmApi.NSApi;

public class CatalogCategory implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;      
		status = nsApi.ns_start_transaction("CatalogCategory");
		status = nsApi.ns_web_url("CatalogCategory",
				"URL=http://{SnBServiceHostUrl}/v1/catalog/category?channel=mobile",
				"HEADER=Accept: application/xml",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=bypass_cache: false",
				"HEADER=Content-Type: application/xml",
				"HEADER=correlation_id: OAPIPERF_OAPIPERF_CatalogCategory_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("CatalogCategory", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		status = nsApi.ns_save_data_eval("output/UsedCategoryId.txt", NS_TRUNC_FILE, "{CategoryId}\n");
		nsApi.ns_end_session();

		return status ;
	}
}
