package CS;
import pacJnvmApi.NSApi;

public class PriceByWebId_GET implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("PriceByWebId_GET");
		status = nsApi.ns_web_url("PriceByWebId_GET",
				"METHOD=GET",
				"URL=http://{SnBServiceHostUrl}/v1/price/webID/{Product_WebID_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: PriceByWebId_{CorrChar}{CorrID}"

				);
		status = nsApi.ns_end_transaction("PriceByWebId_GET", NS_AUTO_STATUS);
		status = nsApi.ns_save_data_eval("output/ValidWebId.txt", NS_APPEND_FILE, "{WebIDSP}\n");
		status = nsApi.ns_page_think_time(0); 
		return status;
	} 
}
