// Converted from Acc_Services_End_To_End_C/PrequalEligibility.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class PrequalEligibility implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{	
	int status = 0;
	nsApi.ns_start_transaction("PrequalEligility");
     nsApi.ns_web_url ("PrequalEligility",
          "URL=https://{AccServiceHostUrl}/v1/profile/prequal/eligibility",
          "METHOD=GET",
          "HEADER=Accept: application/json",
          "HEADER=Content-Type: application/json",
          "HEADER=userId: tracy.mendel@bh.exacttarget.com",
          "HEADER=X-SESSIONID: Gwopq82S1UzmZMmCIGleOwDIRU4OqAErUjfZEQNIPIAcIUZlp2Bg1hpmwJpzK1cR",
          "HEADER=X-PROFILEID: xPXIz4St1gcIfoMUXteaU3gyHR5n5QkfjcmMdM4deQE.",
          "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr7F5",
          "HEADER=correlation-id: PrequalEligibility_{CorrChar}{CorrID}",
);
     nsApi.ns_end_transaction("PrequalEligility", NS_AUTO_STATUS);
	return status;

}

}
