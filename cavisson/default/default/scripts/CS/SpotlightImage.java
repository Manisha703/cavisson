// Converted from WCS_WithNewURL/SpotlightImage.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name: Spotlight
Recorded By:Megha Rawat
Date of recording: 04/07/2017 04:59:11
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class SpotlightImage implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;


		nsApi.ns_start_transaction("SpotlightImage"); 
		nsApi.ns_web_url ("SpotlightImage",
				"URL=http://{WcsHostURL}/{SIUrlFP}",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive", 
				"COOKIE=origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;",
				);
		nsApi.ns_end_transaction("SpotlightImage", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);  

		  return status;


	}


}
