// Converted from Acc_Services_End_To_End_C/LTY_GetLoyaltyProfile.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class LTY_GetLoyaltyProfile implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
      int status = 0;
      nsApi.ns_start_transaction("LTY_GetLoyaltyProfile");
      nsApi.ns_web_url ("LTY_GetLoyaltyProfile",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/{LoyalytID}/profile",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
//          "HEADER=X-APP-API_KEY: {key}",
//        "HEADER=Accept-Language: en-us,en;q=0.5",
  //      "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
    //    "HEADER=Proxy-Connection: keep-alive",
      //  "BODY=$CAVINCLUDE$=LTY_GetLoyaltyProfile.json",
);
    nsApi.ns_end_transaction("LTY_GetLoyaltyProfile", NS_AUTO_STATUS);
	return status;

}


}
