// Converted from Acc_Services_End_To_End_C/AddBillingAdress.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class AddBillingAdress implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("AddBillingAdress");
     nsApi.ns_web_url ("AddBillingAdress",
        "URL=https://{AccServiceHostUrl}/v1/profile/billAddress",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}",
        "HEADER=correlation-id: AddBillingAdress_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=Addbillingadress.json",
);
    nsApi.ns_end_transaction("AddBillingAdress", NS_AUTO_STATUS);
    
    nsApi.ns_start_transaction("ProfileNotify");
    nsApi.ns_web_url ("ProfileNotify",
        "URL=https://{AccServiceHostUrl}/v1/profile/notify",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}.",
        "HEADER=correlation-id: AddBillingAdress_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=ProfileNotify.json",
);
    nsApi.ns_end_transaction("ProfileNotify", NS_AUTO_STATUS);
    	return status;

}


}
