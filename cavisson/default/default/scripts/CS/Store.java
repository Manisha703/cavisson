package CS;
import pacJnvmApi.NSApi;

public class Store implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("Store");
		status = nsApi.ns_web_url ("Store",
				"URL=http://{SnBServiceHostUrl}/v1/stores?postalCode={Zipcode_FP}",
				"METHOD=GET",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: Store_{CorrChar}{CorrID}"				
				);
		status = nsApi.ns_end_transaction("Store", NS_AUTO_STATUS);
		return status;
	}
}
