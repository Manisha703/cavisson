// Converted from WCS_WithNewURL/SaleEvent.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name: SaleEvent
Recorded By: Jitendra
Date of recording: 03/08/2014 04:59:11
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class SaleEvent implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;


		nsApi.ns_start_transaction("SaleEventPage"); 
		nsApi.ns_web_url ("SaleEventPage",
				"URL=http://{WcsHostURL}/sale-event/{UrlFP}",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive", 
				"COOKIE=origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;",
				INLINE_URLS,
				"URL=http://{WcsHostURL}/cs/media/css/ie.css", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/media/javascript/jquery.landing-page-3.0.4.min.js", "COOKIE=origin-perf01;TS96232b;JSESSIONID;TS2b67e6;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/common/session.jsp", "COOKIE=klsbrwcki:378051983;origin-perf01;TS96232b;JSESSIONID;TS2b67e6;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/", "COOKIE=origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/SaleBanner-MRKTSpot1-20140220-Long?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google.com/uds/afs?q=For%20Home&client=kohls-browse&channel=null&hl=en&r=s&qry_lnk=null&qry_ctxt=null&adpage=1&oe=UTF-8&ie=UTF-8&fexp=21404&format=n6&ad=n0&nocache=551393882721706&num=0&output=uds_ads_only&v=3&rurl=httpsss%3A%2F%2Fwww.kohls.com%2Fsale-event%2Ffor-the-home.jsp%3FCN%3D3000028128#slave-1-1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S1?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S2?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S3?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S4?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S1-Thumb?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S2-Thumb?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S3-Thumb?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/AllDepts-Feature-20130528-TopGradient?wid=760&amp;hei=54&amp;fmt=png-alpha", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-FeatureSpot-20140303-S4-Thumb?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/AllDepts-Feature-20130528-BottomGradient?wid=760&amp;hei=54&amp;fmt=png-alpha", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C-Headline?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C1?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C2?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C3?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C4?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C5?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C6?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C7?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C8?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C9?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity1-20140202-C10?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity2-20140202-S1?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity2-20140202-S2?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity2-20140202-S3?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity2-20140202-E1?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media1.kohls.com.edgesuite.net/is/image/kohls/FTH-MarketingEquity2-20140202-E2?scl=1&qlt=100,1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/cs/kohls/kohls_bl/images/facebook.gif", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/cs/kohls/kohls_bl/images/twitter.gif", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://badges.instagram.com/static/images/ig-badge-24.png", "REDIRECT=YES",  "LOCATION=/bluebar/506c801/thirdparty/images/badges/ig-badge-24.png", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/cs/kohls/kohls_bl/images/youtube.gif", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.facebook.com/plugins/likebox.php?href=httpsss%3A%2F%2Fwww.facebook.com%2Fkohls&width=315&colorscheme=light&connections=5&border_color=%23A5A9AC&stream=false&header=false&height=185", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://badges.instagram.com/static/images/ig-badge-sprite-24.png", "REDIRECT=YES",  "LOCATION=/bluebar/506c801/thirdparty/images/badges/ig-badge-sprite-24.png", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://s.btstatic.com/tag.js", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://cdn.brcdn.com/v1/br-trk-5117.js", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/wcs-internal/OmnitureAkamai.jsp", "COOKIE=origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://profile.ak.fbcdn.net/hprofile-ak-ash2/t5/1115733_100001773028694_1850698297_q.jpg", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://profile.ak.fbcdn.net/hprofile-ak-ash1/t5/186695_100004521638195_1514639122_q.jpg", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://s.thebrighttag.com/tag?site=4DPyaxM&H=-zji3ct", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://ww9.kohls.com/b/ss/kohlsatgsit/1/H.25/s94805955282430?AQB=1&ndh=1&t=8%2F2%2F2014%2016%3A57%3A35%206%20-330&ns=kohls&pageName=null&g=httpsss%3A%2F%2F{WcsHostURL}%2Fsale-event%2Fbed-and-bath.jsp&cc=USD&pageType=sale%20event%20landing&events=event1%2Cevent38&products=%3Bproductmerch1&c1=no%20taxonomy&c2=no%20taxonomy&c3=no%20taxonomy&v3=browse&c4=sale%20event%20landing&c5=non-search&c7=no%20taxonomy&v8=non-search&v9=homepage&c16=browse&c17=not%20logged%20in&v17=not%20logged%20in&c18=D%3Dv18&v18=03%3A44%20pm&c19=D%3Dv19&v19=wednesday&c20=D%3Dv20&v20=week%20day&c22=2014-03-05&v22=Kohl's&v23=browse&v24=browse&v25=no%20taxonomy&v26=no%20taxonomy&v27=no%20taxonomy&v28=no%20taxonomy&c39=browse&v39=klsbrwcki%3A%3B%3B&c40=browse&v40=atg&c41=browse&c42=browse&v42=original&c50=w6frTXhhtlnZL8X7ghTf2F2rKB20VpnHQxc0JkXMjXZ3yGNNy3db!-831298662!-1448936212&s=1366x768&c=24&j=1.6&v=Y&k=Y&bw=1366&bh=651&ct=lan&hp=N&AQE=1", "COOKIE=s_vi;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google-analytics.com/__utm.gif?utmwv=5.4.8&utms=1&utmn=1354406425&utmhn={WcsHostURL}&utmcs=utf-8&utmsr=1366x768&utmvp=1349x651&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=12.0%20r0&utmdt=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&utmhid=1720052878&utmr=-&utmp=%2Fsale-event%2Fbed-and-bath.jsp&utmht=1394278055689&utmac=UA-36389270-5&utmcc=__utma%3D201584719.1899030200.1394278056.1394278056.1394278056.1%3B%2B__utmz%3D201584719.1394278056.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmu=D~", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google-analytics.com/__utm.gif?utmwv=5.4.8&utms=2&utmn=773329446&utmhn={WcsHostURL}&utmcs=utf-8&utmsr=1366x768&utmvp=1349x651&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=12.0%20r0&utmdt=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&utmhid=1720052878&utmr=-&utmp=%2Fsale-event%2Fbed-and-bath.jsp&utmht=1394278055714&utmac=UA-36389270-6&utmcc=__utma%3D201584719.1899030200.1394278056.1394278056.1394278056.1%3B%2B__utmz%3D201584719.1394278056.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmmt=1&utmu=D~", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE
					);
		nsApi.ns_end_transaction("SaleEventPage", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);  

		  return status;


	}

}
