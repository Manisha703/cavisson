package CS;
import pacJnvmApi.NSApi;

public class CatalogByActiveDimension_P_Guest implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;

		status = nsApi.ns_start_transaction("CatalogPersonalization_Browse_Guest");
		status = nsApi.ns_web_url("CatalogPersonalization_Browse_Guest",
				"URL=http://{SnBServiceHostUrl}/v1/catalog/{catalogID_FP}?limit=60&offset=1&pid={ProfileId_New_FP}&channel={channel}",
				"HEADER=Accept: application/json, text/html",
				"HEADER=Accept-Language: en-US,en;q=0.8 ",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=Upgrade-Insecure-Requests: 1",
				"HEADER=correlation-id: Catalog_Personalization_{CorrChar}{CorrID}"

				);

		if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
		{

			status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Guest", 0, "CatalogPersonalization_Browse_GuestPdataFalse");
		}

		else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("true"))
		{
			status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Guest", 0, "CatalogPersonalization_Browse_GuestPdataTrue");
		}
		else
		{
			status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Guest", 0, "CatalogPersonalization_Browse_GuestNoPData");

		}
		status = nsApi.ns_page_think_time(0);  


		status = nsApi.ns_start_transaction("CatalogPersonalization_Browse_Mobile_Guest");
		status = nsApi.ns_web_url("CatalogPersonalization_Browse_Mobile_Guest",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?dimensionValueID={CatalogDimID_FP}&isDefaultStore=true&storeNum={StoreId_FP}&shipToStore=true&pid={ProfileId_New_FP}&includeStoreOnlyProducts=true&isVGC=false&offset=1&limit=72&channel={channel}",
				"HEADER=Accept: application/json, text/html",
				"HEADER=Accept-Language: en-US,en;q=0.8 ",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",				
				"HEADER=Upgrade-Insecure-Requests: 1",
				"HEADER=correlation-id: Catalog_Personalization_{CorrChar}{CorrID}"

				);

		if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
		{

			status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Mobile_Guest", 0, "CatalogPersonalization_BrowsePdataFalse");
		}

		else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("true"))
		{
			status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Mobile_Guest", 0, "CatalogPersonalization_BrowsePdataTrue");

		}
		else
		{
			status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Mobile_Guest", 0, "CatalogPersonalization_BrowseNoPData");

		}
		status = nsApi.ns_page_think_time(0);  
		return status;
	}
}

