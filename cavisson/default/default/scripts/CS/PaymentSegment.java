// Converted from Acc_Services_End_To_End_C/PaymentSegment.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class PaymentSegment implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("PaymentSegment");
	nsApi.ns_web_url ("PaymentSegment",
         "URL=https://{AccServiceHostUrl}/v1/profile/segments/payments",
         "METHOD=POST",
         "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr7F5",
         "HEADER=Content-Type: application/json",
         "HEADER=correlation-id: PaymentSegment_{CorrChar}{CorrID}",
         "HEADER=X-SESSIONID: {LPFSessionID1}",
         "HEADER=X-PROFILEID: {LPFProfileID1}",
         "BODY=$CAVINCLUDE$=PaymentSegment.json",
);
	nsApi.ns_end_transaction("PaymentSegment", NS_AUTO_STATUS);
	nsApi.ns_page_think_time(0);
	return status;

}

}
