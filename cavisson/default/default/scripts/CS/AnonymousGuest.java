// Converted from Acc_Services_End_To_End_C/AnonymousGuest.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class AnonymousGuest implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	int i;
	for(i = 1; i <= 3; i++)
  {
	nsApi.ns_start_transaction("AnonymousGuest");
    nsApi.ns_web_url ("AnonymousGuest",
        "URL=https://{AccServiceHostUrl}/v1/profile/session?info=true",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=correlation-id: AnonymousGuest_{CorrChar}{CorrID}",

);
    nsApi.ns_end_transaction("AnonymousGuest", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(2);
  }
	return status;

}

}
