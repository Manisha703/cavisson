// Converted from WCS_WithNewURL/GiftRegistry.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name:GiftRegistryWcs
Recorded By: Megha
Date of recording: 06/26/2014 03:31:48
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class GiftRegistryWcs implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
          if (debug) return status; 
            
		nsApi.ns_start_transaction("GiftRegistry");
		nsApi.ns_web_url ("GiftRegistryPage",
				"URL=http://{WcsHostURL}/gift-registry/{GRUrlFP}", 
				"HEADER=User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
				"HEADER=Accept-Language:en-us,en;q=0.5",
				"HEADER=Accept-Encoding: gzip, deflate",
				"COOKIE=JSESSIONID;klsbrwcki:2254010153781626;VisitorId;kohls_klbc_cookie;kohls_klbd_cookie;mbox;s_cc;gpv_v9;s_sq;_br_mzs;_br_mzv;_br_uid_1;_br_uid_2;cookieSetting;fsr.s;",
				);
		nsApi.ns_end_transaction("GiftRegistry", NS_AUTO_STATUS); 
		nsApi.ns_page_think_time(0);
          return status;		  
	} 

}
