// Converted from CNC_Services_R72_CSM3_YLW_C/RegDelete_Cart.c on Wed May  5 13:30:20 2021
/*-----------------------------------------------------------------------------
Name:RegDelete_Cart
Generated By: Nidhi
Date of generation: 07/12/2017 10:47:42
Flow details:
Build details: 4.1.7 (build# 63)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;




public class RegDelete_Cart implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int cartitemcount = nsApi.ns_get_int_val("SP_Bagitemss_all_count");
		if( cartitemcount >= 2 )
		{
			nsApi.ns_start_transaction("Reg_Delete_Cart");
			nsApi.ns_web_url("Reg_Delete_Cart",
					"URL=https://{CnCServiceHostUrl}/v1/carts/item/{CartItemIdSP}",
					"METHOD=DELETE",
					"HEADER=Content-Type: application/json",
					"HEADER=Accept: application/json",
					"HEADER=Accept-Language: en-US,en;q=0.8",
					"HEADER=X-PROFILEID: {XProfileSP}.",
					"HEADER=profileid: {Orig_ProfileID_FP}",
					"HEADER=sessionStatus: {SessionStatusDP}",
					"HEADER=JWT_token: adSADSAD",
					"HEADER=X-SESSIONID: {XSessionSP}",
					"HEADER=correlation-id: RegDelCart{CorrChar}{CorrID}",
					);
			nsApi.ns_end_transaction("Reg_Delete_Cart", NS_AUTO_STATUS);
		}
		return 0;
	}

}
