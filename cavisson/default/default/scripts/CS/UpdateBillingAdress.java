// Converted from Acc_Services_End_To_End_C/UpdateBillingAdress.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class UpdateBillingAdress implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("UpdateBillingAdress");
     nsApi.ns_web_url ("UpdateBillingAdress",
        "URL=https://{AccServiceHostUrl}/v1/profile/billAddress",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {SessionID}",
         "HEADER=X-PROFILEID: {ProfileID}",
        "HEADER=correlation-id: UpdateBillingAdress_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=UpdateBillingAdress.json",
);
    nsApi.ns_end_transaction("UpdateBillingAdress", NS_AUTO_STATUS);
	return status;

}


}
