package CS;
import pacJnvmApi.NSApi;

public class CatalogByKeywordSuggestions implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("CatalogByKeywordSuggestions");
		status = nsApi.ns_web_url("CatalogByKeywordSuggestions",
				"URL=http://{SnBServiceHostUrl}/v1/catalog/suggestions/products/{Keyword_FP}?storeNum={StoreId_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"	
				);
		status = nsApi.ns_end_transaction("CatalogByKeywordSuggestions", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
