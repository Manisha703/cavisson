// Converted from Acc_Services_End_To_End_C/UpdateProfile.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class UpdateProfile implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("UpdateProfile");
     nsApi.ns_web_url ("UpdateProfile",
        "URL=https://{AccServiceHostUrl}/v1/profile/info",
          "METHOD=POST",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
        "HEADER=X-PROFILEID: {LPFProfileID}",
        "HEADER=correlation-id: UpdateProfile_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=UpdateProfile.json",
);
    nsApi.ns_end_transaction("UpdateProfile", NS_AUTO_STATUS);
    return status;

}


}
