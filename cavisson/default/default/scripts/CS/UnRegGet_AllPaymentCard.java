// Converted from CNC_Services_R72_CSM3_YLW_C/UnRegGet_AllPaymentCard.c on Wed May  5 13:30:19 2021
/*-----------------------------------------------------------------------------
Name: UnRegGet_AllPaymentCard
Generated By: Nidhi
Date of generation: 07/12/2017 10:47:42
Flow details:
Build details: 4.1.7 (build# 63)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class UnRegGet_AllPaymentCard implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		nsApi.ns_start_transaction("UnReg_Get_AllPaymentCard");
		nsApi.ns_web_url("UnReg_Get_AllPaymentCard",
				"URL=https://{CnCServiceHostUrl}/v1/payments",
				"METHOD=GET",
				"HEADER=Content-Type: application/json",
				"HEADER=Accept: application/json",
				"HEADER=Accept-Language: en-US,en;q=0.8",
				"HEADER=X-PROFILEID: {ProfileIdFP}.",
				"HEADER=profileid: {Orig_ProfileID_FP}",
				"HEADER=sessionStatus: {SessionStatusDP}",
				"HEADER=JWT_token: adSADSAD",
				"HEADER=X-SESSIONID: {UnRegSESSIONFP}",
				"HEADER=correlation-id: UnRegGet_AllPaymentCard{CorrChar}{CorrID}",
				);
		nsApi.ns_end_transaction("UnReg_Get_AllPaymentCard", NS_AUTO_STATUS);
		return 0;
	}

}
