// Converted from P2HR_With_Promo_Mixes_CM_0921/BrowsetoCatalogPage.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: BrowsetoCatalogPage
Recorded By: Manish
Date of recording: 07/10/2018 04:15:29
Flow details:
Build details: 4.1.10 (build# 34)
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class BrowsetoCatalogPage implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		// ------------------------------------------------------  User Click on Hamberg menu then select category then sub category  ------------------------------------------------------
		nsApi.ns_start_transaction("NewNavigation");
		nsApi.ns_web_url ("NewNavigation",
				"URL=https://{UIHostUrl}/feature/ESI/Common/NewNavigation/",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;store_location;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;productnum;check;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;AAMC_kohls_0;fltk;aam_uuid;SignalUniversalID;s_stv;mbox;LoginSuccess;s_cc;VisitorBagTotals;VisitorBagSession;isPreQualEligible;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_fid;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("NewNavigation", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(14.413);


		//-------------------------------SaleEvent page ----------------------------------

		nsApi.ns_start_transaction("SaleEvent1");
		nsApi.ns_web_url ("SaleEvent1",
				"URL=https://{UIHostUrl}/sale-event/{SP_SaleEventURL}",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Referer:https://{UIHostUrl}/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=store_location;_abck;SL_Cookie;AKA_A2;bm_sz;mosaic;X-SESSIONID;akacd_www-kohls-com-mosaic-p2;check;kohls_wcs_cookie;kohls_klbd_cookie;dcroute;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;namogooTest;AAMC_kohls_0;fltk;aam_uuid;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;SignalSpring2016;SignalSpring2018;cto_lwid;btpdb.4DPyaxM.dGZjLjYyMTAxMDM;btpdb.4DPyaxM.dGZjLjYyMTAxMTA;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;staleStorage;__gads;_ga;_gid;IR_gbd;IR_PI;btpdb.4DPyaxM.X2J0X2djbXNfaWQ;SignalUniversalID;productnum;ndcd;s_stv;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;CavNV;TS01ada7dd;DCRouteS;LoginSuccess;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;fsr.s;VisitorBagSession;VisitorBagTotals;isPreQualEligible;_gat;rr_rcs;IR_5349;mbox;_scid;correlation-id;X-PROFILEID;CRTOABE;akavpau_www;hl_p;gpv_v9;_sctr;s_sq;RT;CavNVC;CavSF"
				);
		nsApi.ns_end_transaction("SaleEvent1", NS_AUTO_STATUS);    
		//---------------------------------Catalog Page -------------------------------            

		//Check if catalog url not found 
		
		if(!nsApi.ns_eval_string("{SP_CatalogUrl}").equals(""))
		{
			nsApi.ns_start_transaction("CatalogPage");
			nsApi.ns_web_url ("CatalogPage",
					"URL=https://{UIHostUrl}/catalog/{SP_CatalogUrl}&kls_sbp={DV_KLS_SBP}",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
					"HEADER=Upgrade-Insecure-Requests:1",
					"HEADER=Referer:https://{UIHostUrl}/sale-event/mens-clothing.jsp?cc=mens-TN1.0-S-men",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"COOKIE=store_location;_abck;SL_Cookie;AKA_A2;bm_sz;mosaic;X-SESSIONID;akacd_www-kohls-com-mosaic-p2;check;kohls_wcs_cookie;kohls_klbd_cookie;dcroute;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;namogooTest;AAMC_kohls_0;fltk;aam_uuid;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;SignalSpring2016;SignalSpring2018;cto_lwid;btpdb.4DPyaxM.dGZjLjYyMTAxMDM;btpdb.4DPyaxM.dGZjLjYyMTAxMTA;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;staleStorage;__gads;_ga;_gid;IR_gbd;IR_PI;btpdb.4DPyaxM.X2J0X2djbXNfaWQ;ndcd;s_stv;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;LoginSuccess;VisitorBagSession;VisitorBagTotals;isPreQualEligible;_scid;CRTOABE;hl_p;_sctr;fsr.s;SignalUniversalID;CavNV;TS01ada7dd;DCRouteS;productnum;gpv_v9;fsr.a;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;mbox;_gat;rr_rcs;CavSF;IR_5349;akavpau_www;correlation-id;X-PROFILEID;RT;s_sq;kls_p;CavNVC"
					);
 
			if(nsApi.ns_get_int_val("SP_totalRecordsCount") > 1)
			{
				nsApi.ns_end_transaction("CatalogPage", NS_AUTO_STATUS);
			status = nsApi.ns_save_data_eval("/tmp/Hit_CatalogUrl.txt", NS_APPEND_FILE,"{SP_CatalogUrl}\n");
			}
			else
				nsApi.ns_end_transaction_as("CatalogPage", 0, "CatalogPage_ZeroProduct"); 

			nsApi.ns_advance_param("RN_Number");


			nsApi.ns_page_think_time(0);
		}

		nsApi.ns_start_transaction("Web_BrowseSessionJSP");
		nsApi.ns_web_url ("Web_SessionJSP7",
				"METHOD=POST",
				"URL=https://{UIHostUrl}/web/session.jsp?lpf=v2",
				"HEADER=Accept:application/json",
				"HEADER=Accept-Encoding:gzip, deflate, br",
				"HEADER=Accept-Language:en-US,en;q=0.5",
				"HEADER=Content-Type:application/json",
				BODY_BEGIN,
				"{"pageName":"browse","cnValues":"{BrowseJSP}"}",
				BODY_END,
				);
		nsApi.ns_end_transaction("Web_BrowseSessionJSP", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		return status;
	}

}
