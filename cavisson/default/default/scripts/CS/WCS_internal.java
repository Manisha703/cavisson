// Converted from WCS_WithNewURL/WCS_internal.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name: WCS_internal
Recorded By: Ashutosh
Date of recording: 21/06/2019 04:59:11
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class WCS_internal implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		nsApi.ns_start_transaction("Flex");
		nsApi.ns_web_url ("Flex",
				"URL=http://{WcsHostURL}/flex/{flex_values}",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive",
				);

		nsApi.ns_end_transaction("Flex", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		nsApi.ns_start_transaction("Mcom_sale_event");
		nsApi.ns_web_url ("Mcom_sale_event",
				"URL=http://{WcsHostURL}/wcs-internal/mcom/sale-event/{mcom_sale_event_values}",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive",
				);

		nsApi.ns_end_transaction("Mcom_sale_event", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		return status;


	}

}
