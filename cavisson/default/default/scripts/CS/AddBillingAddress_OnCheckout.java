// Converted from P2HR_With_Promo_Mixes_CM_0921/AddBillingAddress_OnCheckout.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: AddBillingAddress_OnCheckout
Generated By: cavisson
Date of generation: 08/24/2018 10:38:09
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;




public class AddBillingAddress_OnCheckout implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;

		
		if(!nsApi.ns_eval_string("{SP_eligibleForExpeditedCheckout}").equals("true"))
		{   

			if(nsApi.ns_eval_string("{SP_billAddId}").equals(""))
			{
				nsApi.ns_start_transaction("AddBillingAddress_OnCheckout"); 

				nsApi.ns_web_url("AddBillingAddress_OnCheckout",
						"URL=https://{UIHostUrl}/cnc/checkout/AddAddress",
						"METHOD=POST",
						"HEADER=Host:{UIHostUrl}",
						"HEADER=Origin:https://{UIHostUrl}",
						"HEADER=Accept:application/json, text/javascript, */*; q=0.01",
						"HEADER=X-Requested-With:XMLHttpRequest",
						"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
						"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
						"HEADER=Accept-Encoding:gzip, deflate",
						"HEADER=Accept-Language:en-us",
						"HEADER=Content-Type:application/json",
						"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;K_favstore;DYN_USER_ID;VisitorId;VisitorUsaFullName;correlation-id;X-PROFILEID",
						BODY_BEGIN,
						"{"isApoFpo":false,"firstName":"Perf","lastName":"Cavisson","addr1":"1 Brewers Way","addr2":"","city":"Milwaukee","state":"WI","postalCode":"53214-3652","phoneNumber":"0999307787","preferredAddress":"false","addressType":"billing","nuData":{"ndpdData":"{\"jvqtrgQngn\":{\"oq\":\"879:662:1366:728:1366:728\",\"wfi\":\"flap-98538\",\"oc\":\"q400qo6n8n86q525\",\"fe\":\"1366k768 24\",\"qvqgm\":\"-330\",\"jxe\":23282,\"syi\":\"snyfr\",\"si\":\"si,btt,zc4,jroz\",\"sn\":\"sn,zcrt,btt,jni\",\"us\":\"por52r04n761257o\",\"cy\":\"Jva32\",\"sg\":\"{\\\"zgc\\\":0,\\\"gf\\\":snyfr,\\\"gr\\\":snyfr}\",\"sp\":\"{\\\"gp\\\":gehr,\\\"ap\\\":gehr}\",\"sf\":\"gehr\",\"jt\":\"78r9qs3735260548\",\"sz\":\"o65521rrr8ps0sos\",\"vce\":\"apvc,0,5o5n166o,2,1;fg,0,dgl_beqre_erivrj_4362514,1;zz,1n20,206,295,;zzf,3r7,0,n,ABC;zzf,3r9,3r9,n,ABC;zzf,3rn,3rn,n,ABC;zzf,3r6,3r6,n,ABC;zzf,3r8,3r8,n,ABC;zzf,3r9,3r9,n,ABC;zzf,3r7,3r7,n,ABC;zzf,3rn,3rn,n,ABC;zzf,3r7,3r7,n,ABC;gf,0,3q49;zzf,3r7,3r7,n,ABC;zzf,2713,2713,32,ABC;zz,74o,203,265,;zzf,1sp5,2710,32,92 82,51s 82,1r,130,-15rn,199q,-1;gf,0,8s53;zzf,270s,270s,32,ABC;zz,1665,200,26r,;gf,0,ppp7;zzf,10no,2710,32,3sp 41,3sp 41,14,pq,-13s6,13qq,-1;zz,3n5,1o1,257,;zzf,236p,2711,32,225 28o,62r 13s0,102,n0s,-6752,68r1,8;zzf,270s,270s,32,ABC;gf,0,12o92;zz,414q,208,q2,;gf,0,16pqs;zz,1q6s,145,28o,;zzf,8onp,rn68,1r,o 7p8,oo o39,n2,260o,-59s,5n0,0;gf,0,215sn;zzf,rn61,rn61,1r,ABC;gf,0,3005o;zzf,rn5s,rn5s,1r,ABC;gf,0,3rnon;zz,35p8,121,23s,;zz,1o44,27o,1r9,;gf,0,43op6;zzf,9p9q,rqn9,1r,1o3 138,317 6r4,98,2391,-2on,27r,6;gf,0,4q863;zzf,rn5s,rn5r,1r,ABC;gf,0,5p2p2;zz,18r09,19q,245,;gf,0,750po;zz,29po,17n,207,;zp,205,137,20n,;zp,1n6,142,240,;zz,r32,281,52,;gf,0,78p73;fg,5o5,4362514,1;zz,qn2,60,ro,;zp,11qo,25,114,;zz,417,2n,11r,snaplobk-pbagrag;zp,no8,1r0,24s,;zz,6r8,283,rr,;gf,0,7p75p;zz,1380,374,o0,;zz,136r,28r,r6,;zp,o8n,123,sn,;zz,2971,273,q6,ge_nqq_hf_yanzr;gf,0,82345;zp,285,173,r1,ge_nqq_hf_sanzr;zp,698,1p8,r4,ge_nqq_hf_yanzr;zz,893,1p4,104,;zp,1r7,158,12p,ge_nqq_hf_nqqerff1;zp,63p,11s,16n,ge_nqq_hf_nqqerff2;zp,30q,132,154,ge_nqq_hf_nqqerff2;zp,450,1n8,np,;zp,2s5,106,19o,ge_nqq_hf_pvgl;zz,3s,105,19o,ge_nqq_hf_pvgl;zp,n75,q4,19n,ge_nqq_hf_pvgl;zz,106p,q1,19n,ge_nqq_hf_pvgl;gf,0,8628n;zp,290,1oq,1n4,ge_nqq_hf_fgngr;zp,678,0,0,ge_nqq_hf_fgngr;zp,3q5,1pr,1n0,ge_nqq_hf_fgngr;zp,3pn,0,0,ge_nqq_hf_fgngr;zz,14,1p3,9s,;zz,128r,2o4,1n3,ge_nqq_hf_mvcpbqr;zp,77,2o4,1n2,ge_nqq_hf_mvcpbqr;zz,133s,2o4,1n7,ge_nqq_hf_mvcpbqr;zp,2pp,210,1r5,;zp,2s3,o6,1q7,ge_nqq_hf_cubar;gf,0,89s48;zp,o31,20o,205,;zz,q33,36p,20p,;\"},\"jg\":\"\"}"}}",
						BODY_END,
						INLINE_URLS,
						"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_address_statesList_Json.jsp", "HEADER=Host:{UIHostUrl}", "HEADER=Accept:application/json, text/javascript, */*; q=0.01", "HEADER=X-Requested-With:XMLHttpRequest", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore", END_INLINE,
						"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_info_details_json.jsp?getShipAdress=getShipAdress", "HEADER=Host:{UIHostUrl}", "HEADER=Accept:application/json, text/javascript, */*; q=0.01", "HEADER=X-Requested-With:XMLHttpRequest", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore", END_INLINE,
						"URL=https://{UIHostUrl}/snb/media/images/myaccount/arrow-link-16x16.png", "HEADER=Host:{UIHostUrl}", "HEADER=Accept:image/webp,image/*,*/*;q=0.8", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;K_favstore;DYN_USER_ID;VisitorId;VisitorUsaFullName;correlation-id;X-PROFILEID", END_INLINE
						);

				
				if(!nsApi.ns_eval_string("{SP_billAddId}").equals(""))
				{
					nsApi.ns_end_transaction("AddBillingAddress_OnCheckout", NS_AUTO_STATUS);    
				}else{
					nsApi.ns_end_transaction_as("AddBillingAddress_OnCheckout", 0, "AddBillingAddress_OnCheckout_Failed"); 
					 status = nsApi.ns_end_session();
				}
			}

		}
		nsApi.ns_page_think_time(0);
          return status;
	}

}
