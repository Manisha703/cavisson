// Converted from Acc_Services_End_To_End_C/Text_Notification.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: Text_Notification
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class Text_Notification implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
  	int status = 0;
	nsApi.ns_start_transaction("Text_Notification");
    nsApi.ns_web_url ("Text_Notification",
		"URL=https://{AccServiceHostUrl}/v1/profile/textnotification",
		"METHOD=POST",
		"HEADER=Accept: application/json",
		"HEADER=Content-Type: application/json",
		"HEADER=channel:web",
          "HEADER=correlation-id: Text_Notification_{CorrChar}{CorrID}",
          "HEADER=X-SESSIONID: {SessionID11}",
          "HEADER=X-PROFILEID: {ProfileID11}",
		"BODY=$CAVINCLUDE$=Text_Notification.json",
       
);
    nsApi.ns_end_transaction("Text_Notification", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(1);
    return status;

}

}
