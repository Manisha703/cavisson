package CS;
import pacJnvmApi.NSApi;

public class YourPrice implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("YourPrice");
		status = nsApi.ns_web_url("YourPrice",

				"URL=http://{SnBServiceHostUrl}/v2/product/{Product_WebID_FP}/price?storeNum={StoreId_FP}",
				"METHOD=POST", 
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_YourPrice_{CorrChar}{CorrID}",
				"BODY=$CAVINCLUDE$=YourPrice1.json"	
				);

		status = nsApi.ns_end_transaction("YourPrice", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		return status;
	}
}

