// Converted from Acc_Services_End_To_End_C/Select_Perk.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name:Select_Perk
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class Select_Perk implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{

	int status = 0;
	nsApi.ns_start_transaction("Select_Perk");
     nsApi.ns_web_url ("Select_Perk",
         "URL=https://{AccServiceHostUrl}/v1/profile/perks",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=correlation-id: Select_Perk_{CorrChar}{CorrID}",
         "HEADER=X-SESSIONID:{LPFSessionID1}",
	    "HEADER=X-PROFILEID: {LPFProfileID1}.",
         "BODY=$CAVINCLUDE$=Select_Perk.json",
);
    nsApi.ns_end_transaction("Select_Perk", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(2);   
    return status;
 
}


}
