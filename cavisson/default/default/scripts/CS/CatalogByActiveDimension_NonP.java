package CS;
import pacJnvmApi.NSApi;

public class CatalogByActiveDimension_NonP implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		int randNo = nsApi.ns_get_random_number_int(1,100);

		if(randNo<=50)
		{    	
			status = nsApi.ns_start_transaction("CatalogByActiveDimension_NonP");
			status = nsApi.ns_web_url("CatalogByActiveDimension_NonP_Webstore",
					"URL=http://{SnBServiceHostUrl}/v1/catalog/{CatalogWebstore_FP}?limit=60&offset=1&isLTL=true&channel={channel}",
					"HEADER=Accept: application/json, text/html",
					"HEADER=Accept-Language: en-US,en;q=0.8 ",
					"HEADER=Content-Type: application/json",
					"HEADER=X-APP-API_KEY: {key}",
					"HEADER=Upgrade-Insecure-Requests: 1",
					"HEADER=correlation-id: Catalog_Personalization_{CorrChar}{CorrID}"

					);
			status = nsApi.ns_end_transaction("CatalogByActiveDimension_NonP_Webstore", NS_AUTO_STATUS); 
			status = nsApi.ns_page_think_time(0);
		}
		else
		{   

			status = nsApi.ns_web_url("CatalogByActiveDimension_NonP_Mobile",
					"URL=http://{SnBServiceHostUrl}/v1/catalog?dimensionValueID={CatalogDimID_FP}&sortID=1&storeNum={StoreId_FP}&isDefaultStore=true&shipToStore=true&includeStoreOnlyProducts=true&isVGC=false&offset=1&limit=72&channel={channel}",
					"HEADER=Accept: application/json, text/html",
					"HEADER=Accept-Language: en-US,en;q=0.8 ",
					"HEADER=Content-Type: application/json",
					"HEADER=Upgrade-Insecure-Requests: 1",
					"HEADER=X-APP-API_KEY: {key}",
					"HEADER=correlation-id: Catalog_Personalization_{CorrChar}{CorrID}"


					);
			status = nsApi.ns_end_transaction("CatalogByActiveDimension_NonP_Mobile", NS_AUTO_STATUS); 
			status = nsApi.ns_page_think_time(0);
		}
		return status;

	}

}
