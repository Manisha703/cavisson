package CS;
import pacJnvmApi.NSApi;

public class InventoryWebID implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("Inventory_WebID_NotAval");
		status = nsApi.ns_web_url("Inventory_WebID",
				"METHOD=POST",
				"URL=http://{SnBServiceHostUrl}/v1/inventory/webID",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_Inventory_WebID_{CorrChar}{CorrID}",
				"BODY=$CAVINCLUDE$=InventoryWebID.json"
				);

		status = nsApi.ns_end_transaction_as("Inventory_WebID_NotAval", NS_AUTO_STATUS,"Inventory_WebID");
		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
