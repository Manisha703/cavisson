// Converted from Acc_Services_End_To_End_C/LTY_updateloyality.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class LTY_updateloyality implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
       int status = 0;
        nsApi.ns_start_transaction("LTY_loyalityupdate_Svc");
        nsApi.ns_web_url ("LTY_loyalityupdate_Svc",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/update",
        "METHOD=POST",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "BODY=$CAVINCLUDE$=LTY_loyalityupdate_Svc.json",
);
    nsApi.ns_end_transaction("LTY_loyalityupdate_Svc", NS_AUTO_STATUS);
    return status;

}

}
