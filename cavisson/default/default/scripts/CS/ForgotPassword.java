// Converted from Acc_Services_End_To_End_C/ForgotPassword.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class ForgotPassword implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("ForgotPassword");
     nsApi.ns_web_url ("ForgotPassword",
        "URL=https://{AccServiceHostUrl}/v1/profile/forgotPassword",
          "METHOD=GET",
          "HEADER=Accept: application/json",
          "HEADER=Content-Type: application/json",
          "HEADER=email: {EMail}@bh.exacttarget.com",

);
    nsApi.ns_end_transaction("ForgotPassword", NS_AUTO_STATUS);
    return status;
}

}
