// Converted from Acc_Services_End_To_End_C/CustomerLookup.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class CustomerLookup implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_save_string(nsApi.ns_get_guid(), "EMailID");
     nsApi.ns_start_transaction("CustomerLookup");
     nsApi.ns_web_url ("CustomerLookup",
        "URL=https://{AccServiceHostUrl}/v1/customer",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
//          "HEADER=X-APP-API_KEY: {key}",
//        "HEADER=Accept-Language: en-us,en;q=0.5",
  //      "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
    //    "HEADER=Proxy-Connection: keep-alive",
        "BODY=$CAVINCLUDE$=Customerlookup.json",
);
    nsApi.ns_end_transaction("CustomerLookup", NS_AUTO_STATUS);
    return status;
}

}
