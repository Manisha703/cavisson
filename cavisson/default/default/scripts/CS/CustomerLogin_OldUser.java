// Converted from Acc_Services_End_To_End_C/CustomerLogin_OldUser.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;




public class CustomerLogin_OldUser implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{

	int status = 0;
    nsApi.ns_start_transaction("CreateAccessToken");
    nsApi.ns_web_url("CreateAccessToken",
       "URL=https://{AccServiceHostUrl}/token",
       "METHOD=POST",
       "HEADER=Accept: application/xml",
       "HEADER=Content-Type: application/x-www-form-urlencoded",
       "HEADER=correlation-id: CustomerLogin_OldUser_{CorrChar}{CorrID}",
       "BODY=grant_type=password&client_id={key1}&secret={skey}&userId={Emailid1}&password=KohlsPerf01",
    );
    nsApi.ns_end_transaction("CreateAccessToken", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(0);
    return status;
}
}
