// Converted from Acc_Services_End_To_End_C/LTY_Activity.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class LTY_Activity implements NsFlow
{
    private boolean debug = true;
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	if (debug) return status;
	//return;
        nsApi.ns_start_transaction("LTY_Activity");
        nsApi.ns_web_url ("LTY_Activity",
        "URL=https://{AccServiceHostUrl}/v2/loyalty/{LPFLTY}/activity",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID1}",
        "HEADER=X-PROFILEID: {LPFProfileID1}",
         "HEADER=correlation-id: LTY_Activity_{CorrChar}{CorrID}",
);
    nsApi.ns_end_transaction("LTY_Activity", NS_AUTO_STATUS);
	return status;
}


}
