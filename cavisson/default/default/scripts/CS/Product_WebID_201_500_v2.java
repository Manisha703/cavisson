package CS;
import pacJnvmApi.NSApi;

public class Product_WebID_201_500_v2 implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("Product_WebID_201_500");
		status = nsApi.ns_web_url("Product_WebID_201_500_1",
				"URL=http://{SnBServiceHostUrl}/v2/product/{Product_WebID_201_500_1_FP}?skuDetail=true",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_ProductWEBID_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("Product_WebID_201_500", NS_AUTO_STATUS);


		status = nsApi.ns_page_think_time(0);
		return status;

	}
}
