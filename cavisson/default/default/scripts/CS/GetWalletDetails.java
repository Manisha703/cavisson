// Converted from Acc_Services_End_To_End_C/GetWalletDetails.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class GetWalletDetails implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	int i = nsApi.ns_get_random_number_int(2, 3);
	int j = 0;
	
	for(j=1;j<=i;j++)
    {
    nsApi.ns_start_transaction("GetWalletDetails_1");
    nsApi.ns_web_url ("GetWalletDetails_1",
        "URL=https://{AccServiceHostUrl}/v1/kohlswallet/info",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID}",
         "HEADER=X-PROFILEID: {LPFProfileID}",
        "HEADER=profileId: {LPFprofileid}",
        "HEADER=correlation-id: GetWalletDetails_{CorrChar}{CorrID}",
);
    nsApi.ns_end_transaction("GetWalletDetails_1", NS_AUTO_STATUS);
	}
	return status;
}	


}
