// Converted from WCS_WithNewURL/BrandLanding.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name: BrandLanding
Recorded By: Jitendra
Date of recording: 03/08/2014 04:59:11
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class BrandLanding implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		nsApi.ns_start_transaction("BrandLandingPage");
		nsApi.ns_web_url ("BrandLandingPage",
				"URL=http://{WcsHostURL}/{UrlFP1}",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive", 
				"COOKIE=origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;",
				INLINE_URLS,
				"URL=http://{WcsHostURL}/cs/media/css/ie.css", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/media/javascript/jquery.landing-page-3.0.4.min.js", "COOKIE=origin-perf01;TS96232b;JSESSIONID;TS2b67e6;s_cc;gpv_v9;s_sq;s_stv;productnum;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/common/session.jsp", "COOKIE=klsbrwcki:378051983;origin-perf01;TS96232b;JSESSIONID;TS2b67e6;s_cc;gpv_v9;s_sq;s_stv;productnum;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/", "COOKIE=origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-lc-logo?scl=1&fmt=png-alpha", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-feature?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-new-arrivals?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-key-items?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-video?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/media/images/StaticContent/flash/LandingPages/rr/mar13/images/slider/loading?scl=1&fmt=gif-alpha", "COOKIE=origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/media/images/lnav-arrow.gif", "COOKIE=origin-perf01;TS96232b;JSESSIONID;TS2b67e6;s_cc;gpv_v9;s_sq;s_stv;productnum;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/media/images/rnav-arrow.gif", "COOKIE=origin-perf01;TS96232b;JSESSIONID;TS2b67e6;s_cc;gpv_v9;s_sq;s_stv;productnum;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-arrow-down?scl=1&fmt=png-alpha", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-trans-black-50-bg?scl=1&fmt=png-alpha", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20130807-trans-gold-90-bg?scl=1&fmt=png-alpha", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://kohls.edgeboss.net/download/kohls/2011/landingpages/ma/swf/video_slide.swf", "METHOD=HEAD", "HEADER=Content-Length: 0", "HEADER=Proxy-Connection: Keep-Alive", "HEADER=Pragma: no-cache", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-trend-1?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-trend-2?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-equity-1?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-trend-3?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-equity-2?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-pinterest?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-equity-3?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20140219-about?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/cs/kohls/kohls_bl/images/facebook.gif", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/cs/kohls/kohls_bl/images/twitter.gif", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://badges.instagram.com/static/images/ig-badge-24.png", "REDIRECT=YES",  "LOCATION=/bluebar/506c801/thirdparty/images/badges/ig-badge-24.png", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/cs/kohls/kohls_bl/images/youtube.gif", "COOKIE=jsessionid_wcs;TSb3471b;origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.facebook.com/plugins/likebox.php?href=httpsss%3A%2F%2Fwww.facebook.com%2Fkohls&width=315&colorscheme=light&connections=5&border_color=%23A5A9AC&stream=false&header=false&height=185", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://s.thebrighttag.com/tag?site=MeUoaP5&H=-3oa03u8", "COOKIE=btpdb.4DPyaxM.YnJpZ2h0dGFnIHVzZXIgaWQ;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://badges.instagram.com/static/images/ig-badge-sprite-24.png", "REDIRECT=YES",  "LOCATION=/bluebar/506c801/thirdparty/images/badges/ig-badge-sprite-24.png", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://media.kohls.com.edgesuite.net/is/image/kohls/LaurenConrad-FeaturedSpace-20130611-lcbio?scl=1", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/wcs-internal/OmnitureAkamai.jsp", "COOKIE=origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://kohls.edgeboss.net/download/kohls/2011/landingpages/ma/swf/video_slide.swf", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://profile.ak.fbcdn.net/hprofile-ak-ash2/t1/p50x50/1460286_685991991431223_932676351_t.jpg", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://ww9.kohls.com/b/ss/kohlsatgsit/1/H.25/s91168259194925?AQB=1&ndh=1&t=8%2F2%2F2014%2016%3A58%3A50%206%20-330&ns=kohls&pageName=null&g=httpsss%3A%2F%2F{WcsHostURL}%2Ffeature%2FDisney.jsp&cc=USD&pageType=our%20brands&events=event38&products=%3Bproductmerch2&v3=browse&c4=our%20brands&c5=non-search&v8=non-search&c9=null&v9=null&c10=null&c11=null&c16=browse&c17=not%20logged%20in&v17=not%20logged%20in&c18=D%3Dv18&v18=03%3A44%20pm&c19=D%3Dv19&v19=wednesday&c20=D%3Dv20&v20=week%20day&c22=2014-03-05&v22=Kohl's&v23=browse&v24=browse&c39=browse&v39=klsbrwcki%3A%3B%3B&c40=browse&v40=atg&c41=browse&c42=browse&v42=original&c50=w6frTXhhtlnZL8X7ghTf2F2rKB20VpnHQxc0JkXMjXZ3yGNNy3db!-831298662!-1448936212&s=1366x768&c=24&j=1.6&v=Y&k=Y&bw=1366&bh=651&ct=lan&hp=N&AQE=1", "COOKIE=s_vi;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google-analytics.com/__utm.gif?utmwv=5.4.8&utms=5&utmn=1104815445&utmhn={WcsHostURL}&utmcs=utf-8&utmsr=1366x768&utmvp=1349x651&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=12.0%20r0&utmdt=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&utmhid=1478408632&utmr=-&utmp=%2Ffeature%2FDisney.jsp&utmht=1394278131039&utmac=UA-36389270-5&utmcc=__utma%3D201584719.1899030200.1394278056.1394278056.1394278056.1%3B%2B__utmz%3D201584719.1394278056.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmu=D~", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google-analytics.com/__utm.gif?utmwv=5.4.8&utms=6&utmn=675352721&utmhn={WcsHostURL}&utmcs=utf-8&utmsr=1366x768&utmvp=1349x651&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=12.0%20r0&utmdt=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&utmhid=1478408632&utmr=-&utmp=%2Ffeature%2FDisney.jsp&utmht=1394278131067&utmac=UA-36389270-6&utmcc=__utma%3D201584719.1899030200.1394278056.1394278056.1394278056.1%3B%2B__utmz%3D201584719.1394278056.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmmt=1&utmu=D~", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/C/Kohls_QA/rondavu.js", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.facebook.com/plugins/likebox.php?href=httpsss%3A%2F%2Fwww.facebook.com%2Fkohls&width=315&colorscheme=light&connections=5&border_color=%23A5A9AC&stream=false&header=false&height=185", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google-analytics.com/__utm.gif?utmwv=5.4.8&utms=7&utmn=2127881679&utmhn={WcsHostURL}&utmt=event&utme=14(6400*0*0*1370*1670*0*1540*5600)(6406*0*0*1377*1674*0*1543*5644)&utmcs=utf-8&utmsr=1366x768&utmvp=1349x651&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=12.0%20r0&utmdt=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&utmhid=1478408632&utmr=-&utmp=%2Ffeature%2FDisney.jsp&utmht=1394278131680&utmac=UA-36389270-5&utmcc=__utma%3D201584719.1899030200.1394278056.1394278056.1394278056.1%3B%2B__utmz%3D201584719.1394278056.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmmt=1&utmu=D~", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google-analytics.com/__utm.gif?utmwv=5.4.8&utms=8&utmn=904817916&utmhn={WcsHostURL}&utmt=event&utme=14(6400*0*0*1370*1670*0*1540*5600)(6406*0*0*1377*1674*0*1543*5644)&utmcs=utf-8&utmsr=1366x768&utmvp=1349x651&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=12.0%20r0&utmdt=Kohl's%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&utmhid=1478408632&utmr=-&utmp=%2Ffeature%2FDisney.jsp&utmht=1394278131692&utmac=UA-36389270-6&utmcc=__utma%3D201584719.1899030200.1394278056.1394278056.1394278056.1%3B%2B__utmz%3D201584719.1394278056.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmmt=1&utmu=D~", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.kohls.com/media/images/StaticContent/flash/LandingPages/lc/fancybox/fancybox?scl=1&fmt=png-alpha", "COOKIE=s_vi;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/media/images/StaticContent/flash/LandingPages/lc/fancybox/fancybox-x?scl=1&fmt=png-alpha", "COOKIE=origin-perf01;TS96232b;cookieSetting;BTgroup;BTgroup20140214;__ar_v4;JSESSIONID;TS16f60a;viewtype;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.kohls.com/media/images/StaticContent/flash/LandingPages/lc/fancybox/fancybox-y?scl=1&fmt=png-alpha", "COOKIE=s_vi;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.kohls.com/media/images/StaticContent/flash/LandingPages/lc/fancybox/fancybox-x?scl=1&fmt=png-alpha", "COOKIE=s_vi;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.kohls.com/media/images/StaticContent/flash/LandingPages/lc/fancybox/blank?scl=1&fmt=gif-alpha", "COOKIE=s_vi;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/js/get_js?c=Kohls_QA", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.googleadservices.com/pagead/conversion/1234/?label=purchase&guid=ON&color=FFFFFF&format=1&language=en_US&script=0", "REDIRECT=YES",  "LOCATION=/pagead/viewthroughconversion/1234/?label=purchase&guid=ON&color=FFFFFF&format=1&language=en_US&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&convclickts=0&random=729432457", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://profile.ak.fbcdn.net/hprofile-ak-ash1/t5/275105_577074308_393571686_q.jpg", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1234/?label=purchase&guid=ON&color=FFFFFF&format=1&language=en_US&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&convclickts=0&random=729432457", "REDIRECT=YES",  "LOCATION=/ads/conversion/1234/?label=purchase&guid=ON&color=FFFFFF&format=1&language=en_US&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=729432457&cdct=2&convclickts=0&random=3911161259", "COOKIE=id;", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/REL/QA/C/Kohls_QA/rondavu_1.Release-409f032_1387243661.min.js", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://www.google.com/ads/conversion/1234/?label=purchase&guid=ON&color=FFFFFF&format=1&language=en_US&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=729432457&cdct=2&convclickts=0&random=3911161259", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/log.gif?customer_name=Kohls_QA&action=preload&url=httpsss%3A%2F%2F{WcsHostURL}%2Ffeature%2FDisney.jsp&timestamp=1394278138456", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/scripts/validator/sociable_validator.js", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://{WcsHostURL}/C/Kohls_QA/rondavu_1.Release-409f032_1387243661.min.css", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE,
				"URL=http://connect.facebook.net/en_US/all.js", "HEADER=Accept-Language: en-US", "HEADER=Proxy-Connection: Keep-Alive", END_INLINE
					);

		nsApi.ns_end_transaction("BrandLandingPage", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);
		return status;

	}

}
