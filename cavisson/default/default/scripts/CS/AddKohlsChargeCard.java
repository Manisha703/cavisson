// Converted from P2HR_With_Promo_Mixes_CM_0921/AddKohlsChargeCard.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: AddKohlsChargeCard
Generated By: cavisson
Date of generation: 08/24/2018 10:38:09
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;




public class AddKohlsChargeCard implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		nsApi.ns_start_transaction("Customer_info_details");
		nsApi.ns_web_url("Customer_info_details",
				"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_info_details_json.jsp?loadPaymentCard=loadPaymentCard&_=1532703380007",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:application/json, text/javascript, */*; q=0.01",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB",
				"COOKIE=_abck;bm_sz;AKA_A2;X-SESSIONID;check;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;__gads;hl_p;CRTOABE;staleStorage;s_cc;AAMC_kohls_0;fltk;aam_uuid;s_stv;VisitorId;VisitorUsaFullName;DYN_USER_ID;K_favstore;fsr.s;VisitorBagTotals;VisitorBagSession;isPreQualEligible;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;JSESSIONID;klsbrwcki|10018408848;store_location;productnum;mbox;CavSF;CavNVC;X-PROFILEID;correlation-id;gpv_v9;s_sq",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/account/media/8.0.0-1491/images/myaccount/white.png", "HEADER=Host:{UIHostUrl}", "HEADER=Accept:image/webp,image/apng,image/*,*/*;q=0.8", "HEADER=Referer:https://{UIHostUrl}/account/media/8.0.0-1491/css/addressvalidationcustom.css", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "COOKIE=_abck;bm_sz;AKA_A2;X-SESSIONID;check;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;__gads;hl_p;CRTOABE;staleStorage;s_cc;AAMC_kohls_0;fltk;aam_uuid;s_stv;VisitorId;VisitorUsaFullName;DYN_USER_ID;K_favstore;fsr.s;VisitorBagTotals;VisitorBagSession;isPreQualEligible;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;JSESSIONID;klsbrwcki|10018408848;store_location;productnum;mbox;CavSF;CavNVC;gpv_v9;s_sq;correlation-id;X-PROFILEID", END_INLINE,
				"URL=https://{UIHostUrl}/account/media/8.0.0-1491/images/myaccount/add.png", "HEADER=Host:{UIHostUrl}", "HEADER=Accept:image/webp,image/apng,image/*,*/*;q=0.8", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "COOKIE=_abck;bm_sz;AKA_A2;X-SESSIONID;check;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;__gads;hl_p;CRTOABE;staleStorage;s_cc;AAMC_kohls_0;fltk;aam_uuid;s_stv;VisitorId;VisitorUsaFullName;DYN_USER_ID;K_favstore;fsr.s;VisitorBagTotals;VisitorBagSession;isPreQualEligible;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;JSESSIONID;klsbrwcki|10018408848;store_location;productnum;mbox;CavSF;CavNVC;gpv_v9;s_sq;correlation-id;X-PROFILEID", END_INLINE,
				"URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?guid=ON&;script=0&data=10170497;aam=10263239", "HEADER=Host:googleads.g.doubleclick.net", "HEADER=Accept:image/webp,image/apng,image/*,*/*;q=0.8", "HEADER=Referer:https://kohls.demdex.net/dest5.html?d_nsid=0", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "COOKIE=IDE", END_INLINE,
				"URL=https://{UIHostUrl}/ads/user-lists/1018012790/?guid=ON&data=10170497;aam=10263239&cdct=2&is_vtc=1&random=1460742375", "HEADER=Host:www.google.com", "HEADER=Referer:https://kohls.demdex.net/dest5.html?d_nsid=0", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "HEADER=Accept:image/webp,image/apng,image/*,*/*;q=0.8", END_INLINE,
				"URL=https://{UIHostUrl}/test_rum?s=000619200242834735255&p=1&m=0&op=domwatcher&pi=4&pid=23&d=23|0|1&lts=144169020", "METHOD=POST", "HEADER=Host:{UIHostUrl}", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "HEADER=Content-Type:text/plain;charset=UTF-8",
				BODY_BEGIN,
				"$CAVINCLUDE_NOPARAM$=http_request_body/customer_address_statesList__main_url_1_1532703851591.body",
				BODY_END,	
				END_INLINE,
				"URL=https://{UIHostUrl}/test_rum?s=000619200242834735255&p=1&m=0&op=el&pi=4&pid=23&d=23&lts=144168935", "METHOD=POST", "HEADER=Host:{UIHostUrl}", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Cache-Control:max-age=0", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "HEADER=Content-Type:text/plain;charset=UTF-8", "COOKIE=ndcd",
				BODY_BEGIN,
				"000619200242834735255|23|4|144168961|userSegmentMask|40",
				BODY_END,
				END_INLINE,
				"URL=https://{UIHostUrl}/test_rum?s=000619200242834735255&p=1&m=0&op=useraction&pi=4&pid=23&d=0|0&lts=144169020", "METHOD=POST", "HEADER=Host:{UIHostUrl}", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Cache-Control:max-age=0", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "HEADER=Content-Type:text/plain;charset=UTF-8", "COOKIE=ndcd",
				BODY_BEGIN,
				"000619200242834735255|23|4|144168960720|0|1000||-2||||0|0|1263|583|||1000|276|",
				BODY_END,
				END_INLINE,
				"URL=https://{UIHostUrl}/test_rum?s=000619200242834735255&p=1&m=0&op=xhrdata&pi=4&d=23|0|||0|-1&lts=144169020", "METHOD=POST", "HEADER=Host:{UIHostUrl}", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Cache-Control:max-age=0", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us",
				"HEADER=correlation-id: WEB", "HEADER=Content-Type:text/plain;charset=UTF-8", "COOKIE=ndcd",
				BODY_BEGIN,
				"https%3A%2F%2F{UIHostUrl}%2Fcommon%2Fuser_check_status.jsp|144169005|200|GET|50|900|%7B%22statusText%22%3A%22OK%22%2C%22query%22%3A%22%22%2C%22url%22%3A%22https%3A%2F%2F{UIHostUrl}%2Fcommon%2Fuser_check_status.jsp%22%2C%22timings%22%3A%7B%22requestStart%22%3A144169005963%2C%22loadEventEnd%22%3A144169006863%7D%7D||0|-1|{-1}|
					https%3A%2F%2F{UIHostUrl}%2Fmyaccount%2Fjson%2Fmyinfo%2Fcustomer_address_statesList_Json.jsp|144169006|200|GET|643|1119|%7B%22statusText%22%3A%22OK%22%2C%22query%22%3A%22%22%2C%22url%22%3A%22https%3A%2F%2F{UIHostUrl}%2Fmyaccount%2Fjson%2Fmyinfo%2Fcustomer_address_statesList_Json.jsp%22%2C%22timings%22%3A%7B%22opened%22%3A144169006889%2C%22requestStart%22%3A144169006890%2C%22header_received%22%3A144169008009%2C%22loading%22%3A144169008009%2C%22done%22%3A144169008009%2C%22loadEventEnd%22%3A144169008009%7D%7D||0|-1|{-1}|
					https%3A%2F%2F{UIHostUrl}%2Fmyaccount%2Fjson%2Fmyinfo%2Fcustomer_info_details_json.jsp|144169006|200|GET|1094|1738|%7B%22statusText%22%3A%22OK%22%2C%22query%22%3A%22%3FloadPaymentCard%3DloadPaymentCard%26_%3D1532703380007%22%2C%22url%22%3A%22https%3A%2F%2F{UIHostUrl}%2Fmyaccount%2Fjson%2Fmyinfo%2Fcustomer_info_details_json.jsp%22%2C%22timings%22%3A%7B%22opened%22%3A144169006892%2C%22requestStart%22%3A144169006893%2C%22header_received%22%3A144169008630%2C%22loading%22%3A144169008630%2C%22done%22%3A144169008631%2C%22loadEventEnd%22%3A144169008631%7D%7D||0|-1|{-1}|",
				BODY_END,
				END_INLINE
					);

		nsApi.ns_end_transaction("Customer_info_details", NS_AUTO_STATUS); 
		return status;

	}

}
