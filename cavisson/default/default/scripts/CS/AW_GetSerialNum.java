// Converted from Acc_Services_End_To_End_C/AW_GetSerialNum.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class AW_GetSerialNum implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("AW_GetSerialNum_1");
     nsApi.ns_web_url ("AW_GetSerialNum_1",
          "URL=https://{AccServiceHostUrl}/v1/passthru/v1/devices/36ed43ac-1f9c-4374-b91c-c1a98b36a129/registrations/pass.com.kohls.y2y",
          "METHOD=GET",
          "HEADER=Accept: application/json",
          "HEADER=Content-Type: application/json",
          "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
          "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr799087896",

);
    nsApi.ns_end_transaction("AW_GetSerialNum_1", NS_AUTO_STATUS);
    return status;
}
   


}
