// Converted from P2HR_With_Promo_Mixes_CM_0921/ZineOne_PDP.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: ZineOne_PDP
Recorded By: cavisson
Date of recording: 10/04/2018 01:18:37
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/

package CS;
import pacJnvmApi.NSApi;


public class ZineOne_PDP implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
          if (debug) return status;

		
		if(nsApi.ns_eval_string("{DV_UserType}").equals("UnReg"))
		{
			nsApi.ns_save_string(nsApi.ns_eval_string("{deviceId}"), "DV_DynID");
		}

		nsApi.ns_start_transaction("Connect");
		nsApi.ns_web_url("Connect_PDP",
				"URL=https://{UIHostUrl}/c3/api/v1/connect/{DV_DynID}?deviceId={deviceId}&os=html5&devicetype=desktop&loadConfig",
				"METHOD=OPTIONS",
				"HEADER=Origin:https://www.kohls.com",
				"HEADER=Access-Control-Request-Method:GET",
				"HEADER=Access-Control-Request-Headers:apikey",
				"HEADER=Accept-Language:en-in",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/1000.gif?memo=CKyqFhIxCi0IARCYEhomNTgxOTY2MDEwMjEwOTEyMDU3ODQzNDY0MTk4MjA5NDQ5MDQyNTYQABoNCKGL190FEgUI6AcQAA", "HEADER=Accept-Language:en-in", "COOKIE=rlas3;pxrc", END_INLINE,
				"URL=https://{UIHostUrl}/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fdpm.demdex.net%252Fibs%253Adpid%253D358%2526dpuuid%253D%2524UID", "HEADER=Accept-Language:en-in", "COOKIE=uuid2", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=358&dpuuid=8682149201375198871", END_INLINE,
				"URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.everesttech.net%2F1x1%3F", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=477&dpuuid=f0b9634fc2baa8dfa41a9f92a22d2516a1bf87656bdf9d4109b9764bc37243a9b0da87c991749652", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=358&dpuuid=8682149201375198871", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=601&dpuuid=76540252144520&random=1538639265", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537072980%26val%3D__EFGSURFER__.__EFGCK__", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
				"URL=https://{UIHostUrl}/req?adID=58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", "COOKIE=__cfduid", END_INLINE,
				"URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fib.adnxs.com%2Fpxj%3Faction%3Dsetuid(%27__EFGSURFER__.__EFGCK__%27)%26bidder%3D51%26seg%3D2634060der%3D51%26seg%3D2634060", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm=&google_tc=", "HEADER=Accept-Language:en-in", "COOKIE=test_cookie", END_INLINE,
				"URL=https://{UIHostUrl}/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239;10170497&crd=CITQGw&cdct=2&is_vtc=1&random=783000770", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fexpires%3D30%26nid%3D2181%26put%3D__EFGSURFER__.__EFGCK__%26v%3D11782", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
				"URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%3D%26piggybackCookie%3D__EFGSURFER__.__EFGCK__", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=771&dpuuid=CAESELbYyrHae0PMFO9yl-KFkVU&google_cver=1", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=aam", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/c3/api/v1/connect/3000000016119254?deviceId=d17fae3e-04eb-49ac-88d0-8ed329e4e9f8&os=html5&devicetype=desktop&loadConfig", "HEADER=Origin:https://www.kohls.com", "HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357", "HEADER=Accept-Language:en-in", "HEADER=Content-Type:text/plain", END_INLINE,
				"URL=https://{UIHostUrl}/sync/?pid=27&fr=1", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/map/?key=a74thHgsfK627J6Ftt8sj5ks52bKe&url=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=[%FT_GUID%]", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=392992ECD9C62F", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://{UIHostUrl}/sync?c=8&r=1&a=1&u=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D28645%26dpuuid%3D%40USERID%40", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=28645&dpuuid=", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=VzdYRm53QUFDbnc0NF92Ug&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F&google_gid=CAESEHM3SJaJY8bV-nFjBwD3jVo&google_cver=1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
				"URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEHM3SJaJY8bV-nFjBwD3jVo&google_cver=1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
				"URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239;10170497&crd=CITQGw&cdct=2&is_vtc=1&random=783000770&ipr=y", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060&google_gid=CAESEHM3SJaJY8bV-nFjBwD3jVo&google_cver=1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
				"URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782&google_gid=CAESEHM3SJaJY8bV-nFjBwD3jVo&google_cver=1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
				"URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEHM3SJaJY8bV-nFjBwD3jVo&google_cver=1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
				"URL=https://pixel.everesttech.net/1x1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=392992ECD9C62F", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://match.adsrvr.org/track/cmb/generic?ttd_pid=aam", "HEADER=Accept-Language:en-in", "COOKIE=TDID;TDCPM", END_INLINE,
				"URL=https://pixel.everesttech.net/1x1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://ps.eyeota.net/match?bid=6j5b2cv&uid=58196601021091205784346419820944904256&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=/match/bounce/?bid=6j5b2cv&uid=58196601021091205784346419820944904256&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", END_INLINE,
				"URL=https://{UIHostUrl}/cms?partner_id=ADOBE&_hosted_id=58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://fei.pro-market.net/engine?site=141472;size=1x1;mimetype=img;csync=di;du=67", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://px.owneriq.net/eucm/p/adpq?redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D53196%26dpuuid%3D(OIQ_UUID)", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ5919256681774901266&uid=Q5919256681774901266&ref=%2Feucm%2Fp%2Fadpq", END_INLINE,
				"URL=https://{UIHostUrl}/p2?c1=9&c2=6034944&c3=2&cs_xi=58196601021091205784346419820944904256&rn=1538639262125&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D58196601021091205784346419820944904256", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=903&dpuuid=0bd0d6c0-2368-4ed8-85ba-86e6ce3e9da5", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://abp.mxptint.net/sn.ashx", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B342_ABF9FDD1_1910A088B&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=28645&dpuuid=", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://{UIHostUrl}/pixel.gif?ch=124&cm=58196601021091205784346419820944904256&redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D79908%26dpuuid%3D%7Bvisitor_id%7D", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=W7XFpc7YQ-eRTTcUZv_5Y7Lg", END_INLINE,
				"URL=https://{UIHostUrl}/d/sync/cookie/generic?https://dpm.demdex.net/ibs:dpid=2340&dpuuid=${ADELPHIC_CUID}", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=ca77f709-c7a9-11e8-8f73-4707f786a281", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=575&dpuuid=7040051198293921105", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://usermatch.krxd.net/um/v2?partner=adobe&id=58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=https://beacon.krxd.net/usermatch.gif?kuid_status=new&partner=adobe&id=58196601021091205784346419820944904256", END_INLINE
					);

		nsApi.ns_end_transaction("Connect", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		if (debug) return status;
		nsApi.ns_start_transaction("ConnectWebSocket");
		nsApi.ns_web_url("ConnectWebSocket1_PDP",
				"URL=https://{UIHostUrl}/c3/api/v1/connectwebsocket/{deviceId}",
				"HEADER=Origin:https://www.kohls.com",
				"HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357",
				"HEADER=Accept-Language:en-in",
				"HEADER=Content-Type:text/plain",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/p2?c1=9&c2=6034944&c3=2&cs_xi=58196601021091205784346419820944904256&rn=1538639262125&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", "COOKIE=UID;UIDR", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=73426&dpuuid=58196601021091205784346419820944904256", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/5w3jqr4k?redir=https%3A%2F%2Fcm.g.doubleclick.net%2Fpixel%3Fgoogle_nid%3Dg8f47s39e399f3fe%26google_push%26google_sc%26google_hm%3D%24%7BTM_USER_ID_BASE64ENC_URLENC%7D", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q5919256681774901266", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=73426&dpuuid=58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/btu4jd3a?redir=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fv%3D7941%26nid%3D2243%26put%3D%24%7BUSER_ID%7D%26expires%3D90", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://ps.eyeota.net/match/bounce/?bid=6j5b2cv&uid=58196601021091205784346419820944904256&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "HEADER=Accept-Language:en-in", "COOKIE=mako_uid;ONPLFTRH", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=2zOZoB1gcjNXzIKA-3szLliWPf7vUUuXvDtCDvSONGRE", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/ZMAwryCI?redir=https%3A%2F%2Fdsum-sec.casalemedia.com%2Frum%3Fcm_dsp_id%3D88%26external_user_id%3D%24%7BTM_USER_ID%7D", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=ca77f709-c7a9-11e8-8f73-4707f786a281", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B342_ABF9FDD1_1910A088B&redir=https://abp.mxptint.net/sn.ashx?ak=1", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=2zOZoB1gcjNXzIKA-3szLliWPf7vUUuXvDtCDvSONGRE", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://{UIHostUrl}/tap.php?v=7941&nid=2243&put=W7XFnwAACnw44_vR&expires=90", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=/tap.php?cookie_redirect=1&v=7941&nid=2243&put=W7XFnwAACnw44_vR&expires=90", END_INLINE,
				"URL=https://cm.g.doubleclick.net/pixel?google_nid=g8f47s39e399f3fe&google_push&google_sc&google_hm=VzdYRm53QUFDbnc0NF92Ug==", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://{UIHostUrl}/rum?cm_dsp_id=88&external_user_id=W7XFnwAACnw44_vR&C=1", END_INLINE,
				"URL=https://beacon.krxd.net/usermatch.gif?kuid_status=new&partner=adobe&id=58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=W7XFpc7YQ-eRTTcUZv_5Y7Lg", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/UH6TUt9n?redir=https%3A%2F%2Fib.adnxs.com%2Fsetuid%3Fentity%3D158%26code%3D%24%7BTM_USER_ID%7D", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/ny75r2x0?redir=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537148856%26val%3D%24%7BTM_USER_ID%7D", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/b9pj45k4?redir=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA%3D%26piggybackCookie%3D%24%7BUSER_ID%7D", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/h0r58thg?redir=https%3A%2F%2Fsync.search.spotxchange.com%2Fpartner%3Fadv_id%3D6409%26uid%3D%24%7BUSER_ID%7D%26img%3D1", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://sync-tm.everesttech.net/upi/pid/r7ifn0SL?redir=https%3A%2F%2Fwww.facebook.com%2Ffr%2Fb.php%3Fp%3D1531105787105294%26e%3D%24%7BTM_USER_ID%7D%26t%3D2592000%26o%3D0", "HEADER=Accept-Language:en-in", "COOKIE=everest_g_v2;everest_session_v2;ev_sync_ax", END_INLINE,
				"URL=https://{UIHostUrl}/tag?site=4DPyaxM&mode=v2&mode=v2&H=1lm7uu8&referrer=https%3A%2F%2Fwww.kohls.com%2F", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/bct?pid=8bc436aa-e0fc-4baa-9c9a-06fbeca87826&puid=58196601021091205784346419820944904256&_ct=img", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://securepubads.g.doubleclick.net/gpt/pubads_impl_257.js", "HEADER=Accept-Language:en-in", "COOKIE=IDE", END_INLINE,
				"URL=https://{UIHostUrl}/?partner=130&mapped=58196601021091205784346419820944904256&redirect=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D161033%26dpuuid%3D%25m", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/setuid?entity=158&code=W7XFnwAACnw44_vR", "HEADER=Accept-Language:en-in", "COOKIE=uuid2", END_INLINE,
				"URL=https://us-u.openx.net/w/1.0/sd?id=537148856&val=W7XFnwAACnw44_vR", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/AdServer/Pug?vcode=bz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA=&piggybackCookie=W7XFnwAACnw44_vR", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/partner?adv_id=6409&uid=W7XFnwAACnw44_vR&img=1", "HEADER=Accept-Language:en-in", "REDIRECT=YES", "LOCATION=/partner?adv_id=6409&uid=W7XFnwAACnw44_vR&img=1&__user_check__=1&sync_id=cbdd575c-c7a9-11e8-9472-1cdff4460b03", END_INLINE,
				"URL=https://{UIHostUrl}/fr/b.php?p=1531105787105294&e=W7XFnwAACnw44_vR&t=2592000&o=0", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://bcp.crwdcntrl.net/map/ct=y/c=9828/tp=ADBE/tpid=58196601021091205784346419820944904256?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D%24%7Bprofile_id%7D", "HEADER=Accept-Language:en-in", "COOKIE=_cc_cc", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=121998&dpuuid=d131f03163dc1895a2bc0fd53fb93ce", END_INLINE
					);
		nsApi.ns_end_transaction("ConnectWebSocket", NS_AUTO_STATUS);

          if (debug) return status;
		nsApi.ns_start_transaction("originId");
		nsApi.ns_web_url ("originId_PDP",
				"URL=https://{UIHostUrl}/c3/api/v1/originId?originId=389c9ecf-3395-4f3c-9832-0bac17277d6a&profileId={deviceId}",
				"METHOD=POST",
				"HEADER=Origin:https://www.kohls.com",
				"HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357",
				"HEADER=Accept-Language:en-in",
				"HEADER=Content-Type:text/plain",
				"PreSnapshot=webpage_1538639273691.png",
				"Snapshot=webpage_1538639274071.png",
				BODY_BEGIN,
				"{}",
				BODY_END,
				INLINE_URLS,
				"URL=https://{UIHostUrl}/lib/bab4054f319c98a48fb79fa3356393230b835099.js?v=2", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://dpm.demdex.net/ibs:dpid=161033&dpuuid=58196601021091205784346419820944904256", "HEADER=Accept-Language:en-in", "COOKIE=demdex;dpm;dextp", END_INLINE,
				"URL=https://{UIHostUrl}/tap.php?cookie_redirect=1&v=7941&nid=2243&put=W7XFnwAACnw44_vR&expires=90", "HEADER=Accept-Language:en-in", "COOKIE=audit;c", END_INLINE,
				"URL=https://us-u.openx.net/w/1.0/sd?cc=1&id=537148856&val=W7XFnwAACnw44_vR", "HEADER=Accept-Language:en-in", "COOKIE=i", END_INLINE,
				"URL=https://{UIHostUrl}/collect.js", "HEADER=Accept-Language:en-in", END_INLINE,
				"URL=https://{UIHostUrl}/c3/api/v1/event", "METHOD=OPTIONS", "HEADER=Origin:https://www.kohls.com", "HEADER=Access-Control-Request-Method:POST", "HEADER=Access-Control-Request-Headers:apikey", "HEADER=Accept-Language:en-in", END_INLINE
				);
		nsApi.ns_end_transaction("originId", NS_AUTO_STATUS);

		if (debug) return status;
		nsApi.ns_start_transaction("Event_appStart");
		nsApi.ns_web_url ("event_PDP",
				"URL=https://{UIHostUrl}/c3/api/v1/event",
				"METHOD=POST",
				"HEADER=Origin:https://www.kohls.com",
				"HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357",
				"HEADER=Accept-Language:en-in",
				"HEADER=Content-Type:text/plain",
				"PreSnapshot=webpage_1538639274517.png",
				"Snapshot=webpage_1538639274857.png",
				BODY_BEGIN,
				"[{"event":"appStart","z1_profile":{"device_id":"{deviceId}","_z1_origin":"5a4be6f6-3a86-406a-ef6b-0c87ced4fd74","firstTimeUser":"true","os":"html5","os_version":"8.1","platform_os":"Windows","language":"en","country":"US","tz":"IST","devicetype":"desktop","browser":"Chrome","browserversion":"Chrome:69.0.3497.100","resolution":"1366x768","_z1_pagename":"{SP_z1pagename}.jsp","_z1_pagepath":"{SP_z1pagepath}.jsp"},"profileId":"{DV_DynID}","z1SDKVersion":"3.1.27","wstoken":"kohls_com:-884024010","color":"Stonewash","prdPV":"{SP_prdPV}"}]",
				BODY_END,
				INLINE_URLS,
				"URL=https://{UIHostUrl}/rum?cm_dsp_id=88&external_user_id=W7XFnwAACnw44_vR&C=1", "HEADER=Accept-Language:en-in", "COOKIE=CMID;CMPS", END_INLINE,
				"URL=https://{UIHostUrl}/partner?adv_id=6409&uid=W7XFnwAACnw44_vR&img=1&__user_check__=1&sync_id=cbdd575c-c7a9-11e8-9472-1cdff4460b03", "HEADER=Accept-Language:en-in", "COOKIE=audience", END_INLINE
				);


		nsApi.ns_end_transaction("Event_appStart", NS_AUTO_STATUS);




		
		if(nsApi.ns_eval_string("{DV_UserType}").equals("UnReg"))
		{
			nsApi.ns_start_transaction("Event_viewedProduct");
			nsApi.ns_web_url ("event_2_PDP",
					"URL=https://{UIHostUrl}/c3/api/v1/event",
					"METHOD=POST",
					"HEADER=Origin:https://www.kohls.com",
					"HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357",
					"HEADER=Accept-Language:en-in",
					"HEADER=Content-Type:text/plain",
					"PreSnapshot=webpage_1538639274864.png",
					"Snapshot=webpage_1538639274876.png",
					BODY_BEGIN,
					"[{"page":"PDP","isKCC":0,"loyaltyId":"N/A","productId":"{SP_productID}","isOnSale":"true","salePrice":"36.99","originalPrice":"{SP_originalPrice}","PDPCategory":"{SP_categoryName}","isLoggedIn":"true","event":"_ws_viewedProduct","z1_profile":{"device_id":"{deviceId}","_z1_origin":"5a4be6f6-3a86-406a-ef6b-0c87ced4fd74","firstTimeUser":"true","os":"html5","os_version":"8.1","platform_os":"Windows","language":"en","country":"US","tz":"IST","devicetype":"desktop","browser":"Chrome","browserversion":"Chrome:69.0.3497.100","resolution":"1366x768","_z1_pagename":"{SP_z1pagename}.jsp","_z1_pagepath":"/product/{SP_z1pagepath}.jsp"},"profileId":"93f2fe79-661d-4c4c-a726-56c14ace69c4","z1SDKVersion":"3.1.27","wstoken":"kohls_com:-884024010","color":"Stonewash","prdPV":"{SP_prdPV}"}]",
					BODY_END
					);
			nsApi.ns_end_transaction("Event_viewedProduct", NS_AUTO_STATUS);
		}else {

			nsApi.ns_start_transaction("Event_viewedProduct");
			nsApi.ns_web_url ("event_3_PDP",
					"URL=https://{UIHostUrl}/c3/api/v1/event",
					"METHOD=POST",
					"HEADER=Origin:https://www.kohls.com",
					"HEADER=apikey:apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357",
					"HEADER=Accept-Language:en-in",
					"HEADER=Content-Type:text/plain",
					"PreSnapshot=webpage_1538639274864.png",
					"Snapshot=webpage_1538639274876.png",
					BODY_BEGIN,
					"[{"page":"PDP","isKCC":0,"loyaltyId":"N/A","productId":"{SP_productID}","isOnSale":"false","salePrice":"N/A","originalPrice":"{SP_originalPrice}","PDPCategory":"{SP_categoryName}","prd_inv":"[{$CAVREPEAT_BLOCK_START,sep(,),Count(SP_bossQty)}{\"skuCode\":\"{SP_zinesku}\",\"ShipAvlQty\":{SP_onlineAvailableQty},\"BopusAvlQty\":{SP_bopusQty},\"BossAvlQty\":{SP_bossQty}}{$CAVREPEAT_BLOCK_END}]","isLoggedIn":"true","event":"_ws_viewedProduct","z1_profile":{"device_id":"{deviceId}","_z1_origin":"ae87c3ec-f884-4db3-d807-fa72a3fda09c","firstTimeUser":"true","os":"html5","os_version":"8.1","platform_os":"Windows","language":"en","country":"US","tz":"IST","devicetype":"desktop","browser":"Chrome","browserversion":"Chrome:69.0.3497.100","resolution":"1366x768","_z1_pagename":"{SP_z1pagename}.jsp","_z1_pagepath":"/product/{SP_z1pagepath}.jsp"},"profileId":"{DV_DynID}","z1SDKVersion":"3.1.27","wstoken":"kohls_com:36011829","color":"Toasted Almond","prdPV":"{SP_prdPV}"}]",
					BODY_END
					);

			nsApi.ns_end_transaction("Event_viewedProduct", NS_AUTO_STATUS);



		}

        return status;
	}

}
