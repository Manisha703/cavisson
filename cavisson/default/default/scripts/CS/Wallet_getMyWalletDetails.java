// Converted from P2HR_With_Promo_Mixes_CM_0921/Wallet_getMyWalletDetails.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: Wallet_getMyWalletDetails
Recorded By: cavisson
Date of recording: 10/08/2018 05:07:42
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class Wallet_getMyWalletDetails implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
          if (debug) return status;

		nsApi.ns_start_transaction("getMyWalletDetails_jsp");
		nsApi.ns_web_url ("getMyWalletDetails_jsp",
				"URL=https://{UIHostUrl}/myaccount/getMyWalletDetails.jsp",
				"HEADER=Host:www-cstressrel.kohlsecommerce.com",
				"HEADER=Accept:application/json, text/javascript, */*; q=0.01",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Referer:https://www-cstressrel.kohlsecommerce.com/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=SL_Cookie;_abck;bm_sz;check;X-SESSIONID;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;staleStorage;productnum;AAMC_kohls_0;aam_uuid;s_stv;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;VisitorId;CavNVC;CavSF;K_favstore;DYN_USER_ID;VisitorUsaFullName;kohls_wcs_cookie;TS0112a040;kohls_klbd_cookie;dcroute;DCRouteS;LoginSuccess;fsr.s;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;fltk;isPreQualEligible;VisitorBagSession;VisitorBagTotals;correlation-id;X-PROFILEID;__gads;_gcl_au;SignalSpring2016;rr_rcs;gpv_v9;s_sq",
				"PreSnapshot=webpage_1538993139725.png",
				"Snapshot=webpage_1538993141888.png"
				);

		nsApi.ns_end_transaction("getMyWalletDetails_jsp", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(1.26);

		nsApi.ns_start_transaction("my_wallet_jsp");
		nsApi.ns_web_url ("my_wallet_jsp",
				"URL=https://{UIHostUrl}/wallet/my_wallet.jsp#offers",
				"HEADER=Host:www-cstressrel.kohlsecommerce.com",
				"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Referer:https://www-cstressrel.kohlsecommerce.com/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=SL_Cookie;_abck;bm_sz;check;X-SESSIONID;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;staleStorage;productnum;AAMC_kohls_0;aam_uuid;s_stv;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;VisitorId;CavNVC;CavSF;K_favstore;DYN_USER_ID;VisitorUsaFullName;kohls_wcs_cookie;TS0112a040;kohls_klbd_cookie;dcroute;DCRouteS;LoginSuccess;fsr.s;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;fltk;isPreQualEligible;VisitorBagSession;VisitorBagTotals;__gads;_gcl_au;SignalSpring2016;rr_rcs;gpv_v9;SignalSpring2018;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;SignalUniversalID;s_sq;correlation-id;X-PROFILEID",
				"PreSnapshot=webpage_1538993169692.png",
				"Snapshot=webpage_1538993147125.png",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", "HEADER=Accept:*/*", END_INLINE
				);

		nsApi.ns_end_transaction("my_wallet_jsp", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(1.26); 

		return status;
	}

}
