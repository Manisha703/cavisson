package CS;
import pacJnvmApi.NSApi;

public class Delivery implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("Delivery");
		status = nsApi.ns_web_url("Delivery",
				"URL=http://{SnBServiceHostUrl}/v1/deliveryInfo/standard",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_Delivery_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("Delivery", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		return status;
	}

}
