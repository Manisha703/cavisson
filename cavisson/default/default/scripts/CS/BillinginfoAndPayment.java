// Converted from P2HR_With_Promo_Mixes_CM_0921/BillinginfoAndPayment.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: BillinginfoAndPayment
Recorded By: cavisson
Date of recording: 07/16/2018 01:54:27
Flow details:
Build details: 4.1.10 (build# 34)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class BillinginfoAndPayment implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;

		//  -------------------------------------------  User click on Billing and Payment information ---------------------------------------------------------


		nsApi.ns_start_transaction("User_check_status");    
		nsApi.ns_web_url ("user_check_status",
				"URL=https://{UIHostUrl}/common/user_check_status.jsp",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;K_favstore;DYN_USER_ID;VisitorId;VisitorUsaFullName;correlation-id;X-PROFILEID;s_stv;gpv_v9;s_sq",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/b/ss/kohlsgnwebstoreqa/10/JS-2.0.0/s8161242462630?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=10%2F6%2F2018%201%3A14%3A4%202%20300&cid.&atgid.&id=3002100005378344&as=1&.atgid&.cid&d.&nsid=0&jsonv=1&.d&mid=46521992378641816542326779016432318821&aamlh=9&ce=UTF-8&ns=kohls&pageName=my%20account%3A%20my%20info%3A%20payment%20info&g=https%3A%2F%2F{UIHostUrl}%2Fmyaccount%2Fv2%2Fmyinfo.jsp&c.&k.&pageDomain={UIHostUrl}&.k&mcid.&version=2.1.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=my%20account&products=%3Bproductmerch2&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&v3=browse&c4=my%20account&v8=non-search&c9=my%20account&v9=my%20account%3A%20my%20info%3A%20shipping%20info&c10=my%20account&c11=my%20account&c16=browse&c17=kohls%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20logged%20in%7Cloyalty%20not%20logged%20in&c18=tue%7Cweekday%7C01%3A00%20am&v18=tue%7Cweekday%7C01%3A00%20am&c22=2018-07-10&v22=desktop&v39=3002100005378344&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=my%20account%3A%20my%20info%3A%20payment%20info&c63=fac52a93-0a88-4cb2-b8a6-e1a061fcb4db&c64=VisitorAPI%20Present&v68=my%20account%3A%20my%20info%3A%20payment%20info&v71=klsbrwcki%7C3002100005378344&v73=no%20loyalty%20id&v76=24lvnz%2Bbiuucww%2Bi3w1evwb1tlif6hq7jo98z%2Bhpqki%3D&c.&a.&activitymap.&page=my%20account%3A%20my%20info%3A%20shipping%20info&link=Billing%20%26%20Payment%20Info&region=leftInfoItem&pageIDType=1&.activitymap&.a&.c&pid=my%20account%3A%20my%20info%3A%20shipping%20info&pidt=1&oid=javascript%3Avoid%280%29%3B&ot=A&s=0x0&j=1.6&v=N&k=Y&bw=1280&bh=583&AQE=1", "HEADER=Accept-Language:en-us", "COOKIE=s_vi;ndcd", END_INLINE
				);
		nsApi.ns_end_transaction("User_check_status", NS_AUTO_STATUS);

		nsApi.ns_start_transaction("PaymentTypeJSON");   

		nsApi.ns_web_url("paymentTypeJSON",
				"URL=https://{UIHostUrl}/myaccount/json/myinfo/paymentTypeJSON",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("PaymentTypeJSON", NS_AUTO_STATUS);    

		nsApi.ns_start_transaction("Customer_address_statesList");
		nsApi.ns_web_url("Billing_customer_address_statesList",
				"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_address_statesList_Json.jsp",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("Customer_address_statesList", NS_AUTO_STATUS);

		nsApi.ns_start_transaction("Customer_info_details_json");    
		nsApi.ns_web_url("Billing_customer_info_details",
				"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_info_details_json.jsp?loadPaymentCard=loadPaymentCard&_=1531203244163",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore;s_stv;gpv_v9;s_sq",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/snb/media/images/visa.png", "HEADER=Accept-Language:en-us", "COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;s_stv;gpv_v9;s_sq;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;K_favstore;DYN_USER_ID;VisitorId;VisitorUsaFullName;correlation-id;X-PROFILEID", END_INLINE,
				"URL=https://{UIHostUrl}/ads/user-lists/1018012790/?guid=ON&data=10170497&cdct=2&is_vtc=1&random=3506873911", END_INLINE,
				"URL=https://{UIHostUrl}/ads/user-lists/1018012790/?guid=ON&data=10170497&cdct=2&is_vtc=1&random=3506873911", "HEADER=Accept-Language:en-us", END_INLINE
				);
		nsApi.ns_end_transaction("Customer_info_details_json", NS_AUTO_STATUS);

		nsApi.ns_page_think_time(0);


		// ---------------------------------------------- Edit billing address -----------------------------------------------------------------
		//Need to work on it ???????????

		nsApi.ns_start_transaction("User_check_status");
		nsApi.ns_web_url ("user_check_status_jsp_31_1",
				"URL=https://{UIHostUrl}/common/user_check_status.jsp",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:application/json, text/javascript, q=0.01",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;X-SESSIONID;K_favstore;DYN_USER_ID;VisitorId;VisitorUsaFullName;correlation-id;X-PROFILEID;s_stv;gpv_v9;s_sq"
				);
		nsApi.ns_end_transaction("User_check_status", NS_AUTO_STATUS);    

		nsApi.ns_start_transaction("customer_info_details_json");

		nsApi.ns_web_url("LoadBilling_Address",
				"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_info_details_json.jsp?loadBilling=loadBilling",
				"HEADER=Host:{UIHostUrl}",
				"HEADER=Accept:application/json, text/javascript, q=0.01",
				"HEADER=X-Requested-With:XMLHttpRequest",
				"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore",
				INLINE_URLS,
				"URL=https://{UIHostUrl}/tbproxy/af/query?client=Chromium", "METHOD=POST", "HEADER=Host:clients1.google.com", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", "HEADER=Content-Type:text/proto",
				BODY_BEGIN,
				"$CAVINCLUDE_NOPARAM$=http_request_body/user_check_status_jsp_31_main_url_1_1531729476932.body",
				BODY_END,
				END_INLINE,
				"URL=https://{UIHostUrl}/Capture/Interactive/Find/v1.00/json3ex.ws?Key=YP19-XT96-BM57-FW16&Text=st&Container=&Origin=USA&Countries=USA&Datasets=&Limit=7&Filter=&Language=en&$block=true&$cache=true&SOURCE=PCA-SCRIPT", "HEADER=Host:api.addressy.com", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", END_INLINE,
				"URL=https://{UIHostUrl}/Capture/Interactive/Find/v1.00/json3ex.ws?Key=YP19-XT96-BM57-FW16&Text=stres&Container=&Origin=USA&Countries=USA&Datasets=&Limit=7&Filter=&Language=en&$block=true&$cache=true&SOURCE=PCA-SCRIPT", "HEADER=Host:api.addressy.com", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", END_INLINE,
				"URL=https://{UIHostUrl}/Capture/Interactive/Find/v1.00/json3ex.ws?Key=YP19-XT96-BM57-FW16&Text=stress&Container=&Origin=USA&Countries=USA&Datasets=&Limit=7&Filter=&Language=en&$block=true&$cache=true&SOURCE=PCA-SCRIPT", "HEADER=Host:api.addressy.com", "HEADER=Origin:https://{UIHostUrl}", "HEADER=Accept:*/*", "HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp", "HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36", "HEADER=Accept-Encoding:gzip, deflate", "HEADER=Accept-Language:en-us", END_INLINE
				);

		nsApi.ns_end_transaction("customer_info_details_json", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0.0);


		//Add Billing Address

		if(nsApi.ns_eval_string("{SP_BillingAddress}").equals(""))
		{
			nsApi.ns_start_transaction("AddBillngAddress");  
			nsApi.ns_web_url("AddBillngAddress",
					"URL=https://{UIHostUrl}/myaccount/json/myinfo/saveBillingAddress",
					"METHOD=POST",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Origin:https://{UIHostUrl}",
					"HEADER=Accept:application/json, text/javascript, q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"HEADER=Content-Type:application/json",
					"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore",
					BODY_BEGIN,
					"{"firstName":"Perf","lastName":"Cavisson","addr1":"1 Brewers Way","addr2":"","city":"Milwaukee","state":"WI","postalCode":"53214","County":"","isUSorAPO":"US","addressType":"Billing","phoneNumber":"4123465644","postalCodeExt":"3652","sessionId":"18870733145929ACCM42f8e01fc4a2","nds-pmd":"{\"jvqtrgQngn\":{\"oq\":\"1366:657:1366:728:1366:728\",\"wfi\":\"flap-102126\",\"oc\":\"q400qo6n8n86q525\",\"fe\":\"1366k768 24\",\"qvqgm\":\"-330\",\"jxe\":111109,\"syi\":\"snyfr\",\"si\":\"si,btt,zc4,jroz\",\"sn\":\"sn,zcrt,btt,jni\",\"us\":\"por52r04n761257o\",\"cy\":\"Jva32\",\"sg\":\"{\\\"zgc\\\":0,\\\"gf\\\":snyfr,\\\"gr\\\":snyfr}\",\"sp\":\"{\\\"gp\\\":gehr,\\\"ap\\\":gehr}\",\"sf\":\"gehr\",\"jt\":\"78r9qs3735260548\",\"sz\":\"o65521rrr8ps0sos\",\"vce\":\"apvc,0,5onpns7q,2,1;fg,0,frnepu,0;zz,286s,491,4,;zzf,3r8,0,n,210 0,7qs0 8071,2q80,2q64,-49674,377qq,-5sso;zzf,3rq,3rq,n,49 0,5s2 306,165,168,-435n,27nr,-12p;zp,1q2,128,1ss,;fg,18r,frnepu,0;zzf,1o0,510,n,0 183,5rs1 0,10p7,10o5,-20p,2p12q,5s6q;zzf,3r7,3r7,n,qp 209,200 2s4q,qqr,qqs,-269op,8qp2,-5s03;zzf,3r9,3r9,n,ABC;gf,0,3q24;zzf,3r8,3r8,n,ABC;zzf,44q,44q,n,ABC;zzf,3r7,3r7,n,ABC;zzf,3r8,3r8,n,ABC;zzf,3r8,3r8,n,ABC;zz,1p4s,471,8,;fg,5pq,frnepu,0,svefgAnzr,0,ynfgAnzr,0,nqqerff1,0,nqqerff2,0,pvgl,0,cbfgnyPbqr,0,cubarAhzore,20;zp,14,234,223,;zzf,4r5,2715,32,49 41,2qnp 4071,4p9,3054,-1161o,17s6o,-59;gf,0,7825;xx,6n1,0,svefgAnzr;ss,0,svefgAnzr;zp,5r,25r,1o8,svefgAnzr;zz,85,25n,1o6,svefgAnzr;xq,33;xq,21s;xq,17;xq,q1;xq,22n;xq,60;xq,r4;xq,12s;so,11,svefgAnzr;xx,o,0,ynfgAnzr;ss,0,ynfgAnzr;xq,11o;xq,q3;xq,qp;xq,65;xq,qq;xq,9s;xq,93;xq,59p;zz,no,3os,1o8,ynfgAnzr;so,q7,ynfgAnzr;zp,51,49q,184,senzr;xx,2qs,0,nqqerff1;ss,0,nqqerff1;zp,71,26r,21n,nqqerff1;zzf,79n,270r,32,94 84,4qn4 39s9,7ss,4rr2,-1945q,1op4r,p4;xq,on;xq,1n9;xq,162;xq,o5;xq,p9;xq,326;xq,o7;xq,75n;xq,94;xq,ps;zz,230,2n4,1po,;gf,0,o340;so,68s,nqqerff1;zzf,p72,270r,32,24 p2,7r7 35rr,238,1611,-9q39,111r4,1r;zzf,2711,2711,32,ABC;xx,96,n,nqqerff1;gf,0,rqr8;ss,0,nqqerff1;zz,74,491,67,;zp,3r6,242,203,nqqerff1;xq,8;xq,9p;xq,15n;so,2op,nqqerff1;xx,n,0,nqqerff2;ss,0,nqqerff2;zp,5r,22p,267,nqqerff2;so,4op,nqqerff2;xx,298n,0,nqqerff2;ss,7,nqqerff2;zz,r,227,2n7,pvgl;zp,59r,228,266,nqqerff2;gf,0,12o5q;xq,3r;zp,31n,228,266,nqqerff2;so,279,nqqerff2;xx,n,0,pvgl;ss,0,pvgl;zp,62,214,2op,pvgl;xq,3o;xq,s3;so,2p7,pvgl;zp,7r,35p,2pn,fgngr;zzf,9,48p4,32,49 p3,80s1 9359,q43,7qn0,-3o684,4n2np,445;zz,933,35p,2p8,fgngr;zp,27,35p,2p8,fgngr;xx,234,0,cbfgnyPbqr;ss,0,cbfgnyPbqr;zp,32,3or,2or,cbfgnyPbqr;zzf,1o35,26s5,32,0 40,n02 p76,r8,8n9,-5053,4srp,3r;so,567,cbfgnyPbqr;xx,14n3,0,cbfgnyPbqr;gf,0,17715;ss,2,cbfgnyPbqr;zz,6s,4pq,73,;zp,499,3o9,2o0,cbfgnyPbqr;xq,64;xq,pp;zp,3n0,3qn,2oo,cbfgnyPbqr;xq,s3;zz,93r,435,2nq,;so,20n,cbfgnyPbqr;zp,58,467,2op,;xx,622,0,cubarAhzore;ss,0,cubarAhzore;zp,6o,271,327,cubarAhzore;xq,288;xq,1sr;xq,20;xq,24;xq,23;xq,24;xq,23;xq,25;xq,22;xq,24;xq,22;xq,25;xq,23;xq,24;xq,24;xq,24;xq,22;xq,25;xq,22;xq,26;xq,21;zz,18o,324,329,;so,154,cubarAhzore;zp,4p,33o,326,;xx,22p,n,cubarAhzore;ss,0,cubarAhzore;zp,43,265,332,cubarAhzore;xq,298;xq,204;xq,25;xq,22;xq,23;xq,24;xq,22;xq,25;xq,22;xq,25;xq,23;xq,26;xq,20;xq,25;xq,22;xq,25;xq,159;xq,qo;xq,n2;xq,112;xq,10q;xq,n0;xq,63;xq,5n;xq,41;xq,5q;xq,89;xq,14;xq,51;xq,66;xq,4;xq,61;xq,6n;zz,2on,266,330,cubarAhzore;gf,0,1o328;so,3n7,cubarAhzore;xx,15p8,n,cubarAhzore;ss,3,cubarAhzore;zz,n5,47r,19n,purpxbhg-pbagnvare;so,37o,cubarAhzore;\",\"ns\":\"\"},\"jg\":\"1.j-756138.1.2.LzSblQ75Uf05C6dPXIbFEt,,.XPISeVYYDQ0UdkV6MQquTjYPnkL2AXvkv7TB2rBn-ckvjZxtHuzHUW0OEqmCbLPulvzwtFs7DN70b1LTA8pZSvsLUTsGCgjH74-avO2cQrPd7fFOgt_169iBgeIO0Bd6PsmZzJNUinBVhhlIRm2hR_OoKnV6CBPSyO7MhEJN-1qDZ5GcuyfVicwqKW-_0TmU9FBsU2v2BbeC8VGhmJKUTKI765pgPZD6aQ5N7AkifOuUmmOm1jytjSk4WmWk0L0z\"}"}",
					BODY_END
					);
			nsApi.ns_end_transaction("AddBillngAddress", NS_AUTO_STATUS);

		}
	


		if (nsApi.ns_get_random_number_int(1,100) < 20)
		{ 

			nsApi.ns_start_transaction("Updated_Billing_Details");

			nsApi.ns_web_url("Updated_Billing_Details_1",
					"URL=https://{UIHostUrl}/myaccount/json/myinfo/updateBillingDetails",
					"METHOD=POST",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Origin:https://{UIHostUrl}",
					"HEADER=Accept:application/json, text/javascript, q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"HEADER=Content-Type:application/json",
					"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore",
					BODY_BEGIN,
					"{"firstName":"Perf{RN_Number}","lastName":"Cavisson","addr1":"1 Brewers Way","addr2":"","city":"Milwaukee","state":"WI","postalCode":"53214","County":"","isUSorAPO":"US","addressType":"Billing","addressKey":"","countryCode":"US","isApoFpo":"","phoneNumber":"4123465644","sessionId":"22537521299513ACCM1842102b35c2","nds-pmd":"{\"jvqtrgQngn\":{\"oq\":\"1366:657:1366:728:1366:728\",\"wfi\":\"flap-102126\",\"oc\":\"q400qo6n8n86q525\",\"fe\":\"1366k768 24\",\"qvqgm\":\"-330\",\"jxe\":769122,\"syi\":\"snyfr\",\"si\":\"si,btt,zc4,jroz\",\"sn\":\"sn,zcrt,btt,jni\",\"us\":\"por52r04n761257o\",\"cy\":\"Jva32\",\"sg\":\"{\\\"zgc\\\":0,\\\"gf\\\":snyfr,\\\"gr\\\":snyfr}\",\"sp\":\"{\\\"gp\\\":gehr,\\\"ap\\\":gehr}\",\"sf\":\"gehr\",\"jt\":\"78r9qs3735260548\",\"sz\":\"o65521rrr8ps0sos\",\"vce\":\"apvc,0,5onpon4n,2,1;fg,0,frnepu,0;zz,1ss,120,181,;zzf,89r,0,n,110 q7,81s 1rp5,9r4,12so,-3qns,1781p,1413;zzf,3r6,3r7,n,ABC;zzf,3r5,3r5,n,ABC;zzf,3r7,3r7,n,ABC;zzf,3r9,3r8,n,ABC;zzf,3r7,3r8,n,ABC;zzf,3r8,3r8,n,ABC;zp,378,152,200,;fg,18o,frnepu,0;zz,26,157,1sr,;zzf,50,578,n,120 pq,2or o2,6p,62,nr3,13pp,312;zzf,3r8,3r9,n,0 82,63o 0,nq,nr,-1734,3qo0,358;zzf,3r8,3r8,n,0 187,2ss3 107,s63,s5p,-16n35,183r8,-457;zz,14nq,35r,1so,;gf,0,43sq;zzf,1267,2714,32,49 41,25 2r08,10n,n68,-qn45,q444,-1s;zz,14ss,35o,268,;zzf,120s,270r,32,79o 80,p95 1q80,219,14sp,-5221,657p,1;zzf,2711,2711,32,ABC;gf,0,n483;zzf,2714,2714,32,ABC;zzf,270o,270o,32,ABC;gf,0,s2n2;zzf,2711,2711,32,ABC;zzf,rn63,rn63,1r,ABC;gf,0,20416;zzf,rn60,rn60,1r,ABC;gf,0,2rr76;zzf,rn63,rn63,1r,ABC;gf,0,3q8q9;zzf,rn5r,rn5r,1r,ABC;gf,0,4p337;zzf,rn5s,rn5s,1r,ABC;gf,0,5nq96;zz,12961,1pr,29q,;gf,0,6q6s7;fg,q35,frnepu,0,svefgAnzr,4,ynfgAnzr,8,nqqerff1,13,nqqerff2,0,pvgl,9,cbfgnyPbqr,10,cubarAhzore,10;zz,461r,48q,6s,;gf,0,72n4n;zz,976r,1rp,2o8,pvgl;gf,0,7p1o8;xx,295,4,svefgAnzr;ss,0,svefgAnzr;zp,5p,22q,193,svefgAnzr;xq,251;so,703,svefgAnzr;\",\"ns\":\"\"},\"jg\":\"1.j-756138.1.2.-YIQD8J0THsNRW_GwR5u9t,,.hPn9Ruzi9DdiOPjjNcgmErNKsfbQPaX1bdmlEiRLm7QFQtDZ_cEUMys1HvoKIPh6tWhxBxI0QtlOsHWav3FHoZNUQO4jolwg_mRk-9mlUdg6aBxF8WKX6x9eVGYO6c8oTr0XkBl5BsE1r7Yd9ouBBofek1vOshwOopJkRrGukQEAnpu3S_i7raI7_1xEjIFU1W78SR8shPoxj2W8q1R8b4V7B5swZFch94cdwYCyxrhfDUVYxk9rXfLBOFBPacTk\"}","postalCodeExt":"3652"}",
					BODY_END
					);

			nsApi.ns_end_transaction("Updated_Billing_Details", NS_AUTO_STATUS);

			nsApi.ns_start_transaction("paymentTypeJSON");
			nsApi.ns_web_url("Updated_Payment_Type",
					"URL=https://{UIHostUrl}/myaccount/json/myinfo/paymentTypeJSON",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Accept:application/json, text/javascript, q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore"
					);
			nsApi.ns_end_transaction("paymentTypeJSON", NS_AUTO_STATUS);

			nsApi.ns_start_transaction("customer_address_statesList_Json");
			nsApi.ns_web_url("Updated_Address_State",
					"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_address_statesList_Json.jsp",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Accept:application/json, text/javascript, q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore"
					);
			nsApi.ns_end_transaction("customer_address_statesList_Json", NS_AUTO_STATUS);

			nsApi.ns_start_transaction("customer_info_details_json");
			nsApi.ns_web_url("Updated_Customer_Details",
					"URL=https://{UIHostUrl}/myaccount/json/myinfo/customer_info_details_json.jsp?loadPaymentCard=loadPaymentCard&_=1531729401183",
					"HEADER=Host:{UIHostUrl}",
					"HEADER=Accept:application/json, text/javascript, q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}/myaccount/v2/myinfo.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"COOKIE=_abck;bm_sz;AKA_A2;__gads;staleStorage;btpdb.QF4SXwN.c2lnbmFsIDFzdCBwYXJ0eSBpZA;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;LoginSuccess;VisitorBagTotals;VisitorBagSession;isPreQualEligible;s_fid;store_location;productnum;s_cc;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;bd_session;SignalUniversalID;mbox;check;AAMC_kohls_0;fltk;aam_uuid;s_stv;gpv_v9;s_sq;X-SESSIONID;X-PROFILEID;correlation-id;VisitorUsaFullName;VisitorId;DYN_USER_ID;K_favstore"
					);
			nsApi.ns_end_transaction("customer_info_details_json", NS_AUTO_STATUS);
		}
		nsApi.ns_page_think_time(0.0);
          return status;
	}


}
