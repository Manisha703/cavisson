// Converted from P2HR_With_Promo_Mixes_CM_0921/Search_PID_201_500.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: Search_PID_201_500
Recorded By: Manish
Date of recording: 08/03/2018 02:04:04
Flow details:
Build details: 4.1.11 (build# 216)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class Search_PID_201_500 implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
          int status =0;
		nsApi.ns_advance_param("FP_PID_201_500");

		nsApi.ns_start_transaction("Search_PID_201_500");
		nsApi.ns_web_url("PDP_search_sku_500",
				"URL=https://{UIHostUrl}/search.jsp?submit-search=&search={FP_PID_201_500}",
				"HEADER=Host:www-cstressrel-green.kohlsecommerce.com",
				"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"HEADER=Upgrade-Insecure-Requests:1",
				"HEADER=Referer:https://www-cstressrel-green.kohlsecommerce.com/",
				"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
				"HEADER=Accept-Encoding:gzip, deflate",
				"HEADER=Accept-Language:en-us",
				"COOKIE=_abck;bm_sz;SL_Cookie;AKA_A2"
				);

		if (nsApi.ns_get_int_val("pid_201_500_notFound") > 0) 
		{ 
			nsApi.ns_end_transaction_as("Search_PID_201_500", 0, "Search_PID_201_500_PDPNotFound");

		}
		else if(nsApi.ns_eval_string("{SP_skuCode}").equals(""))
			nsApi.ns_end_transaction_as("Search_PID_201_500", 0, "Search_PID_201_500_CatalogPage");
		else 
			nsApi.ns_end_transaction("Search_PID_201_500", NS_AUTO_STATUS);


		nsApi.ns_page_think_time(0);
		//nsApi.ns_save_data_ex("/home/cavisson/work/data/P2HR/PDP_UrlHit.txt",NS_APPEND_FILE,"%s",nsApi.ns_eval_string("{FP_PID_201_500}"));        
		status = nsApi.ns_save_data_eval("/tmp/PDP_UrlHit.txt", NS_APPEND_FILE,"{FP_PID_201_500}\n");
		nsApi.ns_start_transaction("Web_PDPSessionJSP");
		nsApi.ns_web_url ("Web_PDPSessionJSP7",
				"METHOD=POST",
				"URL=https://{UIHostUrl}/web/session.jsp?lpf=v2",
				"HEADER=Accept:application/json",
				"HEADER=Accept-Encoding:gzip, deflate, br",
				"HEADER=Accept-Language:en-US,en;q=0.5",
				"HEADER=Content-Type:application/json",
				BODY_BEGIN,
				"{"pageName":"PDP","pId":"{FP_PID_201_500}"}",
				BODY_END,
				);
		nsApi.ns_end_transaction("Web_PDPSessionJSP", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);
		return status;


	}

}
