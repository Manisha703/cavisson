// Converted from Acc_Services_End_To_End_C/PrequalEligibility_Web.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class PrequalEligibility_Web implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{	
	int status = 0;
	nsApi.ns_start_transaction("PrequalEligibility_Web");
     nsApi.ns_web_url ("PrequalEligibility_Web",
        "URL=https://{AccServiceHostUrl}/v1/authenticate",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=X-SESSIONID: {LPFSessionID}",
          "HEADER=X-PROFILEID: {LPFProfileID}",
          "HEADER=X-APP-API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr7F5",
          "HEADER=correlation-id: PrequalEligibility_Web_{CorrChar}{CorrID}",
          "BODY=$CAVINCLUDE$=PrequalEligibility_Web.json",
);
    nsApi.ns_end_transaction("PrequalEligibility_Web", NS_AUTO_STATUS);
	return status;

}

}
