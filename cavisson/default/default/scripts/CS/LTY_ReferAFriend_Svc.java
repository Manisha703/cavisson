// Converted from Acc_Services_End_To_End_C/LTY_ReferAFriend_Svc.c on Wed May  5 22:43:12 2021
/*----------------------------------------------------------------------------
    me:LTY_Activity
    Recorded Name : LTY_ReferAFriend_Svc
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class LTY_ReferAFriend_Svc implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	   int status = 0;
	   nsApi.ns_save_string(nsApi.ns_get_guid(), "EMailID");
        nsApi.ns_start_transaction("LTY_ReferAFriend_Svc");
        nsApi.ns_web_url ("LTY_ReferAFriend_Svc",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/referFriends",
          "METHOD=POST",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID1}",
        "HEADER=X-PROFILEID: {LPFProfileID1}",
        "HEADER=correlation-id: LTY_ReferAFriend_Svc_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=LTY_ReferAFriend_Svc.json",
);
    nsApi.ns_end_transaction("LTY_ReferAFriend_Svc", NS_AUTO_STATUS);
    return status;

}

}
