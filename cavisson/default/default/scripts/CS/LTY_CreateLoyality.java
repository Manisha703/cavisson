// Converted from Acc_Services_End_To_End_C/LTY_CreateLoyality.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class LTY_CreateLoyality implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_save_string(nsApi.ns_get_guid(), "EMailID");	
	nsApi.ns_start_transaction("LTY_CreateLoyality");
     nsApi.ns_web_url ("LTY_CreateLoyality",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/create",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=X-SESSIONID: {SessionID}",
         "BODY=$CAVINCLUDE$=LTY_CreateLoyality.json",
);
    nsApi.ns_end_transaction("LTY_CreateLoyality", NS_AUTO_STATUS);
    status = nsApi.ns_save_data_eval("/home/netstorm/work/data/AMServices/LoyaltyEmailids.txt", NS_APPEND_FILE,
                "{EMailID}");
	return status;
                
}


}
