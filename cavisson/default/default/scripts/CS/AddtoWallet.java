// Converted from Acc_Services_End_To_End_C/AddtoWallet.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class AddtoWallet implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;

    nsApi.ns_start_transaction("AddtoWallet");
    nsApi.ns_web_url ("AddtoWallet",
        "URL=https://{AccServiceHostUrl}/v1/kohlswallet",
        "METHOD=POST",
        "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {WallSessionID}",
        "HEADER=X-PROFILEID: {WallProfileID}",
        "HEADER=profileId: {Wallprofileid}",
        "HEADER=correlation-id: AddtoWallet_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=AddOfferstoWallet.json",
);
    nsApi.ns_end_transaction("AddtoWallet", NS_AUTO_STATUS);
    nsApi.ns_page_think_time(0);
    return status;

}


}
