// Converted from Acc_Services_End_To_End_C/Finish.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: Finish
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class Finish implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{

  	int status = 0;
	nsApi.ns_start_transaction("FinishOtp");
     nsApi.ns_web_url ("FinishOtp_1",
		"URL=http://10.208.3.38/v1/profile/otp/finish",
		"METHOD=POST",
		"HEADER=Accept: application/json",
		"HEADER=Content-Type: application/json",
		"HEADER=channel:web",
		"HEADER=Postman-Token:1a1d80aa-e91e-b0d2-9c15-8fbc87ae45d9",
		"BODY=$CAVINCLUDE$=FinishOtp.json",
       
);
    nsApi.ns_end_transaction("FinishOtp", NS_AUTO_STATUS);
    return status;
}

}
