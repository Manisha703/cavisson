package CS;
import pacJnvmApi.NSApi;

public class CatalogByActiveDimension_P implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		int randNo = nsApi.ns_get_random_number_int(1,100);

		if(randNo<=50)
		{
			status = nsApi.ns_start_transaction("CatalogPersonalization_Browse");
			status = nsApi.ns_web_url("CatalogPersonalization_Browse",
					"URL=http://{SnBServiceHostUrl}/v1/catalog/{CatalogWebstore_FP}?pid={ProfileId_New_FP}&limit=60&offset=1&isLTL=true&channel={channel}",
					"HEADER=Accept: application/json, text/html",
					"HEADER=Accept-Language: en-US,en;q=0.8 ",
					"HEADER=X-APP-API_KEY: {key}",
					"HEADER=Content-Type: application/json",
					"HEADER=Upgrade-Insecure-Requests: 1",
					"HEADER=correlation-id: Catalog_Personalization_{CorrChar}{CorrID}"

					);

			if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase(""))
			{

				status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse", 0, "CatalogPersonalization_BrowsePdataFalse");
			}

			else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("True"))
			{
				status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse", 0, "CatalogPersonalization_BrowsePdataTrue");

			}
			else
			{
				status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse", 0, "CatalogPersonalization_BrowseNoPData");

			}
			status = nsApi.ns_page_think_time(0);  


		}
		else
		{
			status = nsApi.ns_start_transaction("CatalogPersonalization_Browse_Mobile");
			status = nsApi.ns_web_url("CatalogPersonalization_Browse_Mobile",
					"URL=http://{SnBServiceHostUrl}/v1/catalog?dimensionValueID={CatalogDimID_FP}&isDefaultStore=true&storeNum={StoreId_FP}&shipToStore=true&pid={ProfileId_New_FP}&includeStoreOnlyProducts=true&isVGC=false&offset=1&limit=72&channel={channel}",
					"HEADER=Accept: application/json, text/html",
					"HEADER=Accept-Language: en-US,en;q=0.8 ",
					"HEADER=Content-Type: application/json",
					"HEADER=X-APP-API_KEY: {key}",					
					"HEADER=Upgrade-Insecure-Requests: 1",
					"HEADER=correlation-id: Catalog_Personalization_{CorrChar}{CorrID}"

					);

			if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
			{

				status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Mobile", 0, "CatalogPersonalization_BrowsePdataFalse");
			}

			else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("True"))
			{
				status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Mobile", 0, "CatalogPersonalization_BrowsePdataTrue");

			}
			else
			{
				status = nsApi.ns_end_transaction_as("CatalogPersonalization_Browse_Mobile", 0, "CatalogPersonalization_BrowseNoPData");

			}
			status = nsApi.ns_page_think_time(0);  
		}
		return status;
	}
}

