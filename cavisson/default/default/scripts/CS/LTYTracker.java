// Converted from Acc_Services_End_To_End_C/LTYTracker.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class LTYTracker implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("LTYTracker");
     nsApi.ns_web_url ("LTYTracker",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/tracker",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=correlation-id: LTYTracker_{CorrChar}{CorrID}",
         "HEADER=API_KEY: YJj4eXkxp4IdeOaYp80DBEQvsjatr7F5",
          "HEADER=X-SESSIONID: {LPFSessionID1}",
         "HEADER=X-PROFILEID: {LPFProfileID1}.", 
          "BODY=$CAVINCLUDE$=LTYTracker.json",
      
);
    nsApi.ns_end_transaction("LTYTracker", NS_AUTO_STATUS);
   nsApi.ns_save_data_eval("/home/netstorm/work/data/AMServices/LTYPilotUsers5th_track.txt",NS_APPEND_FILE,"{SessionID},{ProfileID},{LTY}");
	return status;

}

}
