// Converted from P2HR_With_Promo_Mixes_CM_0921/Wallet_AddOffer.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: Wallet_AddOffer
Recorded By: cavisson
Date of recording: 10/08/2018 05:07:42
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class Wallet_AddOffer implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
          if (debug) return status;

		if (nsApi.ns_get_random_number_int(1,10) < 5)

		{

			nsApi.ns_start_transaction("Wallet_AddOffer");
			nsApi.ns_web_url ("Wallet_AddOffer",
					"URL=https://{UIHostUrl}//wallet/json/wallet_add_item_json.jsp?_DARGS=/wallet/json/myaccount_wallet_update_forms.jsp",
					"METHOD=POST",
					"HEADER=Host:{UIHostUrl}/",
					"HEADER=Origin:https://{UIHostUrl}/",
					"HEADER=Accept:application/json, text/javascript, */*; q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}//wallet/my_wallet.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
					"COOKIE=SL_Cookie;_abck;bm_sz;check;X-SESSIONID;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;staleStorage;AAMC_kohls_0;aam_uuid;s_stv;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;kohls_wcs_cookie;LoginSuccess;fsr.s;fltk;isPreQualEligible;VisitorBagSession;VisitorBagTotals;__gads;_gcl_au;SignalSpring2016;rr_rcs;SignalSpring2018;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;SignalUniversalID;unbxd.userId;unbxd.visit;unbxd.visitId;IR_gbd;IR_5349;IR_PI;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;dcroute;DCRouteS;akaalb_www-stress1_kohlsecommerce_com;TS0112a040;kohls_klbd_cookie;CavSF;CavNVC;productnum;gpv_v9;s_sq;correlation-id;X-PROFILEID",
					"PreSnapshot=webpage_1538993191063.png",
					"Snapshot=webpage_1538993193966.png",
					BODY_BEGIN,
					"id={PromoCodes}&pin=&nds-pmd={}",
					BODY_END
					);

			nsApi.ns_end_transaction("Wallet_AddOffer", NS_AUTO_STATUS);
		}

		else
		{
			nsApi.ns_start_transaction("Wallet_AddOffer_SUPC");
			nsApi.ns_web_url ("Wallet_AddOffer_SUPC",
					"URL=https://{UIHostUrl}//wallet/json/wallet_add_item_json.jsp?_DARGS=/wallet/json/myaccount_wallet_update_forms.jsp",
					"METHOD=POST",
					"HEADER=Host:{UIHostUrl}/",
					"HEADER=Origin:https://{UIHostUrl}/",
					"HEADER=Accept:application/json, text/javascript, */*; q=0.01",
					"HEADER=X-Requested-With:XMLHttpRequest",
					"HEADER=Referer:https://{UIHostUrl}//wallet/my_wallet.jsp",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					"HEADER=Content-Type:application/x-www-form-urlencoded; charset=UTF-8",
					"COOKIE=SL_Cookie;_abck;bm_sz;check;X-SESSIONID;AMCVS_F0EF5E09512D2CD20A490D4D%40AdobeOrg;s_cc;staleStorage;AAMC_kohls_0;aam_uuid;s_stv;btpdb.MeUoaP5.c2lnbmFsIGZpcnN0IHBhcnR5IGlk;SignalUniversalID;VisitorId;K_favstore;DYN_USER_ID;VisitorUsaFullName;kohls_wcs_cookie;LoginSuccess;fsr.s;fltk;isPreQualEligible;VisitorBagSession;VisitorBagTotals;__gads;_gcl_au;SignalSpring2016;rr_rcs;SignalSpring2018;btpdb.4DPyaxM.dGZjLjYyMDYyMTU;btpdb.4DPyaxM.dGZjLjYyMDYyMDU;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg;btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM;SignalUniversalID;unbxd.userId;unbxd.visit;unbxd.visitId;IR_gbd;IR_5349;IR_PI;mbox;AMCV_F0EF5E09512D2CD20A490D4D%40AdobeOrg;dcroute;DCRouteS;akaalb_www-stress1_kohlsecommerce_com;TS0112a040;kohls_klbd_cookie;CavSF;CavNVC;productnum;gpv_v9;s_sq;correlation-id;X-PROFILEID",
					"PreSnapshot=webpage_1538993191063.png",
					"Snapshot=webpage_1538993193966.png",
					BODY_BEGIN,
					"id={PromoCodesSUPC}&pin=&nds-pmd={}",
					BODY_END
					);

			nsApi.ns_end_transaction("Wallet_AddOffer_SUPC", NS_AUTO_STATUS);
		}
		return status;

	}

}
