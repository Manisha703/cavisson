// Converted from Acc_Services_End_To_End_C/Get_latest_vesrion.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

//#include "LoyaltyUtil.c"


public class Get_latest_vesrion implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("Get_latest_vesrion");
     nsApi.ns_web_url ("Get_latest_vesrion",
        "URL=https://{AccServiceHostUrl}/v1/wallet/apple/loyalty/{version}/passes/{passTypeIdentifier}/{serialNumber}",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
//          "HEADER=X-APP-API_KEY: {key}",
//        "HEADER=Accept-Language: en-us,en;q=0.5",
  //      "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
    //    "HEADER=Proxy-Connection: keep-alive",
 //       "BODY=$CAVINCLUDE$=Utility.json",
);
    nsApi.ns_end_transaction("Get_latest_vesrion", NS_AUTO_STATUS);
    return status;

}
   


}
