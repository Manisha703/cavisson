package CS;
import pacJnvmApi.NSApi;

public class PriceBySKU_GET implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("PriceBySKU_GET");
		status = nsApi.ns_web_url("PriceBySKU_GET",
				"METHOD=GET",
				"URL=http://{SnBServiceHostUrl}/v1/price/sku/{SKUIDFP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: PriceBySKU_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("PriceBySKU_GET", NS_AUTO_STATUS);
		status = nsApi.ns_save_data_eval("output/Stress_ValidSKU.txt", NS_APPEND_FILE, "{SKUSP}\n");
		status = nsApi.ns_page_think_time(0);    
		return status;
	}
}
