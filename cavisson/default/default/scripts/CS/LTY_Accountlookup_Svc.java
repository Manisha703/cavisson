// Converted from Acc_Services_End_To_End_C/LTY_Accountlookup_Svc.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;



public class LTY_Accountlookup_Svc implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
       int status = 0;
        nsApi.ns_start_transaction("LTY_Accountlookup_Svc");
    	   nsApi.ns_web_url ("LTY_Accountlookup_Svc",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/accountLookup",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=X-SESSIONID: {LPFSessionID1}",
        "HEADER=X-PROFILEID: {LPFProfileID1}",
         "HEADER=correlation-id: LTY_Accountlookup_Svc_{CorrChar}{CorrID}",
        "BODY=$CAVINCLUDE$=LTY_Accountlookup_Svc.json",
);
    nsApi.ns_end_transaction("LTY_Accountlookup_Svc", NS_AUTO_STATUS);
	return status;

}

}
