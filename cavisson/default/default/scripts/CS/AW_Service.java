// Converted from Acc_Services_End_To_End_C/AW_Service.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class AW_Service implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("AW_Service");
     nsApi.ns_web_url ("AW_Service",
        "URL=https://{AccServiceHostUrl}/v1/passthru/v1/passes/pass.com.kohls.y2y/jNrJNw5N",          
        "METHOD=GET",
        "HEADER=Accept: application/vnd.apple.pkpass",
        "HEADER=Content-Type: application/json",
        "HEADER=openapi-passthru-target-type: wallet.apple.loyalty",
);
    nsApi.ns_end_transaction("AW_Service", NS_AUTO_STATUS);
    return status;
}
   


}
