package CS;
import pacJnvmApi.NSApi;

public class CatalogByKeyword_P_Guest implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("CatalogByKeyword_P_Guest");
		status = nsApi.ns_web_url("CatalogByKeyword_P_Guest",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?keyword={CatalogKeyword_FP}&storeNum={StoreId_FP}&isDefaultStore=true&svid={visId}&channel={channel}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"	
				);
		if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
		{

			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P_Guest", 0, "CatalogByKeyword_P_GuestPdataFalse");
		}

		else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("true"))
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P_Guest", 0, "CatalogByKeyword_P_GuestPdataTrue");

		}
		else
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P_Guest", 0, "CatalogByKeyword_P_GuestNoPData");

		}

		status = nsApi.ns_start_transaction("CatalogByKeyword_Dimension_P_Guest");
		status = nsApi.ns_web_url("CatalogByKeyword_Dimension_P_Guest",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?dimensionValueID={CatalogDimValueId_FP}&sortID=1&storeNum={StoreId_FP}&isDefaultStore=true&shipToStore=true&keyword={CatalogDimKeyword_FP}&includeStoreOnlyProducts=true&isVGC=false&offset=1&limit=72&svid={visId}&channel={channel}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_CatalogByKeyword_{CorrChar}{CorrID}"
				);

		if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("false"))
		{

			status = nsApi.ns_end_transaction_as("CatalogByKeyword_Dimension_P_Guest", 0, "CatalogByKeyword_PdataFalse");
		}

		else if(nsApi.ns_eval_string("{PInfoWebSP}").equalsIgnoreCase("true"))
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_Dimension_P_Guest", 0, "CatalogByKeyword_PdataTrue");

		}
		else
		{
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_P_Guest", 0, "CatalogByKeyword_NoPData");

		}


		status = nsApi.ns_page_think_time(0);
		return status;
	}
}
