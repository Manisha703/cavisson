// Converted from Mix_Solr_SPA1_R69_C//BrowsePersonalisation.c on Tue May 18 13:11:11 2021
/*-----------------------------------------------------------------------------
Name: BOPUSBrowseQuery
Recorded By: Sandeep
Date of recording: 08/05/2014 05:02:32
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class BrowsePersonalisation implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int i = nsApi.ns_get_random_number_int(1 , 100);
		nsApi.ns_start_transaction("BrowsePersonalisation");
		if (i<=60)
		{
			nsApi.ns_web_url ("BrowsePersonalisation",
					"URL=http://{SolrHost}{AffinityURL_FP}&ltl=false&storeId={BOPUS_URL}&includeStoreOnlyProducts=true&shipToStore=true",
					"HEADER=Accept-Language: en-us,en;q=0.5",
					"HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
					"HEADER=Proxy-Connection: keep-alive",
					INLINE_URLS,
					"URL=http://integration.richrelevance.com/rrserver/p13n_generated.js?a=648c894ab44bc04a&ts=1407237879155&pt=%7Chome_page.pushdown%7Chome_page.ad_160x600%7Chome_page.horizontal_scroller&u=1000450571&s=vvwLTg2HLTgNphQp1w7v8zn0GHj3kmvh1WjsWnmJnRwPwsnscdL2!932978008!1407237863564&cts=http%3A%2F%2F10.210.156.65%3A17010&flv=14.0.0&l=1", "HEADER=Accept-Language: en-us,en;q=0.5", "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7", "HEADER=Proxy-Connection: keep-alive", END_INLINE
					);
		}
		else
		{
			nsApi.ns_web_url ("BrowsePersonalisation_WO_StoreID",
					"URL=http://{SolrHost}{AffinityURL_FP}&ltl=false&includeStoreOnlyProducts=true&shipToStore=true",
					"HEADER=Accept-Language: en-us,en;q=0.5",
					"HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
					"HEADER=Proxy-Connection: keep-alive",
					INLINE_URLS,
					"URL=http://integration.richrelevance.com/rrserver/p13n_generated.js?a=648c894ab44bc04a&ts=1407237879155&pt=%7Chome_page.pushdown%7Chome_page.ad_160x600%7Chome_page.horizontal_scroller&u=1000450571&s=vvwLTg2HLTgNphQp1w7v8zn0GHj3kmvh1WjsWnmJnRwPwsnscdL2!932978008!1407237863564&cts=http%3A%2F%2F10.210.156.65%3A17010&flv=14.0.0&l=1", "HEADER=Accept-Language: en-us,en;q=0.5", "HEADER=Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7", "HEADER=Proxy-Connection: keep-alive", END_INLINE
					);
		}

		nsApi.ns_end_transaction("BrowsePersonalisation", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);
		return 0;

	}

}
