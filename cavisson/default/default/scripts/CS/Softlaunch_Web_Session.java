// Converted from Acc_Services_End_To_End_C/Softlaunch_Web_Session.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: Softlaunch_Web_Session
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class Softlaunch_Web_Session implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	int retval1 = nsApi.ns_add_cookie_val_ex("X-PROFILEID", "", "", nsApi.ns_eval_string("{XProfileid}"));
	int retval2 = nsApi.ns_add_cookie_val_ex("X-SESSIONID", "", "", nsApi.ns_eval_string("{Sessionid}"));
	int retval3 = nsApi.ns_add_cookie_val_ex("AKA_CNC2", "", "", "{\"perf\":\"True\"}");
	
	nsApi.ns_start_transaction("Softlaunch_Web_Session");
    	nsApi.ns_web_url ("Softlaunch_Web_Session",
        "URL=https://{AccServiceHostUrl}/v1/web/profile/session",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=isNewExperience: True",
         "HEADER=X-SESSIONID: {SessionID11}",
         "HEADER=X-PROFILEID: {ProfileID11}",
         "BODY=$CAVINCLUDE$=Softlaunch_Web_Session.json",
);
    nsApi.ns_end_transaction("Softlaunch_Web_Session", NS_AUTO_STATUS);
	return status;

}

}
