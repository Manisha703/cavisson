// Converted from WCS_WithNewURL/HomePage.c on Wed May 19 02:08:50 2021
/*-----------------------------------------------------------------------------
Name:HomePage
Recorded By: Jitendra
Date of recording: 03/08/2014 04:59:11
Flow details:
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;



public class HomePage implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{

		int status = 0;

		nsApi.ns_start_transaction("HomePage"); 
		nsApi.ns_web_url ("WcsHomePage",
				"URL=http://{WcsHostURL}/sites/home.jsp",
				"HEADER=Accept-Language: en-US",
				"HEADER=Proxy-Connection: Keep-Alive"

				);

		nsApi.ns_end_transaction("HomePage", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

		int var1 = nsApi.ns_get_random_number_int(1,1000);
		if(var1 <= 500)
		{
			nsApi.ns_start_transaction("WCS_NewNavigation");
			nsApi.ns_web_url ("WCS_NewNavigation",
					"URL=http://{WcsHostURL}/feature/ESI/Common/NewNavigation/",
					"HEADER=X-Requested-With: XMLHttpRequest",
					"HEADER=Accept-Language: en-us",
					"HEADER=Proxy-Connection: Keep-Alive",
					"COOKIE=origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;"
					);
			nsApi.ns_end_transaction("WCS_NewNavigation", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);

			nsApi.ns_start_transaction("WCS_seodataservice");
			nsApi.ns_web_url ("WCS_seodataservice",
					"URL=http://{WcsHostURL}/wcs-internal/seodataservice",
					"HEADER=X-Requested-With: XMLHttpRequest",
					"HEADER=Accept-Language: en-us",
					"HEADER=Proxy-Connection: Keep-Alive",
					"COOKIE=origin-perf01;TS96232b;cookieSetting;s_cc;gpv_v9;s_sq;s_stv;productnum;__utma;__utmb;__utmc;__utmz;"
					);
			nsApi.ns_end_transaction("WCS_seodataservice", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);

			nsApi.ns_start_transaction("WCS_NewBanner");
			nsApi.ns_web_url ("WCSNewBanner",
					"URL=http://{WcsHostURL}/feature/ESI/Common/NewBanner/",
					"HEADER=Accept-Language: en-US",
					"HEADER=Proxy-Connection: Keep-Alive",
					);

			nsApi.ns_end_transaction("WCS_NewBanner", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);

			nsApi.ns_start_transaction("WCS_signin");
			nsApi.ns_web_url ("WCSsignin",
					"URL=http://{WcsHostURL}/feature/ESI/Common/signin/",
					"HEADER=Accept-Language: en-US",
					"HEADER=Proxy-Connection: Keep-Alive",
					);

			nsApi.ns_end_transaction("WCS_signin", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);

		}

		int var = nsApi.ns_get_random_number_int(1,1000);
		if(var <= 500)
		{

			nsApi.ns_start_transaction("NewHeader");

			nsApi.ns_web_url ("Header",
					"URL=http://{WcsHostURL}/feature/ESI/Common/NewHeader/",
					"HEADER=Accept-Language: en-US",
					"HEADER=Proxy-Connection: Keep-Alive",
					);

			nsApi.ns_end_transaction("NewHeader", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);


			nsApi.ns_start_transaction("Footer");

			nsApi.ns_web_url ("WCS_Footer",
					"URL=http://{WcsHostURL}/feature/ESI/Common/Footer/",
					"HEADER=Accept-Language: en-US",
					"HEADER=Proxy-Connection: Keep-Alive",
					);

			nsApi.ns_end_transaction("Footer", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);
		}
	return status;
}

}
