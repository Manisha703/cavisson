package CS;
import pacJnvmApi.NSApi;

// Test

public class CatalogGetProductCount implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0; 
		status = nsApi.ns_start_transaction("Catalog_GetProductCount");
		status = nsApi.ns_web_url("Catalog_GetProductCount",
				"URL=http://{SnBServiceHostUrl}/v1/catalog/productCount/{catalogID_FP}?keyword={Keyword_FP}&storeNum={StoreId_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation-id: Catalog_GetProductCount_{CorrChar}{CorrID}"
				);

		status = nsApi.ns_end_transaction("Catalog_GetProductCount", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);
		status = nsApi.ns_save_data_eval("output/Catalog_GetProductCount_CatalogIds.txt", NS_TRUNC_FILE, "{catalogID}\n");
		return status;
	}
}
