package CS;
import pacJnvmApi.NSApi;

public class Availability implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("Availability");
		status = nsApi.ns_web_url("Availability",
				"URL=https://{SnBServiceHostUrl}/v1/availability/pickup/{SkuCodeSP}?zipcode={Zipcode_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_Availability_{CorrChar}{CorrID}"
				);
		status = nsApi.ns_end_transaction("Availability", NS_AUTO_STATUS);
		status = nsApi.ns_page_think_time(0);

		return status;	

	}
}

