// Converted from Acc_Services_End_To_End_C/CreateProfile_withLoyaltyID.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class CreateProfile_withLoyaltyID implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
     nsApi.ns_save_string(nsApi.ns_get_guid(), "EMailID");	
	nsApi.ns_start_transaction("CreateProfile_withLoyaltyLink");
     nsApi.ns_web_url ("CreateProfile_withLoyaltyID",
        "URL=https://{AccServiceHostUrl}/v1/profile/create",
          "METHOD=POST",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "BODY=$CAVINCLUDE$=customerprofile_withLoyaltyID.json",
);
    nsApi.ns_end_transaction("CreateProfile_withLoyaltyLink", NS_AUTO_STATUS);
	return status;
}


}
