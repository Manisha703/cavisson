// Converted from P2HR_With_Promo_Mixes_CM_0921/CartItemUpdate_Qty.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: CartItemUpdate_Qty
Generated By: cavisson
Date of generation: 08/24/2018 10:38:09
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;




public class CartItemUpdate_Qty implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		if (nsApi.ns_get_random_number_int(1,100) < 101)
		{ 
			nsApi.ns_start_transaction("CartItemUpdate_Qty");
			nsApi.ns_web_url("CartItemUpdate_Qty",
					"URL=https://{UIHostUrl}/cnc/checkout/cartItems/update",
					"METHOD=POST",
					"HEADER=Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
					"HEADER=Upgrade-Insecure-Requests:1",
					"HEADER=User-Agent:Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
					"HEADER=Accept-Encoding:gzip, deflate",
					"HEADER=Accept-Language:en-us",
					BODY_BEGIN,            
					"{"cartItemId":{SP_update_cartItemId},"cartId":"{SP_cartId}","cartItems":[{"skuId":{SP_update_skuid},"quantity":2,"productId":{SP_update_ProductId},"storeId":""}],"isRemoveLastItem":false}",
					BODY_END    
					);
			nsApi.ns_end_transaction("CartItemUpdate_Qty", NS_AUTO_STATUS);
			nsApi.ns_page_think_time(0);

			
		}
		return status;
	}

}
