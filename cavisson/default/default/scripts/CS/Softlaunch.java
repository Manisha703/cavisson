// Converted from Acc_Services_End_To_End_C/Softlaunch.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: Softlaunch
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;


public class Softlaunch implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	int i = nsApi.ns_get_random_number_int(1,100);
        if (i<=50)
{
	nsApi.ns_start_transaction("Softlaunch");
     nsApi.ns_web_url ("Softlaunch",
        "URL=https://{AccServiceHostUrl}/v1/profile/softlaunch",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=isNewExperience: True",
         "HEADER=X-SESSIONID: {SessionID11}",
         "HEADER=X-PROFILEID: {ProfileID11}",
         "BODY=$CAVINCLUDE$=Softlaunch.json",
);
    nsApi.ns_end_transaction("Softlaunch", NS_AUTO_STATUS);
}
else
{
    nsApi.ns_start_transaction("Without_Softlaunch");
    nsApi.ns_web_url ("Without_Softlaunch",
        "URL=https://{AccServiceHostUrl}/v1/profile/softlaunch",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "HEADER=X-SESSIONID: {SessionID11}",
         "HEADER=X-PROFILEID: {ProfileID11}",
         "BODY=$CAVINCLUDE$=Softlaunch.json",
);
    nsApi.ns_end_transaction("Without_Softlaunch", NS_AUTO_STATUS);
}
	return status;

}

}
