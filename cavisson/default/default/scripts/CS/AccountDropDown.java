// Converted from Acc_Services_End_To_End_C/AccountDropDown.c on Wed May  5 22:43:12 2021
/*-----------------------------------------------------------------------------
    Name: CreateAccountNew
    Recorded By: netstorm
    Date of recording: 01/09/2013 04:46:53
    Flow details:
    Modification History:
  
-----------------------------------------------------------------------------*/



package CS;
import pacJnvmApi.NSApi;

public class AccountDropDown implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
	int status = 0;
	nsApi.ns_start_transaction("AccountDropDown");
     nsApi.ns_web_url ("AccountDropDown",
        "URL=https://{AccServiceHostUrl}/v1/kohlswallet/info",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
        "HEADER=correlation-id: AccountDropDown_{CorrChar}{CorrID}",
         "HEADER=X-SESSIONID: {SessionID}",
          "HEADER=X-PROFILEID: {ProfileID}", 
          "HEADER=profileId: {profileid}",

);
    nsApi.ns_end_transaction("AccountDropDown", NS_AUTO_STATUS);
    return status;

}

}
