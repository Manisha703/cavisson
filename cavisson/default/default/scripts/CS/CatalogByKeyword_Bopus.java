package CS;
import pacJnvmApi.NSApi;

public class CatalogByKeyword_Bopus implements NsFlow
{
	public int execute(NSApi nsApi) throws Exception
	{
		int status = 0;
		status = nsApi.ns_start_transaction("CatalogByKeyword_Bopus");
		status = nsApi.ns_web_url("CatalogByKeyword_Bopus",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?keyword={Keyword_FP}&storeNum={StoreId_FP}",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=bypass_cache: false",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: OAPI_CatalogByKeyword_Bopus1_{CorrChar}{CorrID}"
				);

		if(nsApi.ns_eval_string("{CollectionProductSP}").equalsIgnoreCase(""))
		{    
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_Bopus", NS_AUTO_STATUS, "CatalogByKeyword_Bopus_WithCollection");
		}

		else
		{
			status = nsApi.ns_end_transaction("CatalogByKeyword_Bopus", NS_AUTO_STATUS);
		}
		status = nsApi.ns_page_think_time(0);

		status = nsApi.ns_start_transaction("CatalogByKeyword_Bopus_3");
		status = nsApi.ns_web_url("CatalogByKeyword_Bopus_3",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?keyword={Keyword_FP}&storeNum=323,272,132",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: CatalogByKeyword_Bopus_3_{CorrChar}{CorrID}"
				);

		if(nsApi.ns_eval_string("{CollectionProductSP}").equalsIgnoreCase(""))
		{    
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_Bopus_3", NS_AUTO_STATUS, "CatalogByKeyword_Bopus_3_WithCollection");
		}

		else
		{
			status = nsApi.ns_end_transaction("CatalogByKeyword_Bopus_3", NS_AUTO_STATUS);
		}
		status = nsApi.ns_page_think_time(0);

		status = nsApi.ns_start_transaction("CatalogByKeyword_Bopus_5");
		status = nsApi.ns_web_url("CatalogByKeyword_Bopus_5",
				"URL=http://{SnBServiceHostUrl}/v1/catalog?keyword={Keyword_FP}&storeNum=323,272,132,7,10",
				"HEADER=Accept: application/json",
				"HEADER=Content-Type: application/json",
				"HEADER=X-APP-API_KEY: {key}",
				"HEADER=correlation_id: CatalogByKeyword_Bopus_5_{CorrChar}{CorrID}"
				);

		if(nsApi.ns_eval_string("{CollectionProductSP}").equalsIgnoreCase(""))
		{    
			status = nsApi.ns_end_transaction_as("CatalogByKeyword_Bopus_5", NS_AUTO_STATUS, "CatalogByKeyword_Bopus_5_WithCollection");
		}

		else
		{
			status = nsApi.ns_end_transaction("CatalogByKeyword_Bopus_5", NS_AUTO_STATUS);
		}
		status = nsApi.ns_page_think_time(0);
		return status ;
	}
}
