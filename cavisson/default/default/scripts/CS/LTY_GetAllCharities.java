// Converted from Acc_Services_End_To_End_C/LTY_GetAllCharities.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;

public class LTY_GetAllCharities implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
        int status = 0;
        nsApi.ns_start_transaction("LTY_GetAllCharities");
        nsApi.ns_web_url ("LTY_GetAllCharities",
        "URL=https://{AccServiceHostUrl}/v1/loyalty/{LPFLTY}/getAllCharities?accountType=charity",
          "METHOD=GET",
         "HEADER=Accept: application/json",
        "HEADER=Content-Type: application/json",
         "HEADER=X-SESSIONID: {LPFSessionID1}",
        "HEADER=X-PROFILEID: {LPFProfileID1}",
);
    nsApi.ns_end_transaction("LTY_GetAllCharities", NS_AUTO_STATUS);
	return status;

}

}
