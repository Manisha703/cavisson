// Converted from P2HR_With_Promo_Mixes_CM_0921/WebSession.c on Mon May 17 04:06:22 2021
/*-----------------------------------------------------------------------------
Name: WebSession
Generated By: cavisson
Date of generation: 08/24/2018 10:38:09
Flow details:
Build details: 4.1.12 (build# 36)
Modification History:
-----------------------------------------------------------------------------*/


package CS;
import pacJnvmApi.NSApi;




public class WebSession implements NsFlow
{
	private boolean debug = true;
	public int execute(NSApi nsApi) throws Exception
	{

		int status = 0;

		if (debug) return status;
		nsApi.ns_start_transaction("Web_HomeJSP");
		nsApi.ns_web_url ("Web_Home",
				"METHOD=POST",
				"URL=https://{UIHostUrl}/web/session.jsp?lpf=v2",
				"HEADER=Accept:application/json",
				"HEADER=Accept-Encoding:gzip, deflate, br",
				"HEADER=Accept-Language:en-US,en;q=0.5",
				"HEADER=Content-Type:application/json",
				BODY_BEGIN,
				"{}",
				BODY_END,
				);
		nsApi.ns_end_transaction("Web_HomeJSP", NS_AUTO_STATUS);
		nsApi.ns_page_think_time(0);

          return status;
	}

}
