// Converted from Acc_Services_End_To_End_C/GiftRegistry.c on Wed May  5 22:43:12 2021

package CS;
import pacJnvmApi.NSApi;


public class GiftRegistry implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
{
       int status = 0;
       nsApi.ns_start_transaction("GiftRegistry");
       nsApi.ns_web_url ("GiftRegistry",
         "URL=https://{AccServiceHostUrl}/v1/message/email/giftRegistry",
         "METHOD=POST",
         "HEADER=Accept: application/json",
         "HEADER=Content-Type: application/json",
         "BODY=$CAVINCLUDE$=GiftRegistry.json",
);
    nsApi.ns_end_transaction("GiftRegistry", NS_AUTO_STATUS);
    return status;

}


}
