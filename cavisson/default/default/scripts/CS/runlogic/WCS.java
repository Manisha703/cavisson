/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class WCS
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the WCSInternalOmnitureJSP class
    WCSInternalOmnitureJSP WCSInternalOmnitureJSPObj = new WCSInternalOmnitureJSP();
    //Initialise the BrandLanding class
    BrandLanding BrandLandingObj = new BrandLanding();
    //Initialise the GiftRegistryWcs class
    GiftRegistryWcs GiftRegistryWcsObj = new GiftRegistryWcs();
    //Initialise the HomePage class
    HomePage HomePageObj = new HomePage();
    //Initialise the SaleEvent class
    SaleEvent SaleEventObj = new SaleEvent();
    //Initialise the SpotlightImage class
    SpotlightImage SpotlightImageObj = new SpotlightImage();
    //Initialise the WCS_internal class
    WCS_internal WCS_internalObj = new WCS_internal();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence block - Block1

            //Executing percent block - WCSBlock
            int WCSBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

            //"Percentage random number for block - WCSBlock = %d", WCSBlockpercent

            if(WCSBlockpercent <= 6000)
            {
                WCSInternalOmnitureJSPObj.execute(nsApi);
            }
            else if(WCSBlockpercent <= 9000)
            {

                    //Executing percent block - WCSBlock2 (pct value = 30.0%)
                    int WCSBlock2percent = nsApi.ns_get_random_number_int(1, 10000);

                    //"Percentage random number for block - WCSBlock2 = %d", WCSBlock2percent

                    if(WCSBlock2percent <= 400)
                    {
                        BrandLandingObj.execute(nsApi);
                    }
                    else if(WCSBlock2percent <= 400)
                    {
                        GiftRegistryWcsObj.execute(nsApi);
                    }
                    else if(WCSBlock2percent <= 1300)
                    {
                        HomePageObj.execute(nsApi);
                    }
                    else if(WCSBlock2percent <= 10000)
                    {
                        SaleEventObj.execute(nsApi);
                    }
            }
            else if(WCSBlockpercent <= 9000)
            {
                SpotlightImageObj.execute(nsApi);
            }
            else if(WCSBlockpercent <= 10000)
            {

                    //Executing sequence block - wcs_new_url (pct value = 10.0%)
                    //Executing flow - WCS_internal
                    WCS_internalObj.execute(nsApi);
            }

        //logging
        nsApi.ns_end_session();
    }

}
