/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class Solr
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the SearchPersonalization class
    SearchPersonalization SearchPersonalizationObj = new SearchPersonalization();
    //Initialise the SearchPersonalizationWeb class
    SearchPersonalizationWeb SearchPersonalizationWebObj = new SearchPersonalizationWeb();
    //Initialise the SearchWeb class
    SearchWeb SearchWebObj = new SearchWeb();
    //Initialise the Search class
    Search SearchObj = new Search();
    //Initialise the BrowsePersonalisation class
    BrowsePersonalisation BrowsePersonalisationObj = new BrowsePersonalisation();
    //Initialise the BrowsePersonalisationWeb class
    BrowsePersonalisationWeb BrowsePersonalisationWebObj = new BrowsePersonalisationWeb();
    //Initialise the BrowseWeb class
    BrowseWeb BrowseWebObj = new BrowseWeb();
    //Initialise the Browse class
    Browse BrowseObj = new Browse();
    //Initialise the TypeAheadQuery1 class
    TypeAheadQuery1 TypeAheadQuery1Obj = new TypeAheadQuery1();
    //Initialise the TypeAheadQuery2 class
    TypeAheadQuery2 TypeAheadQuery2Obj = new TypeAheadQuery2();
    //Initialise the TypeAheadQuery3 class
    TypeAheadQuery3 TypeAheadQuery3Obj = new TypeAheadQuery3();
    //Initialise the TypeAheadQuery4 class
    TypeAheadQuery4 TypeAheadQuery4Obj = new TypeAheadQuery4();
    //Initialise the TypeAheadQuery5 class
    TypeAheadQuery5 TypeAheadQuery5Obj = new TypeAheadQuery5();
    //Initialise the TypeAheadQuery6 class
    TypeAheadQuery6 TypeAheadQuery6Obj = new TypeAheadQuery6();
    //Initialise the TypeAheadQuery7 class
    TypeAheadQuery7 TypeAheadQuery7Obj = new TypeAheadQuery7();
    //Initialise the TypeAheadQuery_GenderProp_1 class
    TypeAheadQuery_GenderProp_1 TypeAheadQuery_GenderProp_1Obj = new TypeAheadQuery_GenderProp_1();
    //Initialise the TypeAheadQuery_GenderProp_2 class
    TypeAheadQuery_GenderProp_2 TypeAheadQuery_GenderProp_2Obj = new TypeAheadQuery_GenderProp_2();
    //Initialise the VisualTypeahead class
    VisualTypeahead VisualTypeaheadObj = new VisualTypeahead();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing percent block - Start
        int Startpercent = nsApi.ns_get_random_number_int(1, 10000);

        //"Percentage random number for block - Start = %d", Startpercent

        if(Startpercent <= 4000)
        {

                //Executing percent block - SearchBlock (pct value = 40.0%)
                int SearchBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - SearchBlock = %d", SearchBlockpercent

                if(SearchBlockpercent <= 1500)
                {
                    SearchPersonalizationObj.execute(nsApi);
                }
                else if(SearchBlockpercent <= 3000)
                {
                    SearchPersonalizationWebObj.execute(nsApi);
                }
                else if(SearchBlockpercent <= 8000)
                {
                    SearchWebObj.execute(nsApi);
                }
                else if(SearchBlockpercent <= 10000)
                {
                    SearchObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 8000)
        {

                //Executing percent block - BrowseBlock (pct value = 40.0%)
                int BrowseBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - BrowseBlock = %d", BrowseBlockpercent

                if(BrowseBlockpercent <= 1500)
                {
                    BrowsePersonalisationObj.execute(nsApi);
                }
                else if(BrowseBlockpercent <= 3000)
                {
                    BrowsePersonalisationWebObj.execute(nsApi);
                }
                else if(BrowseBlockpercent <= 8000)
                {
                    BrowseWebObj.execute(nsApi);
                }
                else if(BrowseBlockpercent <= 10000)
                {
                    BrowseObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 9500)
        {

                //Executing percent block - TypeaheadBlock (pct value = 15.0%)
                int TypeaheadBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - TypeaheadBlock = %d", TypeaheadBlockpercent

                if(TypeaheadBlockpercent <= 1500)
                {
                    TypeAheadQuery1Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 3000)
                {
                    TypeAheadQuery2Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 4000)
                {
                    TypeAheadQuery3Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 5000)
                {
                    TypeAheadQuery4Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 6500)
                {
                    TypeAheadQuery5Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 8000)
                {
                    TypeAheadQuery6Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 8000)
                {
                    TypeAheadQuery7Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 10000)
                {
                    TypeAheadQuery_GenderProp_1Obj.execute(nsApi);
                }
                else if(TypeaheadBlockpercent <= 10000)
                {
                    TypeAheadQuery_GenderProp_2Obj.execute(nsApi);
                }
        }
        else if(Startpercent <= 10000)
        {
            VisualTypeaheadObj.execute(nsApi);
        }

        //logging
        nsApi.ns_end_session();
    }

}
