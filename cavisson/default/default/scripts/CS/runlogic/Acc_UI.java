/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class Acc_UI
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the PercentageControl class
    PercentageControl PercentageControlObj = new PercentageControl();
    //Initialise the RegLogin class
    RegLogin RegLoginObj = new RegLogin();
    //Initialise the RegLogin_Loyalty class
    RegLogin_Loyalty RegLogin_LoyaltyObj = new RegLogin_Loyalty();
    //Initialise the RegLogin_NonLoyalty_KCC class
    RegLogin_NonLoyalty_KCC RegLogin_NonLoyalty_KCCObj = new RegLogin_NonLoyalty_KCC();
    //Initialise the SoftLogin_Cookie class
    SoftLogin_Cookie SoftLogin_CookieObj = new SoftLogin_Cookie();
    //Initialise the Soft_LoggedIn class
    Soft_LoggedIn Soft_LoggedInObj = new Soft_LoggedIn();
    //Initialise the My_Info class
    My_Info My_InfoObj = new My_Info();
    //Initialise the Annual_Gross_Income class
    Annual_Gross_Income Annual_Gross_IncomeObj = new Annual_Gross_Income();
    //Initialise the AddAddress class
    AddAddress AddAddressObj = new AddAddress();
    //Initialise the EditShipAddress class
    EditShipAddress EditShipAddressObj = new EditShipAddress();
    //Initialise the RemoveShippingAddress class
    RemoveShippingAddress RemoveShippingAddressObj = new RemoveShippingAddress();
    //Initialise the AddKohlsChargeCard class
    AddKohlsChargeCard AddKohlsChargeCardObj = new AddKohlsChargeCard();
    //Initialise the AddVisaCardDetails class
    AddVisaCardDetails AddVisaCardDetailsObj = new AddVisaCardDetails();
    //Initialise the PurchaseHistory class
    PurchaseHistory PurchaseHistoryObj = new PurchaseHistory();
    //Initialise the BillinginfoAndPayment class
    BillinginfoAndPayment BillinginfoAndPaymentObj = new BillinginfoAndPayment();
    //Initialise the KohlsRewards class
    KohlsRewards KohlsRewardsObj = new KohlsRewards();
    //Initialise the Wallet_getMyWalletDetails class
    Wallet_getMyWalletDetails Wallet_getMyWalletDetailsObj = new Wallet_getMyWalletDetails();
    //Initialise the Wallet_AddGiftCard class
    Wallet_AddGiftCard Wallet_AddGiftCardObj = new Wallet_AddGiftCard();
    //Initialise the Wallet_AddKohlsCash class
    Wallet_AddKohlsCash Wallet_AddKohlsCashObj = new Wallet_AddKohlsCash();
    //Initialise the Wallet_AddOffer class
    Wallet_AddOffer Wallet_AddOfferObj = new Wallet_AddOffer();
    //Initialise the Skip_Functionality class
    Skip_Functionality Skip_FunctionalityObj = new Skip_Functionality();
    //Initialise the Wallet_AddOffer_MUPC class
    Wallet_AddOffer_MUPC Wallet_AddOffer_MUPCObj = new Wallet_AddOffer_MUPC();
    //Initialise the CreateAccount class
    CreateAccount CreateAccountObj = new CreateAccount();
    //Initialise the SessionJsp class
    SessionJsp SessionJspObj = new SessionJsp();
    //Initialise the UserCheckStatus class
    UserCheckStatus UserCheckStatusObj = new UserCheckStatus();
    //Initialise the WcsInternal_Home class
    WcsInternal_Home WcsInternal_HomeObj = new WcsInternal_Home();
    //Initialise the WebSession class
    WebSession WebSessionObj = new WebSession();
    //Initialise the Recommedndation_Home class
    Recommedndation_Home Recommedndation_HomeObj = new Recommedndation_Home();
    //Initialise the ZineOne_Home class
    ZineOne_Home ZineOne_HomeObj = new ZineOne_Home();
    //Initialise the BrowsetoCatalogPage class
    BrowsetoCatalogPage BrowsetoCatalogPageObj = new BrowsetoCatalogPage();
    //Initialise the Recommedndation_SaleEvent class
    Recommedndation_SaleEvent Recommedndation_SaleEventObj = new Recommedndation_SaleEvent();
    //Initialise the CatalogPagination class
    CatalogPagination CatalogPaginationObj = new CatalogPagination();
    //Initialise the CatalogSortBy class
    CatalogSortBy CatalogSortByObj = new CatalogSortBy();
    //Initialise the CatalogRefines class
    CatalogRefines CatalogRefinesObj = new CatalogRefines();
    //Initialise the CatalogView120items class
    CatalogView120items CatalogView120itemsObj = new CatalogView120items();
    //Initialise the Recommedndation_PMP class
    Recommedndation_PMP Recommedndation_PMPObj = new Recommedndation_PMP();
    //Initialise the CatalogPDP class
    CatalogPDP CatalogPDPObj = new CatalogPDP();
    //Initialise the Availability_pickup class
    Availability_pickup Availability_pickupObj = new Availability_pickup();
    //Initialise the ZineOne_PDP class
    ZineOne_PDP ZineOne_PDPObj = new ZineOne_PDP();
    //Initialise the SearchByKeyword class
    SearchByKeyword SearchByKeywordObj = new SearchByKeyword();
    //Initialise the SearchPDP class
    SearchPDP SearchPDPObj = new SearchPDP();
    //Initialise the Recommedndation_PDP class
    Recommedndation_PDP Recommedndation_PDPObj = new Recommedndation_PDP();
    //Initialise the Search_PID_1_50 class
    Search_PID_1_50 Search_PID_1_50Obj = new Search_PID_1_50();
    //Initialise the Search_PID_51_100 class
    Search_PID_51_100 Search_PID_51_100Obj = new Search_PID_51_100();
    //Initialise the Search_PID_101_200 class
    Search_PID_101_200 Search_PID_101_200Obj = new Search_PID_101_200();
    //Initialise the Search_PID_501_1000 class
    Search_PID_501_1000 Search_PID_501_1000Obj = new Search_PID_501_1000();
    //Initialise the Search_PID_1000 class
    Search_PID_1000 Search_PID_1000Obj = new Search_PID_1000();
    //Initialise the Search_PID_201_500 class
    Search_PID_201_500 Search_PID_201_500Obj = new Search_PID_201_500();
    //Initialise the Recommedndation_Search class
    Recommedndation_Search Recommedndation_SearchObj = new Recommedndation_Search();
    //Initialise the TypeHeadSolr class
    TypeHeadSolr TypeHeadSolrObj = new TypeHeadSolr();
    //Initialise the PDPOnly class
    PDPOnly PDPOnlyObj = new PDPOnly();
    //Initialise the AddtoBag class
    AddtoBag AddtoBagObj = new AddtoBag();
    //Initialise the ZineOne_AddtoBag class
    ZineOne_AddtoBag ZineOne_AddtoBagObj = new ZineOne_AddtoBag();
    //Initialise the PersistentBar class
    PersistentBar PersistentBarObj = new PersistentBar();
    //Initialise the Persistent_bar_components class
    Persistent_bar_components Persistent_bar_componentsObj = new Persistent_bar_components();
    //Initialise the ShopingCart class
    ShopingCart ShopingCartObj = new ShopingCart();
    //Initialise the CartItemRemove class
    CartItemRemove CartItemRemoveObj = new CartItemRemove();
    //Initialise the Recommedndation_CNC class
    Recommedndation_CNC Recommedndation_CNCObj = new Recommedndation_CNC();
    //Initialise the Checkout class
    Checkout CheckoutObj = new Checkout();
    //Initialise the SoftLogin_SingedInCheckout class
    SoftLogin_SingedInCheckout SoftLogin_SingedInCheckoutObj = new SoftLogin_SingedInCheckout();
    //Initialise the CartItemRemove_LowQty class
    CartItemRemove_LowQty CartItemRemove_LowQtyObj = new CartItemRemove_LowQty();
    //Initialise the ApplyPromoCode class
    ApplyPromoCode ApplyPromoCodeObj = new ApplyPromoCode();
    //Initialise the ApplyKohlsCash class
    ApplyKohlsCash ApplyKohlsCashObj = new ApplyKohlsCash();
    //Initialise the Apply_SUPC class
    Apply_SUPC Apply_SUPCObj = new Apply_SUPC();
    //Initialise the AddAddress_onCheckout class
    AddAddress_onCheckout AddAddress_onCheckoutObj = new AddAddress_onCheckout();
    //Initialise the ContinueCheckout class
    ContinueCheckout ContinueCheckoutObj = new ContinueCheckout();
    //Initialise the Recommedndation_CNC2 class
    Recommedndation_CNC2 Recommedndation_CNC2Obj = new Recommedndation_CNC2();
    //Initialise the AddPaymentCard_OnCheckout class
    AddPaymentCard_OnCheckout AddPaymentCard_OnCheckoutObj = new AddPaymentCard_OnCheckout();
    //Initialise the AddBillingAddress_OnCheckout class
    AddBillingAddress_OnCheckout AddBillingAddress_OnCheckoutObj = new AddBillingAddress_OnCheckout();
    //Initialise the CartItemUpdate_Qty class
    CartItemUpdate_Qty CartItemUpdate_QtyObj = new CartItemUpdate_Qty();
    //Initialise the ContinueToReviewOrder class
    ContinueToReviewOrder ContinueToReviewOrderObj = new ContinueToReviewOrder();
    //Initialise the PlaceOrder_SL class
    PlaceOrder_SL PlaceOrder_SLObj = new PlaceOrder_SL();
    //Initialise the OrderConfirm class
    OrderConfirm OrderConfirmObj = new OrderConfirm();
    //Initialise the Logout class
    Logout LogoutObj = new Logout();
    //Initialise the SetMyStore class
    SetMyStore SetMyStoreObj = new SetMyStore();
    //Initialise the Search_BopusProduct class
    Search_BopusProduct Search_BopusProductObj = new Search_BopusProduct();
    //Initialise the BopusAddtoBag class
    BopusAddtoBag BopusAddtoBagObj = new BopusAddtoBag();
    //Initialise the Search_BossProduct class
    Search_BossProduct Search_BossProductObj = new Search_BossProduct();
    //Initialise the BossAddtoBag class
    BossAddtoBag BossAddtoBagObj = new BossAddtoBag();
    //Initialise the Home class
    Home HomeObj = new Home();
    //Initialise the CartItem_ShipItFaster class
    CartItem_ShipItFaster CartItem_ShipItFasterObj = new CartItem_ShipItFaster();
    //Initialise the PlaceOrder_Reg class
    PlaceOrder_Reg PlaceOrder_RegObj = new PlaceOrder_Reg();
    //Initialise the RegLogin_Bopus class
    RegLogin_Bopus RegLogin_BopusObj = new RegLogin_Bopus();
    //Initialise the UnReg_Cookie class
    UnReg_Cookie UnReg_CookieObj = new UnReg_Cookie();
    //Initialise the GuestCheckout_SetSession class
    GuestCheckout_SetSession GuestCheckout_SetSessionObj = new GuestCheckout_SetSession();
    //Initialise the UnReg_ContinueCheckout class
    UnReg_ContinueCheckout UnReg_ContinueCheckoutObj = new UnReg_ContinueCheckout();
    //Initialise the UnReg_Orderreview class
    UnReg_Orderreview UnReg_OrderreviewObj = new UnReg_Orderreview();
    //Initialise the UnReg_placeorder class
    UnReg_placeorder UnReg_placeorderObj = new UnReg_placeorder();
    //Initialise the UnReg_Order_confirm class
    UnReg_Order_confirm UnReg_Order_confirmObj = new UnReg_Order_confirm();
    //Initialise the UnReg_Bopus_ContinueCheckout class
    UnReg_Bopus_ContinueCheckout UnReg_Bopus_ContinueCheckoutObj = new UnReg_Bopus_ContinueCheckout();
    //Initialise the UnReg_Bopus_Orderreview class
    UnReg_Bopus_Orderreview UnReg_Bopus_OrderreviewObj = new UnReg_Bopus_Orderreview();
    //Initialise the UnReg_placeorder_bopus class
    UnReg_placeorder_bopus UnReg_placeorder_bopusObj = new UnReg_placeorder_bopus();
    //Initialise the SearchPDP_All_hit class
    SearchPDP_All_hit SearchPDP_All_hitObj = new SearchPDP_All_hit();
    //Initialise the SignInCheckout class
    SignInCheckout SignInCheckoutObj = new SignInCheckout();
    //Initialise the SingedInCheckout class
    SingedInCheckout SingedInCheckoutObj = new SingedInCheckout();
    //Initialise the RegLogin_VGC class
    RegLogin_VGC RegLogin_VGCObj = new RegLogin_VGC();
    //Initialise the VGC_BrowseNAddtoBag class
    VGC_BrowseNAddtoBag VGC_BrowseNAddtoBagObj = new VGC_BrowseNAddtoBag();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing percent block - Start
        int Startpercent = nsApi.ns_get_random_number_int(1, 10000);

        //"Percentage random number for block - Start = %d", Startpercent

        if(Startpercent <= 10000)
        {

                //Executing percent block - Acc (pct value = 100.0%)
                int Accpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - Acc = %d", Accpercent

                if(Accpercent <= 9500)
                {

                        //Executing sequence block - RegisteredUser (pct value = 95.0%)
                        //Executing flow - PercentageControl
                        PercentageControlObj.execute(nsApi);

                            //Executing percent block - RegisteredLogin (pct value = 95.0%)
                            int RegisteredLoginpercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - RegisteredLogin = %d", RegisteredLoginpercent

                            if(RegisteredLoginpercent <= 6000)
                            {
                                RegLoginObj.execute(nsApi);
                            }
                            else if(RegisteredLoginpercent <= 6000)
                            {
                                RegLogin_LoyaltyObj.execute(nsApi);
                            }
                            else if(RegisteredLoginpercent <= 6000)
                            {
                                RegLogin_NonLoyalty_KCCObj.execute(nsApi);
                            }
                            else if(RegisteredLoginpercent <= 10000)
                            {

                                    //Executing sequence block - Softlogin (pct value = 40.0%)
                                    //Executing flow - SoftLogin_Cookie
                                    SoftLogin_CookieObj.execute(nsApi);
                                    //Executing flow - Soft_LoggedIn
                                    Soft_LoggedInObj.execute(nsApi);
                            }

                            //Executing percent block - AccointSettingN (pct value = 95.0%)
                            int AccointSettingNpercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - AccointSettingN = %d", AccointSettingNpercent

                            if(AccointSettingNpercent <= 2500)
                            {

                                    //Executing sequence block - AccountSettings (pct value = 25.0%)
                                    //Executing flow - My_Info
                                    My_InfoObj.execute(nsApi);
                                    //Executing flow - Annual_Gross_Income
                                    Annual_Gross_IncomeObj.execute(nsApi);
                                    //Executing flow - AddAddress
                                    AddAddressObj.execute(nsApi);
                                    //Executing flow - EditShipAddress
                                    EditShipAddressObj.execute(nsApi);
                                    //Executing flow - RemoveShippingAddress
                                    RemoveShippingAddressObj.execute(nsApi);
                                    //Executing flow - AddKohlsChargeCard
                                    AddKohlsChargeCardObj.execute(nsApi);
                                    //Executing flow - AddVisaCardDetails
                                    AddVisaCardDetailsObj.execute(nsApi);
                                    //Executing flow - PurchaseHistory
                                    PurchaseHistoryObj.execute(nsApi);
                                    //Executing flow - BillinginfoAndPayment
                                    BillinginfoAndPaymentObj.execute(nsApi);
                                    //Executing flow - KohlsRewards
                                    KohlsRewardsObj.execute(nsApi);

                                        //Executing percent block - Wallet_flow (pct value = 25.0%)
                                        int Wallet_flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - Wallet_flow = %d", Wallet_flowpercent

                                        if(Wallet_flowpercent <= 9000)
                                        {

                                                //Executing sequence block - Wallet (pct value = 90.0%)
                                                //Executing flow - Wallet_getMyWalletDetails
                                                Wallet_getMyWalletDetailsObj.execute(nsApi);
                                                //Executing flow - Wallet_AddGiftCard
                                                Wallet_AddGiftCardObj.execute(nsApi);
                                                //Executing flow - Wallet_AddKohlsCash
                                                Wallet_AddKohlsCashObj.execute(nsApi);
                                                //Executing flow - Wallet_AddOffer
                                                Wallet_AddOfferObj.execute(nsApi);
                                        }
                                        else if(Wallet_flowpercent <= 9000)
                                        {
                                            Skip_FunctionalityObj.execute(nsApi);
                                        }
                                        else if(Wallet_flowpercent <= 10000)
                                        {
                                            Wallet_AddOffer_MUPCObj.execute(nsApi);
                                        }
                            }
                            else if(AccointSettingNpercent <= 10000)
                            {
                                Skip_FunctionalityObj.execute(nsApi);
                            }
                }
                else if(Accpercent <= 10000)
                {

                        //Executing sequence block - AccountCreation (pct value = 5.0%)
                        //Executing flow - PercentageControl
                        PercentageControlObj.execute(nsApi);
                        //Executing flow - CreateAccount
                        CreateAccountObj.execute(nsApi);

                            //Executing sequence block - AccountSettings (pct value = 5.0%)
                            //Executing flow - My_Info
                            My_InfoObj.execute(nsApi);
                            //Executing flow - Annual_Gross_Income
                            Annual_Gross_IncomeObj.execute(nsApi);
                            //Executing flow - AddAddress
                            AddAddressObj.execute(nsApi);
                            //Executing flow - EditShipAddress
                            EditShipAddressObj.execute(nsApi);
                            //Executing flow - RemoveShippingAddress
                            RemoveShippingAddressObj.execute(nsApi);
                            //Executing flow - AddKohlsChargeCard
                            AddKohlsChargeCardObj.execute(nsApi);
                            //Executing flow - AddVisaCardDetails
                            AddVisaCardDetailsObj.execute(nsApi);
                            //Executing flow - PurchaseHistory
                            PurchaseHistoryObj.execute(nsApi);
                            //Executing flow - BillinginfoAndPayment
                            BillinginfoAndPaymentObj.execute(nsApi);
                            //Executing flow - KohlsRewards
                            KohlsRewardsObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 10000)
        {

                //Executing percent block - Other1 (pct value = 0.0%)
                int Other1percent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - Other1 = %d", Other1percent

                if(Other1percent <= 4000)
                {

                        //Executing percent block - SoftLoginUsers (pct value = 40.0%)
                        int SoftLoginUserspercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - SoftLoginUsers = %d", SoftLoginUserspercent

                        if(SoftLoginUserspercent <= 7000)
                        {

                                //Executing sequence block - SoftLoginUsers_NormalPlaceOrder (pct value = 70.0%)
                                //Executing flow - PercentageControl
                                PercentageControlObj.execute(nsApi);
                                //Executing flow - SoftLogin_Cookie
                                SoftLogin_CookieObj.execute(nsApi);
                                //Executing flow - SessionJsp
                                SessionJspObj.execute(nsApi);
                                //Executing flow - UserCheckStatus
                                UserCheckStatusObj.execute(nsApi);
                                //Executing flow - WcsInternal_Home
                                WcsInternal_HomeObj.execute(nsApi);
                                //Executing flow - WebSession
                                WebSessionObj.execute(nsApi);
                                //Executing flow - Recommedndation_Home
                                Recommedndation_HomeObj.execute(nsApi);
                                //Executing flow - ZineOne_Home
                                ZineOne_HomeObj.execute(nsApi);

                                    //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_BagitemCount:3:< (pct value = 70.0%)"

                                    //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_BagitemCount")
                                    while(nsApi.ns_get_int_val("DV_BagitemCount") < 3)
                                    {

                                            //Executing percent block - BrowsingFlows (pct value = 70.0%)
                                            int BrowsingFlowspercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - BrowsingFlows = %d", BrowsingFlowspercent

                                            if(BrowsingFlowspercent <= 4500)
                                            {

                                                    //Executing sequence block - BrowseToPDP (pct value = 45.0%)
                                                    //Executing flow - BrowsetoCatalogPage
                                                    BrowsetoCatalogPageObj.execute(nsApi);
                                                    //Executing flow - Recommedndation_SaleEvent
                                                    Recommedndation_SaleEventObj.execute(nsApi);

                                                        //Executing sequence block - RefineCatalog (pct value = 45.0%)

                                                            //Executing percent block - RefineOperation (pct value = 45.0%)
                                                            int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                            //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                            if(RefineOperationpercent <= 1800)
                                                            {
                                                                CatalogPaginationObj.execute(nsApi);
                                                            }
                                                            else if(RefineOperationpercent <= 5800)
                                                            {
                                                                CatalogSortByObj.execute(nsApi);
                                                            }
                                                            else if(RefineOperationpercent <= 9800)
                                                            {
                                                                CatalogRefinesObj.execute(nsApi);
                                                            }
                                                            else if(RefineOperationpercent <= 9900)
                                                            {
                                                                CatalogView120itemsObj.execute(nsApi);
                                                            }
                                                            else if(RefineOperationpercent <= 10000)
                                                            {
                                                                Skip_FunctionalityObj.execute(nsApi);
                                                            }
                                                        //Executing flow - Recommedndation_PMP
                                                        Recommedndation_PMPObj.execute(nsApi);
                                                    //Executing flow - CatalogPDP
                                                    CatalogPDPObj.execute(nsApi);
                                                    //Executing flow - Availability_pickup
                                                    Availability_pickupObj.execute(nsApi);
                                                    //Executing flow - ZineOne_PDP
                                                    ZineOne_PDPObj.execute(nsApi);
                                            }
                                            else if(BrowsingFlowspercent <= 5500)
                                            {

                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 10.0%)
                                                    //Executing flow - SearchByKeyword
                                                    SearchByKeywordObj.execute(nsApi);
                                                    //Executing flow - Recommedndation_SaleEvent
                                                    Recommedndation_SaleEventObj.execute(nsApi);
                                                    //Executing flow - SearchPDP
                                                    SearchPDPObj.execute(nsApi);
                                                    //Executing flow - Recommedndation_PDP
                                                    Recommedndation_PDPObj.execute(nsApi);
                                                    //Executing flow - ZineOne_PDP
                                                    ZineOne_PDPObj.execute(nsApi);
                                            }
                                            else if(BrowsingFlowspercent <= 7000)
                                            {

                                                    //Executing sequence block - SearchByPIDS (pct value = 15.0%)

                                                        //Executing percent block - SearchByPID (pct value = 15.0%)
                                                        int SearchByPIDpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - SearchByPID = %d", SearchByPIDpercent

                                                        if(SearchByPIDpercent <= 8000)
                                                        {
                                                            Search_PID_1_50Obj.execute(nsApi);
                                                        }
                                                        else if(SearchByPIDpercent <= 9500)
                                                        {
                                                            Search_PID_51_100Obj.execute(nsApi);
                                                        }
                                                        else if(SearchByPIDpercent <= 9800)
                                                        {
                                                            Search_PID_101_200Obj.execute(nsApi);
                                                        }
                                                        else if(SearchByPIDpercent <= 9900)
                                                        {
                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                        }
                                                        else if(SearchByPIDpercent <= 10000)
                                                        {
                                                            Search_PID_1000Obj.execute(nsApi);
                                                        }
                                                        else if(SearchByPIDpercent <= 10000)
                                                        {
                                                            Search_PID_201_500Obj.execute(nsApi);
                                                        }
                                                    //Executing flow - Recommedndation_Search
                                                    Recommedndation_SearchObj.execute(nsApi);
                                                    //Executing flow - Recommedndation_PDP
                                                    Recommedndation_PDPObj.execute(nsApi);
                                            }
                                            else if(BrowsingFlowspercent <= 8500)
                                            {

                                                    //Executing sequence block - TypeHead (pct value = 15.0%)
                                                    //Executing flow - TypeHeadSolr
                                                    TypeHeadSolrObj.execute(nsApi);
                                                    //Executing flow - ZineOne_PDP
                                                    ZineOne_PDPObj.execute(nsApi);
                                            }
                                            else if(BrowsingFlowspercent <= 10000)
                                            {
                                                PDPOnlyObj.execute(nsApi);
                                            }
                                            //Executing flow - AddtoBag
                                            AddtoBagObj.execute(nsApi);
                                                //Executing flow - ZineOne_AddtoBag
                                                ZineOne_AddtoBagObj.execute(nsApi);
                                                    //Executing flow - PersistentBar
                                                    PersistentBarObj.execute(nsApi);
                                                        //Executing flow - Persistent_bar_components
                                                        Persistent_bar_componentsObj.execute(nsApi);
                                                    }

                                    //"Executing condition block - RemoveItem. NS Variable =  (pct value = 70.0%)"

                                    //"NS Variable value for block - RemoveItem = nsApi.ns_get_int_val("")
                                    if(nsApi.ns_get_int_val("DV_BagitemCount") >= 12)
                                    {

                                            //Executing sequence block - RemoveExtra_Items (pct value = 70.0%)
                                            //Executing flow - ShopingCart
                                            ShopingCartObj.execute(nsApi);
                                            //Executing flow - CartItemRemove
                                            CartItemRemoveObj.execute(nsApi);
                                    }

                                    //Executing percent block - CartNCheckout_Flow (pct value = 70.0%)
                                    int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                    if(CartNCheckout_Flowpercent <= 10000)
                                    {

                                            //Executing sequence block - CartNCheckout (pct value = 100.0%)
                                            //Executing flow - ShopingCart
                                            ShopingCartObj.execute(nsApi);
                                            //Executing flow - Persistent_bar_components
                                            Persistent_bar_componentsObj.execute(nsApi);
                                            //Executing flow - Recommedndation_CNC
                                            Recommedndation_CNCObj.execute(nsApi);
                                            //Executing flow - Checkout
                                            CheckoutObj.execute(nsApi);
                                            //Executing flow - Soft_LoggedIn
                                            Soft_LoggedInObj.execute(nsApi);
                                            //Executing flow - SoftLogin_SingedInCheckout
                                            SoftLogin_SingedInCheckoutObj.execute(nsApi);
                                            //Executing flow - CartItemRemove
                                            CartItemRemoveObj.execute(nsApi);
                                            //Executing flow - CartItemRemove_LowQty
                                            CartItemRemove_LowQtyObj.execute(nsApi);

                                                //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 100.0%)
                                                int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                if(ApplyKholsCashNPromoCodespercent <= 8000)
                                                {
                                                    ApplyPromoCodeObj.execute(nsApi);
                                                }
                                                else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                {
                                                    ApplyKohlsCashObj.execute(nsApi);
                                                }
                                                else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }
                                                else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                {
                                                    Apply_SUPCObj.execute(nsApi);
                                                }
                                            //Executing flow - AddAddress_onCheckout
                                            AddAddress_onCheckoutObj.execute(nsApi);
                                            //Executing flow - ContinueCheckout
                                            ContinueCheckoutObj.execute(nsApi);

                                                //Executing sequence block - RecomendationPayment (pct value = 100.0%)
                                                //Executing flow - Recommedndation_CNC2
                                                Recommedndation_CNC2Obj.execute(nsApi);
                                            //Executing flow - AddPaymentCard_OnCheckout
                                            AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                            //Executing flow - AddBillingAddress_OnCheckout
                                            AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                            //Executing flow - CartItemUpdate_Qty
                                            CartItemUpdate_QtyObj.execute(nsApi);
                                            //Executing flow - ContinueToReviewOrder
                                            ContinueToReviewOrderObj.execute(nsApi);

                                                //Executing sequence block - RecomendationOrderReview (pct value = 100.0%)
                                                //Executing flow - Recommedndation_CNC2
                                                Recommedndation_CNC2Obj.execute(nsApi);

                                                //Executing percent block - PlaceOrder (pct value = 100.0%)
                                                int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                if(PlaceOrderpercent <= 6000)
                                                {

                                                        //Executing sequence block - SL_PlaceOrder (pct value = 60.0%)

                                                            //Executing percent block - ApplySUPC (pct value = 60.0%)
                                                            int ApplySUPCpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                            //"Percentage random number for block - ApplySUPC = %d", ApplySUPCpercent

                                                            if(ApplySUPCpercent <= 3000)
                                                            {
                                                                Apply_SUPCObj.execute(nsApi);
                                                            }
                                                            else if(ApplySUPCpercent <= 10000)
                                                            {
                                                                Skip_FunctionalityObj.execute(nsApi);
                                                            }
                                                        //Executing flow - PlaceOrder_SL
                                                        PlaceOrder_SLObj.execute(nsApi);
                                                        //Executing flow - OrderConfirm
                                                        OrderConfirmObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_CNC
                                                        Recommedndation_CNCObj.execute(nsApi);
                                                }
                                                else if(PlaceOrderpercent <= 7500)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }
                                                else if(PlaceOrderpercent <= 9500)
                                                {

                                                        //Executing percent block - BrowsingFlows (pct value = 20.0%)
                                                        int BrowsingFlowspercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - BrowsingFlows = %d", BrowsingFlowspercent

                                                        if(BrowsingFlowspercent <= 1500)
                                                        {

                                                                //Executing sequence block - BrowseToPDP (pct value = 15.0%)
                                                                //Executing flow - BrowsetoCatalogPage
                                                                BrowsetoCatalogPageObj.execute(nsApi);
                                                                //Executing flow - Recommedndation_SaleEvent
                                                                Recommedndation_SaleEventObj.execute(nsApi);

                                                                    //Executing sequence block - RefineCatalog (pct value = 15.0%)

                                                                        //Executing percent block - RefineOperation (pct value = 15.0%)
                                                                        int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                        if(RefineOperationpercent <= 1800)
                                                                        {
                                                                            CatalogPaginationObj.execute(nsApi);
                                                                        }
                                                                        else if(RefineOperationpercent <= 5800)
                                                                        {
                                                                            CatalogSortByObj.execute(nsApi);
                                                                        }
                                                                        else if(RefineOperationpercent <= 9800)
                                                                        {
                                                                            CatalogRefinesObj.execute(nsApi);
                                                                        }
                                                                        else if(RefineOperationpercent <= 9900)
                                                                        {
                                                                            CatalogView120itemsObj.execute(nsApi);
                                                                        }
                                                                        else if(RefineOperationpercent <= 10000)
                                                                        {
                                                                            Skip_FunctionalityObj.execute(nsApi);
                                                                        }
                                                                    //Executing flow - Recommedndation_PMP
                                                                    Recommedndation_PMPObj.execute(nsApi);
                                                                //Executing flow - CatalogPDP
                                                                CatalogPDPObj.execute(nsApi);
                                                                //Executing flow - Availability_pickup
                                                                Availability_pickupObj.execute(nsApi);
                                                                //Executing flow - ZineOne_PDP
                                                                ZineOne_PDPObj.execute(nsApi);
                                                        }
                                                        else if(BrowsingFlowspercent <= 2500)
                                                        {

                                                                //Executing sequence block - SearchByKeyword_Flow (pct value = 10.0%)
                                                                //Executing flow - SearchByKeyword
                                                                SearchByKeywordObj.execute(nsApi);
                                                                //Executing flow - Recommedndation_SaleEvent
                                                                Recommedndation_SaleEventObj.execute(nsApi);
                                                                //Executing flow - SearchPDP
                                                                SearchPDPObj.execute(nsApi);
                                                                //Executing flow - Recommedndation_PDP
                                                                Recommedndation_PDPObj.execute(nsApi);
                                                                //Executing flow - ZineOne_PDP
                                                                ZineOne_PDPObj.execute(nsApi);
                                                        }
                                                        else if(BrowsingFlowspercent <= 5000)
                                                        {

                                                                //Executing sequence block - SearchByPIDS20 (pct value = 25.0%)

                                                                    //Executing percent block - SearchByPID20 (pct value = 25.0%)
                                                                    int SearchByPID20percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - SearchByPID20 = %d", SearchByPID20percent

                                                                    if(SearchByPID20percent <= 8000)
                                                                    {
                                                                        Search_PID_1_50Obj.execute(nsApi);
                                                                    }
                                                                    else if(SearchByPID20percent <= 9500)
                                                                    {
                                                                        Search_PID_51_100Obj.execute(nsApi);
                                                                    }
                                                                    else if(SearchByPID20percent <= 9800)
                                                                    {
                                                                        Search_PID_101_200Obj.execute(nsApi);
                                                                    }
                                                                    else if(SearchByPID20percent <= 9900)
                                                                    {
                                                                        Search_PID_501_1000Obj.execute(nsApi);
                                                                    }
                                                                    else if(SearchByPID20percent <= 10000)
                                                                    {
                                                                        Search_PID_1000Obj.execute(nsApi);
                                                                    }
                                                                    else if(SearchByPID20percent <= 10000)
                                                                    {
                                                                        Search_PID_201_500Obj.execute(nsApi);
                                                                    }
                                                                //Executing flow - Recommedndation_Search
                                                                Recommedndation_SearchObj.execute(nsApi);
                                                                //Executing flow - Recommedndation_PDP
                                                                Recommedndation_PDPObj.execute(nsApi);
                                                        }
                                                        else if(BrowsingFlowspercent <= 7000)
                                                        {

                                                                //Executing sequence block - TypeHead (pct value = 20.0%)
                                                                //Executing flow - TypeHeadSolr
                                                                TypeHeadSolrObj.execute(nsApi);
                                                                //Executing flow - ZineOne_PDP
                                                                ZineOne_PDPObj.execute(nsApi);
                                                        }
                                                        else if(BrowsingFlowspercent <= 10000)
                                                        {
                                                            PDPOnlyObj.execute(nsApi);
                                                        }
                                                }
                                                else if(PlaceOrderpercent <= 10000)
                                                {

                                                        //Executing percent block - SoftLoginAccountActivity (pct value = 5.0%)
                                                        int SoftLoginAccountActivitypercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - SoftLoginAccountActivity = %d", SoftLoginAccountActivitypercent

                                                        if(SoftLoginAccountActivitypercent <= 10000)
                                                        {

                                                                //Executing sequence block - Account_Sequence (pct value = 100.0%)

                                                                    //Executing sequence block - AccountSettings (pct value = 100.0%)
                                                                    //Executing flow - My_Info
                                                                    My_InfoObj.execute(nsApi);
                                                                    //Executing flow - Annual_Gross_Income
                                                                    Annual_Gross_IncomeObj.execute(nsApi);
                                                                    //Executing flow - AddAddress
                                                                    AddAddressObj.execute(nsApi);
                                                                    //Executing flow - EditShipAddress
                                                                    EditShipAddressObj.execute(nsApi);
                                                                    //Executing flow - RemoveShippingAddress
                                                                    RemoveShippingAddressObj.execute(nsApi);
                                                                    //Executing flow - AddKohlsChargeCard
                                                                    AddKohlsChargeCardObj.execute(nsApi);
                                                                    //Executing flow - AddVisaCardDetails
                                                                    AddVisaCardDetailsObj.execute(nsApi);
                                                                    //Executing flow - PurchaseHistory
                                                                    PurchaseHistoryObj.execute(nsApi);
                                                                    //Executing flow - BillinginfoAndPayment
                                                                    BillinginfoAndPaymentObj.execute(nsApi);
                                                                    //Executing flow - KohlsRewards
                                                                    KohlsRewardsObj.execute(nsApi);

                                                                        //Executing percent block - Wallet_flow (pct value = 100.0%)
                                                                        int Wallet_flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - Wallet_flow = %d", Wallet_flowpercent

                                                                        if(Wallet_flowpercent <= 10000)
                                                                        {

                                                                                //Executing sequence block - Wallet (pct value = 100.0%)
                                                                                //Executing flow - Wallet_getMyWalletDetails
                                                                                Wallet_getMyWalletDetailsObj.execute(nsApi);
                                                                                //Executing flow - Wallet_AddGiftCard
                                                                                Wallet_AddGiftCardObj.execute(nsApi);
                                                                                //Executing flow - Wallet_AddKohlsCash
                                                                                Wallet_AddKohlsCashObj.execute(nsApi);
                                                                                //Executing flow - Wallet_AddOffer
                                                                                Wallet_AddOfferObj.execute(nsApi);
                                                                        }
                                                                        else if(Wallet_flowpercent <= 10000)
                                                                        {
                                                                            Skip_FunctionalityObj.execute(nsApi);
                                                                        }
                                                        }
                                                }
                                            //Executing flow - Logout
                                            LogoutObj.execute(nsApi);
                                    }
                                    else if(CartNCheckout_Flowpercent <= 10000)
                                    {
                                        Skip_FunctionalityObj.execute(nsApi);
                                    }
                        }
                        else if(SoftLoginUserspercent <= 10000)
                        {

                                //Executing percent block - BopusNBoss (pct value = 30.0%)
                                int BopusNBosspercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - BopusNBoss = %d", BopusNBosspercent

                                if(BopusNBosspercent <= 5000)
                                {

                                        //Executing sequence block - SoftLoginUsers_BopusPlaceOrder (pct value = 50.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - SoftLogin_Cookie
                                        SoftLogin_CookieObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - WcsInternal_Home
                                        WcsInternal_HomeObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);
                                        //Executing flow - SetMyStore
                                        SetMyStoreObj.execute(nsApi);
                                        //Executing flow - Recommedndation_Home
                                        Recommedndation_HomeObj.execute(nsApi);

                                            //Executing sequence block - BrowsingFlows (pct value = 50.0%)

                                                //Executing percent block - CataLogBrowsing (pct value = 50.0%)
                                                int CataLogBrowsingpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - CataLogBrowsing = %d", CataLogBrowsingpercent

                                                if(CataLogBrowsingpercent <= 8000)
                                                {

                                                        //Executing sequence block - BrowseToPDP (pct value = 80.0%)
                                                        //Executing flow - BrowsetoCatalogPage
                                                        BrowsetoCatalogPageObj.execute(nsApi);

                                                            //Executing sequence block - RefineCatalog (pct value = 80.0%)

                                                                //Executing percent block - RefineOperation (pct value = 80.0%)
                                                                int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                if(RefineOperationpercent <= 1800)
                                                                {
                                                                    CatalogPaginationObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 5800)
                                                                {
                                                                    CatalogSortByObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9800)
                                                                {
                                                                    CatalogRefinesObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9900)
                                                                {
                                                                    CatalogView120itemsObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 10000)
                                                                {
                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                }
                                                        //Executing flow - Recommedndation_PMP
                                                        Recommedndation_PMPObj.execute(nsApi);
                                                        //Executing flow - CatalogPDP
                                                        CatalogPDPObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_PDP
                                                        Recommedndation_PDPObj.execute(nsApi);
                                                        //Executing flow - Availability_pickup
                                                        Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - ZineOne_PDP
                                                        ZineOne_PDPObj.execute(nsApi);
                                                }
                                                else if(CataLogBrowsingpercent <= 8000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }
                                                else if(CataLogBrowsingpercent <= 10000)
                                                {
                                                    PDPOnlyObj.execute(nsApi);
                                                }

                                                //Executing sequence block - SearchByKeyword_Flow (pct value = 50.0%)
                                                //Executing flow - SearchByKeyword
                                                SearchByKeywordObj.execute(nsApi);
                                                //Executing flow - SearchPDP
                                                SearchPDPObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);

                                                //Executing sequence block - SearchByPIDS19 (pct value = 50.0%)

                                                    //Executing percent block - SearchByPID19 (pct value = 50.0%)
                                                    int SearchByPID19percent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - SearchByPID19 = %d", SearchByPID19percent

                                                    if(SearchByPID19percent <= 8000)
                                                    {
                                                        Search_PID_1_50Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID19percent <= 9500)
                                                    {
                                                        Search_PID_51_100Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID19percent <= 9800)
                                                    {
                                                        Search_PID_101_200Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID19percent <= 9900)
                                                    {
                                                        Search_PID_501_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID19percent <= 10000)
                                                    {
                                                        Search_PID_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID19percent <= 10000)
                                                    {
                                                        Search_PID_201_500Obj.execute(nsApi);
                                                    }
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);

                                                //Executing sequence block - Typehead1 (pct value = 50.0%)
                                                //Executing flow - TypeHeadSolr
                                                TypeHeadSolrObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                            //Executing flow - Search_BopusProduct
                                            Search_BopusProductObj.execute(nsApi);
                                            //Executing flow - PDPOnly
                                            PDPOnlyObj.execute(nsApi);

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_Bopus_BagitemCount:1:< (pct value = 50.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_Bopus_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_Bopus_BagitemCount") < 1)
                                            {
                                                //Executing flow - Search_BopusProduct
                                                Search_BopusProductObj.execute(nsApi);
                                                    //Executing flow - ZineOne_PDP
                                                    ZineOne_PDPObj.execute(nsApi);
                                                        //Executing flow - Availability_pickup
                                                        Availability_pickupObj.execute(nsApi);
                                                            //Executing flow - BopusAddtoBag
                                                            BopusAddtoBagObj.execute(nsApi);
                                                                //Executing flow - ZineOne_AddtoBag
                                                                ZineOne_AddtoBagObj.execute(nsApi);
                                                                    //Executing flow - PersistentBar
                                                                    PersistentBarObj.execute(nsApi);
                                                                        //Executing flow - Persistent_bar_components
                                                                        Persistent_bar_componentsObj.execute(nsApi);
                                                                    }

                                            //"Executing condition block - RemoveItem. NS Variable =  (pct value = 50.0%)"

                                            //"NS Variable value for block - RemoveItem = nsApi.ns_get_int_val("")
                                            if(nsApi.ns_get_int_val("DV_Bopus_BagitemCount") >= 12)
                                            {

                                                    //Executing sequence block - RemoveExtra_Items (pct value = 50.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                            }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 50.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 10000)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 100.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - Checkout
                                                    CheckoutObj.execute(nsApi);
                                                    //Executing flow - Soft_LoggedIn
                                                    Soft_LoggedInObj.execute(nsApi);
                                                    //Executing flow - SoftLogin_SingedInCheckout
                                                    SoftLogin_SingedInCheckoutObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove_LowQty
                                                    CartItemRemove_LowQtyObj.execute(nsApi);

                                                        //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 100.0%)
                                                        int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                        if(ApplyKholsCashNPromoCodespercent <= 8000)
                                                        {
                                                            ApplyPromoCodeObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            ApplyKohlsCashObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Apply_SUPCObj.execute(nsApi);
                                                        }
                                                    //Executing flow - AddAddress_onCheckout
                                                    AddAddress_onCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueCheckout
                                                    ContinueCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddPaymentCard_OnCheckout
                                                    AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddBillingAddress_OnCheckout
                                                    AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueToReviewOrder
                                                    ContinueToReviewOrderObj.execute(nsApi);

                                                        //Executing sequence block - RecomendationOrderReview (pct value = 100.0%)
                                                        //Executing flow - Recommedndation_CNC2
                                                        Recommedndation_CNC2Obj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 100.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 4000)
                                                        {

                                                                //Executing sequence block - SL_PlaceOrder (pct value = 40.0%)

                                                                    //Executing percent block - ApplySUPC (pct value = 40.0%)
                                                                    int ApplySUPCpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - ApplySUPC = %d", ApplySUPCpercent

                                                                    if(ApplySUPCpercent <= 3000)
                                                                    {
                                                                        Apply_SUPCObj.execute(nsApi);
                                                                    }
                                                                    else if(ApplySUPCpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - PlaceOrder_SL
                                                                PlaceOrder_SLObj.execute(nsApi);
                                                                //Executing flow - OrderConfirm
                                                                OrderConfirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 5000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 7500)
                                                        {

                                                                //Executing sequence block - BrowsingFlows (pct value = 25.0%)

                                                                    //Executing percent block - CataLogBrowsing1 (pct value = 25.0%)
                                                                    int CataLogBrowsing1percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - CataLogBrowsing1 = %d", CataLogBrowsing1percent

                                                                    if(CataLogBrowsing1percent <= 8000)
                                                                    {

                                                                            //Executing sequence block - BrowseToPDP (pct value = 80.0%)
                                                                            //Executing flow - BrowsetoCatalogPage
                                                                            BrowsetoCatalogPageObj.execute(nsApi);

                                                                                //Executing sequence block - RefineCatalog (pct value = 80.0%)

                                                                                    //Executing percent block - RefineOperation (pct value = 80.0%)
                                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                                    if(RefineOperationpercent <= 1800)
                                                                                    {
                                                                                        CatalogPaginationObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 5800)
                                                                                    {
                                                                                        CatalogSortByObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9800)
                                                                                    {
                                                                                        CatalogRefinesObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9900)
                                                                                    {
                                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 10000)
                                                                                    {
                                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                                    }
                                                                            //Executing flow - Recommedndation_PMP
                                                                            Recommedndation_PMPObj.execute(nsApi);
                                                                            //Executing flow - CatalogPDP
                                                                            CatalogPDPObj.execute(nsApi);
                                                                            //Executing flow - Recommedndation_PDP
                                                                            Recommedndation_PDPObj.execute(nsApi);
                                                                            //Executing flow - Availability_pickup
                                                                            Availability_pickupObj.execute(nsApi);
                                                                            //Executing flow - ZineOne_PDP
                                                                            ZineOne_PDPObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing1percent <= 8000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing1percent <= 10000)
                                                                    {
                                                                        PDPOnlyObj.execute(nsApi);
                                                                    }

                                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 25.0%)
                                                                    //Executing flow - SearchByKeyword
                                                                    SearchByKeywordObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP
                                                                    SearchPDPObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - SearchByPIDS18 (pct value = 25.0%)

                                                                        //Executing percent block - SearchByPID18 (pct value = 25.0%)
                                                                        int SearchByPID18percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - SearchByPID18 = %d", SearchByPID18percent

                                                                        if(SearchByPID18percent <= 8000)
                                                                        {
                                                                            Search_PID_1_50Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID18percent <= 9500)
                                                                        {
                                                                            Search_PID_51_100Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID18percent <= 9800)
                                                                        {
                                                                            Search_PID_101_200Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID18percent <= 9900)
                                                                        {
                                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID18percent <= 10000)
                                                                        {
                                                                            Search_PID_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID18percent <= 10000)
                                                                        {
                                                                            Search_PID_201_500Obj.execute(nsApi);
                                                                        }
                                                                    //Executing flow - Recommedndation_PDP
                                                                    Recommedndation_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - Typehead1 (pct value = 25.0%)
                                                                    //Executing flow - TypeHeadSolr
                                                                    TypeHeadSolrObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                                //Executing flow - Search_BopusProduct
                                                                Search_BopusProductObj.execute(nsApi);
                                                                //Executing flow - PDPOnly
                                                                PDPOnlyObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing percent block - SoftLoginAccountActivity (pct value = 25.0%)
                                                                int SoftLoginAccountActivitypercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - SoftLoginAccountActivity = %d", SoftLoginAccountActivitypercent

                                                                if(SoftLoginAccountActivitypercent <= 10000)
                                                                {

                                                                        //Executing sequence block - Account_Sequence (pct value = 100.0%)

                                                                            //Executing sequence block - AccountSettings (pct value = 100.0%)
                                                                            //Executing flow - My_Info
                                                                            My_InfoObj.execute(nsApi);
                                                                            //Executing flow - Annual_Gross_Income
                                                                            Annual_Gross_IncomeObj.execute(nsApi);
                                                                            //Executing flow - AddAddress
                                                                            AddAddressObj.execute(nsApi);
                                                                            //Executing flow - EditShipAddress
                                                                            EditShipAddressObj.execute(nsApi);
                                                                            //Executing flow - RemoveShippingAddress
                                                                            RemoveShippingAddressObj.execute(nsApi);
                                                                            //Executing flow - AddKohlsChargeCard
                                                                            AddKohlsChargeCardObj.execute(nsApi);
                                                                            //Executing flow - AddVisaCardDetails
                                                                            AddVisaCardDetailsObj.execute(nsApi);
                                                                            //Executing flow - PurchaseHistory
                                                                            PurchaseHistoryObj.execute(nsApi);
                                                                            //Executing flow - BillinginfoAndPayment
                                                                            BillinginfoAndPaymentObj.execute(nsApi);
                                                                            //Executing flow - KohlsRewards
                                                                            KohlsRewardsObj.execute(nsApi);

                                                                                //Executing percent block - Wallet_flow (pct value = 100.0%)
                                                                                int Wallet_flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                //"Percentage random number for block - Wallet_flow = %d", Wallet_flowpercent

                                                                                if(Wallet_flowpercent <= 10000)
                                                                                {

                                                                                        //Executing sequence block - Wallet (pct value = 100.0%)
                                                                                        //Executing flow - Wallet_getMyWalletDetails
                                                                                        Wallet_getMyWalletDetailsObj.execute(nsApi);
                                                                                        //Executing flow - Wallet_AddGiftCard
                                                                                        Wallet_AddGiftCardObj.execute(nsApi);
                                                                                        //Executing flow - Wallet_AddKohlsCash
                                                                                        Wallet_AddKohlsCashObj.execute(nsApi);
                                                                                        //Executing flow - Wallet_AddOffer
                                                                                        Wallet_AddOfferObj.execute(nsApi);
                                                                                }
                                                                                else if(Wallet_flowpercent <= 10000)
                                                                                {
                                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                                }
                                                                }
                                                        }
                                                    //Executing flow - Logout
                                                    LogoutObj.execute(nsApi);
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                                else if(BopusNBosspercent <= 10000)
                                {

                                        //Executing sequence block - SoftLoginUsers_BossPlaceOrder (pct value = 50.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - SoftLogin_Cookie
                                        SoftLogin_CookieObj.execute(nsApi);
                                        //Executing flow - WcsInternal_Home
                                        WcsInternal_HomeObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);
                                        //Executing flow - SetMyStore
                                        SetMyStoreObj.execute(nsApi);
                                        //Executing flow - Recommedndation_Home
                                        Recommedndation_HomeObj.execute(nsApi);

                                            //Executing sequence block - BrowsingFlows (pct value = 50.0%)

                                                //Executing percent block - CataLogBrowsing14 (pct value = 50.0%)
                                                int CataLogBrowsing14percent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - CataLogBrowsing14 = %d", CataLogBrowsing14percent

                                                if(CataLogBrowsing14percent <= 10000)
                                                {

                                                        //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                        //Executing flow - BrowsetoCatalogPage
                                                        BrowsetoCatalogPageObj.execute(nsApi);

                                                            //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                if(RefineOperationpercent <= 1800)
                                                                {
                                                                    CatalogPaginationObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 5800)
                                                                {
                                                                    CatalogSortByObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9800)
                                                                {
                                                                    CatalogRefinesObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9900)
                                                                {
                                                                    CatalogView120itemsObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 10000)
                                                                {
                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                }
                                                        //Executing flow - Recommedndation_PMP
                                                        Recommedndation_PMPObj.execute(nsApi);
                                                        //Executing flow - CatalogPDP
                                                        CatalogPDPObj.execute(nsApi);
                                                        //Executing flow - Availability_pickup
                                                        Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - ZineOne_PDP
                                                        ZineOne_PDPObj.execute(nsApi);
                                                }
                                                else if(CataLogBrowsing14percent <= 10000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }

                                                //Executing sequence block - SearchByKeyword_Flow (pct value = 50.0%)
                                                //Executing flow - SearchByKeyword
                                                SearchByKeywordObj.execute(nsApi);
                                                //Executing flow - SearchPDP
                                                SearchPDPObj.execute(nsApi);
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);

                                                //Executing sequence block - SearchByPIDS17 (pct value = 50.0%)

                                                    //Executing percent block - SearchByPID17 (pct value = 50.0%)
                                                    int SearchByPID17percent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - SearchByPID17 = %d", SearchByPID17percent

                                                    if(SearchByPID17percent <= 8000)
                                                    {
                                                        Search_PID_1_50Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID17percent <= 9500)
                                                    {
                                                        Search_PID_51_100Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID17percent <= 9800)
                                                    {
                                                        Search_PID_101_200Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID17percent <= 9900)
                                                    {
                                                        Search_PID_501_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID17percent <= 10000)
                                                    {
                                                        Search_PID_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID17percent <= 10000)
                                                    {
                                                        Search_PID_201_500Obj.execute(nsApi);
                                                    }
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);

                                                //Executing sequence block - Typehead1 (pct value = 50.0%)
                                                //Executing flow - TypeHeadSolr
                                                TypeHeadSolrObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                            //Executing flow - Search_BopusProduct
                                            Search_BopusProductObj.execute(nsApi);
                                            //Executing flow - PDPOnly
                                            PDPOnlyObj.execute(nsApi);

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_Boss_BagitemCount:1:< (pct value = 50.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_Boss_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_Boss_BagitemCount") < 1)
                                            {
                                                //Executing flow - Search_BossProduct
                                                Search_BossProductObj.execute(nsApi);
                                                    //Executing flow - Availability_pickup
                                                    Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - BossAddtoBag
                                                        BossAddtoBagObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                                //Executing flow - PersistentBar
                                                                PersistentBarObj.execute(nsApi);
                                                                    //Executing flow - Persistent_bar_components
                                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                                }

                                            //"Executing condition block - RemoveItem. NS Variable =  (pct value = 50.0%)"

                                            //"NS Variable value for block - RemoveItem = nsApi.ns_get_int_val("")
                                            if(nsApi.ns_get_int_val("DV_Boss_BagitemCount") >= 12)
                                            {

                                                    //Executing sequence block - RemoveExtra_Items (pct value = 50.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                            }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 50.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 10000)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 100.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - Checkout
                                                    CheckoutObj.execute(nsApi);
                                                    //Executing flow - Soft_LoggedIn
                                                    Soft_LoggedInObj.execute(nsApi);
                                                    //Executing flow - SoftLogin_SingedInCheckout
                                                    SoftLogin_SingedInCheckoutObj.execute(nsApi);

                                                        //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 100.0%)
                                                        int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                        if(ApplyKholsCashNPromoCodespercent <= 6000)
                                                        {
                                                            ApplyPromoCodeObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            ApplyKohlsCashObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Apply_SUPCObj.execute(nsApi);
                                                        }
                                                    //Executing flow - AddAddress_onCheckout
                                                    AddAddress_onCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueCheckout
                                                    ContinueCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddPaymentCard_OnCheckout
                                                    AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddBillingAddress_OnCheckout
                                                    AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueToReviewOrder
                                                    ContinueToReviewOrderObj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 100.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 4000)
                                                        {

                                                                //Executing sequence block - SL_PlaceOrder (pct value = 40.0%)

                                                                    //Executing percent block - ApplySUPC (pct value = 40.0%)
                                                                    int ApplySUPCpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - ApplySUPC = %d", ApplySUPCpercent

                                                                    if(ApplySUPCpercent <= 3000)
                                                                    {
                                                                        Apply_SUPCObj.execute(nsApi);
                                                                    }
                                                                    else if(ApplySUPCpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - PlaceOrder_SL
                                                                PlaceOrder_SLObj.execute(nsApi);
                                                                //Executing flow - OrderConfirm
                                                                OrderConfirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 5000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 7500)
                                                        {

                                                                //Executing sequence block - SoftLoginAccountActivity (pct value = 25.0%)

                                                                    //Executing sequence block - AccountSettings (pct value = 25.0%)
                                                                    //Executing flow - My_Info
                                                                    My_InfoObj.execute(nsApi);
                                                                    //Executing flow - Annual_Gross_Income
                                                                    Annual_Gross_IncomeObj.execute(nsApi);
                                                                    //Executing flow - AddAddress
                                                                    AddAddressObj.execute(nsApi);
                                                                    //Executing flow - EditShipAddress
                                                                    EditShipAddressObj.execute(nsApi);
                                                                    //Executing flow - RemoveShippingAddress
                                                                    RemoveShippingAddressObj.execute(nsApi);
                                                                    //Executing flow - AddKohlsChargeCard
                                                                    AddKohlsChargeCardObj.execute(nsApi);
                                                                    //Executing flow - AddVisaCardDetails
                                                                    AddVisaCardDetailsObj.execute(nsApi);
                                                                    //Executing flow - PurchaseHistory
                                                                    PurchaseHistoryObj.execute(nsApi);
                                                                    //Executing flow - BillinginfoAndPayment
                                                                    BillinginfoAndPaymentObj.execute(nsApi);
                                                                    //Executing flow - KohlsRewards
                                                                    KohlsRewardsObj.execute(nsApi);

                                                                        //Executing percent block - Wallet_flow (pct value = 25.0%)
                                                                        int Wallet_flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - Wallet_flow = %d", Wallet_flowpercent

                                                                        if(Wallet_flowpercent <= 10000)
                                                                        {

                                                                                //Executing sequence block - Wallet (pct value = 100.0%)
                                                                                //Executing flow - Wallet_getMyWalletDetails
                                                                                Wallet_getMyWalletDetailsObj.execute(nsApi);
                                                                                //Executing flow - Wallet_AddGiftCard
                                                                                Wallet_AddGiftCardObj.execute(nsApi);
                                                                                //Executing flow - Wallet_AddKohlsCash
                                                                                Wallet_AddKohlsCashObj.execute(nsApi);
                                                                                //Executing flow - Wallet_AddOffer
                                                                                Wallet_AddOfferObj.execute(nsApi);
                                                                        }
                                                                        else if(Wallet_flowpercent <= 10000)
                                                                        {
                                                                            Skip_FunctionalityObj.execute(nsApi);
                                                                        }
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing sequence block - BrowsingFlows (pct value = 25.0%)

                                                                    //Executing percent block - CataLogBrowsing13 (pct value = 25.0%)
                                                                    int CataLogBrowsing13percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - CataLogBrowsing13 = %d", CataLogBrowsing13percent

                                                                    if(CataLogBrowsing13percent <= 10000)
                                                                    {

                                                                            //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                                            //Executing flow - BrowsetoCatalogPage
                                                                            BrowsetoCatalogPageObj.execute(nsApi);

                                                                                //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                                    //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                                    if(RefineOperationpercent <= 1800)
                                                                                    {
                                                                                        CatalogPaginationObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 5800)
                                                                                    {
                                                                                        CatalogSortByObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9800)
                                                                                    {
                                                                                        CatalogRefinesObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9900)
                                                                                    {
                                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 10000)
                                                                                    {
                                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                                    }
                                                                            //Executing flow - Recommedndation_PMP
                                                                            Recommedndation_PMPObj.execute(nsApi);
                                                                            //Executing flow - CatalogPDP
                                                                            CatalogPDPObj.execute(nsApi);
                                                                            //Executing flow - Availability_pickup
                                                                            Availability_pickupObj.execute(nsApi);
                                                                            //Executing flow - ZineOne_PDP
                                                                            ZineOne_PDPObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing13percent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }

                                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 25.0%)
                                                                    //Executing flow - SearchByKeyword
                                                                    SearchByKeywordObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP
                                                                    SearchPDPObj.execute(nsApi);
                                                                    //Executing flow - Recommedndation_PDP
                                                                    Recommedndation_PDPObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - SearchByPIDS16 (pct value = 25.0%)

                                                                        //Executing percent block - SearchByPID16 (pct value = 25.0%)
                                                                        int SearchByPID16percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - SearchByPID16 = %d", SearchByPID16percent

                                                                        if(SearchByPID16percent <= 8000)
                                                                        {
                                                                            Search_PID_1_50Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID16percent <= 9500)
                                                                        {
                                                                            Search_PID_51_100Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID16percent <= 9800)
                                                                        {
                                                                            Search_PID_101_200Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID16percent <= 9900)
                                                                        {
                                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID16percent <= 10000)
                                                                        {
                                                                            Search_PID_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID16percent <= 10000)
                                                                        {
                                                                            Search_PID_201_500Obj.execute(nsApi);
                                                                        }
                                                                    //Executing flow - Recommedndation_PDP
                                                                    Recommedndation_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - Typehead1 (pct value = 25.0%)
                                                                    //Executing flow - TypeHeadSolr
                                                                    TypeHeadSolrObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                                //Executing flow - Search_BopusProduct
                                                                Search_BopusProductObj.execute(nsApi);
                                                                //Executing flow - PDPOnly
                                                                PDPOnlyObj.execute(nsApi);
                                                        }
                                                    //Executing flow - Logout
                                                    LogoutObj.execute(nsApi);
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                        }
                }
                else if(Other1percent <= 10000)
                {

                        //Executing percent block - NormalUsers (pct value = 60.0%)
                        int NormalUserspercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - NormalUsers = %d", NormalUserspercent

                        if(NormalUserspercent <= 3900)
                        {

                                //Executing percent block - RegisteredUsers (pct value = 39.0%)
                                int RegisteredUserspercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - RegisteredUsers = %d", RegisteredUserspercent

                                if(RegisteredUserspercent <= 8000)
                                {

                                        //Executing sequence block - RegisteredUsers_NormalPlaceOrder (pct value = 80.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - Home
                                        HomeObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - WcsInternal_Home
                                        WcsInternal_HomeObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);
                                        //Executing flow - Recommedndation_Home
                                        Recommedndation_HomeObj.execute(nsApi);
                                        //Executing flow - ZineOne_Home
                                        ZineOne_HomeObj.execute(nsApi);

                                            //Executing percent block - RegisteredLogin (pct value = 80.0%)
                                            int RegisteredLoginpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - RegisteredLogin = %d", RegisteredLoginpercent

                                            if(RegisteredLoginpercent <= 10000)
                                            {
                                                RegLoginObj.execute(nsApi);
                                            }
                                            else if(RegisteredLoginpercent <= 10000)
                                            {
                                                RegLogin_LoyaltyObj.execute(nsApi);
                                            }
                                            else if(RegisteredLoginpercent <= 10000)
                                            {
                                                RegLogin_NonLoyalty_KCCObj.execute(nsApi);
                                            }

                                            //Executing percent block - AccointSettingN (pct value = 80.0%)
                                            int AccointSettingNpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - AccointSettingN = %d", AccointSettingNpercent

                                            if(AccointSettingNpercent <= 2500)
                                            {

                                                    //Executing sequence block - AccountSettings (pct value = 25.0%)
                                                    //Executing flow - My_Info
                                                    My_InfoObj.execute(nsApi);
                                                    //Executing flow - Annual_Gross_Income
                                                    Annual_Gross_IncomeObj.execute(nsApi);
                                                    //Executing flow - AddAddress
                                                    AddAddressObj.execute(nsApi);
                                                    //Executing flow - EditShipAddress
                                                    EditShipAddressObj.execute(nsApi);
                                                    //Executing flow - RemoveShippingAddress
                                                    RemoveShippingAddressObj.execute(nsApi);
                                                    //Executing flow - AddKohlsChargeCard
                                                    AddKohlsChargeCardObj.execute(nsApi);
                                                    //Executing flow - AddVisaCardDetails
                                                    AddVisaCardDetailsObj.execute(nsApi);
                                                    //Executing flow - PurchaseHistory
                                                    PurchaseHistoryObj.execute(nsApi);
                                                    //Executing flow - BillinginfoAndPayment
                                                    BillinginfoAndPaymentObj.execute(nsApi);
                                                    //Executing flow - KohlsRewards
                                                    KohlsRewardsObj.execute(nsApi);

                                                        //Executing percent block - Wallet_flow (pct value = 25.0%)
                                                        int Wallet_flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - Wallet_flow = %d", Wallet_flowpercent

                                                        if(Wallet_flowpercent <= 9000)
                                                        {

                                                                //Executing sequence block - Wallet (pct value = 90.0%)
                                                                //Executing flow - Wallet_getMyWalletDetails
                                                                Wallet_getMyWalletDetailsObj.execute(nsApi);
                                                                //Executing flow - Wallet_AddGiftCard
                                                                Wallet_AddGiftCardObj.execute(nsApi);
                                                                //Executing flow - Wallet_AddKohlsCash
                                                                Wallet_AddKohlsCashObj.execute(nsApi);
                                                                //Executing flow - Wallet_AddOffer
                                                                Wallet_AddOfferObj.execute(nsApi);
                                                        }
                                                        else if(Wallet_flowpercent <= 9000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(Wallet_flowpercent <= 10000)
                                                        {
                                                            Wallet_AddOffer_MUPCObj.execute(nsApi);
                                                        }
                                            }
                                            else if(AccointSettingNpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_BagitemCount:3:< (pct value = 80.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_BagitemCount") < 3)
                                            {

                                                    //Executing percent block - BrowsingFlows (pct value = 80.0%)
                                                    int BrowsingFlowspercent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - BrowsingFlows = %d", BrowsingFlowspercent

                                                    if(BrowsingFlowspercent <= 4500)
                                                    {

                                                            //Executing sequence block - BrowseToPDP (pct value = 45.0%)
                                                            //Executing flow - BrowsetoCatalogPage
                                                            BrowsetoCatalogPageObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_SaleEvent
                                                            Recommedndation_SaleEventObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);

                                                                //Executing sequence block - RefineCatalog (pct value = 45.0%)

                                                                    //Executing percent block - RefineOperation (pct value = 45.0%)
                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                    if(RefineOperationpercent <= 1800)
                                                                    {
                                                                        CatalogPaginationObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 5800)
                                                                    {
                                                                        CatalogSortByObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 9800)
                                                                    {
                                                                        CatalogRefinesObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 9900)
                                                                    {
                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - Recommedndation_PMP
                                                                Recommedndation_PMPObj.execute(nsApi);
                                                            //Executing flow - CatalogPDP
                                                            CatalogPDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - Availability_pickup
                                                            Availability_pickupObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 5500)
                                                    {

                                                            //Executing sequence block - SearchByKeyword_Flow (pct value = 10.0%)
                                                            //Executing flow - SearchByKeyword
                                                            SearchByKeywordObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_Search
                                                            Recommedndation_SearchObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);
                                                            //Executing flow - SearchPDP
                                                            SearchPDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 7000)
                                                    {

                                                            //Executing sequence block - SearchByPIDS15 (pct value = 15.0%)

                                                                //Executing percent block - SearchByPID15 (pct value = 15.0%)
                                                                int SearchByPID15percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - SearchByPID15 = %d", SearchByPID15percent

                                                                if(SearchByPID15percent <= 8000)
                                                                {
                                                                    Search_PID_1_50Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID15percent <= 9500)
                                                                {
                                                                    Search_PID_51_100Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID15percent <= 9800)
                                                                {
                                                                    Search_PID_101_200Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID15percent <= 9900)
                                                                {
                                                                    Search_PID_501_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID15percent <= 10000)
                                                                {
                                                                    Search_PID_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID15percent <= 10000)
                                                                {
                                                                    Search_PID_201_500Obj.execute(nsApi);
                                                                }
                                                            //Executing flow - Recommedndation_Search
                                                            Recommedndation_SearchObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 8500)
                                                    {

                                                            //Executing sequence block - TypeHead (pct value = 15.0%)
                                                            //Executing flow - TypeHeadSolr
                                                            TypeHeadSolrObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 10000)
                                                    {
                                                        PDPOnlyObj.execute(nsApi);
                                                    }
                                                    //Executing flow - AddtoBag
                                                    AddtoBagObj.execute(nsApi);
                                                        //Executing flow - ZineOne_AddtoBag
                                                        ZineOne_AddtoBagObj.execute(nsApi);
                                                            //Executing flow - PersistentBar
                                                            PersistentBarObj.execute(nsApi);
                                                        }

                                            //"Executing condition block - RemoveItem. NS Variable =  (pct value = 80.0%)"

                                            //"NS Variable value for block - RemoveItem = nsApi.ns_get_int_val("")
                                            if(nsApi.ns_get_int_val("DV_BagitemCount") >= 12)
                                            {

                                                    //Executing sequence block - RemoveExtra_Items (pct value = 80.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                            }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 80.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 9900)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 99.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove_LowQty
                                                    CartItemRemove_LowQtyObj.execute(nsApi);
                                                    //Executing flow - CartItem_ShipItFaster
                                                    CartItem_ShipItFasterObj.execute(nsApi);
                                                    //Executing flow - Checkout
                                                    CheckoutObj.execute(nsApi);

                                                        //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 99.0%)
                                                        int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                        if(ApplyKholsCashNPromoCodespercent <= 7000)
                                                        {
                                                            ApplyPromoCodeObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            ApplyKohlsCashObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Apply_SUPCObj.execute(nsApi);
                                                        }
                                                    //Executing flow - AddAddress_onCheckout
                                                    AddAddress_onCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueCheckout
                                                    ContinueCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddPaymentCard_OnCheckout
                                                    AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddBillingAddress_OnCheckout
                                                    AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - CartItemUpdate_Qty
                                                    CartItemUpdate_QtyObj.execute(nsApi);
                                                    //Executing flow - ContinueToReviewOrder
                                                    ContinueToReviewOrderObj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 99.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 6000)
                                                        {

                                                                //Executing sequence block - Reg_PlaceOrder (pct value = 60.0%)

                                                                    //Executing percent block - ApplySUPC (pct value = 60.0%)
                                                                    int ApplySUPCpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - ApplySUPC = %d", ApplySUPCpercent

                                                                    if(ApplySUPCpercent <= 3000)
                                                                    {
                                                                        Apply_SUPCObj.execute(nsApi);
                                                                    }
                                                                    else if(ApplySUPCpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - PlaceOrder_Reg
                                                                PlaceOrder_RegObj.execute(nsApi);
                                                                //Executing flow - OrderConfirm
                                                                OrderConfirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 6000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing sequence block - BrowsingFlows (pct value = 40.0%)

                                                                    //Executing percent block - CataLogBrowsing12 (pct value = 40.0%)
                                                                    int CataLogBrowsing12percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - CataLogBrowsing12 = %d", CataLogBrowsing12percent

                                                                    if(CataLogBrowsing12percent <= 10000)
                                                                    {

                                                                            //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                                            //Executing flow - BrowsetoCatalogPage
                                                                            BrowsetoCatalogPageObj.execute(nsApi);

                                                                                //Executing sequence block - RefineOperation (pct value = 100.0%)
                                                                                //Executing flow - CatalogPagination
                                                                                CatalogPaginationObj.execute(nsApi);
                                                                                //Executing flow - CatalogSortBy
                                                                                CatalogSortByObj.execute(nsApi);
                                                                                //Executing flow - CatalogRefines
                                                                                CatalogRefinesObj.execute(nsApi);
                                                                                //Executing flow - CatalogView120items
                                                                                CatalogView120itemsObj.execute(nsApi);
                                                                            //Executing flow - CatalogPDP
                                                                            CatalogPDPObj.execute(nsApi);
                                                                            //Executing flow - Availability_pickup
                                                                            Availability_pickupObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing12percent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }

                                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 40.0%)
                                                                    //Executing flow - SearchByKeyword
                                                                    SearchByKeywordObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP
                                                                    SearchPDPObj.execute(nsApi);

                                                                    //Executing sequence block - SearchByPIDS14 (pct value = 40.0%)

                                                                        //Executing percent block - SearchByPID14 (pct value = 40.0%)
                                                                        int SearchByPID14percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - SearchByPID14 = %d", SearchByPID14percent

                                                                        if(SearchByPID14percent <= 8000)
                                                                        {
                                                                            Search_PID_1_50Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID14percent <= 9500)
                                                                        {
                                                                            Search_PID_51_100Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID14percent <= 9800)
                                                                        {
                                                                            Search_PID_101_200Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID14percent <= 9900)
                                                                        {
                                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID14percent <= 10000)
                                                                        {
                                                                            Search_PID_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID14percent <= 10000)
                                                                        {
                                                                            Search_PID_201_500Obj.execute(nsApi);
                                                                        }
                                                                //Executing flow - TypeHeadSolr
                                                                TypeHeadSolrObj.execute(nsApi);
                                                        }
                                                    //Executing flow - Logout
                                                    LogoutObj.execute(nsApi);
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                                else if(RegisteredUserspercent <= 10000)
                                {

                                        //Executing sequence block - RegisteredUsers_BopusPlaceOrder (pct value = 20.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - Home
                                        HomeObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - UserCheckStatus
                                        UserCheckStatusObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);

                                            //Executing percent block - RegisteredBopusLogin (pct value = 20.0%)
                                            int RegisteredBopusLoginpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - RegisteredBopusLogin = %d", RegisteredBopusLoginpercent

                                            if(RegisteredBopusLoginpercent <= 10000)
                                            {
                                                RegLogin_BopusObj.execute(nsApi);
                                            }
                                        //Executing flow - SetMyStore
                                        SetMyStoreObj.execute(nsApi);
                                        //Executing flow - Recommedndation_Home
                                        Recommedndation_HomeObj.execute(nsApi);

                                            //Executing percent block - AccountSettingN (pct value = 20.0%)
                                            int AccountSettingNpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - AccountSettingN = %d", AccountSettingNpercent

                                            if(AccountSettingNpercent <= 2500)
                                            {

                                                    //Executing sequence block - AccountSettings (pct value = 25.0%)
                                                    //Executing flow - My_Info
                                                    My_InfoObj.execute(nsApi);
                                                    //Executing flow - Annual_Gross_Income
                                                    Annual_Gross_IncomeObj.execute(nsApi);
                                                    //Executing flow - AddAddress
                                                    AddAddressObj.execute(nsApi);
                                                    //Executing flow - EditShipAddress
                                                    EditShipAddressObj.execute(nsApi);
                                                    //Executing flow - RemoveShippingAddress
                                                    RemoveShippingAddressObj.execute(nsApi);
                                                    //Executing flow - AddKohlsChargeCard
                                                    AddKohlsChargeCardObj.execute(nsApi);
                                                    //Executing flow - AddVisaCardDetails
                                                    AddVisaCardDetailsObj.execute(nsApi);
                                                    //Executing flow - PurchaseHistory
                                                    PurchaseHistoryObj.execute(nsApi);
                                                    //Executing flow - BillinginfoAndPayment
                                                    BillinginfoAndPaymentObj.execute(nsApi);
                                                    //Executing flow - KohlsRewards
                                                    KohlsRewardsObj.execute(nsApi);

                                                        //Executing percent block - Wallet_flow (pct value = 25.0%)
                                                        int Wallet_flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - Wallet_flow = %d", Wallet_flowpercent

                                                        if(Wallet_flowpercent <= 10000)
                                                        {

                                                                //Executing sequence block - Wallet (pct value = 100.0%)
                                                                //Executing flow - Wallet_getMyWalletDetails
                                                                Wallet_getMyWalletDetailsObj.execute(nsApi);
                                                                //Executing flow - Wallet_AddGiftCard
                                                                Wallet_AddGiftCardObj.execute(nsApi);
                                                                //Executing flow - Wallet_AddKohlsCash
                                                                Wallet_AddKohlsCashObj.execute(nsApi);
                                                                //Executing flow - Wallet_AddOffer
                                                                Wallet_AddOfferObj.execute(nsApi);
                                                        }
                                                        else if(Wallet_flowpercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                            }
                                            else if(AccountSettingNpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }

                                            //Executing sequence block - BrowsingFlows (pct value = 20.0%)

                                                //Executing percent block - CataLogBrowsing11 (pct value = 20.0%)
                                                int CataLogBrowsing11percent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - CataLogBrowsing11 = %d", CataLogBrowsing11percent

                                                if(CataLogBrowsing11percent <= 10000)
                                                {

                                                        //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                        //Executing flow - BrowsetoCatalogPage
                                                        BrowsetoCatalogPageObj.execute(nsApi);

                                                            //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                if(RefineOperationpercent <= 1800)
                                                                {
                                                                    CatalogPaginationObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 5800)
                                                                {
                                                                    CatalogSortByObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9800)
                                                                {
                                                                    CatalogRefinesObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9900)
                                                                {
                                                                    CatalogView120itemsObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 10000)
                                                                {
                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                }
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);
                                                        //Executing flow - CatalogPDP
                                                        CatalogPDPObj.execute(nsApi);
                                                        //Executing flow - Availability_pickup
                                                        Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - ZineOne_PDP
                                                        ZineOne_PDPObj.execute(nsApi);
                                                }
                                                else if(CataLogBrowsing11percent <= 10000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }

                                                //Executing sequence block - SearchByKeyword_Flow (pct value = 20.0%)
                                                //Executing flow - SearchByKeyword
                                                SearchByKeywordObj.execute(nsApi);
                                                //Executing flow - SearchPDP
                                                SearchPDPObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);

                                                //Executing sequence block - SearchByPIDS13 (pct value = 20.0%)

                                                    //Executing percent block - SearchByPID13 (pct value = 20.0%)
                                                    int SearchByPID13percent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - SearchByPID13 = %d", SearchByPID13percent

                                                    if(SearchByPID13percent <= 8000)
                                                    {
                                                        Search_PID_1_50Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID13percent <= 9500)
                                                    {
                                                        Search_PID_51_100Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID13percent <= 9800)
                                                    {
                                                        Search_PID_101_200Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID13percent <= 9900)
                                                    {
                                                        Search_PID_501_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID13percent <= 10000)
                                                    {
                                                        Search_PID_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID13percent <= 10000)
                                                    {
                                                        Search_PID_201_500Obj.execute(nsApi);
                                                    }

                                                //Executing sequence block - Typehead1 (pct value = 20.0%)
                                                //Executing flow - TypeHeadSolr
                                                TypeHeadSolrObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                            //Executing flow - PDPOnly
                                            PDPOnlyObj.execute(nsApi);

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_Bopus_BagitemCount:1:< (pct value = 20.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_Bopus_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_Bopus_BagitemCount") < 1)
                                            {
                                                //Executing flow - Search_BopusProduct
                                                Search_BopusProductObj.execute(nsApi);
                                                    //Executing flow - Availability_pickup
                                                    Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - BopusAddtoBag
                                                        BopusAddtoBagObj.execute(nsApi);
                                                            //Executing flow - ZineOne_AddtoBag
                                                            ZineOne_AddtoBagObj.execute(nsApi);
                                                                //Executing flow - PersistentBar
                                                                PersistentBarObj.execute(nsApi);
                                                            }

                                            //"Executing condition block - RemoveItem. NS Variable =  (pct value = 20.0%)"

                                            //"NS Variable value for block - RemoveItem = nsApi.ns_get_int_val("")
                                            if(nsApi.ns_get_int_val("DV_Bopus_BagitemCount") >= 10)
                                            {

                                                    //Executing sequence block - RemoveExtra_Items (pct value = 20.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                            }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 20.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 9900)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 99.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove_LowQty
                                                    CartItemRemove_LowQtyObj.execute(nsApi);
                                                    //Executing flow - Checkout
                                                    CheckoutObj.execute(nsApi);

                                                        //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 99.0%)
                                                        int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                        if(ApplyKholsCashNPromoCodespercent <= 8000)
                                                        {
                                                            ApplyPromoCodeObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            ApplyKohlsCashObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Apply_SUPCObj.execute(nsApi);
                                                        }
                                                    //Executing flow - AddAddress_onCheckout
                                                    AddAddress_onCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueCheckout
                                                    ContinueCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddPaymentCard_OnCheckout
                                                    AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddBillingAddress_OnCheckout
                                                    AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueToReviewOrder
                                                    ContinueToReviewOrderObj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 99.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 4000)
                                                        {

                                                                //Executing sequence block - Reg_PlaceOrder (pct value = 40.0%)

                                                                    //Executing percent block - ApplySUPC (pct value = 40.0%)
                                                                    int ApplySUPCpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - ApplySUPC = %d", ApplySUPCpercent

                                                                    if(ApplySUPCpercent <= 3000)
                                                                    {
                                                                        Apply_SUPCObj.execute(nsApi);
                                                                    }
                                                                    else if(ApplySUPCpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - PlaceOrder_Reg
                                                                PlaceOrder_RegObj.execute(nsApi);
                                                                //Executing flow - OrderConfirm
                                                                OrderConfirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 5000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing sequence block - BrowsingFlows (pct value = 50.0%)

                                                                    //Executing percent block - CataLogBrowsing10 (pct value = 50.0%)
                                                                    int CataLogBrowsing10percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - CataLogBrowsing10 = %d", CataLogBrowsing10percent

                                                                    if(CataLogBrowsing10percent <= 10000)
                                                                    {

                                                                            //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                                            //Executing flow - BrowsetoCatalogPage
                                                                            BrowsetoCatalogPageObj.execute(nsApi);

                                                                                //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                                    //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                                    if(RefineOperationpercent <= 1800)
                                                                                    {
                                                                                        CatalogPaginationObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 5800)
                                                                                    {
                                                                                        CatalogSortByObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9800)
                                                                                    {
                                                                                        CatalogRefinesObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9900)
                                                                                    {
                                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 10000)
                                                                                    {
                                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                                    }
                                                                                //Executing flow - Recommedndation_PMP
                                                                                Recommedndation_PMPObj.execute(nsApi);
                                                                            //Executing flow - CatalogPDP
                                                                            CatalogPDPObj.execute(nsApi);
                                                                            //Executing flow - Availability_pickup
                                                                            Availability_pickupObj.execute(nsApi);
                                                                            //Executing flow - ZineOne_PDP
                                                                            ZineOne_PDPObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing10percent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }

                                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 50.0%)
                                                                    //Executing flow - SearchByKeyword
                                                                    SearchByKeywordObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP
                                                                    SearchPDPObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - SearchByPIDS12 (pct value = 50.0%)

                                                                        //Executing percent block - SearchByPID12 (pct value = 50.0%)
                                                                        int SearchByPID12percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - SearchByPID12 = %d", SearchByPID12percent

                                                                        if(SearchByPID12percent <= 8000)
                                                                        {
                                                                            Search_PID_1_50Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID12percent <= 9500)
                                                                        {
                                                                            Search_PID_51_100Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID12percent <= 9800)
                                                                        {
                                                                            Search_PID_101_200Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID12percent <= 9900)
                                                                        {
                                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID12percent <= 10000)
                                                                        {
                                                                            Search_PID_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID12percent <= 10000)
                                                                        {
                                                                            Search_PID_201_500Obj.execute(nsApi);
                                                                        }

                                                                    //Executing sequence block - Typehead1 (pct value = 50.0%)
                                                                    //Executing flow - TypeHeadSolr
                                                                    TypeHeadSolrObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                                //Executing flow - PDPOnly
                                                                PDPOnlyObj.execute(nsApi);
                                                        }
                                                    //Executing flow - Logout
                                                    LogoutObj.execute(nsApi);
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                        }
                        else if(NormalUserspercent <= 9900)
                        {

                                //Executing percent block - GuestUsersAll (pct value = 60.0%)
                                int GuestUsersAllpercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - GuestUsersAll = %d", GuestUsersAllpercent

                                if(GuestUsersAllpercent <= 4000)
                                {

                                        //Executing sequence block - UnRegUsers_NormalPlaceOrder (pct value = 40.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);
                                        //Executing flow - UnReg_Cookie
                                        UnReg_CookieObj.execute(nsApi);
                                        //Executing flow - Recommedndation_Home
                                        Recommedndation_HomeObj.execute(nsApi);

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_BagitemCount:2:< (pct value = 40.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_BagitemCount") < 2)
                                            {

                                                    //Executing percent block - BrowsingFlows (pct value = 40.0%)
                                                    int BrowsingFlowspercent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - BrowsingFlows = %d", BrowsingFlowspercent

                                                    if(BrowsingFlowspercent <= 4500)
                                                    {

                                                            //Executing sequence block - BrowseToPDP (pct value = 45.0%)
                                                            //Executing flow - BrowsetoCatalogPage
                                                            BrowsetoCatalogPageObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_SaleEvent
                                                            Recommedndation_SaleEventObj.execute(nsApi);

                                                                //Executing sequence block - RefineCatalog (pct value = 45.0%)

                                                                    //Executing percent block - RefineOperation (pct value = 45.0%)
                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                    if(RefineOperationpercent <= 1800)
                                                                    {
                                                                        CatalogPaginationObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 5800)
                                                                    {
                                                                        CatalogSortByObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 9800)
                                                                    {
                                                                        CatalogRefinesObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 9900)
                                                                    {
                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - Recommedndation_PMP
                                                                Recommedndation_PMPObj.execute(nsApi);
                                                            //Executing flow - CatalogPDP
                                                            CatalogPDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - Availability_pickup
                                                            Availability_pickupObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 5500)
                                                    {

                                                            //Executing sequence block - SearchByKeyword_Flow (pct value = 10.0%)
                                                            //Executing flow - SearchByKeyword
                                                            SearchByKeywordObj.execute(nsApi);
                                                            //Executing flow - SearchPDP
                                                            SearchPDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 7000)
                                                    {

                                                            //Executing sequence block - SearchByPIDS11 (pct value = 15.0%)

                                                                //Executing percent block - SearchByPID11 (pct value = 15.0%)
                                                                int SearchByPID11percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - SearchByPID11 = %d", SearchByPID11percent

                                                                if(SearchByPID11percent <= 8000)
                                                                {
                                                                    Search_PID_1_50Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID11percent <= 9500)
                                                                {
                                                                    Search_PID_51_100Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID11percent <= 9800)
                                                                {
                                                                    Search_PID_101_200Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID11percent <= 9900)
                                                                {
                                                                    Search_PID_501_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID11percent <= 10000)
                                                                {
                                                                    Search_PID_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID11percent <= 10000)
                                                                {
                                                                    Search_PID_201_500Obj.execute(nsApi);
                                                                }
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 8500)
                                                    {

                                                            //Executing sequence block - TypeHead (pct value = 15.0%)
                                                            //Executing flow - TypeHeadSolr
                                                            TypeHeadSolrObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 10000)
                                                    {
                                                        PDPOnlyObj.execute(nsApi);
                                                    }
                                                    //Executing flow - AddtoBag
                                                    AddtoBagObj.execute(nsApi);
                                                        //Executing flow - ZineOne_AddtoBag
                                                        ZineOne_AddtoBagObj.execute(nsApi);
                                                            //Executing flow - PersistentBar
                                                            PersistentBarObj.execute(nsApi);
                                                                //Executing flow - Persistent_bar_components
                                                                Persistent_bar_componentsObj.execute(nsApi);
                                                            }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 40.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 9900)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 99.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove_LowQty
                                                    CartItemRemove_LowQtyObj.execute(nsApi);
                                                    //Executing flow - GuestCheckout_SetSession
                                                    GuestCheckout_SetSessionObj.execute(nsApi);

                                                        //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 99.0%)
                                                        int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                        if(ApplyKholsCashNPromoCodespercent <= 8000)
                                                        {
                                                            ApplyPromoCodeObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            ApplyKohlsCashObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                    //Executing flow - UnReg_ContinueCheckout
                                                    UnReg_ContinueCheckoutObj.execute(nsApi);
                                                    //Executing flow - CartItemUpdate_Qty
                                                    CartItemUpdate_QtyObj.execute(nsApi);
                                                    //Executing flow - UnReg_Orderreview
                                                    UnReg_OrderreviewObj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 99.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 4500)
                                                        {

                                                                //Executing sequence block - UnReg_PlaceOrder (pct value = 45.0%)
                                                                //Executing flow - UnReg_placeorder
                                                                UnReg_placeorderObj.execute(nsApi);
                                                                //Executing flow - UnReg_Order_confirm
                                                                UnReg_Order_confirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 4500)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing percent block - BrowsingFlows (pct value = 55.0%)
                                                                int BrowsingFlowspercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - BrowsingFlows = %d", BrowsingFlowspercent

                                                                if(BrowsingFlowspercent <= 1500)
                                                                {

                                                                        //Executing sequence block - BrowseToPDP (pct value = 15.0%)
                                                                        //Executing flow - BrowsetoCatalogPage
                                                                        BrowsetoCatalogPageObj.execute(nsApi);
                                                                        //Executing flow - Recommedndation_SaleEvent
                                                                        Recommedndation_SaleEventObj.execute(nsApi);

                                                                            //Executing sequence block - RefineCatalog (pct value = 15.0%)

                                                                                //Executing percent block - RefineOperation (pct value = 15.0%)
                                                                                int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                                if(RefineOperationpercent <= 1800)
                                                                                {
                                                                                    CatalogPaginationObj.execute(nsApi);
                                                                                }
                                                                                else if(RefineOperationpercent <= 5800)
                                                                                {
                                                                                    CatalogSortByObj.execute(nsApi);
                                                                                }
                                                                                else if(RefineOperationpercent <= 9800)
                                                                                {
                                                                                    CatalogRefinesObj.execute(nsApi);
                                                                                }
                                                                                else if(RefineOperationpercent <= 9900)
                                                                                {
                                                                                    CatalogView120itemsObj.execute(nsApi);
                                                                                }
                                                                                else if(RefineOperationpercent <= 10000)
                                                                                {
                                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                                }
                                                                            //Executing flow - Recommedndation_PMP
                                                                            Recommedndation_PMPObj.execute(nsApi);
                                                                        //Executing flow - CatalogPDP
                                                                        CatalogPDPObj.execute(nsApi);
                                                                        //Executing flow - Recommedndation_PDP
                                                                        Recommedndation_PDPObj.execute(nsApi);
                                                                        //Executing flow - Availability_pickup
                                                                        Availability_pickupObj.execute(nsApi);
                                                                        //Executing flow - ZineOne_PDP
                                                                        ZineOne_PDPObj.execute(nsApi);
                                                                }
                                                                else if(BrowsingFlowspercent <= 2500)
                                                                {

                                                                        //Executing sequence block - SearchByKeyword_Flow (pct value = 10.0%)
                                                                        //Executing flow - SearchByKeyword
                                                                        SearchByKeywordObj.execute(nsApi);
                                                                        //Executing flow - SearchPDP
                                                                        SearchPDPObj.execute(nsApi);
                                                                        //Executing flow - Recommedndation_PDP
                                                                        Recommedndation_PDPObj.execute(nsApi);
                                                                        //Executing flow - ZineOne_PDP
                                                                        ZineOne_PDPObj.execute(nsApi);
                                                                }
                                                                else if(BrowsingFlowspercent <= 4500)
                                                                {

                                                                        //Executing sequence block - SearchByPIDS10 (pct value = 20.0%)

                                                                            //Executing percent block - SearchByPID10 (pct value = 20.0%)
                                                                            int SearchByPID10percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                            //"Percentage random number for block - SearchByPID10 = %d", SearchByPID10percent

                                                                            if(SearchByPID10percent <= 8000)
                                                                            {
                                                                                Search_PID_1_50Obj.execute(nsApi);
                                                                            }
                                                                            else if(SearchByPID10percent <= 9500)
                                                                            {
                                                                                Search_PID_51_100Obj.execute(nsApi);
                                                                            }
                                                                            else if(SearchByPID10percent <= 9800)
                                                                            {
                                                                                Search_PID_101_200Obj.execute(nsApi);
                                                                            }
                                                                            else if(SearchByPID10percent <= 9900)
                                                                            {
                                                                                Search_PID_501_1000Obj.execute(nsApi);
                                                                            }
                                                                            else if(SearchByPID10percent <= 10000)
                                                                            {
                                                                                Search_PID_1000Obj.execute(nsApi);
                                                                            }
                                                                            else if(SearchByPID10percent <= 10000)
                                                                            {
                                                                                Search_PID_201_500Obj.execute(nsApi);
                                                                            }
                                                                        //Executing flow - Recommedndation_PDP
                                                                        Recommedndation_PDPObj.execute(nsApi);
                                                                }
                                                                else if(BrowsingFlowspercent <= 6500)
                                                                {

                                                                        //Executing sequence block - TypeHead (pct value = 20.0%)
                                                                        //Executing flow - TypeHeadSolr
                                                                        TypeHeadSolrObj.execute(nsApi);
                                                                        //Executing flow - Recommedndation_PDP
                                                                        Recommedndation_PDPObj.execute(nsApi);
                                                                        //Executing flow - ZineOne_PDP
                                                                        ZineOne_PDPObj.execute(nsApi);
                                                                }
                                                                else if(BrowsingFlowspercent <= 10000)
                                                                {
                                                                    PDPOnlyObj.execute(nsApi);
                                                                }
                                                        }
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                                else if(GuestUsersAllpercent <= 7000)
                                {

                                        //Executing sequence block - UnRegUsers_BopusPlaceOrder (pct value = 30.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);
                                        //Executing flow - UnReg_Cookie
                                        UnReg_CookieObj.execute(nsApi);
                                        //Executing flow - SetMyStore
                                        SetMyStoreObj.execute(nsApi);

                                            //Executing sequence block - BrowsingFlows (pct value = 30.0%)

                                                //Executing percent block - CataLogBrowsing9 (pct value = 30.0%)
                                                int CataLogBrowsing9percent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - CataLogBrowsing9 = %d", CataLogBrowsing9percent

                                                if(CataLogBrowsing9percent <= 10000)
                                                {

                                                        //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                        //Executing flow - BrowsetoCatalogPage
                                                        BrowsetoCatalogPageObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_SaleEvent
                                                        Recommedndation_SaleEventObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_PMP
                                                        Recommedndation_PMPObj.execute(nsApi);

                                                            //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                if(RefineOperationpercent <= 1800)
                                                                {
                                                                    CatalogPaginationObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 5800)
                                                                {
                                                                    CatalogSortByObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9800)
                                                                {
                                                                    CatalogRefinesObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9900)
                                                                {
                                                                    CatalogView120itemsObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 10000)
                                                                {
                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                }
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);
                                                        //Executing flow - CatalogPDP
                                                        CatalogPDPObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_PDP
                                                        Recommedndation_PDPObj.execute(nsApi);
                                                        //Executing flow - Availability_pickup
                                                        Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - ZineOne_PDP
                                                        ZineOne_PDPObj.execute(nsApi);
                                                }
                                                else if(CataLogBrowsing9percent <= 10000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }

                                                //Executing sequence block - SearchByKeyword_Flow (pct value = 30.0%)
                                                //Executing flow - SearchByKeyword
                                                SearchByKeywordObj.execute(nsApi);
                                                //Executing flow - Recommedndation_Search
                                                Recommedndation_SearchObj.execute(nsApi);
                                                //Executing flow - Recommedndation_PMP
                                                Recommedndation_PMPObj.execute(nsApi);
                                                //Executing flow - SearchPDP
                                                SearchPDPObj.execute(nsApi);
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);

                                                //Executing sequence block - SearchByPIDS9 (pct value = 30.0%)

                                                    //Executing percent block - SearchByPID9 (pct value = 30.0%)
                                                    int SearchByPID9percent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - SearchByPID9 = %d", SearchByPID9percent

                                                    if(SearchByPID9percent <= 8000)
                                                    {
                                                        Search_PID_1_50Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID9percent <= 9500)
                                                    {
                                                        Search_PID_51_100Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID9percent <= 9800)
                                                    {
                                                        Search_PID_101_200Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID9percent <= 9900)
                                                    {
                                                        Search_PID_501_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID9percent <= 10000)
                                                    {
                                                        Search_PID_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID9percent <= 10000)
                                                    {
                                                        Search_PID_201_500Obj.execute(nsApi);
                                                    }
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);

                                                //Executing sequence block - Typehead1 (pct value = 30.0%)
                                                //Executing flow - TypeHeadSolr
                                                TypeHeadSolrObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                            //Executing flow - PDPOnly
                                            PDPOnlyObj.execute(nsApi);

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_Bopus_BagitemCount:1:< (pct value = 30.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_Bopus_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_Bopus_BagitemCount") < 1)
                                            {
                                                //Executing flow - Search_BopusProduct
                                                Search_BopusProductObj.execute(nsApi);
                                                    //Executing flow - Recommedndation_Search
                                                    Recommedndation_SearchObj.execute(nsApi);
                                                        //Executing flow - BopusAddtoBag
                                                        BopusAddtoBagObj.execute(nsApi);
                                                            //Executing flow - ZineOne_AddtoBag
                                                            ZineOne_AddtoBagObj.execute(nsApi);
                                                                //Executing flow - PersistentBar
                                                                PersistentBarObj.execute(nsApi);
                                                            }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 30.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 9900)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 99.0%)
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove_LowQty
                                                    CartItemRemove_LowQtyObj.execute(nsApi);
                                                    //Executing flow - GuestCheckout_SetSession
                                                    GuestCheckout_SetSessionObj.execute(nsApi);
                                                    //Executing flow - UnReg_Bopus_ContinueCheckout
                                                    UnReg_Bopus_ContinueCheckoutObj.execute(nsApi);

                                                        //Executing percent block - ApplyKholsCashNPromoCodes (pct value = 99.0%)
                                                        int ApplyKholsCashNPromoCodespercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - ApplyKholsCashNPromoCodes = %d", ApplyKholsCashNPromoCodespercent

                                                        if(ApplyKholsCashNPromoCodespercent <= 0)
                                                        {
                                                            ApplyPromoCodeObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 2000)
                                                        {
                                                            ApplyKohlsCashObj.execute(nsApi);
                                                        }
                                                        else if(ApplyKholsCashNPromoCodespercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                    //Executing flow - UnReg_Bopus_Orderreview
                                                    UnReg_Bopus_OrderreviewObj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 99.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 4000)
                                                        {

                                                                //Executing sequence block - UnReg_PlaceOrder (pct value = 40.0%)
                                                                //Executing flow - UnReg_placeorder_bopus
                                                                UnReg_placeorder_bopusObj.execute(nsApi);
                                                                //Executing flow - UnReg_Order_confirm
                                                                UnReg_Order_confirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 5000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing sequence block - BrowsingFlows (pct value = 50.0%)

                                                                    //Executing percent block - CataLogBrowsing8 (pct value = 50.0%)
                                                                    int CataLogBrowsing8percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - CataLogBrowsing8 = %d", CataLogBrowsing8percent

                                                                    if(CataLogBrowsing8percent <= 10000)
                                                                    {

                                                                            //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                                            //Executing flow - BrowsetoCatalogPage
                                                                            BrowsetoCatalogPageObj.execute(nsApi);
                                                                            //Executing flow - Recommedndation_SaleEvent
                                                                            Recommedndation_SaleEventObj.execute(nsApi);
                                                                            //Executing flow - Recommedndation_PMP
                                                                            Recommedndation_PMPObj.execute(nsApi);

                                                                                //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                                    //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                                    if(RefineOperationpercent <= 1800)
                                                                                    {
                                                                                        CatalogPaginationObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 5800)
                                                                                    {
                                                                                        CatalogSortByObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9800)
                                                                                    {
                                                                                        CatalogRefinesObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9900)
                                                                                    {
                                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 10000)
                                                                                    {
                                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                                    }
                                                                                //Executing flow - Recommedndation_PMP
                                                                                Recommedndation_PMPObj.execute(nsApi);
                                                                            //Executing flow - CatalogPDP
                                                                            CatalogPDPObj.execute(nsApi);
                                                                            //Executing flow - Recommedndation_PDP
                                                                            Recommedndation_PDPObj.execute(nsApi);
                                                                            //Executing flow - Availability_pickup
                                                                            Availability_pickupObj.execute(nsApi);
                                                                            //Executing flow - ZineOne_PDP
                                                                            ZineOne_PDPObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing8percent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }

                                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 50.0%)
                                                                    //Executing flow - SearchByKeyword
                                                                    SearchByKeywordObj.execute(nsApi);
                                                                    //Executing flow - Recommedndation_Search
                                                                    Recommedndation_SearchObj.execute(nsApi);
                                                                    //Executing flow - Recommedndation_PMP
                                                                    Recommedndation_PMPObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP
                                                                    SearchPDPObj.execute(nsApi);
                                                                    //Executing flow - Recommedndation_PDP
                                                                    Recommedndation_PDPObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - SearchByPIDS8 (pct value = 50.0%)

                                                                        //Executing percent block - SearchByPID8 (pct value = 50.0%)
                                                                        int SearchByPID8percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - SearchByPID8 = %d", SearchByPID8percent

                                                                        if(SearchByPID8percent <= 8000)
                                                                        {
                                                                            Search_PID_1_50Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID8percent <= 9500)
                                                                        {
                                                                            Search_PID_51_100Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID8percent <= 9800)
                                                                        {
                                                                            Search_PID_101_200Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID8percent <= 9900)
                                                                        {
                                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID8percent <= 10000)
                                                                        {
                                                                            Search_PID_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID8percent <= 10000)
                                                                        {
                                                                            Search_PID_201_500Obj.execute(nsApi);
                                                                        }
                                                                    //Executing flow - Recommedndation_PDP
                                                                    Recommedndation_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - Typehead1 (pct value = 50.0%)
                                                                    //Executing flow - TypeHeadSolr
                                                                    TypeHeadSolrObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                                //Executing flow - PDPOnly
                                                                PDPOnlyObj.execute(nsApi);
                                                        }
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                                else if(GuestUsersAllpercent <= 10000)
                                {

                                        //Executing sequence block - SignInCheckoutFlow (pct value = 30.0%)
                                        //Executing flow - PercentageControl
                                        PercentageControlObj.execute(nsApi);
                                        //Executing flow - SessionJsp
                                        SessionJspObj.execute(nsApi);
                                        //Executing flow - WebSession
                                        WebSessionObj.execute(nsApi);
                                        //Executing flow - UnReg_Cookie
                                        UnReg_CookieObj.execute(nsApi);

                                            //Executing sequence block - BrowsingFlows (pct value = 30.0%)

                                                //Executing percent block - CataLogBrowsing7 (pct value = 30.0%)
                                                int CataLogBrowsing7percent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - CataLogBrowsing7 = %d", CataLogBrowsing7percent

                                                if(CataLogBrowsing7percent <= 10000)
                                                {

                                                        //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                        //Executing flow - BrowsetoCatalogPage
                                                        BrowsetoCatalogPageObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_SaleEvent
                                                        Recommedndation_SaleEventObj.execute(nsApi);

                                                            //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                if(RefineOperationpercent <= 1800)
                                                                {
                                                                    CatalogPaginationObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 5800)
                                                                {
                                                                    CatalogSortByObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9800)
                                                                {
                                                                    CatalogRefinesObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 9900)
                                                                {
                                                                    CatalogView120itemsObj.execute(nsApi);
                                                                }
                                                                else if(RefineOperationpercent <= 10000)
                                                                {
                                                                    Skip_FunctionalityObj.execute(nsApi);
                                                                }
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);
                                                        //Executing flow - CatalogPDP
                                                        CatalogPDPObj.execute(nsApi);
                                                        //Executing flow - Recommedndation_PDP
                                                        Recommedndation_PDPObj.execute(nsApi);
                                                        //Executing flow - Availability_pickup
                                                        Availability_pickupObj.execute(nsApi);
                                                        //Executing flow - ZineOne_PDP
                                                        ZineOne_PDPObj.execute(nsApi);
                                                }
                                                else if(CataLogBrowsing7percent <= 10000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }

                                                //Executing sequence block - SearchByKeyword_Flow (pct value = 30.0%)
                                                //Executing flow - SearchByKeyword
                                                SearchByKeywordObj.execute(nsApi);
                                                //Executing flow - Recommedndation_Search
                                                Recommedndation_SearchObj.execute(nsApi);
                                                //Executing flow - Recommedndation_PMP
                                                Recommedndation_PMPObj.execute(nsApi);
                                                //Executing flow - SearchPDP
                                                SearchPDPObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                                //Executing flow - SearchPDP_All_hit
                                                SearchPDP_All_hitObj.execute(nsApi);

                                                //Executing sequence block - SearchByPIDS7 (pct value = 30.0%)

                                                    //Executing percent block - SearchByPID7 (pct value = 30.0%)
                                                    int SearchByPID7percent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - SearchByPID7 = %d", SearchByPID7percent

                                                    if(SearchByPID7percent <= 8000)
                                                    {
                                                        Search_PID_1_50Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID7percent <= 9500)
                                                    {
                                                        Search_PID_51_100Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID7percent <= 9800)
                                                    {
                                                        Search_PID_101_200Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID7percent <= 9900)
                                                    {
                                                        Search_PID_501_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID7percent <= 10000)
                                                    {
                                                        Search_PID_1000Obj.execute(nsApi);
                                                    }
                                                    else if(SearchByPID7percent <= 10000)
                                                    {
                                                        Search_PID_201_500Obj.execute(nsApi);
                                                    }
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);

                                                //Executing sequence block - Typehead1 (pct value = 30.0%)
                                                //Executing flow - TypeHeadSolr
                                                TypeHeadSolrObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);

                                            //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_BagitemCount:10:< (pct value = 30.0%)"

                                            //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_BagitemCount")
                                            while(nsApi.ns_get_int_val("DV_BagitemCount") < 10)
                                            {

                                                    //Executing percent block - BrowsingFlows (pct value = 30.0%)
                                                    int BrowsingFlowspercent = nsApi.ns_get_random_number_int(1, 10000);

                                                    //"Percentage random number for block - BrowsingFlows = %d", BrowsingFlowspercent

                                                    if(BrowsingFlowspercent <= 1500)
                                                    {

                                                            //Executing sequence block - BrowseToPDP (pct value = 15.0%)
                                                            //Executing flow - BrowsetoCatalogPage
                                                            BrowsetoCatalogPageObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_SaleEvent
                                                            Recommedndation_SaleEventObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);

                                                                //Executing sequence block - RefineCatalog (pct value = 15.0%)

                                                                    //Executing percent block - RefineOperation (pct value = 15.0%)
                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                    if(RefineOperationpercent <= 1800)
                                                                    {
                                                                        CatalogPaginationObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 5800)
                                                                    {
                                                                        CatalogSortByObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 9800)
                                                                    {
                                                                        CatalogRefinesObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 9900)
                                                                    {
                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                    }
                                                                    else if(RefineOperationpercent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }
                                                                //Executing flow - Recommedndation_PMP
                                                                Recommedndation_PMPObj.execute(nsApi);
                                                            //Executing flow - CatalogPDP
                                                            CatalogPDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - Availability_pickup
                                                            Availability_pickupObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 2500)
                                                    {

                                                            //Executing sequence block - SearchByKeyword_Flow (pct value = 10.0%)
                                                            //Executing flow - SearchByKeyword
                                                            SearchByKeywordObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_Search
                                                            Recommedndation_SearchObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PMP
                                                            Recommedndation_PMPObj.execute(nsApi);
                                                            //Executing flow - SearchPDP
                                                            SearchPDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 4500)
                                                    {

                                                            //Executing sequence block - SearchByPIDS6 (pct value = 20.0%)

                                                                //Executing percent block - SearchByPID6 (pct value = 20.0%)
                                                                int SearchByPID6percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - SearchByPID6 = %d", SearchByPID6percent

                                                                if(SearchByPID6percent <= 8000)
                                                                {
                                                                    Search_PID_1_50Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID6percent <= 9500)
                                                                {
                                                                    Search_PID_51_100Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID6percent <= 9800)
                                                                {
                                                                    Search_PID_101_200Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID6percent <= 9900)
                                                                {
                                                                    Search_PID_501_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID6percent <= 10000)
                                                                {
                                                                    Search_PID_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID6percent <= 10000)
                                                                {
                                                                    Search_PID_201_500Obj.execute(nsApi);
                                                                }
                                                            //Executing flow - Recommedndation_Search
                                                            Recommedndation_SearchObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 6500)
                                                    {

                                                            //Executing sequence block - TypeHead (pct value = 20.0%)
                                                            //Executing flow - TypeHeadSolr
                                                            TypeHeadSolrObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                            //Executing flow - Recommedndation_PDP
                                                            Recommedndation_PDPObj.execute(nsApi);
                                                    }
                                                    else if(BrowsingFlowspercent <= 10000)
                                                    {
                                                        PDPOnlyObj.execute(nsApi);
                                                    }
                                                    //Executing flow - AddtoBag
                                                    AddtoBagObj.execute(nsApi);
                                                        //Executing flow - ZineOne_AddtoBag
                                                        ZineOne_AddtoBagObj.execute(nsApi);
                                                            //Executing flow - PersistentBar
                                                            PersistentBarObj.execute(nsApi);
                                                        }

                                            //Executing percent block - CartNCheckout_Flow (pct value = 30.0%)
                                            int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                            if(CartNCheckout_Flowpercent <= 9900)
                                            {

                                                    //Executing sequence block - CartNCheckout (pct value = 99.0%)
                                                    //Executing flow - Persistent_bar_components
                                                    Persistent_bar_componentsObj.execute(nsApi);
                                                    //Executing flow - SignInCheckout
                                                    SignInCheckoutObj.execute(nsApi);
                                                    //Executing flow - SingedInCheckout
                                                    SingedInCheckoutObj.execute(nsApi);
                                                    //Executing flow - ShopingCart
                                                    ShopingCartObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove
                                                    CartItemRemoveObj.execute(nsApi);
                                                    //Executing flow - CartItemRemove_LowQty
                                                    CartItemRemove_LowQtyObj.execute(nsApi);
                                                    //Executing flow - AddAddress_onCheckout
                                                    AddAddress_onCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueCheckout
                                                    ContinueCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddPaymentCard_OnCheckout
                                                    AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - AddBillingAddress_OnCheckout
                                                    AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                                    //Executing flow - ContinueToReviewOrder
                                                    ContinueToReviewOrderObj.execute(nsApi);

                                                        //Executing percent block - PlaceOrder (pct value = 99.0%)
                                                        int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                        if(PlaceOrderpercent <= 2500)
                                                        {

                                                                //Executing sequence block - Reg_PlaceOrder (pct value = 25.0%)
                                                                //Executing flow - PlaceOrder_Reg
                                                                PlaceOrder_RegObj.execute(nsApi);
                                                                //Executing flow - OrderConfirm
                                                                OrderConfirmObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 5000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                        else if(PlaceOrderpercent <= 10000)
                                                        {

                                                                //Executing sequence block - BrowsingFlows (pct value = 50.0%)

                                                                    //Executing percent block - CataLogBrowsing6 (pct value = 50.0%)
                                                                    int CataLogBrowsing6percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                    //"Percentage random number for block - CataLogBrowsing6 = %d", CataLogBrowsing6percent

                                                                    if(CataLogBrowsing6percent <= 10000)
                                                                    {

                                                                            //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                                            //Executing flow - BrowsetoCatalogPage
                                                                            BrowsetoCatalogPageObj.execute(nsApi);
                                                                            //Executing flow - Recommedndation_SaleEvent
                                                                            Recommedndation_SaleEventObj.execute(nsApi);

                                                                                //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                                    //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                                    int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                                    //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                                    if(RefineOperationpercent <= 1800)
                                                                                    {
                                                                                        CatalogPaginationObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 5800)
                                                                                    {
                                                                                        CatalogSortByObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9800)
                                                                                    {
                                                                                        CatalogRefinesObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 9900)
                                                                                    {
                                                                                        CatalogView120itemsObj.execute(nsApi);
                                                                                    }
                                                                                    else if(RefineOperationpercent <= 10000)
                                                                                    {
                                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                                    }
                                                                                //Executing flow - Recommedndation_PMP
                                                                                Recommedndation_PMPObj.execute(nsApi);
                                                                            //Executing flow - CatalogPDP
                                                                            CatalogPDPObj.execute(nsApi);
                                                                            //Executing flow - Recommedndation_PDP
                                                                            Recommedndation_PDPObj.execute(nsApi);
                                                                            //Executing flow - Availability_pickup
                                                                            Availability_pickupObj.execute(nsApi);
                                                                            //Executing flow - ZineOne_PDP
                                                                            ZineOne_PDPObj.execute(nsApi);
                                                                    }
                                                                    else if(CataLogBrowsing6percent <= 10000)
                                                                    {
                                                                        Skip_FunctionalityObj.execute(nsApi);
                                                                    }

                                                                    //Executing sequence block - SearchByKeyword_Flow (pct value = 50.0%)
                                                                    //Executing flow - SearchByKeyword
                                                                    SearchByKeywordObj.execute(nsApi);
                                                                    //Executing flow - Recommedndation_Search
                                                                    Recommedndation_SearchObj.execute(nsApi);
                                                                    //Executing flow - Recommedndation_PMP
                                                                    Recommedndation_PMPObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP
                                                                    SearchPDPObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                                    //Executing flow - SearchPDP_All_hit
                                                                    SearchPDP_All_hitObj.execute(nsApi);

                                                                    //Executing sequence block - SearchByPIDS5 (pct value = 50.0%)

                                                                        //Executing percent block - SearchByPID5 (pct value = 50.0%)
                                                                        int SearchByPID5percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                        //"Percentage random number for block - SearchByPID5 = %d", SearchByPID5percent

                                                                        if(SearchByPID5percent <= 8000)
                                                                        {
                                                                            Search_PID_1_50Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID5percent <= 9500)
                                                                        {
                                                                            Search_PID_51_100Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID5percent <= 9800)
                                                                        {
                                                                            Search_PID_101_200Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID5percent <= 9900)
                                                                        {
                                                                            Search_PID_501_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID5percent <= 10000)
                                                                        {
                                                                            Search_PID_1000Obj.execute(nsApi);
                                                                        }
                                                                        else if(SearchByPID5percent <= 10000)
                                                                        {
                                                                            Search_PID_201_500Obj.execute(nsApi);
                                                                        }
                                                                    //Executing flow - Recommedndation_PDP
                                                                    Recommedndation_PDPObj.execute(nsApi);

                                                                    //Executing sequence block - Typehead1 (pct value = 50.0%)
                                                                    //Executing flow - TypeHeadSolr
                                                                    TypeHeadSolrObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                        }

                                                        //Executing percent block - AccountSetting (pct value = 99.0%)
                                                        int AccountSettingpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - AccountSetting = %d", AccountSettingpercent

                                                        if(AccountSettingpercent <= 2500)
                                                        {

                                                                //Executing sequence block - AccountSettings (pct value = 25.0%)
                                                                //Executing flow - My_Info
                                                                My_InfoObj.execute(nsApi);
                                                                //Executing flow - Annual_Gross_Income
                                                                Annual_Gross_IncomeObj.execute(nsApi);
                                                                //Executing flow - AddAddress
                                                                AddAddressObj.execute(nsApi);
                                                                //Executing flow - EditShipAddress
                                                                EditShipAddressObj.execute(nsApi);
                                                                //Executing flow - RemoveShippingAddress
                                                                RemoveShippingAddressObj.execute(nsApi);
                                                                //Executing flow - AddKohlsChargeCard
                                                                AddKohlsChargeCardObj.execute(nsApi);
                                                                //Executing flow - AddVisaCardDetails
                                                                AddVisaCardDetailsObj.execute(nsApi);
                                                                //Executing flow - PurchaseHistory
                                                                PurchaseHistoryObj.execute(nsApi);
                                                        }
                                                        else if(AccountSettingpercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                    //Executing flow - Logout
                                                    LogoutObj.execute(nsApi);
                                            }
                                            else if(CartNCheckout_Flowpercent <= 10000)
                                            {
                                                Skip_FunctionalityObj.execute(nsApi);
                                            }
                                }
                        }
                        else if(NormalUserspercent <= 10000)
                        {

                                //Executing sequence block - AccountCreation (pct value = 1.0%)
                                //Executing flow - PercentageControl
                                PercentageControlObj.execute(nsApi);
                                //Executing flow - Home
                                HomeObj.execute(nsApi);
                                //Executing flow - CreateAccount
                                CreateAccountObj.execute(nsApi);

                                    //Executing sequence block - AccountSettings (pct value = 1.0%)
                                    //Executing flow - My_Info
                                    My_InfoObj.execute(nsApi);
                                    //Executing flow - Annual_Gross_Income
                                    Annual_Gross_IncomeObj.execute(nsApi);
                                    //Executing flow - AddAddress
                                    AddAddressObj.execute(nsApi);
                                    //Executing flow - EditShipAddress
                                    EditShipAddressObj.execute(nsApi);
                                    //Executing flow - RemoveShippingAddress
                                    RemoveShippingAddressObj.execute(nsApi);
                                    //Executing flow - AddKohlsChargeCard
                                    AddKohlsChargeCardObj.execute(nsApi);
                                    //Executing flow - AddVisaCardDetails
                                    AddVisaCardDetailsObj.execute(nsApi);
                                    //Executing flow - PurchaseHistory
                                    PurchaseHistoryObj.execute(nsApi);
                                    //Executing flow - BillinginfoAndPayment
                                    BillinginfoAndPaymentObj.execute(nsApi);
                                    //Executing flow - KohlsRewards
                                    KohlsRewardsObj.execute(nsApi);

                                    //Executing sequence block - BrowsingFlows (pct value = 1.0%)

                                        //Executing percent block - CataLogBrowsing5 (pct value = 1.0%)
                                        int CataLogBrowsing5percent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - CataLogBrowsing5 = %d", CataLogBrowsing5percent

                                        if(CataLogBrowsing5percent <= 10000)
                                        {

                                                //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                //Executing flow - BrowsetoCatalogPage
                                                BrowsetoCatalogPageObj.execute(nsApi);
                                                //Executing flow - Recommedndation_SaleEvent
                                                Recommedndation_SaleEventObj.execute(nsApi);
                                                //Executing flow - Recommedndation_PMP
                                                Recommedndation_PMPObj.execute(nsApi);

                                                    //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                        //Executing percent block - RefineOperation (pct value = 100.0%)
                                                        int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                        if(RefineOperationpercent <= 1800)
                                                        {
                                                            CatalogPaginationObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 5800)
                                                        {
                                                            CatalogSortByObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 9800)
                                                        {
                                                            CatalogRefinesObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 9900)
                                                        {
                                                            CatalogView120itemsObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                    //Executing flow - Recommedndation_PMP
                                                    Recommedndation_PMPObj.execute(nsApi);
                                                //Executing flow - CatalogPDP
                                                CatalogPDPObj.execute(nsApi);
                                                //Executing flow - Recommedndation_PDP
                                                Recommedndation_PDPObj.execute(nsApi);
                                                //Executing flow - Availability_pickup
                                                Availability_pickupObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                        }
                                        else if(CataLogBrowsing5percent <= 10000)
                                        {
                                            Skip_FunctionalityObj.execute(nsApi);
                                        }

                                        //Executing sequence block - SearchByKeyword_Flow (pct value = 1.0%)
                                        //Executing flow - SearchByKeyword
                                        SearchByKeywordObj.execute(nsApi);
                                        //Executing flow - Recommedndation_Search
                                        Recommedndation_SearchObj.execute(nsApi);
                                        //Executing flow - Recommedndation_PMP
                                        Recommedndation_PMPObj.execute(nsApi);
                                        //Executing flow - SearchPDP
                                        SearchPDPObj.execute(nsApi);
                                        //Executing flow - Recommedndation_PDP
                                        Recommedndation_PDPObj.execute(nsApi);
                                        //Executing flow - ZineOne_PDP
                                        ZineOne_PDPObj.execute(nsApi);

                                        //Executing sequence block - SearchByPIDS4 (pct value = 1.0%)

                                            //Executing percent block - SearchByPID4 (pct value = 1.0%)
                                            int SearchByPID4percent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - SearchByPID4 = %d", SearchByPID4percent

                                            if(SearchByPID4percent <= 8000)
                                            {
                                                Search_PID_1_50Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID4percent <= 9500)
                                            {
                                                Search_PID_51_100Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID4percent <= 9800)
                                            {
                                                Search_PID_101_200Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID4percent <= 9900)
                                            {
                                                Search_PID_501_1000Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID4percent <= 10000)
                                            {
                                                Search_PID_1000Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID4percent <= 10000)
                                            {
                                                Search_PID_201_500Obj.execute(nsApi);
                                            }

                                        //Executing sequence block - Typehead1 (pct value = 1.0%)
                                        //Executing flow - TypeHeadSolr
                                        TypeHeadSolrObj.execute(nsApi);
                                        //Executing flow - ZineOne_PDP
                                        ZineOne_PDPObj.execute(nsApi);
                                    //Executing flow - PDPOnly
                                    PDPOnlyObj.execute(nsApi);
                        }
                        else if(NormalUserspercent <= 10000)
                        {

                                //Executing sequence block - RegisteredUsers_VGCPlaceOrder (pct value = 0.0%)
                                //Executing flow - PercentageControl
                                PercentageControlObj.execute(nsApi);
                                //Executing flow - Home
                                HomeObj.execute(nsApi);
                                //Executing flow - WebSession
                                WebSessionObj.execute(nsApi);
                                //Executing flow - SessionJsp
                                SessionJspObj.execute(nsApi);

                                    //Executing percent block - RegisteredVGCLogin (pct value = 0.0%)
                                    int RegisteredVGCLoginpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - RegisteredVGCLogin = %d", RegisteredVGCLoginpercent

                                    if(RegisteredVGCLoginpercent <= 10000)
                                    {
                                        RegLogin_VGCObj.execute(nsApi);
                                    }

                                    //Executing percent block - AccountSetting (pct value = 0.0%)
                                    int AccountSettingpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - AccountSetting = %d", AccountSettingpercent

                                    if(AccountSettingpercent <= 2500)
                                    {

                                            //Executing sequence block - AccountSettings (pct value = 25.0%)
                                            //Executing flow - My_Info
                                            My_InfoObj.execute(nsApi);
                                            //Executing flow - Annual_Gross_Income
                                            Annual_Gross_IncomeObj.execute(nsApi);
                                            //Executing flow - AddAddress
                                            AddAddressObj.execute(nsApi);
                                            //Executing flow - EditShipAddress
                                            EditShipAddressObj.execute(nsApi);
                                            //Executing flow - RemoveShippingAddress
                                            RemoveShippingAddressObj.execute(nsApi);
                                            //Executing flow - AddKohlsChargeCard
                                            AddKohlsChargeCardObj.execute(nsApi);
                                            //Executing flow - AddVisaCardDetails
                                            AddVisaCardDetailsObj.execute(nsApi);
                                            //Executing flow - PurchaseHistory
                                            PurchaseHistoryObj.execute(nsApi);
                                            //Executing flow - BillinginfoAndPayment
                                            BillinginfoAndPaymentObj.execute(nsApi);
                                            //Executing flow - KohlsRewards
                                            KohlsRewardsObj.execute(nsApi);
                                    }
                                    else if(AccountSettingpercent <= 10000)
                                    {
                                        Skip_FunctionalityObj.execute(nsApi);
                                    }
                                //Executing flow - SetMyStore
                                SetMyStoreObj.execute(nsApi);

                                    //Executing sequence block - BrowsingFlows (pct value = 0.0%)

                                        //Executing percent block - CataLogBrowsing4 (pct value = 0.0%)
                                        int CataLogBrowsing4percent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - CataLogBrowsing4 = %d", CataLogBrowsing4percent

                                        if(CataLogBrowsing4percent <= 10000)
                                        {

                                                //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                //Executing flow - BrowsetoCatalogPage
                                                BrowsetoCatalogPageObj.execute(nsApi);

                                                    //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                        //Executing percent block - RefineOperation (pct value = 100.0%)
                                                        int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                        //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                        if(RefineOperationpercent <= 1800)
                                                        {
                                                            CatalogPaginationObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 5800)
                                                        {
                                                            CatalogSortByObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 9800)
                                                        {
                                                            CatalogRefinesObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 9900)
                                                        {
                                                            CatalogView120itemsObj.execute(nsApi);
                                                        }
                                                        else if(RefineOperationpercent <= 10000)
                                                        {
                                                            Skip_FunctionalityObj.execute(nsApi);
                                                        }
                                                    //Executing flow - Recommedndation_PMP
                                                    Recommedndation_PMPObj.execute(nsApi);
                                                //Executing flow - CatalogPDP
                                                CatalogPDPObj.execute(nsApi);
                                                //Executing flow - Availability_pickup
                                                Availability_pickupObj.execute(nsApi);
                                                //Executing flow - ZineOne_PDP
                                                ZineOne_PDPObj.execute(nsApi);
                                        }
                                        else if(CataLogBrowsing4percent <= 10000)
                                        {
                                            Skip_FunctionalityObj.execute(nsApi);
                                        }

                                        //Executing sequence block - SearchByKeyword_Flow (pct value = 0.0%)
                                        //Executing flow - SearchByKeyword
                                        SearchByKeywordObj.execute(nsApi);
                                        //Executing flow - SearchPDP
                                        SearchPDPObj.execute(nsApi);
                                        //Executing flow - ZineOne_PDP
                                        ZineOne_PDPObj.execute(nsApi);
                                        //Executing flow - SearchPDP_All_hit
                                        SearchPDP_All_hitObj.execute(nsApi);

                                        //Executing sequence block - SearchByPIDS3 (pct value = 0.0%)

                                            //Executing percent block - SearchByPID3 (pct value = 0.0%)
                                            int SearchByPID3percent = nsApi.ns_get_random_number_int(1, 10000);

                                            //"Percentage random number for block - SearchByPID3 = %d", SearchByPID3percent

                                            if(SearchByPID3percent <= 8000)
                                            {
                                                Search_PID_1_50Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID3percent <= 9500)
                                            {
                                                Search_PID_51_100Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID3percent <= 9800)
                                            {
                                                Search_PID_101_200Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID3percent <= 9900)
                                            {
                                                Search_PID_501_1000Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID3percent <= 10000)
                                            {
                                                Search_PID_1000Obj.execute(nsApi);
                                            }
                                            else if(SearchByPID3percent <= 10000)
                                            {
                                                Search_PID_201_500Obj.execute(nsApi);
                                            }

                                        //Executing sequence block - Typehead1 (pct value = 0.0%)
                                        //Executing flow - TypeHeadSolr
                                        TypeHeadSolrObj.execute(nsApi);
                                        //Executing flow - ZineOne_PDP
                                        ZineOne_PDPObj.execute(nsApi);
                                    //Executing flow - PDPOnly
                                    PDPOnlyObj.execute(nsApi);

                                    //"Executing while block - BrowseNSearch_AddtoBag. NS Variable = DV_VGC_BagitemCount:1:< (pct value = 0.0%)"

                                    //"NS Variable value for block - BrowseNSearch_AddtoBag = nsApi.ns_get_int_val("DV_VGC_BagitemCount")
                                    while(nsApi.ns_get_int_val("DV_VGC_BagitemCount") < 1)
                                    {
                                        //Executing flow - VGC_BrowseNAddtoBag
                                        VGC_BrowseNAddtoBagObj.execute(nsApi);
                                            //Executing flow - ZineOne_AddtoBag
                                            ZineOne_AddtoBagObj.execute(nsApi);
                                        }

                                    //"Executing condition block - RemoveItem. NS Variable =  (pct value = 0.0%)"

                                    //"NS Variable value for block - RemoveItem = nsApi.ns_get_int_val("")
                                    if(nsApi.ns_get_int_val("DV_VGC_BagitemCount") >= 6)
                                    {

                                            //Executing sequence block - RemoveExtra_Items (pct value = 0.0%)
                                            //Executing flow - ShopingCart
                                            ShopingCartObj.execute(nsApi);
                                            //Executing flow - CartItemRemove
                                            CartItemRemoveObj.execute(nsApi);
                                    }

                                    //Executing percent block - CartNCheckout_Flow (pct value = 0.0%)
                                    int CartNCheckout_Flowpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - CartNCheckout_Flow = %d", CartNCheckout_Flowpercent

                                    if(CartNCheckout_Flowpercent <= 9900)
                                    {

                                            //Executing sequence block - CartNCheckout (pct value = 99.0%)
                                            //Executing flow - Checkout
                                            CheckoutObj.execute(nsApi);
                                            //Executing flow - AddAddress_onCheckout
                                            AddAddress_onCheckoutObj.execute(nsApi);
                                            //Executing flow - ContinueCheckout
                                            ContinueCheckoutObj.execute(nsApi);
                                            //Executing flow - AddPaymentCard_OnCheckout
                                            AddPaymentCard_OnCheckoutObj.execute(nsApi);
                                            //Executing flow - AddBillingAddress_OnCheckout
                                            AddBillingAddress_OnCheckoutObj.execute(nsApi);
                                            //Executing flow - ContinueToReviewOrder
                                            ContinueToReviewOrderObj.execute(nsApi);

                                                //Executing percent block - PlaceOrder (pct value = 99.0%)
                                                int PlaceOrderpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                //"Percentage random number for block - PlaceOrder = %d", PlaceOrderpercent

                                                if(PlaceOrderpercent <= 2000)
                                                {

                                                        //Executing sequence block - Reg_PlaceOrder (pct value = 20.0%)
                                                        //Executing flow - PlaceOrder_Reg
                                                        PlaceOrder_RegObj.execute(nsApi);
                                                        //Executing flow - OrderConfirm
                                                        OrderConfirmObj.execute(nsApi);
                                                }
                                                else if(PlaceOrderpercent <= 4000)
                                                {
                                                    Skip_FunctionalityObj.execute(nsApi);
                                                }
                                                else if(PlaceOrderpercent <= 10000)
                                                {

                                                        //Executing sequence block - BrowsingFlows (pct value = 60.0%)

                                                            //Executing percent block - CataLogBrowsing3 (pct value = 60.0%)
                                                            int CataLogBrowsing3percent = nsApi.ns_get_random_number_int(1, 10000);

                                                            //"Percentage random number for block - CataLogBrowsing3 = %d", CataLogBrowsing3percent

                                                            if(CataLogBrowsing3percent <= 10000)
                                                            {

                                                                    //Executing sequence block - BrowseToPDP (pct value = 100.0%)
                                                                    //Executing flow - BrowsetoCatalogPage
                                                                    BrowsetoCatalogPageObj.execute(nsApi);

                                                                        //Executing sequence block - RefineCatalog (pct value = 100.0%)

                                                                            //Executing percent block - RefineOperation (pct value = 100.0%)
                                                                            int RefineOperationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                                                            //"Percentage random number for block - RefineOperation = %d", RefineOperationpercent

                                                                            if(RefineOperationpercent <= 1800)
                                                                            {
                                                                                CatalogPaginationObj.execute(nsApi);
                                                                            }
                                                                            else if(RefineOperationpercent <= 5800)
                                                                            {
                                                                                CatalogSortByObj.execute(nsApi);
                                                                            }
                                                                            else if(RefineOperationpercent <= 9800)
                                                                            {
                                                                                CatalogRefinesObj.execute(nsApi);
                                                                            }
                                                                            else if(RefineOperationpercent <= 9900)
                                                                            {
                                                                                CatalogView120itemsObj.execute(nsApi);
                                                                            }
                                                                            else if(RefineOperationpercent <= 10000)
                                                                            {
                                                                                Skip_FunctionalityObj.execute(nsApi);
                                                                            }
                                                                        //Executing flow - Recommedndation_PMP
                                                                        Recommedndation_PMPObj.execute(nsApi);
                                                                    //Executing flow - CatalogPDP
                                                                    CatalogPDPObj.execute(nsApi);
                                                                    //Executing flow - Availability_pickup
                                                                    Availability_pickupObj.execute(nsApi);
                                                                    //Executing flow - ZineOne_PDP
                                                                    ZineOne_PDPObj.execute(nsApi);
                                                            }
                                                            else if(CataLogBrowsing3percent <= 10000)
                                                            {
                                                                Skip_FunctionalityObj.execute(nsApi);
                                                            }

                                                            //Executing sequence block - SearchByKeyword_Flow (pct value = 60.0%)
                                                            //Executing flow - SearchByKeyword
                                                            SearchByKeywordObj.execute(nsApi);
                                                            //Executing flow - SearchPDP
                                                            SearchPDPObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                            //Executing flow - SearchPDP_All_hit
                                                            SearchPDP_All_hitObj.execute(nsApi);

                                                            //Executing sequence block - SearchByPIDS2 (pct value = 60.0%)

                                                                //Executing percent block - SearchByPID2 (pct value = 60.0%)
                                                                int SearchByPID2percent = nsApi.ns_get_random_number_int(1, 10000);

                                                                //"Percentage random number for block - SearchByPID2 = %d", SearchByPID2percent

                                                                if(SearchByPID2percent <= 8000)
                                                                {
                                                                    Search_PID_1_50Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID2percent <= 9500)
                                                                {
                                                                    Search_PID_51_100Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID2percent <= 9800)
                                                                {
                                                                    Search_PID_101_200Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID2percent <= 9900)
                                                                {
                                                                    Search_PID_501_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID2percent <= 10000)
                                                                {
                                                                    Search_PID_1000Obj.execute(nsApi);
                                                                }
                                                                else if(SearchByPID2percent <= 10000)
                                                                {
                                                                    Search_PID_201_500Obj.execute(nsApi);
                                                                }

                                                            //Executing sequence block - Typehead1 (pct value = 60.0%)
                                                            //Executing flow - TypeHeadSolr
                                                            TypeHeadSolrObj.execute(nsApi);
                                                            //Executing flow - ZineOne_PDP
                                                            ZineOne_PDPObj.execute(nsApi);
                                                        //Executing flow - PDPOnly
                                                        PDPOnlyObj.execute(nsApi);
                                                }
                                            //Executing flow - Logout
                                            LogoutObj.execute(nsApi);
                                    }
                                    else if(CartNCheckout_Flowpercent <= 10000)
                                    {
                                        Skip_FunctionalityObj.execute(nsApi);
                                    }
                        }
                }
        }

        //logging
        nsApi.ns_end_session();
    }

}
