/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class SnB_Services
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the CatalogCategory class
    CatalogCategory CatalogCategoryObj = new CatalogCategory();
    //Initialise the CatalogByMonetization class
    CatalogByMonetization CatalogByMonetizationObj = new CatalogByMonetization();
    //Initialise the CatalogGetProductCount class
    CatalogGetProductCount CatalogGetProductCountObj = new CatalogGetProductCount();
    //Initialise the Catalog_VisualTypeAhead class
    Catalog_VisualTypeAhead Catalog_VisualTypeAheadObj = new Catalog_VisualTypeAhead();
    //Initialise the CatalogByActiveDimension_P_Guest class
    CatalogByActiveDimension_P_Guest CatalogByActiveDimension_P_GuestObj = new CatalogByActiveDimension_P_Guest();
    //Initialise the CatalogByKeyword_Bopus class
    CatalogByKeyword_Bopus CatalogByKeyword_BopusObj = new CatalogByKeyword_Bopus();
    //Initialise the CatalogByKeywordSuggestions class
    CatalogByKeywordSuggestions CatalogByKeywordSuggestionsObj = new CatalogByKeywordSuggestions();
    //Initialise the CatalogByActiveDimension_NonP class
    CatalogByActiveDimension_NonP CatalogByActiveDimension_NonPObj = new CatalogByActiveDimension_NonP();
    //Initialise the CatalogByActiveDimension_P class
    CatalogByActiveDimension_P CatalogByActiveDimension_PObj = new CatalogByActiveDimension_P();
    //Initialise the CatalogByKeyword_NonP class
    CatalogByKeyword_NonP CatalogByKeyword_NonPObj = new CatalogByKeyword_NonP();
    //Initialise the CatalogByKeyword_P class
    CatalogByKeyword_P CatalogByKeyword_PObj = new CatalogByKeyword_P();
    //Initialise the CatalogByKeyword_P_Guest class
    CatalogByKeyword_P_Guest CatalogByKeyword_P_GuestObj = new CatalogByKeyword_P_Guest();
    //Initialise the Catalog_StaticMessages class
    Catalog_StaticMessages Catalog_StaticMessagesObj = new Catalog_StaticMessages();
    //Initialise the Product_WebID_5_v2 class
    Product_WebID_5_v2 Product_WebID_5_v2Obj = new Product_WebID_5_v2();
    //Initialise the Product_WebID_5_10_v2 class
    Product_WebID_5_10_v2 Product_WebID_5_10_v2Obj = new Product_WebID_5_10_v2();
    //Initialise the Product_WebID_10_30_v2 class
    Product_WebID_10_30_v2 Product_WebID_10_30_v2Obj = new Product_WebID_10_30_v2();
    //Initialise the Product_WebID_30_50_v2 class
    Product_WebID_30_50_v2 Product_WebID_30_50_v2Obj = new Product_WebID_30_50_v2();
    //Initialise the Product_WebID_51_100_v2 class
    Product_WebID_51_100_v2 Product_WebID_51_100_v2Obj = new Product_WebID_51_100_v2();
    //Initialise the Continue class
    Continue ContinueObj = new Continue();
    //Initialise the Product_WebID_201_500_v2 class
    Product_WebID_201_500_v2 Product_WebID_201_500_v2Obj = new Product_WebID_201_500_v2();
    //Initialise the Product_WebID_501_1000_v2 class
    Product_WebID_501_1000_v2 Product_WebID_501_1000_v2Obj = new Product_WebID_501_1000_v2();
    //Initialise the Product_WebID_1000_v2 class
    Product_WebID_1000_v2 Product_WebID_1000_v2Obj = new Product_WebID_1000_v2();
    //Initialise the Availability class
    Availability AvailabilityObj = new Availability();
    //Initialise the Delivery class
    Delivery DeliveryObj = new Delivery();
    //Initialise the ProductStaticMessages class
    ProductStaticMessages ProductStaticMessagesObj = new ProductStaticMessages();
    //Initialise the PriceBySKU_GET class
    PriceBySKU_GET PriceBySKU_GETObj = new PriceBySKU_GET();
    //Initialise the PriceByWebId_GET class
    PriceByWebId_GET PriceByWebId_GETObj = new PriceByWebId_GET();
    //Initialise the InventorySKU class
    InventorySKU InventorySKUObj = new InventorySKU();
    //Initialise the InventoryWebID class
    InventoryWebID InventoryWebIDObj = new InventoryWebID();
    //Initialise the Store class
    Store StoreObj = new Store();
    //Initialise the StoreAddress class
    StoreAddress StoreAddressObj = new StoreAddress();
    //Initialise the StoreLatitude class
    StoreLatitude StoreLatitudeObj = new StoreLatitude();
    //Initialise the StoreRadius class
    StoreRadius StoreRadiusObj = new StoreRadius();
    //Initialise the YourPrice class
    YourPrice YourPriceObj = new YourPrice();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing percent block - Start
        int Startpercent = nsApi.ns_get_random_number_int(1, 10000);

        //"Percentage random number for block - Start = %d", Startpercent

        if(Startpercent <= 5000)
        {

                //Executing percent block - Catalog (pct value = 50.0%)
                int Catalogpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - Catalog = %d", Catalogpercent

                if(Catalogpercent <= 0)
                {
                    CatalogCategoryObj.execute(nsApi);
                }
                else if(Catalogpercent <= 200)
                {
                    CatalogByMonetizationObj.execute(nsApi);
                }
                else if(Catalogpercent <= 500)
                {
                    CatalogGetProductCountObj.execute(nsApi);
                }
                else if(Catalogpercent <= 800)
                {
                    Catalog_VisualTypeAheadObj.execute(nsApi);
                }
                else if(Catalogpercent <= 800)
                {
                    CatalogByActiveDimension_P_GuestObj.execute(nsApi);
                }
                else if(Catalogpercent <= 800)
                {
                    CatalogByKeyword_BopusObj.execute(nsApi);
                }
                else if(Catalogpercent <= 1400)
                {
                    CatalogByKeywordSuggestionsObj.execute(nsApi);
                }
                else if(Catalogpercent <= 6400)
                {

                        //Executing percent block - Dimensions (pct value = 50.0%)
                        int Dimensionspercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - Dimensions = %d", Dimensionspercent

                        if(Dimensionspercent <= 4000)
                        {
                            CatalogByActiveDimension_NonPObj.execute(nsApi);
                        }
                        else if(Dimensionspercent <= 8000)
                        {
                            CatalogByActiveDimension_PObj.execute(nsApi);
                        }
                        else if(Dimensionspercent <= 10000)
                        {
                            CatalogByActiveDimension_P_GuestObj.execute(nsApi);
                        }
                }
                else if(Catalogpercent <= 9600)
                {

                        //Executing percent block - Keyword (pct value = 32.0%)
                        int Keywordpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - Keyword = %d", Keywordpercent

                        if(Keywordpercent <= 4000)
                        {
                            CatalogByKeyword_NonPObj.execute(nsApi);
                        }
                        else if(Keywordpercent <= 8000)
                        {
                            CatalogByKeyword_PObj.execute(nsApi);
                        }
                        else if(Keywordpercent <= 10000)
                        {
                            CatalogByKeyword_P_GuestObj.execute(nsApi);
                        }
                }
                else if(Catalogpercent <= 10000)
                {
                    Catalog_StaticMessagesObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 7000)
        {

                //Executing percent block - Product (pct value = 20.0%)
                int Productpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - Product = %d", Productpercent

                if(Productpercent <= 10000)
                {

                        //Executing sequence block - ProductAPIBlock (pct value = 100.0%)

                            //Executing percent block - Product_Web_1_50 (pct value = 100.0%)
                            int Product_Web_1_50percent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - Product_Web_1_50 = %d", Product_Web_1_50percent

                            if(Product_Web_1_50percent <= 2500)
                            {
                                Product_WebID_5_v2Obj.execute(nsApi);
                            }
                            else if(Product_Web_1_50percent <= 5000)
                            {
                                Product_WebID_5_10_v2Obj.execute(nsApi);
                            }
                            else if(Product_Web_1_50percent <= 7500)
                            {
                                Product_WebID_10_30_v2Obj.execute(nsApi);
                            }
                            else if(Product_Web_1_50percent <= 10000)
                            {
                                Product_WebID_30_50_v2Obj.execute(nsApi);
                            }

                            //Executing percent block - Product_ID_51_100 (pct value = 100.0%)
                            int Product_ID_51_100percent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - Product_ID_51_100 = %d", Product_ID_51_100percent

                            if(Product_ID_51_100percent <= 1000)
                            {
                                Product_WebID_51_100_v2Obj.execute(nsApi);
                            }
                            else if(Product_ID_51_100percent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }

                            //Executing percent block - Product_ID_201_500 (pct value = 100.0%)
                            int Product_ID_201_500percent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - Product_ID_201_500 = %d", Product_ID_201_500percent

                            if(Product_ID_201_500percent <= 1000)
                            {
                                Product_WebID_201_500_v2Obj.execute(nsApi);
                            }
                            else if(Product_ID_201_500percent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }

                            //Executing percent block - Product_ID_501_1000 (pct value = 100.0%)
                            int Product_ID_501_1000percent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - Product_ID_501_1000 = %d", Product_ID_501_1000percent

                            if(Product_ID_501_1000percent <= 1000)
                            {
                                Product_WebID_501_1000_v2Obj.execute(nsApi);
                            }
                            else if(Product_ID_501_1000percent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }

                            //Executing percent block - Product_ID_1000 (pct value = 100.0%)
                            int Product_ID_1000percent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - Product_ID_1000 = %d", Product_ID_1000percent

                            if(Product_ID_1000percent <= 1000)
                            {
                                Product_WebID_1000_v2Obj.execute(nsApi);
                            }
                            else if(Product_ID_1000percent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }

                            //Executing percent block - Availabity_Pickup (pct value = 100.0%)
                            int Availabity_Pickuppercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - Availabity_Pickup = %d", Availabity_Pickuppercent

                            if(Availabity_Pickuppercent <= 7500)
                            {
                                AvailabilityObj.execute(nsApi);
                            }
                            else if(Availabity_Pickuppercent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }

                            //Executing percent block - DeliveryInfo (pct value = 100.0%)
                            int DeliveryInfopercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - DeliveryInfo = %d", DeliveryInfopercent

                            if(DeliveryInfopercent <= 500)
                            {
                                DeliveryObj.execute(nsApi);
                            }
                            else if(DeliveryInfopercent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }

                            //Executing percent block - StaticMessages (pct value = 100.0%)
                            int StaticMessagespercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - StaticMessages = %d", StaticMessagespercent

                            if(StaticMessagespercent <= 500)
                            {
                                ProductStaticMessagesObj.execute(nsApi);
                            }
                            else if(StaticMessagespercent <= 10000)
                            {
                                ContinueObj.execute(nsApi);
                            }
                }
        }
        else if(Startpercent <= 7100)
        {

                //Executing percent block - Price (pct value = 1.0%)
                int Pricepercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - Price = %d", Pricepercent

                if(Pricepercent <= 2000)
                {
                    PriceBySKU_GETObj.execute(nsApi);
                }
                else if(Pricepercent <= 10000)
                {
                    PriceByWebId_GETObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 7200)
        {

                //Executing percent block - Inventory (pct value = 1.0%)
                int Inventorypercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - Inventory = %d", Inventorypercent

                if(Inventorypercent <= 10000)
                {
                    InventorySKUObj.execute(nsApi);
                }
                else if(Inventorypercent <= 10000)
                {
                    InventoryWebIDObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 7700)
        {

                //Executing percent block - StoreAPI (pct value = 5.0%)
                int StoreAPIpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - StoreAPI = %d", StoreAPIpercent

                if(StoreAPIpercent <= 2500)
                {
                    StoreObj.execute(nsApi);
                }
                else if(StoreAPIpercent <= 5000)
                {
                    StoreAddressObj.execute(nsApi);
                }
                else if(StoreAPIpercent <= 7500)
                {
                    StoreLatitudeObj.execute(nsApi);
                }
                else if(StoreAPIpercent <= 10000)
                {
                    StoreRadiusObj.execute(nsApi);
                }
        }
        else if(Startpercent <= 10000)
        {
            YourPriceObj.execute(nsApi);
        }

        //logging
        nsApi.ns_end_session();
    }

}
