/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class runlogic
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the PercentageControl class
    PercentageControl PercentageControlObj = new PercentageControl();
    //Initialise the RegLogin class
    RegLogin RegLoginObj = new RegLogin();
    //Initialise the RegLogin_Loyalty class
    RegLogin_Loyalty RegLogin_LoyaltyObj = new RegLogin_Loyalty();
    //Initialise the RegLogin_NonLoyalty_KCC class
    RegLogin_NonLoyalty_KCC RegLogin_NonLoyalty_KCCObj = new RegLogin_NonLoyalty_KCC();
    //Initialise the SoftLogin_Cookie class
    SoftLogin_Cookie SoftLogin_CookieObj = new SoftLogin_Cookie();
    //Initialise the Soft_LoggedIn class
    Soft_LoggedIn Soft_LoggedInObj = new Soft_LoggedIn();
    //Initialise the My_Info class
    My_Info My_InfoObj = new My_Info();
    //Initialise the Annual_Gross_Income class
    Annual_Gross_Income Annual_Gross_IncomeObj = new Annual_Gross_Income();
    //Initialise the AddAddress class
    AddAddress AddAddressObj = new AddAddress();
    //Initialise the EditShipAddress class
    EditShipAddress EditShipAddressObj = new EditShipAddress();
    //Initialise the RemoveShippingAddress class
    RemoveShippingAddress RemoveShippingAddressObj = new RemoveShippingAddress();
    //Initialise the AddKohlsChargeCard class
    AddKohlsChargeCard AddKohlsChargeCardObj = new AddKohlsChargeCard();
    //Initialise the AddVisaCardDetails class
    AddVisaCardDetails AddVisaCardDetailsObj = new AddVisaCardDetails();
    //Initialise the PurchaseHistory class
    PurchaseHistory PurchaseHistoryObj = new PurchaseHistory();
    //Initialise the BillinginfoAndPayment class
    BillinginfoAndPayment BillinginfoAndPaymentObj = new BillinginfoAndPayment();
    //Initialise the KohlsRewards class
    KohlsRewards KohlsRewardsObj = new KohlsRewards();
    //Initialise the Wallet_getMyWalletDetails class
    Wallet_getMyWalletDetails Wallet_getMyWalletDetailsObj = new Wallet_getMyWalletDetails();
    //Initialise the Wallet_AddGiftCard class
    Wallet_AddGiftCard Wallet_AddGiftCardObj = new Wallet_AddGiftCard();
    //Initialise the Wallet_AddKohlsCash class
    Wallet_AddKohlsCash Wallet_AddKohlsCashObj = new Wallet_AddKohlsCash();
    //Initialise the Wallet_AddOffer class
    Wallet_AddOffer Wallet_AddOfferObj = new Wallet_AddOffer();
    //Initialise the Skip_Functionality class
    Skip_Functionality Skip_FunctionalityObj = new Skip_Functionality();
    //Initialise the Wallet_AddOffer_MUPC class
    Wallet_AddOffer_MUPC Wallet_AddOffer_MUPCObj = new Wallet_AddOffer_MUPC();
    //Initialise the CreateAccount class
    CreateAccount CreateAccountObj = new CreateAccount();
    //Initialise the SessionJsp class
    SessionJsp SessionJspObj = new SessionJsp();
    //Initialise the UserCheckStatus class
    UserCheckStatus UserCheckStatusObj = new UserCheckStatus();
    //Initialise the WcsInternal_Home class
    WcsInternal_Home WcsInternal_HomeObj = new WcsInternal_Home();
    //Initialise the WebSession class
    WebSession WebSessionObj = new WebSession();
    //Initialise the Recommedndation_Home class
    Recommedndation_Home Recommedndation_HomeObj = new Recommedndation_Home();
    //Initialise the ZineOne_Home class
    ZineOne_Home ZineOne_HomeObj = new ZineOne_Home();
    //Initialise the BrowsetoCatalogPage class
    BrowsetoCatalogPage BrowsetoCatalogPageObj = new BrowsetoCatalogPage();
    //Initialise the Recommedndation_SaleEvent class
    Recommedndation_SaleEvent Recommedndation_SaleEventObj = new Recommedndation_SaleEvent();
    //Initialise the CatalogPagination class
    CatalogPagination CatalogPaginationObj = new CatalogPagination();
    //Initialise the CatalogSortBy class
    CatalogSortBy CatalogSortByObj = new CatalogSortBy();
    //Initialise the CatalogRefines class
    CatalogRefines CatalogRefinesObj = new CatalogRefines();
    //Initialise the CatalogView120items class
    CatalogView120items CatalogView120itemsObj = new CatalogView120items();
    //Initialise the Recommedndation_PMP class
    Recommedndation_PMP Recommedndation_PMPObj = new Recommedndation_PMP();
    //Initialise the CatalogPDP class
    CatalogPDP CatalogPDPObj = new CatalogPDP();
    //Initialise the Availability_pickup class
    Availability_pickup Availability_pickupObj = new Availability_pickup();
    //Initialise the ZineOne_PDP class
    ZineOne_PDP ZineOne_PDPObj = new ZineOne_PDP();
    //Initialise the SearchByKeyword class
    SearchByKeyword SearchByKeywordObj = new SearchByKeyword();
    //Initialise the SearchPDP class
    SearchPDP SearchPDPObj = new SearchPDP();
    //Initialise the Recommedndation_PDP class
    Recommedndation_PDP Recommedndation_PDPObj = new Recommedndation_PDP();
    //Initialise the Search_PID_1_50 class
    Search_PID_1_50 Search_PID_1_50Obj = new Search_PID_1_50();
    //Initialise the Search_PID_51_100 class
    Search_PID_51_100 Search_PID_51_100Obj = new Search_PID_51_100();
    //Initialise the Search_PID_101_200 class
    Search_PID_101_200 Search_PID_101_200Obj = new Search_PID_101_200();
    //Initialise the Search_PID_501_1000 class
    Search_PID_501_1000 Search_PID_501_1000Obj = new Search_PID_501_1000();
    //Initialise the Search_PID_1000 class
    Search_PID_1000 Search_PID_1000Obj = new Search_PID_1000();
    //Initialise the Search_PID_201_500 class
    Search_PID_201_500 Search_PID_201_500Obj = new Search_PID_201_500();
    //Initialise the Recommedndation_Search class
    Recommedndation_Search Recommedndation_SearchObj = new Recommedndation_Search();
    //Initialise the TypeHeadSolr class
    TypeHeadSolr TypeHeadSolrObj = new TypeHeadSolr();
    //Initialise the PDPOnly class
    PDPOnly PDPOnlyObj = new PDPOnly();
    //Initialise the AddtoBag class
    AddtoBag AddtoBagObj = new AddtoBag();
    //Initialise the ZineOne_AddtoBag class
    ZineOne_AddtoBag ZineOne_AddtoBagObj = new ZineOne_AddtoBag();
    //Initialise the PersistentBar class
    PersistentBar PersistentBarObj = new PersistentBar();
    //Initialise the Persistent_bar_components class
    Persistent_bar_components Persistent_bar_componentsObj = new Persistent_bar_components();
    //Initialise the ShopingCart class
    ShopingCart ShopingCartObj = new ShopingCart();
    //Initialise the CartItemRemove class
    CartItemRemove CartItemRemoveObj = new CartItemRemove();
    //Initialise the Recommedndation_CNC class
    Recommedndation_CNC Recommedndation_CNCObj = new Recommedndation_CNC();
    //Initialise the Checkout class
    Checkout CheckoutObj = new Checkout();
    //Initialise the SoftLogin_SingedInCheckout class
    SoftLogin_SingedInCheckout SoftLogin_SingedInCheckoutObj = new SoftLogin_SingedInCheckout();
    //Initialise the CartItemRemove_LowQty class
    CartItemRemove_LowQty CartItemRemove_LowQtyObj = new CartItemRemove_LowQty();
    //Initialise the ApplyPromoCode class
    ApplyPromoCode ApplyPromoCodeObj = new ApplyPromoCode();
    //Initialise the ApplyKohlsCash class
    ApplyKohlsCash ApplyKohlsCashObj = new ApplyKohlsCash();
    //Initialise the Apply_SUPC class
    Apply_SUPC Apply_SUPCObj = new Apply_SUPC();
    //Initialise the AddAddress_onCheckout class
    AddAddress_onCheckout AddAddress_onCheckoutObj = new AddAddress_onCheckout();
    //Initialise the ContinueCheckout class
    ContinueCheckout ContinueCheckoutObj = new ContinueCheckout();
    //Initialise the Recommedndation_CNC2 class
    Recommedndation_CNC2 Recommedndation_CNC2Obj = new Recommedndation_CNC2();
    //Initialise the AddPaymentCard_OnCheckout class
    AddPaymentCard_OnCheckout AddPaymentCard_OnCheckoutObj = new AddPaymentCard_OnCheckout();
    //Initialise the AddBillingAddress_OnCheckout class
    AddBillingAddress_OnCheckout AddBillingAddress_OnCheckoutObj = new AddBillingAddress_OnCheckout();
    //Initialise the CartItemUpdate_Qty class
    CartItemUpdate_Qty CartItemUpdate_QtyObj = new CartItemUpdate_Qty();
    //Initialise the ContinueToReviewOrder class
    ContinueToReviewOrder ContinueToReviewOrderObj = new ContinueToReviewOrder();
    //Initialise the PlaceOrder_SL class
    PlaceOrder_SL PlaceOrder_SLObj = new PlaceOrder_SL();
    //Initialise the OrderConfirm class
    OrderConfirm OrderConfirmObj = new OrderConfirm();
    //Initialise the Logout class
    Logout LogoutObj = new Logout();
    //Initialise the SetMyStore class
    SetMyStore SetMyStoreObj = new SetMyStore();
    //Initialise the Search_BopusProduct class
    Search_BopusProduct Search_BopusProductObj = new Search_BopusProduct();
    //Initialise the BopusAddtoBag class
    BopusAddtoBag BopusAddtoBagObj = new BopusAddtoBag();
    //Initialise the Search_BossProduct class
    Search_BossProduct Search_BossProductObj = new Search_BossProduct();
    //Initialise the BossAddtoBag class
    BossAddtoBag BossAddtoBagObj = new BossAddtoBag();
    //Initialise the Home class
    Home HomeObj = new Home();
    //Initialise the CartItem_ShipItFaster class
    CartItem_ShipItFaster CartItem_ShipItFasterObj = new CartItem_ShipItFaster();
    //Initialise the PlaceOrder_Reg class
    PlaceOrder_Reg PlaceOrder_RegObj = new PlaceOrder_Reg();
    //Initialise the RegLogin_Bopus class
    RegLogin_Bopus RegLogin_BopusObj = new RegLogin_Bopus();
    //Initialise the UnReg_Cookie class
    UnReg_Cookie UnReg_CookieObj = new UnReg_Cookie();
    //Initialise the GuestCheckout_SetSession class
    GuestCheckout_SetSession GuestCheckout_SetSessionObj = new GuestCheckout_SetSession();
    //Initialise the UnReg_ContinueCheckout class
    UnReg_ContinueCheckout UnReg_ContinueCheckoutObj = new UnReg_ContinueCheckout();
    //Initialise the UnReg_Orderreview class
    UnReg_Orderreview UnReg_OrderreviewObj = new UnReg_Orderreview();
    //Initialise the UnReg_placeorder class
    UnReg_placeorder UnReg_placeorderObj = new UnReg_placeorder();
    //Initialise the UnReg_Order_confirm class
    UnReg_Order_confirm UnReg_Order_confirmObj = new UnReg_Order_confirm();
    //Initialise the UnReg_Bopus_ContinueCheckout class
    UnReg_Bopus_ContinueCheckout UnReg_Bopus_ContinueCheckoutObj = new UnReg_Bopus_ContinueCheckout();
    //Initialise the UnReg_Bopus_Orderreview class
    UnReg_Bopus_Orderreview UnReg_Bopus_OrderreviewObj = new UnReg_Bopus_Orderreview();
    //Initialise the UnReg_placeorder_bopus class
    UnReg_placeorder_bopus UnReg_placeorder_bopusObj = new UnReg_placeorder_bopus();
    //Initialise the SearchPDP_All_hit class
    SearchPDP_All_hit SearchPDP_All_hitObj = new SearchPDP_All_hit();
    //Initialise the SignInCheckout class
    SignInCheckout SignInCheckoutObj = new SignInCheckout();
    //Initialise the SingedInCheckout class
    SingedInCheckout SingedInCheckoutObj = new SingedInCheckout();
    //Initialise the RegLogin_VGC class
    RegLogin_VGC RegLogin_VGCObj = new RegLogin_VGC();
    //Initialise the VGC_BrowseNAddtoBag class
    VGC_BrowseNAddtoBag VGC_BrowseNAddtoBagObj = new VGC_BrowseNAddtoBag();
    //Initialise the AddDataforAddtoBag class
    AddDataforAddtoBag AddDataforAddtoBagObj = new AddDataforAddtoBag();
    //Initialise the AddDataforAddtoBagBopus class
    AddDataforAddtoBagBopus AddDataforAddtoBagBopusObj = new AddDataforAddtoBagBopus();
    //Initialise the AddDataforAddtoBagBoss class
    AddDataforAddtoBagBoss AddDataforAddtoBagBossObj = new AddDataforAddtoBagBoss();
    //Initialise the CatalogCategory class
    CatalogCategory CatalogCategoryObj = new CatalogCategory();
    //Initialise the CatalogByMonetization class
    CatalogByMonetization CatalogByMonetizationObj = new CatalogByMonetization();
    //Initialise the CatalogGetProductCount class
    CatalogGetProductCount CatalogGetProductCountObj = new CatalogGetProductCount();
    //Initialise the Catalog_VisualTypeAhead class
    Catalog_VisualTypeAhead Catalog_VisualTypeAheadObj = new Catalog_VisualTypeAhead();
    //Initialise the CatalogByActiveDimension_P_Guest class
    CatalogByActiveDimension_P_Guest CatalogByActiveDimension_P_GuestObj = new CatalogByActiveDimension_P_Guest();
    //Initialise the CatalogByKeyword_Bopus class
    CatalogByKeyword_Bopus CatalogByKeyword_BopusObj = new CatalogByKeyword_Bopus();
    //Initialise the CatalogByKeywordSuggestions class
    CatalogByKeywordSuggestions CatalogByKeywordSuggestionsObj = new CatalogByKeywordSuggestions();
    //Initialise the CatalogByActiveDimension_NonP class
    CatalogByActiveDimension_NonP CatalogByActiveDimension_NonPObj = new CatalogByActiveDimension_NonP();
    //Initialise the CatalogByActiveDimension_P class
    CatalogByActiveDimension_P CatalogByActiveDimension_PObj = new CatalogByActiveDimension_P();
    //Initialise the CatalogByKeyword_NonP class
    CatalogByKeyword_NonP CatalogByKeyword_NonPObj = new CatalogByKeyword_NonP();
    //Initialise the CatalogByKeyword_P class
    CatalogByKeyword_P CatalogByKeyword_PObj = new CatalogByKeyword_P();
    //Initialise the CatalogByKeyword_P_Guest class
    CatalogByKeyword_P_Guest CatalogByKeyword_P_GuestObj = new CatalogByKeyword_P_Guest();
    //Initialise the Catalog_StaticMessages class
    Catalog_StaticMessages Catalog_StaticMessagesObj = new Catalog_StaticMessages();
    //Initialise the Product_WebID_5_v2 class
    Product_WebID_5_v2 Product_WebID_5_v2Obj = new Product_WebID_5_v2();
    //Initialise the Product_WebID_5_10_v2 class
    Product_WebID_5_10_v2 Product_WebID_5_10_v2Obj = new Product_WebID_5_10_v2();
    //Initialise the Product_WebID_10_30_v2 class
    Product_WebID_10_30_v2 Product_WebID_10_30_v2Obj = new Product_WebID_10_30_v2();
    //Initialise the Product_WebID_30_50_v2 class
    Product_WebID_30_50_v2 Product_WebID_30_50_v2Obj = new Product_WebID_30_50_v2();
    //Initialise the Product_WebID_51_100_v2 class
    Product_WebID_51_100_v2 Product_WebID_51_100_v2Obj = new Product_WebID_51_100_v2();
    //Initialise the Continue class
    Continue ContinueObj = new Continue();
    //Initialise the Product_WebID_201_500_v2 class
    Product_WebID_201_500_v2 Product_WebID_201_500_v2Obj = new Product_WebID_201_500_v2();
    //Initialise the Product_WebID_501_1000_v2 class
    Product_WebID_501_1000_v2 Product_WebID_501_1000_v2Obj = new Product_WebID_501_1000_v2();
    //Initialise the Product_WebID_1000_v2 class
    Product_WebID_1000_v2 Product_WebID_1000_v2Obj = new Product_WebID_1000_v2();
    //Initialise the Availability class
    Availability AvailabilityObj = new Availability();
    //Initialise the Delivery class
    Delivery DeliveryObj = new Delivery();
    //Initialise the ProductStaticMessages class
    ProductStaticMessages ProductStaticMessagesObj = new ProductStaticMessages();
    //Initialise the PriceBySKU_GET class
    PriceBySKU_GET PriceBySKU_GETObj = new PriceBySKU_GET();
    //Initialise the PriceByWebId_GET class
    PriceByWebId_GET PriceByWebId_GETObj = new PriceByWebId_GET();
    //Initialise the InventorySKU class
    InventorySKU InventorySKUObj = new InventorySKU();
    //Initialise the InventoryWebID class
    InventoryWebID InventoryWebIDObj = new InventoryWebID();
    //Initialise the Store class
    Store StoreObj = new Store();
    //Initialise the StoreAddress class
    StoreAddress StoreAddressObj = new StoreAddress();
    //Initialise the StoreLatitude class
    StoreLatitude StoreLatitudeObj = new StoreLatitude();
    //Initialise the StoreRadius class
    StoreRadius StoreRadiusObj = new StoreRadius();
    //Initialise the YourPrice class
    YourPrice YourPriceObj = new YourPrice();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence block - Start
        //Executing flow - PercentageControl
        PercentageControlObj.execute(nsApi);
        //Executing flow - RegLogin
        RegLoginObj.execute(nsApi);
        //Executing flow - RegLogin_Loyalty
        RegLogin_LoyaltyObj.execute(nsApi);
        //Executing flow - RegLogin_NonLoyalty_KCC
        RegLogin_NonLoyalty_KCCObj.execute(nsApi);
        //Executing flow - SoftLogin_Cookie
        SoftLogin_CookieObj.execute(nsApi);
        //Executing flow - Soft_LoggedIn
        Soft_LoggedInObj.execute(nsApi);
        //Executing flow - My_Info
        My_InfoObj.execute(nsApi);
        //Executing flow - Annual_Gross_Income
        Annual_Gross_IncomeObj.execute(nsApi);
        //Executing flow - AddAddress
        AddAddressObj.execute(nsApi);
        //Executing flow - EditShipAddress
        EditShipAddressObj.execute(nsApi);
        //Executing flow - RemoveShippingAddress
        RemoveShippingAddressObj.execute(nsApi);
        //Executing flow - AddKohlsChargeCard
        AddKohlsChargeCardObj.execute(nsApi);
        //Executing flow - AddVisaCardDetails
        AddVisaCardDetailsObj.execute(nsApi);
        //Executing flow - PurchaseHistory
        PurchaseHistoryObj.execute(nsApi);
        //Executing flow - BillinginfoAndPayment
        BillinginfoAndPaymentObj.execute(nsApi);
        //Executing flow - KohlsRewards
        KohlsRewardsObj.execute(nsApi);
        //Executing flow - Wallet_getMyWalletDetails
        Wallet_getMyWalletDetailsObj.execute(nsApi);
        //Executing flow - Wallet_AddGiftCard
        Wallet_AddGiftCardObj.execute(nsApi);
        //Executing flow - Wallet_AddKohlsCash
        Wallet_AddKohlsCashObj.execute(nsApi);
        //Executing flow - Wallet_AddOffer
        Wallet_AddOfferObj.execute(nsApi);
        //Executing flow - Skip_Functionality
        Skip_FunctionalityObj.execute(nsApi);
        //Executing flow - Wallet_AddOffer_MUPC
        Wallet_AddOffer_MUPCObj.execute(nsApi);
        //Executing flow - CreateAccount
        CreateAccountObj.execute(nsApi);
        //Executing flow - SessionJsp
        SessionJspObj.execute(nsApi);
        //Executing flow - UserCheckStatus
        UserCheckStatusObj.execute(nsApi);
        //Executing flow - WcsInternal_Home
        WcsInternal_HomeObj.execute(nsApi);
        //Executing flow - WebSession
        WebSessionObj.execute(nsApi);
        //Executing flow - Recommedndation_Home
        Recommedndation_HomeObj.execute(nsApi);
        //Executing flow - ZineOne_Home
        ZineOne_HomeObj.execute(nsApi);
        //Executing flow - BrowsetoCatalogPage
        BrowsetoCatalogPageObj.execute(nsApi);
        //Executing flow - Recommedndation_SaleEvent
        Recommedndation_SaleEventObj.execute(nsApi);
        //Executing flow - CatalogPagination
        CatalogPaginationObj.execute(nsApi);
        //Executing flow - CatalogSortBy
        CatalogSortByObj.execute(nsApi);
        //Executing flow - CatalogRefines
        CatalogRefinesObj.execute(nsApi);
        //Executing flow - CatalogView120items
        CatalogView120itemsObj.execute(nsApi);
        //Executing flow - Recommedndation_PMP
        Recommedndation_PMPObj.execute(nsApi);
        //Executing flow - CatalogPDP
        CatalogPDPObj.execute(nsApi);
        //Executing flow - Availability_pickup
        Availability_pickupObj.execute(nsApi);
        //Executing flow - ZineOne_PDP
        ZineOne_PDPObj.execute(nsApi);
        //Executing flow - SearchByKeyword
        SearchByKeywordObj.execute(nsApi);
        //Executing flow - SearchPDP
        SearchPDPObj.execute(nsApi);
        //Executing flow - Recommedndation_PDP
        Recommedndation_PDPObj.execute(nsApi);
        //Executing flow - Search_PID_1_50
        Search_PID_1_50Obj.execute(nsApi);
        //Executing flow - Search_PID_51_100
        Search_PID_51_100Obj.execute(nsApi);
        //Executing flow - Search_PID_101_200
        Search_PID_101_200Obj.execute(nsApi);
        //Executing flow - Search_PID_501_1000
        Search_PID_501_1000Obj.execute(nsApi);
        //Executing flow - Search_PID_1000
        Search_PID_1000Obj.execute(nsApi);
        //Executing flow - Search_PID_201_500
        Search_PID_201_500Obj.execute(nsApi);
        //Executing flow - Recommedndation_Search
        Recommedndation_SearchObj.execute(nsApi);
        //Executing flow - TypeHeadSolr
        TypeHeadSolrObj.execute(nsApi);
        //Executing flow - PDPOnly
        PDPOnlyObj.execute(nsApi);
        //Executing flow - AddtoBag
        AddtoBagObj.execute(nsApi);
        //Executing flow - ZineOne_AddtoBag
        ZineOne_AddtoBagObj.execute(nsApi);
        //Executing flow - PersistentBar
        PersistentBarObj.execute(nsApi);
        //Executing flow - Persistent_bar_components
        Persistent_bar_componentsObj.execute(nsApi);
        //Executing flow - ShopingCart
        ShopingCartObj.execute(nsApi);
        //Executing flow - CartItemRemove
        CartItemRemoveObj.execute(nsApi);
        //Executing flow - Recommedndation_CNC
        Recommedndation_CNCObj.execute(nsApi);
        //Executing flow - Checkout
        CheckoutObj.execute(nsApi);
        //Executing flow - SoftLogin_SingedInCheckout
        SoftLogin_SingedInCheckoutObj.execute(nsApi);
        //Executing flow - CartItemRemove_LowQty
        CartItemRemove_LowQtyObj.execute(nsApi);
        //Executing flow - ApplyPromoCode
        ApplyPromoCodeObj.execute(nsApi);
        //Executing flow - ApplyKohlsCash
        ApplyKohlsCashObj.execute(nsApi);
        //Executing flow - Apply_SUPC
        Apply_SUPCObj.execute(nsApi);
        //Executing flow - AddAddress_onCheckout
        AddAddress_onCheckoutObj.execute(nsApi);
        //Executing flow - ContinueCheckout
        ContinueCheckoutObj.execute(nsApi);
        //Executing flow - Recommedndation_CNC2
        Recommedndation_CNC2Obj.execute(nsApi);
        //Executing flow - AddPaymentCard_OnCheckout
        AddPaymentCard_OnCheckoutObj.execute(nsApi);
        //Executing flow - AddBillingAddress_OnCheckout
        AddBillingAddress_OnCheckoutObj.execute(nsApi);
        //Executing flow - CartItemUpdate_Qty
        CartItemUpdate_QtyObj.execute(nsApi);
        //Executing flow - ContinueToReviewOrder
        ContinueToReviewOrderObj.execute(nsApi);
        //Executing flow - PlaceOrder_SL
        PlaceOrder_SLObj.execute(nsApi);
        //Executing flow - OrderConfirm
        OrderConfirmObj.execute(nsApi);
        //Executing flow - Logout
        LogoutObj.execute(nsApi);
        //Executing flow - SetMyStore
        SetMyStoreObj.execute(nsApi);
        //Executing flow - Search_BopusProduct
        Search_BopusProductObj.execute(nsApi);
        //Executing flow - BopusAddtoBag
        BopusAddtoBagObj.execute(nsApi);
        //Executing flow - Search_BossProduct
        Search_BossProductObj.execute(nsApi);
        //Executing flow - BossAddtoBag
        BossAddtoBagObj.execute(nsApi);
        //Executing flow - Home
        HomeObj.execute(nsApi);
        //Executing flow - CartItem_ShipItFaster
        CartItem_ShipItFasterObj.execute(nsApi);
        //Executing flow - PlaceOrder_Reg
        PlaceOrder_RegObj.execute(nsApi);
        //Executing flow - RegLogin_Bopus
        RegLogin_BopusObj.execute(nsApi);
        //Executing flow - UnReg_Cookie
        UnReg_CookieObj.execute(nsApi);
        //Executing flow - GuestCheckout_SetSession
        GuestCheckout_SetSessionObj.execute(nsApi);
        //Executing flow - UnReg_ContinueCheckout
        UnReg_ContinueCheckoutObj.execute(nsApi);
        //Executing flow - UnReg_Orderreview
        UnReg_OrderreviewObj.execute(nsApi);
        //Executing flow - UnReg_placeorder
        UnReg_placeorderObj.execute(nsApi);
        //Executing flow - UnReg_Order_confirm
        UnReg_Order_confirmObj.execute(nsApi);
        //Executing flow - UnReg_Bopus_ContinueCheckout
        UnReg_Bopus_ContinueCheckoutObj.execute(nsApi);
        //Executing flow - UnReg_Bopus_Orderreview
        UnReg_Bopus_OrderreviewObj.execute(nsApi);
        //Executing flow - UnReg_placeorder_bopus
        UnReg_placeorder_bopusObj.execute(nsApi);
        //Executing flow - SearchPDP_All_hit
        SearchPDP_All_hitObj.execute(nsApi);
        //Executing flow - SignInCheckout
        SignInCheckoutObj.execute(nsApi);
        //Executing flow - SingedInCheckout
        SingedInCheckoutObj.execute(nsApi);
        //Executing flow - RegLogin_VGC
        RegLogin_VGCObj.execute(nsApi);
        //Executing flow - VGC_BrowseNAddtoBag
        VGC_BrowseNAddtoBagObj.execute(nsApi);
        //Executing flow - AddDataforAddtoBag
        AddDataforAddtoBagObj.execute(nsApi);
        //Executing flow - AddDataforAddtoBagBopus
        AddDataforAddtoBagBopusObj.execute(nsApi);
        //Executing flow - AddDataforAddtoBagBoss
        AddDataforAddtoBagBossObj.execute(nsApi);
        //Executing flow - CatalogCategory
        CatalogCategoryObj.execute(nsApi);
        //Executing flow - CatalogByMonetization
        CatalogByMonetizationObj.execute(nsApi);
        //Executing flow - CatalogGetProductCount
        CatalogGetProductCountObj.execute(nsApi);
        //Executing flow - Catalog_VisualTypeAhead
        Catalog_VisualTypeAheadObj.execute(nsApi);
        //Executing flow - CatalogByActiveDimension_P_Guest
        CatalogByActiveDimension_P_GuestObj.execute(nsApi);
        //Executing flow - CatalogByKeyword_Bopus
        CatalogByKeyword_BopusObj.execute(nsApi);
        //Executing flow - CatalogByKeywordSuggestions
        CatalogByKeywordSuggestionsObj.execute(nsApi);
        //Executing flow - CatalogByActiveDimension_NonP
        CatalogByActiveDimension_NonPObj.execute(nsApi);
        //Executing flow - CatalogByActiveDimension_P
        CatalogByActiveDimension_PObj.execute(nsApi);
        //Executing flow - CatalogByKeyword_NonP
        CatalogByKeyword_NonPObj.execute(nsApi);
        //Executing flow - CatalogByKeyword_P
        CatalogByKeyword_PObj.execute(nsApi);
        //Executing flow - CatalogByKeyword_P_Guest
        CatalogByKeyword_P_GuestObj.execute(nsApi);
        //Executing flow - Catalog_StaticMessages
        Catalog_StaticMessagesObj.execute(nsApi);
        //Executing flow - Product_WebID_5_v2
        Product_WebID_5_v2Obj.execute(nsApi);
        //Executing flow - Product_WebID_5_10_v2
        Product_WebID_5_10_v2Obj.execute(nsApi);
        //Executing flow - Product_WebID_10_30_v2
        Product_WebID_10_30_v2Obj.execute(nsApi);
        //Executing flow - Product_WebID_30_50_v2
        Product_WebID_30_50_v2Obj.execute(nsApi);
        //Executing flow - Product_WebID_51_100_v2
        Product_WebID_51_100_v2Obj.execute(nsApi);
        //Executing flow - Continue
        ContinueObj.execute(nsApi);
        //Executing flow - Product_WebID_201_500_v2
        Product_WebID_201_500_v2Obj.execute(nsApi);
        //Executing flow - Product_WebID_501_1000_v2
        Product_WebID_501_1000_v2Obj.execute(nsApi);
        //Executing flow - Product_WebID_1000_v2
        Product_WebID_1000_v2Obj.execute(nsApi);
        //Executing flow - Availability
        AvailabilityObj.execute(nsApi);
        //Executing flow - Delivery
        DeliveryObj.execute(nsApi);
        //Executing flow - ProductStaticMessages
        ProductStaticMessagesObj.execute(nsApi);
        //Executing flow - PriceBySKU_GET
        PriceBySKU_GETObj.execute(nsApi);
        //Executing flow - PriceByWebId_GET
        PriceByWebId_GETObj.execute(nsApi);
        //Executing flow - InventorySKU
        InventorySKUObj.execute(nsApi);
        //Executing flow - InventoryWebID
        InventoryWebIDObj.execute(nsApi);
        //Executing flow - Store
        StoreObj.execute(nsApi);
        //Executing flow - StoreAddress
        StoreAddressObj.execute(nsApi);
        //Executing flow - StoreLatitude
        StoreLatitudeObj.execute(nsApi);
        //Executing flow - StoreRadius
        StoreRadiusObj.execute(nsApi);
        //Executing flow - YourPrice
        YourPriceObj.execute(nsApi);

        //logging
        nsApi.ns_end_session();
    }

}
