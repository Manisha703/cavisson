/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class CnC_Services
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the RegSessionStatus class
    RegSessionStatus RegSessionStatusObj = new RegSessionStatus();
    //Initialise the GenerateSessionID class
    GenerateSessionID GenerateSessionIDObj = new GenerateSessionID();
    //Initialise the Authenticate1 class
    Authenticate1 Authenticate1Obj = new Authenticate1();
    //Initialise the RegGet_Mini_Cart class
    RegGet_Mini_Cart RegGet_Mini_CartObj = new RegGet_Mini_Cart();
    //Initialise the RegGet_Cart class
    RegGet_Cart RegGet_CartObj = new RegGet_Cart();
    //Initialise the RegAdd_Cart class
    RegAdd_Cart RegAdd_CartObj = new RegAdd_Cart();
    //Initialise the RegGet_AllAddressByType class
    RegGet_AllAddressByType RegGet_AllAddressByTypeObj = new RegGet_AllAddressByType();
    //Initialise the RegAdd_ShippingAddress class
    RegAdd_ShippingAddress RegAdd_ShippingAddressObj = new RegAdd_ShippingAddress();
    //Initialise the RegAdd_BillingAddress class
    RegAdd_BillingAddress RegAdd_BillingAddressObj = new RegAdd_BillingAddress();
    //Initialise the RegGet_AddressById class
    RegGet_AddressById RegGet_AddressByIdObj = new RegGet_AddressById();
    //Initialise the Continue class
    Continue ContinueObj = new Continue();
    //Initialise the Updadte_CartShipMethod class
    Updadte_CartShipMethod Updadte_CartShipMethodObj = new Updadte_CartShipMethod();
    //Initialise the Updadte_Shipment class
    Updadte_Shipment Updadte_ShipmentObj = new Updadte_Shipment();
    //Initialise the RegUpdate_Addresses class
    RegUpdate_Addresses RegUpdate_AddressesObj = new RegUpdate_Addresses();
    //Initialise the RegDelete_Address class
    RegDelete_Address RegDelete_AddressObj = new RegDelete_Address();
    //Initialise the RegUpdate_BillingAddresses class
    RegUpdate_BillingAddresses RegUpdate_BillingAddressesObj = new RegUpdate_BillingAddresses();
    //Initialise the RegGet_AllPaymentCard class
    RegGet_AllPaymentCard RegGet_AllPaymentCardObj = new RegGet_AllPaymentCard();
    //Initialise the RegGet_PaymentCard class
    RegGet_PaymentCard RegGet_PaymentCardObj = new RegGet_PaymentCard();
    //Initialise the RegAdd_PaymentCard class
    RegAdd_PaymentCard RegAdd_PaymentCardObj = new RegAdd_PaymentCard();
    //Initialise the RegApplyCreditcardPayment class
    RegApplyCreditcardPayment RegApplyCreditcardPaymentObj = new RegApplyCreditcardPayment();
    //Initialise the RegUpdate_PaymentCard class
    RegUpdate_PaymentCard RegUpdate_PaymentCardObj = new RegUpdate_PaymentCard();
    //Initialise the RegDelete_PaymentCard class
    RegDelete_PaymentCard RegDelete_PaymentCardObj = new RegDelete_PaymentCard();
    //Initialise the RegApply_Coupon class
    RegApply_Coupon RegApply_CouponObj = new RegApply_Coupon();
    //Initialise the Reg_Remove_Coupon class
    Reg_Remove_Coupon Reg_Remove_CouponObj = new Reg_Remove_Coupon();
    //Initialise the RegApply_GiftCard class
    RegApply_GiftCard RegApply_GiftCardObj = new RegApply_GiftCard();
    //Initialise the RegGet_KohlsCash class
    RegGet_KohlsCash RegGet_KohlsCashObj = new RegGet_KohlsCash();
    //Initialise the RegUpdate_Cart class
    RegUpdate_Cart RegUpdate_CartObj = new RegUpdate_Cart();
    //Initialise the RegDelete_Cart class
    RegDelete_Cart RegDelete_CartObj = new RegDelete_Cart();
    //Initialise the RegReview_Cart class
    RegReview_Cart RegReview_CartObj = new RegReview_Cart();
    //Initialise the RegPlace_Order class
    RegPlace_Order RegPlace_OrderObj = new RegPlace_Order();
    //Initialise the RegOrderDetails class
    RegOrderDetails RegOrderDetailsObj = new RegOrderDetails();
    //Initialise the RegPurchase_History class
    RegPurchase_History RegPurchase_HistoryObj = new RegPurchase_History();
    //Initialise the Update_AddCartAddress class
    Update_AddCartAddress Update_AddCartAddressObj = new Update_AddCartAddress();
    //Initialise the UnRegSessionStatus class
    UnRegSessionStatus UnRegSessionStatusObj = new UnRegSessionStatus();
    //Initialise the UnRegGet_Mini_Cart class
    UnRegGet_Mini_Cart UnRegGet_Mini_CartObj = new UnRegGet_Mini_Cart();
    //Initialise the UnRegGet_Cart class
    UnRegGet_Cart UnRegGet_CartObj = new UnRegGet_Cart();
    //Initialise the UnRegAdd_Cart class
    UnRegAdd_Cart UnRegAdd_CartObj = new UnRegAdd_Cart();
    //Initialise the UnRegAdd_CartShipment class
    UnRegAdd_CartShipment UnRegAdd_CartShipmentObj = new UnRegAdd_CartShipment();
    //Initialise the UnRegAdd_CartPayment class
    UnRegAdd_CartPayment UnRegAdd_CartPaymentObj = new UnRegAdd_CartPayment();
    //Initialise the UnRegUpdate_Cart class
    UnRegUpdate_Cart UnRegUpdate_CartObj = new UnRegUpdate_Cart();
    //Initialise the UnRegDelete_Cart class
    UnRegDelete_Cart UnRegDelete_CartObj = new UnRegDelete_Cart();
    //Initialise the UnReg_CartReview class
    UnReg_CartReview UnReg_CartReviewObj = new UnReg_CartReview();
    //Initialise the UnRegPlace_Order class
    UnRegPlace_Order UnRegPlace_OrderObj = new UnRegPlace_Order();
    //Initialise the UnRegOrder_Details class
    UnRegOrder_Details UnRegOrder_DetailsObj = new UnRegOrder_Details();
    //Initialise the Initiate_payfone class
    Initiate_payfone Initiate_payfoneObj = new Initiate_payfone();
    //Initialise the PollingApi_Payfone class
    PollingApi_Payfone PollingApi_PayfoneObj = new PollingApi_Payfone();
    //Initialise the FeedbackApi_Payfone class
    FeedbackApi_Payfone FeedbackApi_PayfoneObj = new FeedbackApi_Payfone();
    //Initialise the Guest_Cancel_Order class
    Guest_Cancel_Order Guest_Cancel_OrderObj = new Guest_Cancel_Order();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing percent block - ServicesOperations
        int ServicesOperationspercent = nsApi.ns_get_random_number_int(1, 10000);

        //"Percentage random number for block - ServicesOperations = %d", ServicesOperationspercent

        if(ServicesOperationspercent <= 7000)
        {

                //Executing sequence block - RegUserBlock (pct value = 70.0%)
                //Executing flow - RegSessionStatus
                RegSessionStatusObj.execute(nsApi);
                //Executing flow - GenerateSessionID
                GenerateSessionIDObj.execute(nsApi);
                //Executing flow - Authenticate1
                Authenticate1Obj.execute(nsApi);

                    //Executing sequence block - RegOperationBlock (pct value = 70.0%)

                        //Executing percent block - RegMiniCartBlock (pct value = 70.0%)
                        int RegMiniCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegMiniCartBlock = %d", RegMiniCartBlockpercent

                        if(RegMiniCartBlockpercent <= 10000)
                        {
                            RegGet_Mini_CartObj.execute(nsApi);
                        }
                        else if(RegMiniCartBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing percent block - RegCartBlock (pct value = 70.0%)
                        int RegCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegCartBlock = %d", RegCartBlockpercent

                        if(RegCartBlockpercent <= 10000)
                        {

                                //Executing sequence block - RegCartOperationBlock (pct value = 100.0%)
                                //Executing flow - RegGet_Cart
                                RegGet_CartObj.execute(nsApi);

                                    //Executing percent block - RegAddCartBlock (pct value = 100.0%)
                                    int RegAddCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - RegAddCartBlock = %d", RegAddCartBlockpercent

                                    if(RegAddCartBlockpercent <= 10000)
                                    {

                                            //Executing sequence block - RegAddCart (pct value = 100.0%)
                                            //Executing flow - RegAdd_Cart
                                            RegAdd_CartObj.execute(nsApi);
                                            //Executing flow - RegGet_Cart
                                            RegGet_CartObj.execute(nsApi);
                                    }
                                    else if(RegAddCartBlockpercent <= 10000)
                                    {
                                        //executing abandon block - Abandon
                                        nsApi.ns_end_session();
                                    }
                        }
                        else if(RegCartBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing sequence block - RegAddressBlock (pct value = 70.0%)

                            //Executing percent block - RegAddressOperationBlock (pct value = 70.0%)
                            int RegAddressOperationBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - RegAddressOperationBlock = %d", RegAddressOperationBlockpercent

                            if(RegAddressOperationBlockpercent <= 7000)
                            {

                                    //Executing sequence block - RegAddressBlock (pct value = 70.0%)
                                    //Executing flow - RegGet_AllAddressByType
                                    RegGet_AllAddressByTypeObj.execute(nsApi);
                                    //Executing flow - RegAdd_ShippingAddress
                                    RegAdd_ShippingAddressObj.execute(nsApi);
                                    //Executing flow - RegAdd_BillingAddress
                                    RegAdd_BillingAddressObj.execute(nsApi);

                                        //Executing percent block - GetAddressByIdBlock (pct value = 70.0%)
                                        int GetAddressByIdBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - GetAddressByIdBlock = %d", GetAddressByIdBlockpercent

                                        if(GetAddressByIdBlockpercent <= 2000)
                                        {
                                            RegGet_AddressByIdObj.execute(nsApi);
                                        }
                                        else if(GetAddressByIdBlockpercent <= 10000)
                                        {
                                            ContinueObj.execute(nsApi);
                                        }

                                        //Executing percent block - RegUpdateShipmentBlock (pct value = 70.0%)
                                        int RegUpdateShipmentBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - RegUpdateShipmentBlock = %d", RegUpdateShipmentBlockpercent

                                        if(RegUpdateShipmentBlockpercent <= 2000)
                                        {
                                            Updadte_CartShipMethodObj.execute(nsApi);
                                        }
                                        else if(RegUpdateShipmentBlockpercent <= 5000)
                                        {
                                            Updadte_ShipmentObj.execute(nsApi);
                                        }
                                        else if(RegUpdateShipmentBlockpercent <= 10000)
                                        {
                                            ContinueObj.execute(nsApi);
                                        }

                                        //Executing percent block - RegAddressUpdateDeleteBlock (pct value = 70.0%)
                                        int RegAddressUpdateDeleteBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - RegAddressUpdateDeleteBlock = %d", RegAddressUpdateDeleteBlockpercent

                                        if(RegAddressUpdateDeleteBlockpercent <= 4000)
                                        {
                                            RegUpdate_AddressesObj.execute(nsApi);
                                        }
                                        else if(RegAddressUpdateDeleteBlockpercent <= 8000)
                                        {
                                            RegDelete_AddressObj.execute(nsApi);
                                        }
                                        else if(RegAddressUpdateDeleteBlockpercent <= 10000)
                                        {
                                            RegUpdate_BillingAddressesObj.execute(nsApi);
                                        }
                                        else if(RegAddressUpdateDeleteBlockpercent <= 10000)
                                        {
                                            ContinueObj.execute(nsApi);
                                        }
                            }
                            else if(RegAddressOperationBlockpercent <= 9000)
                            {
                                ContinueObj.execute(nsApi);
                            }
                            else if(RegAddressOperationBlockpercent <= 10000)
                            {
                                //executing abandon block - Abandon
                                nsApi.ns_end_session();
                            }

                        //Executing sequence block - RegPaymentBlock (pct value = 70.0%)

                            //Executing percent block - RegPaymentOperationBlock (pct value = 70.0%)
                            int RegPaymentOperationBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - RegPaymentOperationBlock = %d", RegPaymentOperationBlockpercent

                            if(RegPaymentOperationBlockpercent <= 7000)
                            {

                                    //Executing sequence block - RegPaymentBlock (pct value = 70.0%)
                                    //Executing flow - RegGet_AllPaymentCard
                                    RegGet_AllPaymentCardObj.execute(nsApi);

                                        //Executing percent block - GetPaymentByIdBlock (pct value = 70.0%)
                                        int GetPaymentByIdBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - GetPaymentByIdBlock = %d", GetPaymentByIdBlockpercent

                                        if(GetPaymentByIdBlockpercent <= 2000)
                                        {
                                            RegGet_PaymentCardObj.execute(nsApi);
                                        }
                                        else if(GetPaymentByIdBlockpercent <= 10000)
                                        {
                                            ContinueObj.execute(nsApi);
                                        }

                                        //Executing percent block - PaymentType (pct value = 70.0%)
                                        int PaymentTypepercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - PaymentType = %d", PaymentTypepercent

                                        if(PaymentTypepercent <= 10000)
                                        {

                                                //Executing sequence block - CreditCard (pct value = 100.0%)
                                                //Executing flow - RegAdd_PaymentCard
                                                RegAdd_PaymentCardObj.execute(nsApi);
                                                //Executing flow - RegApplyCreditcardPayment
                                                RegApplyCreditcardPaymentObj.execute(nsApi);
                                        }
                                        else if(PaymentTypepercent <= 10000)
                                        {
                                            ContinueObj.execute(nsApi);
                                        }

                                        //Executing percent block - RegPaymentUpdateDeleteBlock (pct value = 70.0%)
                                        int RegPaymentUpdateDeleteBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                        //"Percentage random number for block - RegPaymentUpdateDeleteBlock = %d", RegPaymentUpdateDeleteBlockpercent

                                        if(RegPaymentUpdateDeleteBlockpercent <= 4000)
                                        {
                                            RegUpdate_PaymentCardObj.execute(nsApi);
                                        }
                                        else if(RegPaymentUpdateDeleteBlockpercent <= 8000)
                                        {
                                            RegDelete_PaymentCardObj.execute(nsApi);
                                        }
                                        else if(RegPaymentUpdateDeleteBlockpercent <= 10000)
                                        {
                                            ContinueObj.execute(nsApi);
                                        }
                            }
                            else if(RegPaymentOperationBlockpercent <= 9000)
                            {
                                ContinueObj.execute(nsApi);
                            }
                            else if(RegPaymentOperationBlockpercent <= 10000)
                            {
                                //executing abandon block - Abandon
                                nsApi.ns_end_session();
                            }

                        //Executing percent block - RegApplyOfferBlock (pct value = 70.0%)
                        int RegApplyOfferBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegApplyOfferBlock = %d", RegApplyOfferBlockpercent

                        if(RegApplyOfferBlockpercent <= 10000)
                        {

                                //Executing sequence block - RegPromoCode (pct value = 100.0%)
                                //Executing flow - RegApply_Coupon
                                RegApply_CouponObj.execute(nsApi);

                                    //Executing percent block - RemovePromo (pct value = 100.0%)
                                    int RemovePromopercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - RemovePromo = %d", RemovePromopercent

                                    if(RemovePromopercent <= 3000)
                                    {
                                        Reg_Remove_CouponObj.execute(nsApi);
                                    }
                                    else if(RemovePromopercent <= 10000)
                                    {
                                        ContinueObj.execute(nsApi);
                                    }
                        }
                        else if(RegApplyOfferBlockpercent <= 10000)
                        {
                            RegApply_GiftCardObj.execute(nsApi);
                        }
                        else if(RegApplyOfferBlockpercent <= 10000)
                        {
                            RegGet_KohlsCashObj.execute(nsApi);
                        }
                        else if(RegApplyOfferBlockpercent <= 10000)
                        {
                            ContinueObj.execute(nsApi);
                        }

                        //Executing percent block - RegUpdateAndDeleteBlock (pct value = 70.0%)
                        int RegUpdateAndDeleteBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegUpdateAndDeleteBlock = %d", RegUpdateAndDeleteBlockpercent

                        if(RegUpdateAndDeleteBlockpercent <= 4000)
                        {

                                //Executing sequence block - RegUpdateCartBlock (pct value = 40.0%)
                                //Executing flow - RegUpdate_Cart
                                RegUpdate_CartObj.execute(nsApi);
                                //Executing flow - RegGet_Cart
                                RegGet_CartObj.execute(nsApi);
                        }
                        else if(RegUpdateAndDeleteBlockpercent <= 8000)
                        {

                                //Executing sequence block - RegDeleteCartBlock (pct value = 40.0%)
                                //Executing flow - RegDelete_Cart
                                RegDelete_CartObj.execute(nsApi);
                                //Executing flow - RegGet_Cart
                                RegGet_CartObj.execute(nsApi);
                        }
                        else if(RegUpdateAndDeleteBlockpercent <= 10000)
                        {

                                //Executing sequence block - RegContinueBlock (pct value = 20.0%)
                                //Executing flow - Continue
                                ContinueObj.execute(nsApi);
                        }

                        //Executing percent block - RegReviewCartBlock (pct value = 70.0%)
                        int RegReviewCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegReviewCartBlock = %d", RegReviewCartBlockpercent

                        if(RegReviewCartBlockpercent <= 9000)
                        {
                            RegReview_CartObj.execute(nsApi);
                        }
                        else if(RegReviewCartBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing percent block - RegPlaceOrderBlock (pct value = 70.0%)
                        int RegPlaceOrderBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegPlaceOrderBlock = %d", RegPlaceOrderBlockpercent

                        if(RegPlaceOrderBlockpercent <= 10000)
                        {

                                //Executing sequence block - RegPlaceOrderClock (pct value = 100.0%)
                                //Executing flow - RegGet_Cart
                                RegGet_CartObj.execute(nsApi);

                                    //Executing percent block - PCT_RBLOCK (pct value = 100.0%)
                                    int PCT_RBLOCKpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - PCT_RBLOCK = %d", PCT_RBLOCKpercent

                                    if(PCT_RBLOCKpercent <= 2500)
                                    {
                                        RegPlace_OrderObj.execute(nsApi);
                                    }
                                    else if(PCT_RBLOCKpercent <= 10000)
                                    {
                                        //executing abandon block - Abandon
                                        nsApi.ns_end_session();
                                    }
                                //Executing flow - RegOrderDetails
                                RegOrderDetailsObj.execute(nsApi);
                        }
                        else if(RegPlaceOrderBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing percent block - RegOrderInfoBlock (pct value = 70.0%)
                        int RegOrderInfoBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - RegOrderInfoBlock = %d", RegOrderInfoBlockpercent

                        if(RegOrderInfoBlockpercent <= 4000)
                        {
                            RegPurchase_HistoryObj.execute(nsApi);
                        }
                        else if(RegOrderInfoBlockpercent <= 4000)
                        {
                            Update_AddCartAddressObj.execute(nsApi);
                        }
                        else if(RegOrderInfoBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }
        }
        else if(ServicesOperationspercent <= 10000)
        {

                //Executing sequence block - UnRegUserBlock (pct value = 30.0%)
                //Executing flow - UnRegSessionStatus
                UnRegSessionStatusObj.execute(nsApi);
                //Executing flow - GenerateSessionID
                GenerateSessionIDObj.execute(nsApi);

                    //Executing sequence block - UnRegOperationBlock (pct value = 30.0%)

                        //Executing percent block - UnRegMiniCartBlock (pct value = 30.0%)
                        int UnRegMiniCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - UnRegMiniCartBlock = %d", UnRegMiniCartBlockpercent

                        if(UnRegMiniCartBlockpercent <= 10000)
                        {
                            UnRegGet_Mini_CartObj.execute(nsApi);
                        }
                        else if(UnRegMiniCartBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing percent block - UnRegCartBlock (pct value = 30.0%)
                        int UnRegCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - UnRegCartBlock = %d", UnRegCartBlockpercent

                        if(UnRegCartBlockpercent <= 10000)
                        {

                                //Executing sequence block - UnRegCartOperationBlock (pct value = 100.0%)
                                //Executing flow - UnRegGet_Cart
                                UnRegGet_CartObj.execute(nsApi);

                                    //Executing percent block - UnRegAddCartBlock (pct value = 100.0%)
                                    int UnRegAddCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - UnRegAddCartBlock = %d", UnRegAddCartBlockpercent

                                    if(UnRegAddCartBlockpercent <= 10000)
                                    {

                                            //Executing sequence block - UnRegAddCart (pct value = 100.0%)
                                            //Executing flow - UnRegAdd_Cart
                                            UnRegAdd_CartObj.execute(nsApi);
                                            //Executing flow - UnRegGet_Cart
                                            UnRegGet_CartObj.execute(nsApi);
                                    }
                                    else if(UnRegAddCartBlockpercent <= 10000)
                                    {
                                        //executing abandon block - Abandon
                                        nsApi.ns_end_session();
                                    }
                        }
                        else if(UnRegCartBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing sequence block - UnRegAddressBlock (pct value = 30.0%)

                            //Executing percent block - UnRegAddressOperationblock (pct value = 30.0%)
                            int UnRegAddressOperationblockpercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - UnRegAddressOperationblock = %d", UnRegAddressOperationblockpercent

                            if(UnRegAddressOperationblockpercent <= 8000)
                            {

                                    //Executing sequence block - UnRegAddAddressBlock (pct value = 80.0%)
                                    //Executing flow - UnRegAdd_CartShipment
                                    UnRegAdd_CartShipmentObj.execute(nsApi);
                            }
                            else if(UnRegAddressOperationblockpercent <= 10000)
                            {
                                //executing abandon block - Abandon
                                nsApi.ns_end_session();
                            }

                        //Executing sequence block - UnRegPaymentBlock (pct value = 30.0%)

                            //Executing percent block - UnRegPaymentOperationBlock (pct value = 30.0%)
                            int UnRegPaymentOperationBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                            //"Percentage random number for block - UnRegPaymentOperationBlock = %d", UnRegPaymentOperationBlockpercent

                            if(UnRegPaymentOperationBlockpercent <= 8000)
                            {

                                    //Executing sequence block - UnRegAddPaymentBlock (pct value = 80.0%)
                                    //Executing flow - UnRegAdd_CartPayment
                                    UnRegAdd_CartPaymentObj.execute(nsApi);
                            }
                            else if(UnRegPaymentOperationBlockpercent <= 10000)
                            {
                                //executing abandon block - Abandon
                                nsApi.ns_end_session();
                            }

                        //Executing percent block - UnRegUpdateAndDeleteCartBlock (pct value = 30.0%)
                        int UnRegUpdateAndDeleteCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - UnRegUpdateAndDeleteCartBlock = %d", UnRegUpdateAndDeleteCartBlockpercent

                        if(UnRegUpdateAndDeleteCartBlockpercent <= 4000)
                        {

                                //Executing sequence block - UnRegUpdateCartBlock (pct value = 40.0%)
                                //Executing flow - UnRegUpdate_Cart
                                UnRegUpdate_CartObj.execute(nsApi);
                                //Executing flow - UnRegGet_Cart
                                UnRegGet_CartObj.execute(nsApi);
                        }
                        else if(UnRegUpdateAndDeleteCartBlockpercent <= 8000)
                        {

                                //Executing sequence block - UnRegDeleteCartBlock (pct value = 40.0%)
                                //Executing flow - UnRegDelete_Cart
                                UnRegDelete_CartObj.execute(nsApi);
                                //Executing flow - UnRegGet_Cart
                                UnRegGet_CartObj.execute(nsApi);
                        }
                        else if(UnRegUpdateAndDeleteCartBlockpercent <= 10000)
                        {

                                //Executing sequence block - UnRegContinueBlock (pct value = 20.0%)
                                //Executing flow - Continue
                                ContinueObj.execute(nsApi);
                        }

                        //Executing percent block - UnRegReviewCartBlock (pct value = 30.0%)
                        int UnRegReviewCartBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - UnRegReviewCartBlock = %d", UnRegReviewCartBlockpercent

                        if(UnRegReviewCartBlockpercent <= 8000)
                        {
                            UnReg_CartReviewObj.execute(nsApi);
                        }
                        else if(UnRegReviewCartBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing percent block - UnRegPlaceOrdeBlock (pct value = 30.0%)
                        int UnRegPlaceOrdeBlockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - UnRegPlaceOrdeBlock = %d", UnRegPlaceOrdeBlockpercent

                        if(UnRegPlaceOrdeBlockpercent <= 10000)
                        {

                                //Executing sequence block - UnRegPaceOderBlock (pct value = 100.0%)
                                //Executing flow - UnRegGet_Cart
                                UnRegGet_CartObj.execute(nsApi);

                                    //Executing percent block - PCT_BLOCK (pct value = 100.0%)
                                    int PCT_BLOCKpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - PCT_BLOCK = %d", PCT_BLOCKpercent

                                    if(PCT_BLOCKpercent <= 2500)
                                    {
                                        UnRegPlace_OrderObj.execute(nsApi);
                                    }
                                    else if(PCT_BLOCKpercent <= 10000)
                                    {
                                        //executing abandon block - Abandon
                                        nsApi.ns_end_session();
                                    }
                                //Executing flow - UnRegOrder_Details
                                UnRegOrder_DetailsObj.execute(nsApi);
                        }
                        else if(UnRegPlaceOrdeBlockpercent <= 10000)
                        {
                            //executing abandon block - Abandon
                            nsApi.ns_end_session();
                        }

                        //Executing percent block - Payfone_Block (pct value = 30.0%)
                        int Payfone_Blockpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - Payfone_Block = %d", Payfone_Blockpercent

                        if(Payfone_Blockpercent <= 10000)
                        {

                                //Executing sequence block - Payfone (pct value = 100.0%)
                                //Executing flow - Initiate_payfone
                                Initiate_payfoneObj.execute(nsApi);
                                //Executing flow - PollingApi_Payfone
                                PollingApi_PayfoneObj.execute(nsApi);
                                //Executing flow - FeedbackApi_Payfone
                                FeedbackApi_PayfoneObj.execute(nsApi);
                        }
                        else if(Payfone_Blockpercent <= 10000)
                        {
                            Guest_Cancel_OrderObj.execute(nsApi);
                        }
                        else if(Payfone_Blockpercent <= 10000)
                        {
                            ContinueObj.execute(nsApi);
                        }
        }

        //logging
        nsApi.ns_end_session();
    }

}
