/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package CS.runlogic;

import CS.*;

import pacJnvmApi.NSApi;

public class Acc_Services
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the Authenticate class
    Authenticate AuthenticateObj = new Authenticate();
    //Initialise the Signout class
    Signout SignoutObj = new Signout();
    //Initialise the LTYTracker class
    LTYTracker LTYTrackerObj = new LTYTracker();
    //Initialise the AccountDropDown class
    AccountDropDown AccountDropDownObj = new AccountDropDown();
    //Initialise the CreateProfile class
    CreateProfile CreateProfileObj = new CreateProfile();
    //Initialise the UpdateProfile class
    UpdateProfile UpdateProfileObj = new UpdateProfile();
    //Initialise the ForgotPassword class
    ForgotPassword ForgotPasswordObj = new ForgotPassword();
    //Initialise the UpdatePassword class
    UpdatePassword UpdatePasswordObj = new UpdatePassword();
    //Initialise the ResetPassword class
    ResetPassword ResetPasswordObj = new ResetPassword();
    //Initialise the GetProfile class
    GetProfile GetProfileObj = new GetProfile();
    //Initialise the AddBillingAdress class
    AddBillingAdress AddBillingAdressObj = new AddBillingAdress();
    //Initialise the AddShippingAdress class
    AddShippingAdress AddShippingAdressObj = new AddShippingAdress();
    //Initialise the UpdateBillingAdress class
    UpdateBillingAdress UpdateBillingAdressObj = new UpdateBillingAdress();
    //Initialise the UpdatePaymentType class
    UpdatePaymentType UpdatePaymentTypeObj = new UpdatePaymentType();
    //Initialise the DeletePayment class
    DeletePayment DeletePaymentObj = new DeletePayment();
    //Initialise the LTY_CreateLoyality class
    LTY_CreateLoyality LTY_CreateLoyalityObj = new LTY_CreateLoyality();
    //Initialise the CreateProfile_withLoyaltyEnroll class
    CreateProfile_withLoyaltyEnroll CreateProfile_withLoyaltyEnrollObj = new CreateProfile_withLoyaltyEnroll();
    //Initialise the CreateProfile_withLoyaltyID class
    CreateProfile_withLoyaltyID CreateProfile_withLoyaltyIDObj = new CreateProfile_withLoyaltyID();
    //Initialise the LTY_Accountlookup_Svc class
    LTY_Accountlookup_Svc LTY_Accountlookup_SvcObj = new LTY_Accountlookup_Svc();
    //Initialise the LTY_Activity class
    LTY_Activity LTY_ActivityObj = new LTY_Activity();
    //Initialise the LTY_GetAllCharities class
    LTY_GetAllCharities LTY_GetAllCharitiesObj = new LTY_GetAllCharities();
    //Initialise the LTY_PointAdjustment class
    LTY_PointAdjustment LTY_PointAdjustmentObj = new LTY_PointAdjustment();
    //Initialise the LTY_PointBalance class
    LTY_PointBalance LTY_PointBalanceObj = new LTY_PointBalance();
    //Initialise the LTY_ReferAFriend_Svc class
    LTY_ReferAFriend_Svc LTY_ReferAFriend_SvcObj = new LTY_ReferAFriend_Svc();
    //Initialise the LTY_updateloyality class
    LTY_updateloyality LTY_updateloyalityObj = new LTY_updateloyality();
    //Initialise the LTY_RegisterEvent class
    LTY_RegisterEvent LTY_RegisterEventObj = new LTY_RegisterEvent();
    //Initialise the AnonymousGuest class
    AnonymousGuest AnonymousGuestObj = new AnonymousGuest();
    //Initialise the SignIn class
    SignIn SignInObj = new SignIn();
    //Initialise the SoftLogin class
    SoftLogin SoftLoginObj = new SoftLogin();
    //Initialise the Store class
    Store StoreObj = new Store();
    //Initialise the StoreLatitude class
    StoreLatitude StoreLatitudeObj = new StoreLatitude();
    //Initialise the StoreAddress class
    StoreAddress StoreAddressObj = new StoreAddress();
    //Initialise the StoreRadius class
    StoreRadius StoreRadiusObj = new StoreRadius();
    //Initialise the AddtoWallet class
    AddtoWallet AddtoWalletObj = new AddtoWallet();
    //Initialise the GetWalletDetails class
    GetWalletDetails GetWalletDetailsObj = new GetWalletDetails();
    //Initialise the WalletNotification class
    WalletNotification WalletNotificationObj = new WalletNotification();
    //Initialise the Refresh_Wallet class
    Refresh_Wallet Refresh_WalletObj = new Refresh_Wallet();
    //Initialise the PrequalEligibility class
    PrequalEligibility PrequalEligibilityObj = new PrequalEligibility();
    //Initialise the PrequalEligibility_Web class
    PrequalEligibility_Web PrequalEligibility_WebObj = new PrequalEligibility_Web();
    //Initialise the PrequalInquiry_Web class
    PrequalInquiry_Web PrequalInquiry_WebObj = new PrequalInquiry_Web();
    //Initialise the PrequalLookup_Web class
    PrequalLookup_Web PrequalLookup_WebObj = new PrequalLookup_Web();
    //Initialise the AW_GetLatestVersion class
    AW_GetLatestVersion AW_GetLatestVersionObj = new AW_GetLatestVersion();
    //Initialise the AW_Service class
    AW_Service AW_ServiceObj = new AW_Service();
    //Initialise the AW_GetSerialNum class
    AW_GetSerialNum AW_GetSerialNumObj = new AW_GetSerialNum();
    //Initialise the AW_RegisterPush class
    AW_RegisterPush AW_RegisterPushObj = new AW_RegisterPush();
    //Initialise the AW_LogService class
    AW_LogService AW_LogServiceObj = new AW_LogService();
    //Initialise the AW_UnRegister class
    AW_UnRegister AW_UnRegisterObj = new AW_UnRegister();
    //Initialise the GiftRegistry class
    GiftRegistry GiftRegistryObj = new GiftRegistry();
    //Initialise the Segment class
    Segment SegmentObj = new Segment();
    //Initialise the Segment_KCC class
    Segment_KCC Segment_KCCObj = new Segment_KCC();
    //Initialise the PaymentSegment class
    PaymentSegment PaymentSegmentObj = new PaymentSegment();
    //Initialise the Segment_EmailID class
    Segment_EmailID Segment_EmailIDObj = new Segment_EmailID();
    //Initialise the Address_Geo_Code_validation class
    Address_Geo_Code_validation Address_Geo_Code_validationObj = new Address_Geo_Code_validation();
    //Initialise the Geo_Code_validation class
    Geo_Code_validation Geo_Code_validationObj = new Geo_Code_validation();
    //Initialise the Initiate class
    Initiate InitiateObj = new Initiate();
    //Initialise the Polling class
    Polling PollingObj = new Polling();
    //Initialise the Finish class
    Finish FinishObj = new Finish();
    //Initialise the Text_Notification class
    Text_Notification Text_NotificationObj = new Text_Notification();
    //Initialise the Delete_Text_Notification class
    Delete_Text_Notification Delete_Text_NotificationObj = new Delete_Text_Notification();
    //Initialise the Retrieve_PickUp class
    Retrieve_PickUp Retrieve_PickUpObj = new Retrieve_PickUp();
    //Initialise the Add_PickUp class
    Add_PickUp Add_PickUpObj = new Add_PickUp();
    //Initialise the Delete_PickUp class
    Delete_PickUp Delete_PickUpObj = new Delete_PickUp();
    //Initialise the Update_PickUp class
    Update_PickUp Update_PickUpObj = new Update_PickUp();
    //Initialise the Refresh_Wallet_Mobile class
    Refresh_Wallet_Mobile Refresh_Wallet_MobileObj = new Refresh_Wallet_Mobile();
    //Initialise the Select_Preference class
    Select_Preference Select_PreferenceObj = new Select_Preference();
    //Initialise the Select_Perk class
    Select_Perk Select_PerkObj = new Select_Perk();
    //Initialise the Retrieve_Preferences_List class
    Retrieve_Preferences_List Retrieve_Preferences_ListObj = new Retrieve_Preferences_List();
    //Initialise the Get_Preferences_List class
    Get_Preferences_List Get_Preferences_ListObj = new Get_Preferences_List();
    //Initialise the Softlaunch class
    Softlaunch SoftlaunchObj = new Softlaunch();
    //Initialise the Softlaunch_Session class
    Softlaunch_Session Softlaunch_SessionObj = new Softlaunch_Session();
    //Initialise the Softlaunch_Web_Session class
    Softlaunch_Web_Session Softlaunch_Web_SessionObj = new Softlaunch_Web_Session();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing percent block - Start
        int Startpercent = nsApi.ns_get_random_number_int(1, 10000);

        //"Percentage random number for block - Start = %d", Startpercent

        if(Startpercent <= 10000)
        {

                //Executing percent block - AccountDomain (pct value = 100.0%)
                int AccountDomainpercent = nsApi.ns_get_random_number_int(1, 10000);

                //"Percentage random number for block - AccountDomain = %d", AccountDomainpercent

                if(AccountDomainpercent <= 10000)
                {

                        //Executing percent block - ACM (pct value = 100.0%)
                        int ACMpercent = nsApi.ns_get_random_number_int(1, 10000);

                        //"Percentage random number for block - ACM = %d", ACMpercent

                        if(ACMpercent <= 900)
                        {

                                //Executing percent block - AM_Authenticate (pct value = 9.0%)
                                int AM_Authenticatepercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Authenticate = %d", AM_Authenticatepercent

                                if(AM_Authenticatepercent <= 9800)
                                {
                                    AuthenticateObj.execute(nsApi);
                                }
                                else if(AM_Authenticatepercent <= 10000)
                                {
                                    SignoutObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 900)
                        {
                            LTYTrackerObj.execute(nsApi);
                        }
                        else if(ACMpercent <= 1000)
                        {
                            AccountDropDownObj.execute(nsApi);
                        }
                        else if(ACMpercent <= 2000)
                        {

                                //Executing percent block - AM_UserProfile (pct value = 10.0%)
                                int AM_UserProfilepercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_UserProfile = %d", AM_UserProfilepercent

                                if(AM_UserProfilepercent <= 400)
                                {
                                    CreateProfileObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 800)
                                {
                                    UpdateProfileObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 800)
                                {
                                    ForgotPasswordObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 800)
                                {
                                    UpdatePasswordObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 800)
                                {
                                    ResetPasswordObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 8400)
                                {
                                    GetProfileObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 8800)
                                {
                                    AddBillingAdressObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 9200)
                                {
                                    AddShippingAdressObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 9600)
                                {
                                    UpdateBillingAdressObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 10000)
                                {
                                    UpdatePaymentTypeObj.execute(nsApi);
                                }
                                else if(AM_UserProfilepercent <= 10000)
                                {
                                    DeletePaymentObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 2000)
                        {

                                //Executing percent block - AM_Loyalty (pct value = 0.0%)
                                int AM_Loyaltypercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Loyalty = %d", AM_Loyaltypercent

                                if(AM_Loyaltypercent <= 0)
                                {
                                    LTY_CreateLoyalityObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 1000)
                                {
                                    CreateProfile_withLoyaltyEnrollObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 2000)
                                {
                                    CreateProfile_withLoyaltyIDObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 3000)
                                {
                                    LTY_Accountlookup_SvcObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 5000)
                                {
                                    LTY_ActivityObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 6000)
                                {
                                    LTY_GetAllCharitiesObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 6000)
                                {
                                    LTY_PointAdjustmentObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 8000)
                                {
                                    LTY_PointBalanceObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 9000)
                                {
                                    LTY_ReferAFriend_SvcObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 9000)
                                {
                                    LTY_updateloyalityObj.execute(nsApi);
                                }
                                else if(AM_Loyaltypercent <= 10000)
                                {
                                    LTY_RegisterEventObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 7600)
                        {

                                //Executing percent block - AM_Session (pct value = 56.0%)
                                int AM_Sessionpercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Session = %d", AM_Sessionpercent

                                if(AM_Sessionpercent <= 4000)
                                {
                                    AnonymousGuestObj.execute(nsApi);
                                }
                                else if(AM_Sessionpercent <= 8000)
                                {
                                    SignInObj.execute(nsApi);
                                }
                                else if(AM_Sessionpercent <= 10000)
                                {
                                    SoftLoginObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 7600)
                        {

                                //Executing percent block - AM_Store (pct value = 0.0%)
                                int AM_Storepercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Store = %d", AM_Storepercent

                                if(AM_Storepercent <= 3500)
                                {
                                    StoreObj.execute(nsApi);
                                }
                                else if(AM_Storepercent <= 6000)
                                {
                                    StoreLatitudeObj.execute(nsApi);
                                }
                                else if(AM_Storepercent <= 7500)
                                {
                                    StoreAddressObj.execute(nsApi);
                                }
                                else if(AM_Storepercent <= 10000)
                                {
                                    StoreRadiusObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 7900)
                        {

                                //Executing percent block - AM_Wallet (pct value = 3.0%)
                                int AM_Walletpercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Wallet = %d", AM_Walletpercent

                                if(AM_Walletpercent <= 0)
                                {
                                    AddtoWalletObj.execute(nsApi);
                                }
                                else if(AM_Walletpercent <= 4000)
                                {
                                    GetWalletDetailsObj.execute(nsApi);
                                }
                                else if(AM_Walletpercent <= 6500)
                                {
                                    WalletNotificationObj.execute(nsApi);
                                }
                                else if(AM_Walletpercent <= 10000)
                                {
                                    Refresh_WalletObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 7900)
                        {

                                //Executing percent block - AM_Preequal (pct value = 0.0%)
                                int AM_Preequalpercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Preequal = %d", AM_Preequalpercent

                                if(AM_Preequalpercent <= 10000)
                                {
                                    PrequalEligibilityObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 8000)
                        {

                                //Executing percent block - AM_Prequal_Web (pct value = 1.0%)
                                int AM_Prequal_Webpercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Prequal_Web = %d", AM_Prequal_Webpercent

                                if(AM_Prequal_Webpercent <= 3000)
                                {
                                    PrequalEligibility_WebObj.execute(nsApi);
                                }
                                else if(AM_Prequal_Webpercent <= 8000)
                                {
                                    PrequalInquiry_WebObj.execute(nsApi);
                                }
                                else if(AM_Prequal_Webpercent <= 10000)
                                {
                                    PrequalLookup_WebObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 8000)
                        {

                                //Executing percent block - Apple_Wallet (pct value = 0.0%)
                                int Apple_Walletpercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - Apple_Wallet = %d", Apple_Walletpercent

                                if(Apple_Walletpercent <= 10000)
                                {
                                    AW_GetLatestVersionObj.execute(nsApi);
                                }
                                else if(Apple_Walletpercent <= 10000)
                                {
                                    AW_ServiceObj.execute(nsApi);
                                }
                                else if(Apple_Walletpercent <= 10000)
                                {
                                    AW_GetSerialNumObj.execute(nsApi);
                                }
                                else if(Apple_Walletpercent <= 10000)
                                {
                                    AW_RegisterPushObj.execute(nsApi);
                                }
                                else if(Apple_Walletpercent <= 10000)
                                {
                                    AW_LogServiceObj.execute(nsApi);
                                }
                                else if(Apple_Walletpercent <= 10000)
                                {
                                    AW_UnRegisterObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 8100)
                        {

                                //Executing percent block - AM_Message (pct value = 1.0%)
                                int AM_Messagepercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - AM_Message = %d", AM_Messagepercent

                                if(AM_Messagepercent <= 10000)
                                {
                                    GiftRegistryObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 8500)
                        {

                                //Executing sequence block - Segment_Block (pct value = 4.0%)
                                //Executing flow - Segment
                                SegmentObj.execute(nsApi);
                                //Executing flow - Segment_KCC
                                Segment_KCCObj.execute(nsApi);
                                //Executing flow - PaymentSegment
                                PaymentSegmentObj.execute(nsApi);
                                //Executing flow - Segment_EmailID
                                Segment_EmailIDObj.execute(nsApi);
                        }
                        else if(ACMpercent <= 8700)
                        {

                                //Executing sequence block - Address (pct value = 2.0%)

                                    //Executing percent block - Address_Validation (pct value = 2.0%)
                                    int Address_Validationpercent = nsApi.ns_get_random_number_int(1, 10000);

                                    //"Percentage random number for block - Address_Validation = %d", Address_Validationpercent

                                    if(Address_Validationpercent <= 5000)
                                    {
                                        Address_Geo_Code_validationObj.execute(nsApi);
                                    }
                                    else if(Address_Validationpercent <= 10000)
                                    {
                                        Geo_Code_validationObj.execute(nsApi);
                                    }
                        }
                        else if(ACMpercent <= 8700)
                        {

                                //Executing sequence block - PayFone (pct value = 0.0%)
                                //Executing flow - Initiate
                                InitiateObj.execute(nsApi);
                                //Executing flow - Polling
                                PollingObj.execute(nsApi);
                                //Executing flow - Finish
                                FinishObj.execute(nsApi);
                        }
                        else if(ACMpercent <= 9600)
                        {

                                //Executing sequence block - TextNotification (pct value = 9.0%)
                                //Executing flow - Text_Notification
                                Text_NotificationObj.execute(nsApi);
                                //Executing flow - Delete_Text_Notification
                                Delete_Text_NotificationObj.execute(nsApi);
                                //Executing flow - Retrieve_PickUp
                                Retrieve_PickUpObj.execute(nsApi);
                                //Executing flow - Add_PickUp
                                Add_PickUpObj.execute(nsApi);
                                //Executing flow - Delete_PickUp
                                Delete_PickUpObj.execute(nsApi);
                                //Executing flow - Refresh_Wallet
                                Refresh_WalletObj.execute(nsApi);
                                //Executing flow - Update_PickUp
                                Update_PickUpObj.execute(nsApi);
                                //Executing flow - Refresh_Wallet_Mobile
                                Refresh_Wallet_MobileObj.execute(nsApi);
                        }
                        else if(ACMpercent <= 9600)
                        {

                                //Executing percent block - SelectPreference (pct value = 0.0%)
                                int SelectPreferencepercent = nsApi.ns_get_random_number_int(1, 10000);

                                //"Percentage random number for block - SelectPreference = %d", SelectPreferencepercent

                                if(SelectPreferencepercent <= 2500)
                                {
                                    Select_PreferenceObj.execute(nsApi);
                                }
                                else if(SelectPreferencepercent <= 5000)
                                {
                                    Select_PerkObj.execute(nsApi);
                                }
                                else if(SelectPreferencepercent <= 7500)
                                {
                                    Retrieve_Preferences_ListObj.execute(nsApi);
                                }
                                else if(SelectPreferencepercent <= 10000)
                                {
                                    Get_Preferences_ListObj.execute(nsApi);
                                }
                        }
                        else if(ACMpercent <= 10000)
                        {

                                //Executing sequence block - Soft_launch (pct value = 4.0%)
                                //Executing flow - Softlaunch
                                SoftlaunchObj.execute(nsApi);
                                //Executing flow - Softlaunch_Session
                                Softlaunch_SessionObj.execute(nsApi);
                                //Executing flow - Softlaunch_Web_Session
                                Softlaunch_Web_SessionObj.execute(nsApi);
                        }
                }
        }

        //logging
        nsApi.ns_end_session();
    }

}
