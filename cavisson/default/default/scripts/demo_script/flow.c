/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 03/25/2022 11:42:49
    Flow details:
    Build details: 4.6.1 (build# 93)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=http://www.flipkart.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "REDIRECT=YES",
        "LOCATION=https://www.flipkart.com/",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwhmbGlwa2FydANjb20AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://www.flipkart.com/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Dest:document", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCXJ1a21pbmltMQhmbGl4Y2FydANjb20AAAEAAQAAKRAAAAAAAABNAAwASQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app_modules.chunk.94b5e7.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app.chunk.9adf7d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Home.chunk.9440b9.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABEXN0YXRpYy1hc3NldHMtd2ViCGZsaXhjYXJ0A2NvbQAAAQABAAApEAAAAAAAAEUADABBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/raven.3.22.3.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/runtime.95858ad1.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/vendor.chunk.bf58a224.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_modules.chunk.ac48891f.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_common.chunk.db774788.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app.chunk.b7fa5477.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/c69716cd83f8878e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/844/140/image/3815163f408b65fb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/ef180fc5898dfd20.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/1f4b8cabe98448eb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/timer_a73398.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kuef2q80/deodorant/w/u/4/morning-dew-deodorant-spray-yardley-london-women-original-imag7jf7f6ybuc6d.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k0wqwsw0/sticker/f/m/d/living-room-design-blue-birds-with-pink-flowers-medium-65-712-original-imafkhkenfcsz4hj.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_69e7ec.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/batman-returns/omni/omni16.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/px/gNtTli3A/init.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/ksru0sw0/top/h/y/d/m-40720-urbanic-original-imag69upgg6ykgtc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kz8qsnk0/collapsible-wardrobe/v/q/q/pc-plastic-pvc-pp-collapsible-wardrobe-12-door-white-metal-and-original-imagbazgnxdtfbwz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/ke8uv0w0-0/sunglass/p/r/y/135555-s-vincent-chase-original-imafuywa5zyqnzz4.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kyxb9u80/gas-stove/y/x/c/4-3-5-hf4bstlb-hindflame-95-original-imagb2f4crargjym.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/krkyt8w0/shoe/6/b/r/11-hmi55-11-adidas-vicblu-ftwwht-solred-conavy-original-imag5cd4ayngenun.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/150/150/image/e3ff066cfd6881cb.jpg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/464/708/image/36f00a7331b4abcf.jpg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/klv7ekw0/shoe/g/z/j/7-642-loafers-90-kzaara-mehandi-original-imagywbkemjkgapz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jjelq4w0/shoe/e/f/c/pikaw20180014-8-provogue-tan-original-imaf6zj82yshc3sp.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jqqy6q80/shoe/f/y/q/pro-np-aw11-7-provogue-olive-original-imafczv5gedg2hur.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k4a7c7k0/shoe/g/v/p/rc724a-006-6-red-chief-tan-original-imafnff5hksm6dnc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k7nnrm80/sandal/y/c/m/ss-520-d-grey-neon-orange-41-sparx-d-grey-neon-ornage-original-imafpuhrmcsy425z.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k66sh3k0/shoe/h/z/z/wht-blk-chn-rd-6-fila-wht-blk-chn-rd-original-imafzp5pdxwzs2zv.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/koenl3k0/shoe/p/d/n/6-hiv65-reebok-smoky-indigo-true-grey-nacho-original-imag2uzybmt598nz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jvfk58w0/shoe/k/d/q/rc5070-7-red-chief-elephant-tan-original-imafgcys2ducmknh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/28bd10243125450e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/0ea5db73aeeb9296.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/2d86c8d4f37e8af3.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;SN", END_INLINE,
            "URL=https://dpm.demdex.net/id?d_visid_ver=1.5.4&d_rtbd=json&d_ver=2&d_orgid=17EB401053DAF4840A490D4C%40AdobeOrg&d_nsid=0&d_cb=s_c_il%5B0%5D._setMarketingCloudFields", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA2RwbQZkZW1kZXgDbmV0AAABAAEAACkQAAAAAAAAVQAMAFEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector");
    ns_web_url ("collector",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dEB4QYmoDAgsACxAIAh4QYmoDAwMKBBAIEGVbXAEAEB4QYmoDAgQAABAIAh4QYmoDAgAFGABAI%5EBwQCBx4QYSmoDAgsFAhAIAQQCAh4QYmoD%7DDNKAgILBh8AIAwQGCgMKCgYABQEBBxDNa4QYmoDAwICBh%3EAIAwQGCgMKDC%60gYABQKE%7DGAGR4QYmoDAgACBBAIEFFRCgMKVlACH1NRAgMfAwNXUKR9TCgBRHwoHCwIGA1dUAVFTARAeE7GJ7qAwICCgoQCFRTXkFXT09v&appId=PXgNtTli3A&tag=v7.6.2&uuid=cc818db0-ac01-11ec-a82c-859041ef3ca3&ft=263&seq=0&en=NTA&pc=1346820823718221&rsc=1",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABFGNvbGxlY3Rvci1weGdudHRsaTNhCHB4LWNsb3VkA25ldAAAAQABAAApEAAAAAAAAEIADAA-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Home.chunk.40ec52db.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-regular-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-medium-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://flipkart.d1.sc.omtrdc.net/id?d_visid_ver=1.5.4&callback=s_c_il%5B0%5D._setAnalyticsFields&mcorgid=17EB401053DAF4840A490D4C%40AdobeOrg&mid=84290952911474648655406836606388072339", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCGZsaXBrYXJ0AmQxAnNjBm9tdHJkYwNuZXQAAAEAAQAAKRAAAAAAAABKAAwARgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-regular-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-medium-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("callback");
    ns_web_url ("callback",
        "URL=https://rome.api.flipkart.com/api/1/connekt/push/callback",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:apikey,content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBHJvbWUDYXBpCGZsaXBrYXJ0A2NvbQAAAQABAAApEAAAAAAAAE4ADABKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("callback", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("callback_2");
    ns_web_url ("callback_2",
        "URL=https://rome.api.flipkart.com/api/1/connekt/push/callback",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=apiKey:KrWcJnCSZFBLFR39DtHYySjcDCHg2LeC3sxdx7646n7iy7oy",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg",
        BODY_BEGIN,
            "{"type":"PN","eventType":"TICKLE","cargo":"{\"permissionStatus\":\"denied\"}"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rome.api.flipkart.com/api/1/fdp", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Access-Control-Request-Headers:content-type,x-user-agent", "HEADER=Origin:https://www.flipkart.com", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/c69716cd83f8878e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/844/140/image/3815163f408b65fb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/ef180fc5898dfd20.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/1f4b8cabe98448eb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/timer_a73398.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kuef2q80/deodorant/w/u/4/morning-dew-deodorant-spray-yardley-london-women-original-imag7jf7f6ybuc6d.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k0wqwsw0/sticker/f/m/d/living-room-design-blue-birds-with-pink-flowers-medium-65-712-original-imafkhkenfcsz4hj.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("callback_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp");
    ns_web_url ("fdp",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg",
        BODY_BEGIN,
            "[{"nc":{"ssid":"yelcau6xvk0000001648188428456","mpid":"FLIPKART","pn":"homepage","pt":"hp","ss":"BasePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic"},"e":[{"en":"PV","ib":false,"id":false,"cat":null,"t":1648188428753}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/150/150/ksru0sw0/top/h/y/d/m-40720-urbanic-original-imag69upgg6ykgtc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kz8qsnk0/collapsible-wardrobe/v/q/q/pc-plastic-pvc-pp-collapsible-wardrobe-12-door-white-metal-and-original-imagbazgnxdtfbwz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/ke8uv0w0-0/sunglass/p/r/y/135555-s-vincent-chase-original-imafuywa5zyqnzz4.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kyxb9u80/gas-stove/y/x/c/4-3-5-hf4bstlb-hindflame-95-original-imagb2f4crargjym.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_69e7ec.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/krkyt8w0/shoe/6/b/r/11-hmi55-11-adidas-vicblu-ftwwht-solred-conavy-original-imag5cd4ayngenun.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/150/150/image/e3ff066cfd6881cb.jpg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/464/708/image/36f00a7331b4abcf.jpg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://desa.fkapi.net/1.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://desb.fkapi.net/1.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_2");
    ns_web_url ("collector_2",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIBAgEQHhBWEAhJEGJqAwIHAAAQCBAACwULAQYLVgMKVFFRBVZQBQJQBQcLCwUKBwQBUFcBBxAeEGJqAwIKBgIQCBBRCkdeXQBGVUBWVVFWR0JcA1tBAhAeEGJqAwIGBAYQCBBQBwoEUVdRAwpTBwsEVgZQV1RTAQZTAQRXCgBUU1MEUBAeEGJqAwICCgIQCBAHVgNWBwAEAQUAUVAFVlMBUQMGUQoDBAJRVAJRAVRTChAeEGJqAwMAAQIQCBBRU1NXBAcDAQYFVAFWVAoEAwtTUANXVFMDB1cKBgsLABAeEGJqAwIDBgMQCAMEBgoDCgoGAAUHBgQeEGJqAwIGAwoQCBABCgQKBgIKBQEEAQcEAwoHAQEFARAeEAkCDAIOCgINCQwJDwwLAg8JCQ0JEAgQBg0DDQEFDQIGAwYAAwQNAAYGAgYQHhBiagMDAwoDEAgEBAseEGJqAwIABgsQCBADVlRQUVZWUBAeEGJqAwIAAQoQCBAQHhBiagMCCwsHEAgQAwIAAgVQAFQQHhBiagMCBwQFEAgQAwIAAgVQAFQQHhBiagMDAwsAEAgQCwJXBAcGBAcQHhBiagMCAgQHEAhGQEdXHhBiagMDAwcBEAhGQEdXHhBiagMCBwILEAhGQEdXHhBiagMCAAAFEAhGQEdXHhBiagMDAAYLEAhGQEdXHhBiagMDAAcBEAgQBmtxHQZrcVAGa3FgBmtxcwZrcVYGa3FwBmtxVgZrcWcGa3F1Bmt1YQZrcV0Ga3FQBmtxcwZrcWYGa3VhBmtxAAZrcWoGa3FwBmtxaAZrcXUGa3FWBmtxcQZrdWEGa3VUBmt1YQZrcQEGa3FRBmtxZAZrcVcGa3FQBmtxcAZrcVMGa3VhBmt1UwZrcVwGa3FRBmtxUAZrcXUGa3FqBmtxZQZrdWEGa3FaBmtxdQZrcWYGa3F1BmtxagZrcXAGa3VQEB4QYmoDAwAHBBAIEFFTAlZRAAZRVFQHAVRTVAABBFZWAgJWBwALBwcCVAcFEB4QYmoDAgsACxAIAh4QYmoDAgAGChAIAB4QYmoDAgUCBxAIEGZLQld3QEBdQAgScVNcXF1GEkBXU1YSQkBdQldARksSFQIVEl1UElxHXl5uXBISEhJTRhJhRhIaWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dQkodVXxGZl5bAXMdW1xbRhxYQQgACAMBBgsLG25cEhISElNGEl9cEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAQILAgQbblwSEhISU0YSUFwSGlpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUJKHVV8RmZeWwFzHVtcW0YcWEEIAAgACwsCBBtuXBISEhJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAAsKBQtuXBISEhJTRhJWEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1AU0RXXBwBHAAAHAEcWEEIAAgGBgQHGxAeEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dEB4QYmoDAgEDAxAIaW8eEGJqAwIFBgYQCBAQHhBiagMCAgYEEAhUU15BVx4QYmoDAgADChAIEFcCV1NUAwJXEB4QYmoDAgMEABAIEFYDCwMFUVMGEB4QYmoDAgsGAhAIEAUFBARTBwBWEB4QYmoDAwACCxAIEAMKAlZWBVcBEB4QYmoDAgYLChAIEARTCwIBBQpWEB4QYmoDAgULAhAIaW8eEGJqAwMCAwIQCAIeEGJqAwIACgsQCEZAR1ceEGJqAwMCBgEQCEZAR1ceEGJqAwIEAgYQCFRTXkFXHhBiagMCAAsEEAgQV1wfZ2EQHhBiagMDAwoEEAgQZVtcAQAQHhBiagMCAQsFEAhpEFdcH2dhEB4QV1wQbx4QYmoDAgYFABAIEH9dSFteXlMdBxwCEhplW1xWXUVBEnxmEgMCHAIJEmVbXAQGCRJKBAYbEnNCQl5XZVdQeVtGHQcBBRwBBBIaeXpmf34eEl5bWVcSdVdRWV0bEnFaQF1fVx0LAhwCHAYGAQIcCwESYVNUU0BbHQcBBRwBBBAeEGJqAwIFBwoQCEZAR1ceEGJqAwICCwsQCB8BAQIeEGJqAwIBAQQQCAoeEGJqAwIBBQEQCAAeEGJqAwIKAgAQCBB1V1FZXRAeEGJqAwIEAAoQCBAAAgIBAgMCBRAeEGJqAwMCAQsQCBAHHAISGmVbXFZdRUESfGYSAwIcAgkSZVtcBAYJEkoEBhsSc0JCXldlV1B5W0YdBwEFHAEEEhp5emZ%2Ffh4SXltZVxJ1V1FZXRsScVpAXV9XHQsCHAIcBgYBAhwLARJhU1RTQFsdBwEFHAEEEB4QYmoDAgcGBRAIRkBHVx4QYmoDAgMFBhAIRkBHVx4QYmoDAgUFBxAIHwMeEGJqAwIHAQsQCBB8V0ZBUVNCVxAeEGJqAwIDCgsQCBB%2FXUhbXl5TEB4QYmoDAgELAhAIRkBHVx4QYmoDAgsEARAIAwICHhBiagMCAgoDEAhUU15BVx4QYmoDAgELCxAIBRwEBx4QYmoDAgAFARAIEAZVEB4QYmoDAgcLBxAIRkBHVx4QYmoDAgoAABAIRkBHVx4QYmoDAwABBxAIEEoKBBAeEGJqAwMAAQsQCBAQHhBiagMDAAYCEAgQZVtcVl1FQRAeEGJqAwMABgMQCBADAhwCEB4QYmoDAwAGABAIEAsCHAIcBgYBAhwLARAeEGJqAwIHBAMQCAMHAQQeEGJqAwIGCwsQCAoEBh4QYmoDAgoGARAIAwcBBB4QYmoDAgoHAhAICgMEHhBiagMDAwMBEAgQAwcBBGoKBAYQHhBiagMCBQAGEAgABh4QYmoDAgIKCxAIAAYeEGJqAwIAAgYQCAMHAQQeEGJqAwMDAQoQCAUDBh4QYmoDAwMFAhAIAh4GQYmoDAwMFBhAIAh4QYmoDAgAGARAIRkBHVx4QYmoDAgABCxAIAwYBCgoLAQYeEGJqAwIABAUQCAADBQAEBgsGBQAeEGJqAwIHBwMQCAMFBgEACg%5EAEHhBiagMCBwcKEAgQdEBbEn9TQBIABxIAAgAAEgMDCAEFCAIKEnV%2FZhkCBwECEhp7XFZbUxJhRlNcVlNAVhJmW19XGxAeEGJqAwIAAQQQCFRTXkFXHhBiagMCBgICEAhUU15BVx4QYmoDAgcBAhAIVFNeQVceEGJqAwMCBAIQCEZAR1ceEGJqAwIKAgMQCAIeEGJqAwIBCwYQCFRTXkFXHhBiagMCASgcKEAgQRFtBW1BeVxAeEGJqAwMDAAEQCFRTXkFXHhBiagMCAgsEEAgCHhBiagMCCgUAEAgDBwEEHhBiagMDAgAKEAhGQEdXHhBiagMCAQQEEAgFAwceEGJqAwIHCgcQCBBfW0FBW1xVEB4QYmoDAgsFBBAIRkBHVx4QYmoDAgAHAhAIRkBHVx4QYmoDAgAHCxAIVFNeQVceEGJqAwIDBwQQCEZAR1ceEGJqAwIFAwAQCAceEGJqAwIHBwcQCAALHhBiagMCAQYFEAgCHhBiagMCAwMLEAgBHhBiagMDAgIAEAhGQEdXHhBiagMCBgMCEAhUU15BVx4QYmoDAwIDChAIVFNeQVceEGJq%3AAwMABgEQCEZAR1ceEGJqAwMABgYQCBBmS0JXd0BAEXUAIEnFTXFxdRhJAV1NWEkJAXUJXQEZLEhVFW1ZGWhUSXVQSR1xWV1RbXFdWEB4QYmoDAwAGBxAIEEVXUFNlbRhAeEGJqAwMABgQQCAEBHO%3EENhBiagMDAAYFEA%5BhUU15BVx4QYmoDAwIHBxAIEAMGBk4HBk4HBk4DCgsJOBAoQHhBiagMCBgAAEAgDAwMeEGJqAwIEBwsQCEZAR1ceEGJqAwIBAwQQCEZAR1ceEGJqAwIFBgAQCBBUU15BVxAeEGJqAwMDBgoQCBBUU15BVxAeEGJqAwIKBgQQCAMeEGJqAwIBAAEQCAMeEGJqAwMCAwcQCBAQHhBDiagMCBwsLEAhpEF5dU1ZmW19XQRAeEFFBWxAeEFNCQhAeEEBHXEZbX1cQbx4QYmoDAgIDAhAIVFNeQVceEGJqAwIAAAcQCFRTXkFXHhBiagMCCgcHEAhUU15BVx4QYmoDAwIEBxAIVFNeQVceEGJqAwIGBwQQCFRTXkFXHhBiagMCBgYDEAhUU15BVx4QYmoDAgILChAIVFNeQVceEGJqAwIHBwUQCFRTXkFXHhBiagMCAwUCEAhUU15BVx4QYmoDAgoABhAIVFNeQVceEGJqAwICCgUQCFRTXkFXHhBiagMDAgYAEAhUU15BVx4QYmoDAgoLAxAIAB4QYmoDAgQAABAIAx4QYmoDAgAFABAIBAQDAx4QYmoDAgIGAxAIAwQGCgMKCgYACgMCBB4QYmoDAgsFAhAIAQQCAh4QYmoDAgILBhAIAwQGCgMKCgYABQp_%3AEBBx4QYmoDAwICBhAIAwQGCgMKCgYACwICAR4QYmoDDAgACBBAIEFFRCgMKVlACH1NRAgMfAwNXUR9TCgBRHwoHCwIGA1dUAVFTARAeEGJqAwICCgoQCFRTXkFXT08eSRBGEAgQYmoDAgQGBBAeEFYQCEkQYmoDAgQKChAIEAFRUVYAU1FTEB4QYmoDAwMBARAIAh4QYmoDAgADBxAIEAMCUFcDVlcHEB4QYmoDAgUEAhAIAh4QYmoDAgEDBhAIEFFUVgQLCgZWEB4QYmoDAgMBBBAIAwIeEGJqAwIAAwQQCBADUQEBVgoHBBAeEGJqAwIFAQUQCEZAR1mceEGJqAwIBAAsQCAIeEGJqAwIEBgcQCBBWBQcLAwMGVBAeEGJqAwIEBAEQCAIeEGJqAwIEAAAQCAAeEGJqAwIABQAQCAUABwQeEGJqAwMCAgYQCAMEBgoDCgoGAAsCAgEeEGJqAwIAAgQQCBBRUQoDClZQAh9TUQ7IDHwMDV1EfUwoAUR8KBwsCBgNXVAFRUwEQHhBiagM7CAgoKEAhUU15BV09Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=cc818db0-ac01-11ec-a82c-859041ef3ca3&ft=263&seq=1&en=NTA&cs=680a846a513c1455e659fa395acc73f40932b166ab71b9df667a3659810a4f13&pc=1457966730301921&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B4%F3%A0%84%B2%F3%A0%84%B7%F3%A0%84%B5%F3%A0%84%B4%F3%A0%84%B6&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=2",
        BODY_END,
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/login_img_c4a81e.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/klv7ekw0/shoe/g/z/j/7-642-loafers-90-kzaara-mehandi-original-imagywbkemjkgapz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jjelq4w0/shoe/e/f/c/pikaw20180014-8-provogue-tan-original-imaf6zj82yshc3sp.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jqqy6q80/shoe/f/y/q/pro-np-aw11-7-provogue-olive-original-imafczv5gedg2hur.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k4a7c7k0/shoe/g/v/p/rc724a-006-6-red-chief-tan-original-imafnff5hksm6dnc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k7nnrm80/sandal/y/c/m/ss-520-d-grey-neon-orange-41-sparx-d-grey-neon-ornage-original-imafpuhrmcsy425z.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k66sh3k0/shoe/h/z/z/wht-blk-chn-rd-6-fila-wht-blk-chn-rd-original-imafzp5pdxwzs2zv.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBGRlc2EFZmthcGkDbmV0AAABAAEAACkQAAAAAAAAVQAMAFEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBGRlc2IFZmthcGkDbmV0AAABAAEAACkQAAAAAAAAVQAMAFEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/koenl3k0/shoe/p/d/n/6-hiv65-reebok-smoky-indigo-true-grey-nacho-original-imag2uzybmt598nz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jvfk58w0/shoe/k/d/q/rc5070-7-red-chief-elephant-tan-original-imafgcys2ducmknh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/28bd10243125450e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/0ea5db73aeeb9296.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/2d86c8d4f37e8af3.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/ab7e2b022a4587dd.jpg?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/dff3f7adcf3a90c6.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/71050627a56b4693.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/1688/280/image/3815163f408b65fb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/82b3ca5fb2301045.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/1688/280/image/ef180fc5898dfd20.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("callback_3");
    ns_web_url ("callback_3",
        "URL=https://1.rome.api.flipkart.com/api/1/connekt/push/callback",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:apikey,content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABATEEcm9tZQNhcGkIZmxpcGthcnQDY29tAAABAAEAACkQAAAAAAAATAAMAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://desr.fkapi.net/?TI164818842216200180224434770721730017262782886130882680036219971948&0&0&1&35.46&174.53&85.99&1&22.68&183.76&58.08", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBGRlc3IFZmthcGkDbmV0AAABAAEAACkQAAAAAAAAVQAMAFEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/f15c02bfeb02d15d.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/22fddf3c7da4c4f4.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/0ff199d1bd27eb98.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/69c6589653afdb9a.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/29327f40e9c4d26b.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("callback_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("callback_4");
    ns_web_url ("callback_4",
        "URL=https://1.rome.api.flipkart.com/api/1/connekt/push/callback",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=apiKey:KrWcJnCSZFBLFR39DtHYySjcDCHg2LeC3sxdx7646n7iy7oy",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3",
        BODY_BEGIN,
            "{"type":"PN","eventType":"TICKLE","cargo":"{\"permissionStatus\":\"denied\"}"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.flipkart.com/osdd.xml?v=2", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/promos/new/20150528-140547-favicon-retina.ico", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/1688/280/image/1f4b8cabe98448eb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/1688/280/image/c69716cd83f8878e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("callback_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_3");
    ns_web_url ("collector_3",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKBgcQHhBWEAhJEGJqAwIKAQIQCAoACx4QYmoDAwMGAxAIBgEGHhBiagMCBQIHEAgQZktCV3dAQF1ACBJxU1xcXUYSQFdTVhJCQF1CV0BGSxIVAhUSXVQSXEdeXm5cEhISElNGEmFGEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAwEGCwsbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFNdEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAEIAAIGCgUbblwSEhISU0YSemZ%2FfGnBdVkt3XldfV1%5ExGHFYSGlpGRkJBCB0dQUZTRltRHS1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11%3AENOcHVhBHUBTRFdcHAEcAAAcARx%3EYQQgACAYGBAcbEB4QYmoDAwIABRAIEF9dR0FXXURXQBAeEGJqAwIGAwEN%5BQQCAoDAAceEGJqAwIFAgoQCBBGQEdXEB4QYmoDsAwMKCxAIRkBHVx4QYmoDAgEEBRADIEHR9YpH8MdntkCFxGWh_9RWlte%3AVhoHGxDAeEGJqAwIEAAAQCAEeEGJqAwIABQAQCAMBBQAGHhBiagMDAgIGEAgDBAYKAwoKBgEEAgEFHhBiagMCAAIEEAgQUVEKAwpWUAIfU1ECAx8DA1dRHm1MKAFEfCgcLAgYDV1QBUVMBEB4QYmoDAgIKChAIVFNeQVceEGJqAwIBBAIQCBBaRkZCQQgdHUVFR7RxUXlt7CWVNARhxRXV8dEE9Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=cc818db0-ac01-11ec-a82c-859041ef3ca3&ft=263&seq=2&en=NTA&cs=680a846a513c1455e659fa395acc73f40932b166ab71b9df667a3659810a4f13&pc=3677918682988907&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B4%F3%A0%84%B2%F3%A0%84%B7%F3%A0%84%B5%F3%A0%84%B4%F3%A0%84%B6&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=3",
        BODY_END
    );

    ns_end_transaction("collector_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_4");
    ns_web_url ("collector_4",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_4_url_0_1_1648188770945.body",
        BODY_END
    );

    ns_end_transaction("collector_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_5");
    ns_web_url ("collector_5",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIFBwMQHhBWEAhJEGJqAwIBBAUQCBB2e2QBDHZ7ZAMMdntkAwx2e2QDDHMQHhBiagMDAgALEAgCHAsCAgIABgYDBgIEAAceEGJqAwICAwsQCAIcAQsLCwsLBAMKBwECAAUBBgYeEGJqAwIDAQUQCAMAHhBiagMCCwECEAgDAgcCHAEGCwsFBwcKBwsBBQceEGGJqAwIH%5EBgAQCAMAAB4QYmSoDAgEGBBAIAQMeEGJqAwIKBwMQCAMeEGJ%3AENOqAwIEAAAQC%3EAceEGJqAwIABQAQCAMLBgAFHhBiagEN%5BMDAgIGEAgDBAYKAwoKBsgYDBAsEHhBiagMDCAApIEEAgQ_UVE%3AKAwDpWUAIfU1ECAx8DA1dRH1MKAFEfCgcLAgYDV1QBUVMBEB4QYmoDAgIKChAImVFNeQVceEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxU7Xlt7CWVNARhxRXV8dEE9Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=cc818db0-ac01-11ec-a82c-859041ef3ca3&ft=263&seq=4&en=NTA&cs=680a846a513c1455e659fa395acc73f40932b166ab71b9df667a3659810a4f13&pc=1808911578919901&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B4%F3%A0%84%B2%F3%A0%84%B7%F3%A0%84%B5%F3%A0%84%B4%F3%A0%84%B6&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=5",
        BODY_END
    );

    ns_end_transaction("collector_5", NS_AUTO_STATUS);
    ns_page_think_time(6.854);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("fdp_2");
    ns_web_url ("fdp_2",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fdp_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_3");
    ns_web_url ("fdp_3",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"yelcau6xvk0000001648188428456","mpid":"FLIPKART","pn":"homepage","pt":"hp","ss":"BasePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic"},"e":[{"en":"AppEvents","ev":[{"name":"FCP","value":7132.50499998685},{"name":"TTFB","value":1607.0599999511614}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Home","pageUri":"/"},"t":1648188430548},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.1I8LF8WQ0P84","ct":"bannerAdCard","p":"1","t":1648188433465,"st":1648188431923,"f":true,"pv":89},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.MDNCQY5F87SS","ct":"bannerAdCard","p":"2","t":1648188438448,"st":1648188433763,"f":true,"pv":88},{"en":"AppEvents","ev":[{"name":"LCP","value":8064.535},{"name":"FID","value":15.050000045448542}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Home","pageUri":"/"},"t":1648188438456},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.KJEM381CR1RM","ct":"bannerAdCard","p":"3","t":1648188443446,"st":1648188436975,"f":true,"pv":88},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.WT8A2IVYIWLA","ct":"bannerAdCard","p":"4","t":1648188443446,"st":1648188440299,"f":true,"pv":89},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.1I8LF8WQ0P84","ct":"bannerAdCard","p":"1","t":1648188448446,"st":1648188443599,"f":true,"pv":89}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.flipkart.com/clothing-and-accessories/bottomwear/pr?sid=clo,vua&p[]=facets.ideal_for%255B%255D%3DMen&p[]=facets.ideal_for%255B%255D%3Dmen&otracker=categorytree&fm=neo%2Fmerchandising&iid=M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.8HARX8UX7IX5&otracker=hp_rich_navigation_2_2.navigationCard.RICH_NAVIGATION_Fashion~Men%2527s%2BBottom%2BWear_8HARX8UX7IX5&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&cid=8HARX8UX7IX5", END_INLINE
    );

    ns_end_transaction("fdp_3", NS_AUTO_STATUS);
    ns_page_think_time(69.402);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("Browse_chunk_4eeda8_css");
    ns_web_url ("Browse_chunk_4eeda8_css",
        "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Browse.chunk.4eeda8.css",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:style",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Browse.chunk.15210220.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("Browse_chunk_4eeda8_css", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_6");
    ns_web_url ("collector_6",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_6_url_0_1_1648188770971.body",
        BODY_END
    );

    ns_end_transaction("collector_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_4");
    ns_web_url ("fdp_4",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"yelcau6xvk0000001648188428456","mpid":"FLIPKART","pn":"homepage","pt":"hp","ss":"BasePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic"},"e":[{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.MDNCQY5F87SS","ct":"bannerAdCard","p":"2","t":1648188453958,"st":1648188446787,"f":true,"pv":89},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.KJEM381CR1RM","ct":"bannerAdCard","p":"3","t":1648188458544,"st":1648188452796,"f":true,"pv":88},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.WT8A2IVYIWLA","ct":"bannerAdCard","p":"4","t":1648188458544,"st":1648188455346,"f":true,"pv":89}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/NavMenu.chunk.05b448.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/NavMenu.chunk.7e7b9f66.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fetch");
    ns_web_url ("fetch",
        "URL=https://1.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fetch", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_2");
    ns_web_url ("fetch_2",
        "URL=https://1.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "{"pageUri":"/clothing-and-accessories/bottomwear/pr?sid=clo,vua&p[]=facets.ideal_for%255B%255D%3DMen&p[]=facets.ideal_for%255B%255D%3Dmen&otracker=categorytree&fm=neo%2Fmerchandising&iid=M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.8HARX8UX7IX5&otracker=hp_rich_navigation_2_2.navigationCard.RICH_NAVIGATION_Fashion~Men%2527s%2BBottom%2BWear_8HARX8UX7IX5&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&cid=8HARX8UX7IX5","pageContext":{"fetchSeoData":true,"paginatedFetch":false,"pageNumber":1},"requestContext":{"type":"BROWSE_PAGE","ssid":"5z2tfo2jhc0000001648188463307","sqid":"jd30bwq6v40000001648188463307"}}",
        BODY_END
    );

    ns_end_transaction("fetch_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_5");
    ns_web_url ("fdp_5",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"5z2tfo2jhc0000001648188463307","mpid":"FLIPKART","pn":"homepage","pt":"hp","ss":"BasePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"S","pat":"SEARCH","sqid":"jd30bwq6v40000001648188463307","t":1648188463307},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.1I8LF8WQ0P84","ct":"bannerAdCard","p":"1","t":1648188463325,"st":1648188459356,"f":true,"pv":89},{"en":"DWI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_5","wk":"5.rhsAnnouncement.RHS_ANNOUNCEMENT_4","ct":"contentCollection","p":4,"t":1648188463325,"st":1648188430656,"f":true,"pv":66},{"en":"DWI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS","wk":"2.navigationCard.RICH_NAVIGATION","ct":"contentCollection","p":1,"t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.G6ZC4RAJ9OHU","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"1","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.CBUR1Q46W5F1","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"2","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.ZRQ4DKH28K8J","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"3","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.B5YIQCE8VHYO","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"4","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.JKA0LKU8OMVP","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"5","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.A7YT3X39TTON","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"6","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.LO4IWVHA61BX","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"7","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.V4ZPKTOAO321","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"8","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.CSZJ9U6U82HR","wk":"2.navigationCard.RICH_NAVIGATION","ct":"navigationCard","p":"9","t":1648188463325,"st":1648188430655,"f":true,"pv":74},{"en":"DWI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3","wk":"3.bannerAdCard.BANNERADS_2","p":2,"t":1648188463325,"st":1648188430655,"f":true,"pv":100},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4.1W2LPEFEI5T7","ct":"OfferCard","p":"1","t":1648188463325,"st":1648188430655,"f":true,"pv":61},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4.APX1P81KHBLM","ct":"OfferCard","p":"2","t":1648188463325,"st":1648188430655,"f":true,"pv":61},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4.CTFXA8CZDYE4","ct":"OfferCard","p":"3","t":1648188463325,"st":1648188430655,"f":true,"pv":61},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4.G1EKGHZDD30I","ct":"OfferCard","p":"4","t":1648188463325,"st":1648188430655,"f":true,"pv":61},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4.RODPIC81QH8X","ct":"OfferCard","p":"5","t":1648188463325,"st":1648188430656,"f":true,"pv":61},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4.SQKNVSG1YPJD","ct":"OfferCard","p":"6","t":1648188463325,"st":1648188430656,"f":true,"pv":61},{"en":"DWI","p":3,"iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_4","wk":"4.dealCard.OMU_3","t":1648188463325,"st":1648188430656,"f":true,"pv":61},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_5.SQWDWVNI37JS","wk":"5.rhsAnnouncement.RHS_ANNOUNCEMENT_4","ct":"rhsAnnouncement","p":"1","t":1648188463325,"st":1648188430656,"f":true,"pv":66},{"en":"DCI","iid":"M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_3.MDNCQY5F87SS","ct":"bannerAdCard","p":"2","t":1648188463442,"st":1648188462136,"f":true,"pv":89}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Access-Control-Request-Headers:content-type", "HEADER=Origin:https://www.flipkart.com", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBnBsYS10awhmbGlwa2FydANuZXQAAAEAAQAAKRAAAAAAAABQAAwATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DJEAFKH5GJAZKGWZ7%2CJEAFUSGZVYZPPGDG%2CSRTFTFCBRKGYTQEF%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=browsePage&cd%5Bpcat%5D=clothing-and-accessories,bottomwear&cd%5Bbrand%5D=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k1118cw0/jean/w/z/7/32-10064487-roadster-original-imafkzpjzzerxcgf.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ke0a7ww0-0/jean/b/w/8/28-11274476-roadster-original-imafusgzfqds3hmz.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kc9eufk0/short/s/e/u/xxl-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzzxh7tj7g.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/l0sgyvk0/track-pant/v/y/r/s-combo-tr1-vebnor-original-imagcgf9cqsgzwvb.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kzegk280/track-pant/y/k/a/m-vit9018-vitaan-original-imagbfcufkbfhtnv.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCWdvb2dsZWFkcwFnC2RvdWJsZWNsaWNrA25ldAAAAQABAAApEAAAAAAAAEgADABEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwhmYWNlYm9vawNjb20AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dxBnb29nbGVhZHNlcnZpY2VzA2NvbQAAAQABAAApEAAAAAAAAEsADABHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/jvtujrk0/jean/w/u/m/32-8813589-roadster-original-imafgmsbtbbvgw3s.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/l111lzk0/track-pant/4/d/b/xl-solid-men-blue-and-dark-grey-track-pant-foxter-original-imagczuwkzb4ankd.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ky3b0y80/track-pant/y/u/h/l-solid-men-black-rama-track-pants-foxter-original-imagaekzhjypkqcg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kiulxu80-0/short/m/k/h/xxl-ic-24446-indiclub-original-imafyjsgfahqbcr3.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kly2aa80/track-pant/g/n/d/m-trackpant9013-vitaan-original-imagyydmyng5gzce.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kggviq80-0/jean/0/u/u/36-11691082-roadster-original-imafwzxsgzfjgehy.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("eventBatch");
    ns_web_url ("eventBatch",
        "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"events":[{"adunits":[{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXGq0ZVAG7NIAnjBJcHd2mty2Yr1OAYdfgtCoY3SM07Rmg==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXH2YN0hVrtCuI0IcPr3fan628QQoyFbqVga3MwfV/WP6A==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXE/FO920sEYykYPdECyJtmr4dr6uUAM3CbA3WttmN0dLg==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXHOQRw74X3zNfcaafdFcrep3w8Gd88P/Tpa/XfpRqNVfw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXFGwRFA6wWbko0Ccvn3TjbAAYSU1/kl5Cp6aWyR281NmQ==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXGp41fBHlR6/4UzNXRy7PTHnUC2ThLPlcVAHnbgyCZhbw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXHbzYQ5R4KPlN38xQN15BOyauU3KTt4vGGmOKlklOHnOQ==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXFUPN2lP9ATacHHa/AOff+RV08KFbz5A7WM9wERg7JqVg==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXGBKk3QbAR7+bbkeHUetWnmxYLtTg4djjytp+E1U/HIGw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXHNx4oOERkOXvkSIpOlNmaSs0GQHcC3z/rtZvmsBeEJDw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXEDz5BVKlURnSgKLdR/KzTZ4BCDrtNcZ66hL1B4b6VwOw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXELTai8j4/24l+Y1qHjwpdbW4mcy4hbQBaW3qCR75o6Gw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXHbI51+4dtxd4K/EoF0QQpLEiZNOn9JnN8KbXJn1dmjuw==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXHpkJz2P1pnawS68rPjLXs24YusDVvAbkBtErILshblew==","bannerId":"-1"},{"impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXF9WPNw4jxWcqmshlqJMCF40r9tPnW4YEk/ArMehNFI2A==","bannerId":"-1"}],"event":"adBeacon","dontTrack":false,"eventTime":1648188464306,"responseId":"b7ef5489-bb27-4b14-b063-de3d35cf7460","id":"b7ef5489-bb27-4b14-b063-de3d35cf7460"}],"eventDispatchTime":1648188464306,"dontTrack":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=428518592", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&ocp_id=MFw9YtrRJvKl4t4PirqA-A0&random=1059386879&sscte=1&crd=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=test_cookie", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DJEAFKH5GJAZKGWZ7%2CJEAFUSGZVYZPPGDG%2CSRTFTFCBRKGYTQEF%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=1734028788", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=3231347987", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k687wy80/jean/y/g/g/32-10764214-roadster-original-imafzztgnzhnknzg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ksc46fk0/track-pant/b/1/n/xl-solid-men-black-and-blue-track-pants-foxter-original-imag5x8scgs9hh67.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kjd6nww0-0/track-pant/x/d/e/xl-tblrdjogj22-bljogj23-tripr-original-imafyygggbnyrhwx.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kiqbma80-0/short/w/x/i/xxl-ic-24445-indiclub-original-imafygpfzcyx5gjy.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kwzap3k0/jean/t/8/4/32-maw21elppjn521-metronaut-original-imag9jeurfxaedcz.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwZnb29nbGUDY29tAAABAAEAACkQAAAAAAAAVQAMAFEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ky7lci80/jean/s/p/w/32-kjb-1648-killer-original-imagahwhnwfhneae.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kvgzyq80/jean/r/g/c/32-maw21jn788-metronaut-original-imag8da4ubtpanmg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kyeqjrk0/track-pant/f/b/2/s-solid-men-black-maroon-track-pants-foxter-original-imaganewrkwngwhc.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("eventBatch", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json");
    ns_web_url ("json",
        "URL=https://update.googleapis.com/service/update2/json?cup2key=10:2832905279&cup2hreq=4f3afbc8cdfff11f4af3c201a7be4e755c1027f2c1757d8ec4980f973a591b2b",
        "METHOD=POST",
        "HEADER=X-Goog-Update-AppId:jflookgnkcckhobaglndicnbbgbonegd,giekcmmlnklenlaomppkphknjmnnpneh,cmahhnpholdijhjokonmfdjbfmklppij,khaoiebndkojlmppeemjhbpbandiljpe,obedbbhbpmojnkanicioggnmelmoomoc,jamhcnnkihinmdlkakkaopbjbbcngflc,oimompecagnajdejgnnjijobebaeigek,ggkkehgbnfjpeggfpleeakpidbkibbmn,hfnkpimlhhgieaddgfemjhofmfblmnib,gcmjkmgdlgnkkcocmoeiminaijmmjnii,pdafiollngonhoadbmdoemagnfpdphbe,llkgjffcdpffmhiakmfcdcblohccpfmo,ojhpjlocmbogdgmfpkhlaaeamibhnphh,aemomkdncapdnfajjbbcbdebjljbpmpj,eeigpngbgcognadeebkilcpcaedhellh",
        "HEADER=X-Goog-Update-Interactivity:bg",
        "HEADER=X-Goog-Update-Updater:chromium-90.0.4430.93",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"jflookgnkcckhobaglndicnbbgbonegd","cohort":"1:s7x:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.350b2f02af39a4e941e289bdb53186f6d25e05575b4196a2b16e690ce28e08d9"}]},"ping":{"ping_freshness":"{586a6142-0caf-4575-a069-643ec931e9d9}","rd":5460},"updatecheck":{},"version":"2749"},{"appid":"giekcmmlnklenlaomppkphknjmnnpneh","cohort":"1:j5l:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.fd515ec0dc30d25a09641b8b83729234bc50f4511e35ce17d24fd996252eaace"}]},"ping":{"ping_freshness":"{c4b89dad-797b-4c78-bf80-3668e2a9f821}","rd":5460},"updatecheck":{},"version":"7"},{"appid":"cmahhnpholdijhjokonmfdjbfmklppij","cohort":"1:wr3:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.b4ddbdce4f8d5c080328aa34c19cb533f2eedec580b5d97dc14f74935e4756b7"}]},"ping":{"ping_freshness":"{749124ce-b253-4ba9-ac6d-a3b3a27e7bd0}","rd":5460},"updatecheck":{},"version":"1.0.6"},{"appid":"khaoiebndkojlmppeemjhbpbandiljpe","cohort":"1:cux:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.ffd1d2d75a8183b0a1081bd03a7ce1d140fded7a9fb52cf3ae864cd4d408ceb4"}]},"ping":{"ping_freshness":"{a9d18132-dbaf-4ade-a627-5b7679cebb63}","rd":5460},"updatecheck":{},"version":"43"},{"accept_locale":"ENUS","appid":"obedbbhbpmojnkanicioggnmelmoomoc","cohort":"1:s6f:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.bf7ff6f0630dc589b30d575f87ff966abf1ce510b4231828e749af2fffd2fe04"}]},"ping":{"ping_freshness":"{29d9df77-641e-414f-a2e9-a716ca037306}","rd":5460},"updatecheck":{},"version":"20210827.393763216"},{"appid":"jamhcnnkihinmdlkakkaopbjbbcngflc","cohort":"1:wvr:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.742b6d763c86aae514e42a31bfb754f43ef6fd2de416bda1538ae5eb8ee36724"}]},"ping":{"ping_freshness":"{76619966-b9f8-49ff-be91-7546d40750ed}","rd":5460},"updatecheck":{},"version":"95.0.4630.0"},{"appid":"oimompecagnajdejgnnjijobebaeigek","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.0727b38159b38ffa3633510444ece15c86417962e8cac59c59002f13b50239ac"}]},"ping":{"ping_freshness":"{55a3e678-197b-4988-8caf-a0567ac5d3a7}","rd":5460},"updatecheck":{},"version":"4.10.2209.0"},{"appid":"ggkkehgbnfjpeggfpleeakpidbkibbmn","cohort":"1:ut9:","cohorthint":"M80ToM99","cohortname":"M80ToM99","enabled":true,"packages":{"package":[{"fp":"1.0dc23b37d1fa49e00e0a208bfcb91839be99b9e85162814b05987c2eba1b3701"}]},"ping":{"ping_freshness":"{39147431-4abf-4c2e-b4d5-a5c298c471ea}","rd":5460},"updatecheck":{},"version":"2021.8.23.1140"},{"appid":"hfnkpimlhhgieaddgfemjhofmfblmnib","cohort":"1:jcl:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.0809a1484b584237fcff12cd9659a7cc1819efd66455697e589bb396c1409c1c"}]},"ping":{"ping_freshness":"{8ff920d4-0384-4e7d-bf60-51c4158e0d4d}","rd":5460},"updatecheck":{},"version":"7033"},{"appid":"gcmjkmgdlgnkkcocmoeiminaijmmjnii","cohort":"1:bm1:11vf@0.01","cohorthint":"M54AndUp","cohortname":"M54ToM99","enabled":true,"packages":{"package":[{"fp":"1.b45fc5f3479dc7b07e8e5822a11785819b7f1c249c9b47dcffcb28edbbc2d706"}]},"ping":{"ping_freshness":"{c1603117-9f10-4f48-955e-8b069c916632}","rd":5460},"updatecheck":{},"version":"9.29.4"},{"appid":"pdafiollngonhoadbmdoemagnfpdphbe","cohort":"1:vz3:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.54b93e249d02a0f9061e8f70866d4668a0260db9ae43483810ab78f97f3eaa2a"}]},"ping":{"ping_freshness":"{98952547-41c0-412e-8197-f7ff0e731c46}","rd":5460},"updatecheck":{},"version":"2021.8.17.1300"},{"appid":"llkgjffcdpffmhiakmfcdcblohccpfmo","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.2731bdeddb1470bf2f7ae9c585e7315be52a8ce98b8af698ece8e500426e378a"}]},"ping":{"ping_freshness":"{c335b703-3320-41a7-b595-eb9f94f64c7c}","rd":5460},"updatecheck":{},"version":"1.0.0.8"},{"appid":"ojhpjlocmbogdgmfpkhlaaeamibhnphh","cohort":"1:w0x:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.478aa915e78878e332a0b4bb4d2a6fb67ff1c7f7b62fe906f47095ba5ae112d0"}]},"ping":{"ping_freshness":"{139543af-3f8f-4321-ba2b-b30abfe5f0da}","rd":5460},"updatecheck":{},"version":"1"},{"appid":"aemomkdncapdnfajjbbcbdebjljbpmpj","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.c25787c5c76ff9c4c50a87d32802301c9ed80d934830d677bbc6629e290cb5aa"}]},"ping":{"ping_freshness":"{0e4d9f41-26e8-48e9-bb96-ae04de3df3c1}","rd":5460},"updatecheck":{},"version":"1.0.6.0"},{"appid":"eeigpngbgcognadeebkilcpcaedhellh","cohort":"1:w59:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.c64c9c1008f3ba5f6e18b3ca524bc98dcd8acfae0a2720a8f1f3ef0f8d643d05"}]},"ping":{"ping_freshness":"{576a682d-bcf1-46c4-a341-8548a501358c}","rd":5460},"updatecheck":{},"version":"2020.11.2.164946"}],"arch":"x64","dedup":"cr","domainjoined":false,"hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{24853352-4c7e-4611-994f-9902023b0d5d}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBnVwZGF0ZQpnb29nbGVhcGlzA2NvbQAAAQABAAApEAAAAAAAAE4ADABKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/jvqzo280/jean/x/s/p/28-8275725-roadster-original-imafghgfdqfzf7t8.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ku79vgw0/jean/y/v/a/32-36087-0314-levi-s-original-imag7dmhwzgmjjuv.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kirr24w0-0/short/g/m/y/l-cic-4546-indiclub-original-imafyhzasvcw4ya2.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kzllrbk0/short/y/n/t/xl-togr-gyshort-believer-tripr-original-imagbkkr9kb949zh.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=1059386879&crd=&is_vtc=1&random=328740375", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/l02r1jk0/short/v/k/c/l-tnv-rdshortabs1-tripr-original-imagby6ywq87gxgg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/l13whow0/short/j/g/w/l-tgy-nvshortskullbeard-tripr-original-imagcqzngzgq9gtu.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kfoapow0-0/jean/2/7/7/28-2312013-roadster-original-imafw2g2edzejgyc.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kfoapow0-0/jean/p/l/g/30-5669114-roadster-original-imafw2g4ehzduvch.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("json", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("swatch");
    ns_web_url ("swatch",
        "URL=https://1.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=428518592&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DJEAFKH5GJAZKGWZ7%2CJEAFUSGZVYZPPGDG%2CSRTFTFCBRKGYTQEF%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=1734028788&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=3231347987&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kfoapow0-0/trouser/v/x/5/38-t-130-trouser-baleno-black-aa-ad-av-original-imafw2jugbuxmnq9.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwZnb29nbGUCY28CaW4AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=1059386879&crd=&is_vtc=1&random=328740375&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ksc46fk0/track-pant/g/f/z/xl-trackpant9013-vitaan-original-imag5xhs54zrnyfk.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kjiwfbk0-0/jean/b/y/m/36-12303778-roadster-original-imafz2nwfdvqjs76.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("swatch", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_2");
    ns_web_url ("swatch_2",
        "URL=https://1.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pidLidMap":{"JEAFKH5GJAZKGWZ7":"LSTJEAFKH5GJAZKGWZ7DXTN4U"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/580/696/kvgzyq80/jean/r/w/n/32-maw21jn789-metronaut-original-imag8da4fseyh7gr.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kzblocw0/track-pant/w/j/4/s-combo-tr1-vebnor-original-imagbcrupkh6s8yr.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kua4r680/track-pant/z/x/q/l-patti1-acrux-original-imag7fwxgrxrdt6z.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kwmfqfk0/track-pant/0/e/y/xxl-solid-men-grey-track-pants-foxter-original-imag99872enajxtm.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/km57hjk0/track-pant/g/z/9/l-trackpant9015-vitaan-original-imagf4dhrzfenqgt.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/jsc3ssw0/jean/j/x/z/32-2525011-roadster-original-imafdwskaazvbwsy.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/jw6pifk0/jean/y/g/f/30-8962465-roadster-original-imafgwqeyydtfyuc.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kza68i80/trouser/l/d/s/34-slim-fit-men-black-cotton-lycra-blend-trousers-moonvelly-original-imagbbwygq4jgsgk.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k2xmd8w0/track-pant/s/2/e/l-1jg-jog-cargo-blk-jugular-original-imafkgqdb9kk8wrb.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("swatch_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_3");
    ns_web_url ("swatch_3",
        "URL=https://1.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pidLidMap":{"TKPGBFCUWHXEKJF4":"LSTTKPGBFCUWHXEKJF4RCAA62"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END
    );

    ns_end_transaction("swatch_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_4");
    ns_web_url ("swatch_4",
        "URL=https://1.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pidLidMap":{"SRTFTFCBRKGYTQEF":"LSTSRTFTFCBRKGYTQEF47YPOY"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END
    );

    ns_end_transaction("swatch_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("eventBatch_2");
    ns_web_url ("eventBatch_2",
        "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"events":[{"duration":658,"maxViewPercentage":100,"viewStartTime":1648188465441,"eventTime":1648188468438,"dontTrack":false,"event":"adView","adUnit":"SUMMARY","pageView":"GRID_VIEW","bannerId":"-1","impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXGq0ZVAG7NIAnjBJcHd2mty2Yr1OAYdfgtCoY3SM07Rmg==","listingId":"LSTJEAFKH5GJAZKGWZ7DXTN4U","id":"b7ef5489-bb27-4b14-b063-de3d35cf7460","responseId":"b7ef5489-bb27-4b14-b063-de3d35cf7460"},{"duration":658,"maxViewPercentage":100,"viewStartTime":1648188465441,"eventTime":1648188468438,"dontTrack":false,"event":"adView","adUnit":"SUMMARY","pageView":"GRID_VIEW","bannerId":"-1","impressionId":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXH2YN0hVrtCuI0IcPr3fan628QQoyFbqVga3MwfV/WP6A==","listingId":"LSTJEAFUSGZVYZPPGDGRDBSEA","id":"b7ef5489-bb27-4b14-b063-de3d35cf7460","responseId":"b7ef5489-bb27-4b14-b063-de3d35cf7460"}],"eventDispatchTime":1648188470442,"dontTrack":false}",
        BODY_END
    );

    ns_end_transaction("eventBatch_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_2");
    ns_web_url ("json_2",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"jflookgnkcckhobaglndicnbbgbonegd","event":[{"download_time_ms":4319,"downloaded":27297,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"2789","previousversion":"2749","total":27297,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/a3op3tiajhsjqgzp6n6ba745ke_2789/jflookgnkcckhobaglndicnbbgbonegd_2789_all_acbofsvhtusseyecsmdpwk2biznq.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.daa12b60c80830f0626ac0e5f0dc807415d4cc15b8c947232ddf6ff4e6994ef4","nextversion":"2789","previousfp":"1.350b2f02af39a4e941e289bdb53186f6d25e05575b4196a2b16e690ce28e08d9","previousversion":"2749"}],"version":"2789"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{d64425aa-b578-4f56-97cc-89062d74eded}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_6");
    ns_web_url ("fdp_6",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"5z2tfo2jhc0000001648188463307","ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"PV","ib":true,"id":false,"cat":"MensClothingJeans","t":1648188464309},{"en":"DCI","iid":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXGq0ZVAG7NIAnjBJcHd2mty2Yr1OAYdfgtCoY3SM07Rmg==","ct":"ProductCard","p":"1","t":1648188468438,"st":1648188465441,"f":true,"pv":100},{"en":"DCI","iid":"en_OJvviyOwBwMbk93LiXThpa8lk0mwuRLbJfR1yf3VtXH2YN0hVrtCuI0IcPr3fan628QQoyFbqVga3MwfV/WP6A==","ct":"ProductCard","p":"2","t":1648188468438,"st":1648188465441,"f":true,"pv":100},{"en":"DCI","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH","ct":"ProductCard","p":"3","t":1648188468438,"st":1648188465441,"f":true,"pv":100},{"en":"DCI","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.TKPGCGFAHPY4UEXJ.SEARCH","ct":"ProductCard","p":"4","t":1648188468438,"st":1648188465441,"f":true,"pv":100}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_3");
    ns_web_url ("json_3",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"khaoiebndkojlmppeemjhbpbandiljpe","event":[{"download_time_ms":4058,"downloaded":5585,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"48","previousversion":"43","total":5585,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/acfifkd2sz342wmnbw7yvdwmtc5a_48/khaoiebndkojlmppeemjhbpbandiljpe_48_win_ccfl2wvh5b5bfuztfguafrvlpm.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.5f1c8af8a15da419e629cc50d85e7326cda080bd1f7df8ac38a16b98e0a2739b","nextversion":"48","previousfp":"1.ffd1d2d75a8183b0a1081bd03a7ce1d140fded7a9fb52cf3ae864cd4d408ceb4","previousversion":"43"}],"version":"48"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{f02f441f-5bc7-4879-9ef5-fb59ba89e070}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_4");
    ns_web_url ("json_4",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"obedbbhbpmojnkanicioggnmelmoomoc","event":[{"download_time_ms":4038,"downloaded":997358,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"20220315.435616495","previousversion":"20210827.393763216","total":997358,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/acslf4vggz2ngp4q6emd3gdlropa_20220315.435616495/obedbbhbpmojnkanicioggnmelmoomoc_20220315.435616495_all_ENUS_g3m7zxwn5gnbiejkeyu7eh2rpq.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.ca0088695b463dcbe69afd92d9a2858e7878bffd561678349454938beb4c78f5","nextversion":"20220315.435616495","previousfp":"1.bf7ff6f0630dc589b30d575f87ff966abf1ce510b4231828e749af2fffd2fe04","previousversion":"20210827.393763216"}],"version":"20220315.435616495"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{7a396075-97cb-437b-b79d-67ffc1ab1fd6}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_5");
    ns_web_url ("json_5",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"jamhcnnkihinmdlkakkaopbjbbcngflc","event":[{"download_time_ms":4032,"downloaded":818392,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"102.0.4962.3","previousversion":"95.0.4630.0","total":818392,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/mb54b4qhcqw4bnw3wiuzknmdgi_102.0.4962.3/jamhcnnkihinmdlkakkaopbjbbcngflc_102.0.4962.3_all_ach2webcaqfkvf2unq2mwfxo32xa.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.238e44f11d5414cd936d401514b2df1c29c6054143bc6397d952b1c8fb5fe506","nextversion":"102.0.4962.3","previousfp":"1.742b6d763c86aae514e42a31bfb754f43ef6fd2de416bda1538ae5eb8ee36724","previousversion":"95.0.4630.0"}],"version":"102.0.4962.3"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{e6340ad3-e245-4de1-9523-b56697e7a9e5}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_5", NS_AUTO_STATUS);
    ns_page_think_time(4.005);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("status");
    ns_web_url ("status",
        "URL=https://1.rome.api.flipkart.com/api/6/user/signup/status",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABATEEcm9tZQNhcGkIZmxpcGthcnQDY29tAAABAAEAACkQAAAAAAAATAAMAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", END_INLINE
    );

    ns_end_transaction("status", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_6");
    ns_web_url ("json_6",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"ggkkehgbnfjpeggfpleeakpidbkibbmn","event":[{"download_time_ms":4027,"downloaded":10047,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"2022.3.14.1147","previousversion":"2021.8.23.1140","total":10047,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/dyb3wieb35bbpv64pnsym5fbji_2022.3.14.1147/ggkkehgbnfjpeggfpleeakpidbkibbmn_2022.3.14.1147_all_adu2ewrzm5fugsecmuxlx655fijq.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.bfa1a6198e6ca1d70da66818e4de60ea78606520ef33ece1433eb5a3402560e9","nextversion":"2022.3.14.1147","previousfp":"1.0dc23b37d1fa49e00e0a208bfcb91839be99b9e85162814b05987c2eba1b3701","previousversion":"2021.8.23.1140"}],"version":"2022.3.14.1147"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{b962189f-66b3-4066-9781-10ce2afd16f3}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("status_2");
    ns_web_url ("status_2",
        "URL=https://1.rome.api.flipkart.com/api/6/user/signup/status",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "{"loginId":["+917683831142"],"supportAllStates":true}",
        BODY_END
    );

    ns_end_transaction("status_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_7");
    ns_web_url ("fdp_7",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"5z2tfo2jhc0000001648188463307","ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":null,"iss":true,"lid":["+917683831142"],"fid":"WEBSITE-LOGIN","t":1648188501447}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_7", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("generate");
    ns_web_url ("generate",
        "URL=https://1.rome.api.flipkart.com/api/7/user/otp/generate",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("generate", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("generate_2");
    ns_web_url ("generate_2",
        "URL=https://1.rome.api.flipkart.com/api/7/user/otp/generate",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "{"loginId":"+917683831142"}",
        BODY_END
    );

    ns_end_transaction("generate_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_8");
    ns_web_url ("fdp_8",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"5z2tfo2jhc0000001648188463307","ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":null,"iss":true,"lid":"+917683831142","fid":"WEBSITE-LOGIN","t":1648188501945}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_7");
    ns_web_url ("json_7",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"hfnkpimlhhgieaddgfemjhofmfblmnib","event":[{"download_time_ms":4065,"downloaded":25360,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"7235","previousversion":"7033","total":25360,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/ggnukd4rcp4pjt6zlbgmjhs7se_7235/hfnkpimlhhgieaddgfemjhofmfblmnib_7235_all_aclj2ljbhchnbknpjfwxbyhabznq.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.448d7a8ca0db34e8d838c7afc1aafec46683b5d08b6e012d3e000ce4afdbd38d","nextversion":"7235","previousfp":"1.0809a1484b584237fcff12cd9659a7cc1819efd66455697e589bb396c1409c1c","previousversion":"7033"}],"version":"7235"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{e49fbf9a-4c08-48bf-9442-fae2376e9aeb}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_8");
    ns_web_url ("json_8",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"gcmjkmgdlgnkkcocmoeiminaijmmjnii","event":[{"download_time_ms":4029,"downloaded":38693,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"9.34.0","previousversion":"9.29.4","total":38693,"url":"http://edgedl.me.gvt1.com/edgedl/release2/chrome_component/bdaqy7owebp4aswjnc7iwpm2ie_9.34.0/gcmjkmgdlgnkkcocmoeiminaijmmjnii_9.34.0_all_acb7qksdc2wjznjioir7p6lt3dwq.crx3"},{"eventresult":1,"eventtype":3,"nextfp":"1.93823a4e71e764b932ee22dfcf84c24429867a440c5e480e55be527ac30de1ae","nextversion":"9.34.0","previousfp":"1.b45fc5f3479dc7b07e8e5822a11785819b7f1c249c9b47dcffcb28edbbc2d706","previousversion":"9.29.4"}],"version":"9.34.0"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{fad2038d-7bb0-4484-8ede-e417085b3b99}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("json_9");
    ns_web_url ("json_9",
        "URL=https://update.googleapis.com/service/update2/json",
        "METHOD=POST",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chromium","acceptformat":"crx2,crx3","app":[{"appid":"llkgjffcdpffmhiakmfcdcblohccpfmo","event":[{"download_time_ms":4101,"downloaded":2876,"downloader":"bits","eventresult":1,"eventtype":14,"nextversion":"1.0.0.13","previousversion":"1.0.0.8","total":2876,"url":"http://edgedl.me.gvt1.com/edgedl/chromewebstore/L2Nocm9tZV9leHRlbnNpb24vYmxvYnMvODJiQUFYYVJaZ0k5di1hUFlXS1prX2xDZw/1.0.0.13_llkgjffcdpffmhiakmfcdcblohccpfmo.crx"},{"eventresult":1,"eventtype":3,"nextfp":"1.ab8d70a60ce0fba1355fad4edab88fd4d1bccc566b230998180183d1d776992b","nextversion":"1.0.0.13","previousfp":"1.2731bdeddb1470bf2f7ae9c585e7315be52a8ce98b8af698ece8e500426e378a","previousversion":"1.0.0.8"}],"version":"1.0.0.13"}],"arch":"x64","dedup":"cr","hw":{"physmemory":8},"lang":"en-US","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.22000.556"},"prodversion":"90.0.4430.93","protocol":"3.1","requestid":"{12dd6ea8-ef18-47d6-9a98-ab028eec649c}","sessionid":"{a4b9d2a8-3d62-46a9-8526-5f75e9c91df7}","updaterversion":"90.0.4430.93"}}",
        BODY_END
    );

    ns_end_transaction("json_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("generate_3");
    ns_web_url ("generate_3",
        "URL=https://1.rome.api.flipkart.com/api/7/user/otp/generate",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "{"loginId":"+917683831142"}",
        BODY_END
    );

    ns_end_transaction("generate_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_9");
    ns_web_url ("fdp_9",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"5z2tfo2jhc0000001648188463307","ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":null,"iss":true,"lid":"+917683831142","fid":"WEBSITE-LOGIN","t":1648188542394}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_10");
    ns_web_url ("fdp_10",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"5z2tfo2jhc0000001648188463307","ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"7vva2zwk7k0000001648188428753","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"DCC","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH","ct":"productCard","p":"3","t":1648188555830}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_10", NS_AUTO_STATUS);
    ns_page_think_time(0.008);

    ns_start_transaction("itm6286e2fccabd2");
    ns_web_url ("itm6286e2fccabd2",
        "URL=https://www.flipkart.com/gyrfalcon-solid-men-multicolor-regular-shorts/p/itm6286e2fccabd2?pid=SRTFTFCBRKGYTQEF&lid=LSTSRTFTFCBRKGYTQEF47YPOY&marketplace=FLIPKART&store=clo%2Fvua&srno=b_1_3&otracker=browse&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&fm=organic&iid=b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH&ppt=browse&ppn=browse&ssid=5z2tfo2jhc0000001648188463307",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;SN;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq"
    );

    ns_end_transaction("itm6286e2fccabd2", NS_AUTO_STATUS);
    ns_page_think_time(0.606);

    //Page Auto split for Image Link '' Clicked by User
    ns_start_transaction("dns_query");
    ns_web_url ("dns_query",
        "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwhmbGlwa2FydANjb20AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
        "HEADER=Accept-Language:*",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABEXN0YXRpYy1hc3NldHMtd2ViCGZsaXhjYXJ0A2NvbQAAAQABAAApEAAAAAAAAEUADABBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCXJ1a21pbmltMQhmbGl4Y2FydANjb20AAAEAAQAAKRAAAAAAAABNAAwASQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app_modules.chunk.94b5e7.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app.chunk.9adf7d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Product.chunk.097bc0.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/runtime.95858ad1.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/NavMenu.chunk.05b448.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/raven.3.22.3.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/vendor.chunk.bf58a224.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_modules.chunk.ac48891f.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_common.chunk.db774788.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app.chunk.b7fa5477.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/kc9eufk0/short/s/e/u/xxl-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzzxh7tj7g.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/kc9eufk0/short/s/e/u/xxl-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzw3x84v8u.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/kc9eufk0/short/s/e/u/xl-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzqtmsphsn.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/kc9eufk0/short/s/e/u/l-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzsmmkpxbf.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/kc9eufk0/short/s/e/u/m-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzuh4u9mcs.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/info-basic_6c1a38.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_69e7ec.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/NavMenu.chunk.7e7b9f66.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/batman-returns/omni/omni16.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/px/gNtTli3A/init.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/lockin/100/24/images/CCO__PP_2019-07-14.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/100/10/promos/01/12/2018/8aa01ab4-de88-4a46-9d93-5c7f3ebac2df.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202106/blobio-imr-202106_9dc9c5e800c941849bce41f6c3088db0.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202104/blobio-imr-202104_03cf9fdb9c75425c9c44a51a2390db74.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202104/blobio-imr-202104_7414adb5e32f4606bcc542bd39952239.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202107/blobio-imr-202107_7a476eef813241f5ad07f42fefc78acc.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202106/blobio-imr-202106_5c0156da14df4ce8b94f05974bb5b46c.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202106/blobio-imr-202106_9dc9c5e800c941849bce41f6c3088db0.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202104/blobio-imr-202104_03cf9fdb9c75425c9c44a51a2390db74.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202104/blobio-imr-202104_7414adb5e32f4606bcc542bd39952239.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202107/blobio-imr-202107_7a476eef813241f5ad07f42fefc78acc.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Product.chunk.a708467d.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-regular-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-medium-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("dns_query", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_7");
    ns_web_url ("collector_7",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dVUtAVFNeUV1cH0FdXltWH19XXB9fR15GW1FdXl1AH0BXVUdeU0AfQVpdQEZBHUIdW0ZfBAAKBFcAVFFRU1BWAA1CW1YPYWBmdGZ0cXBgeXVrZmN3dBReW1YPfmFmYWBmdGZ0cXBgeXVrZmN3dAYFa2J9axRfU0BZV0ZCXlNRVw90fntieXNgZhRBRl1AVw9RXl0XAHRER1MUQUBcXQ9QbQNtARRdRkBTUVlXQA9QQF1FQVcUXUZAU1FZV0ADD1pCbUBbUVptXFNEW1VTRltdXG1ie3x8d3ZtXFddFwB0X1dAUVpTXFZbQVtcVW18c218c2Rtd2pic3xG2c3B+d21cU0RbVVNGW11ccVNAVm1RUW0AbX4DbURbV0UfU15eFFRfD11AVVNcW1EUW1tWD1AFV1Q%5ESHBgoLH1BQAAU%7DDNfBlADBh9QAgQBH1ZXAVYBB1FUKBQYEAhx8hYGZ0ZnRxcGB5DdWtmY3d0HGF3c2BxehRCQkYPUEBdRUFXFENJCXA9QQF1FQVcUQUFbVg8HSABGVF0AWFpRAgICAgICAwQGCgMKCgYEAQECBRAeEGJqAwIa%3ELAAsQDCAIeEGJqAwMDCgQQCBBlW1wBABAeEGJqAw%60KIEAAAQCAIeEGJqAwIABQAQCAADBgQeEGJqAwILBQIQCAEEAgIeEGJqAwICCwYQCAMEBgo%7DDCgoHBwoCAwUeEGJqAwMCAgYQCAMEBgoDCGgoHBwoKCAAMeEGJqAwIAAgQQCBADU1ALCgAKAh9TUQIAHwMDV1EfUAcHVB9XUAQBU1ALUwZTBVQQ7HhBiagM7CAgoKEAhUU15BV09Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=1ab98280-ac02-11ec-b55f-eb63ab9a4a7f&ft=263&seq=0&en=NTA&pc=2880833722070881&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=1",
        BODY_END
    );

    ns_end_transaction("collector_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_11");
    ns_web_url ("fdp_11",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"ez4yhjtfhs0000001648188558595","iid":"couonunlu80000001648188558759","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"OI","fid":"elzbnzdw5c0000001648188558758","oids":"nb:mp:01dc7aa028,nb:mp:11a4040a13,nb:mp:02abde5f08,nb:mp:012d284527,nb:mp:02b5be0710","t":1648188558758}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBHJvbWUDYXBpCGZsaXBrYXJ0A2NvbQAAAQABAAApEAAAAAAAAE4ADABKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Bvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Bvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Becomm_totalvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCWdvb2dsZWFkcwFnC2RvdWJsZWNsaWNrA25ldAAAAQABAAApEAAAAAAAAEgADABEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=productPage&cd%5Bpcat%5D=clothing-and-accessories,bottomwear,shorts,men-s-shorts,gyrfalcon-men-s-shorts&cd%5Bpname%5D=GYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&cd%5Bvalue%5D=403&cd%5Bbrand%5D=GYRFALCON", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=ViewContent&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=SRTFTFCBRKGYTQEF", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/768/922/kc9eufk0/short/s/e/u/xxl-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzzxh7tj7g.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/374/449/kc9eufk0/short/s/e/u/xxl-g-mshort-navy-an-mil-2pcs-gyrfalcon-original-imaftfkzzxh7tj7g.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/374/449/ksm49e80/t-shirt/t/h/w/m-mts-isk-3colorwmn-1pcs-m-iskave-original-imag653zdjnzjhqs.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/kq6yefk0/shoe/p/s/r/10-fashion-star-black-165-beige-10-hotstyle-beige-black-original-imag4992vm7yfvfq.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwhmYWNlYm9vawNjb20AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/k5bcscw0/accessories-combo/s/g/m/sac-skso-32-loopa-original-imafnyy7w9g9zgzu.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/kxnl6kw0/t-shirt/a/2/v/m-sbs-try-this-original-imaga299mymbzqvd.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/kxxl9jk0/sweatshirt/g/b/o/xxl-fc6039-fastcolors-original-imagaa73g4zrpyvx.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_12");
    ns_web_url ("fdp_12",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"ez4yhjtfhs0000001648188558595","iid":"couonunlu80000001648188558759","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"ITI","fid":"vc4ne8z4740000001648188558764","iid":"blobio-imr-202106_9dc9c5e800c941849bce41f6c3088db0.jpg","pid":"SRTFTFCBRKGYTQEF","pos":1,"t":1648188558764},{"en":"ITI","fid":"n09mbyzi680000001648188558764","iid":"blobio-imr-202104_03cf9fdb9c75425c9c44a51a2390db74.jpg","pid":"SRTFTFCBRKGYTQEF","pos":2,"t":1648188558764},{"en":"ITI","fid":"gbtafipyqo0000001648188558764","iid":"blobio-imr-202104_7414adb5e32f4606bcc542bd39952239.jpg","pid":"SRTFTFCBRKGYTQEF","pos":3,"t":1648188558764},{"en":"ITI","fid":"knbm6u9rkw0000001648188558764","iid":"blobio-imr-202107_7a476eef813241f5ad07f42fefc78acc.jpg","pid":"SRTFTFCBRKGYTQEF","pos":4,"t":1648188558764},{"en":"ITI","fid":"oxqlx3bo5c0000001648188558764","iid":"blobio-imr-202106_5c0156da14df4ce8b94f05974bb5b46c.jpg","pid":"SRTFTFCBRKGYTQEF","pos":5,"t":1648188558764},{"en":"ITCI","fid":"mwh6gnswuo0000001648188558764","pid":"SRTFTFCBRKGYTQEF","is":4,"tic":102,"t":1648188558764},{"en":"ITI","fid":"gpu7lrnu0w0000001648188558764","iid":"blobio-imr-202106_9dc9c5e800c941849bce41f6c3088db0.jpg","pid":"SRTFTFCBRKGYTQEF","pos":1,"rid":"c36a656b-f655-4804-8d41-b96ecfc79164","t":1648188558764},{"en":"ITCI","fid":"hbuc0wntzk0000001648188558764","pid":"SRTFTFCBRKGYTQEF","is":1,"rid":"c36a656b-f655-4804-8d41-b96ecfc79164","tic":1,"t":1648188558764},{"en":"RI","fid":"8fb06y00pc0000001648188558764","rid":"c36a656b-f655-4804-8d41-b96ecfc79164","tp":"product","t":1648188558764},{"en":"ITI","fid":"qnah37sda80000001648188558764","iid":"blobio-imr-202104_03cf9fdb9c75425c9c44a51a2390db74.jpg","pid":"SRTFTFCBRKGYTQEF","pos":1,"rid":"ea0d4af1-5f0f-4ac4-9b5a-1e7e6f4fd4dd","t":1648188558764},{"en":"ITI","fid":"gqum96vp000000001648188558764","iid":"blobio-imr-202104_7414adb5e32f4606bcc542bd39952239.jpg","pid":"SRTFTFCBRKGYTQEF","pos":2,"rid":"ea0d4af1-5f0f-4ac4-9b5a-1e7e6f4fd4dd","t":1648188558764},{"en":"ITCI","fid":"kvhq5g2rao0000001648188558764","pid":"SRTFTFCBRKGYTQEF","is":2,"rid":"ea0d4af1-5f0f-4ac4-9b5a-1e7e6f4fd4dd","tic":2,"t":1648188558764},{"en":"RI","fid":"2f6ntvyki80000001648188558764","rid":"ea0d4af1-5f0f-4ac4-9b5a-1e7e6f4fd4dd","tp":"product","t":1648188558764},{"en":"ITI","fid":"cmg6njshow0000001648188558764","iid":"blobio-imr-202107_7a476eef813241f5ad07f42fefc78acc.jpg","pid":"SRTFTFCBRKGYTQEF","pos":1,"rid":"c0bc175a-984b-48eb-826c-6f608e86fd67","t":1648188558764},{"en":"ITCI","fid":"xr1j08idj40000001648188558764","pid":"SRTFTFCBRKGYTQEF","is":1,"rid":"c0bc175a-984b-48eb-826c-6f608e86fd67","tic":1,"t":1648188558764},{"en":"RI","fid":"e5ua0cim2o0000001648188558764","rid":"c0bc175a-984b-48eb-826c-6f608e86fd67","tp":"product","t":1648188558764}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Bvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&is_vtc=1&random=569846369", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Bvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&is_vtc=1&random=50574018", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Becomm_totalvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&is_vtc=1&random=2370989694", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_8");
    ns_web_url ("collector_8",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_8_url_0_1_1648188771586.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Becomm_totalvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&is_vtc=1&random=2370989694&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Bvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&is_vtc=1&random=50574018&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DSRTFTFCBRKGYTQEF%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cshorts%2Cmen-s-shorts%2Cgyrfalcon-men-s-shorts%3Bbrand%3DGYRFALCON%3Bvalue%3D403%3Bpname%3DGYRFALCON%20Solid%20Men%20Multicolor%20Regular%20Shorts&is_vtc=1&random=569846369&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/lockin/671/160/images/CCO__PP_2019-07-14.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_9");
    ns_web_url ("collector_9",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKBgcQHhBWEAhJEGJqAwIKAQIQCAMCCwseEGJqAwMDBgMQCAAKHhBiagMCBQIHEAgQZktCV3dAQF1ACBJxU1xcXUYSQFdTVhJCQF1CV0BGSxIVAhUSXVQSXEdeXm5cEhISElNGEmFGEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAwEGCwsbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFNdEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAEIAAIGCgUbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFYSGlpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHUBTRFdcHAEcAAAcARxYQQgACAYGBAcbEB4QYmoDAwIABRAIEF9dR0FXXURXQBAeEGJqAwIGAwQQCAMLBAUeEGJqAwIFAgoQCBBGQEdXEB4QYmoDAwMKCxAIRkBHVx4QYmoDAgEEBRAIEBFRXVxGU1tcV0AMdntkCFxGWh9RWlteVhoDGwx2e2QIXEZaH1FaW15WGgGMbDHZ7ZAhcRlofUVpbXlYaAxsMdntkCFxGWh9RWlteVhoBGxAeEGJqAwIEAAAQCAEeEGJqAwIABQAQCAYDAwIeEGJqAwMCAgYQCAMEBgoDCgoHBAIEAwceEGJqAwIAAgQQC%5ESBADU1ALCgAKAh9TUQIAHwM%3AENDV1EfUAcHVB9XUAQBU1ALUwZTBVQQHhBiagMCAgoKEAhUOU15BVx4QYmo%3EDAgEEAhAIEFpGRkJBCB0dRUEVFHFReW0JZU0BGHFFdXx1VS0BUU15RXVwfQV1eW1YfX1dcH19HXkZbUV1eXNUAfQFdVR15TQB9BWl1ARkEdQh1bRl8EAAoEVwBUUVFTUFYADUJbVg9hYGZ0ZnRxcGB5dWtmY3d0FF5bVg9+YWZhYGZ0ZnRxcGB5dWtmY3d0BgVrYn1rFF9T_%3BQFlXRkJeU1EFXD3R+e2J5c2BmFEFGXUBXD1FeXRcAdERHUxRBQFxdD1BtA20BFF1GQFNRWNOVdAD1BAXUVBVxRdRkBTUVlXQAMPWkJtQFtRWm1cU0RbVVNGW11cbWJ7fHx3dm1cV10XAHRfV0BRWlNcVltBW1xVbXxzbXxzZG13amJzfHZzcH53bVxTRFt%3AVU0ZbXVxxU0BWbVFRbQBtfgNtRFtXRR9TXl4UVF8PXUBVU1xbURRbW1YPUAGVXVAcGCgsfU%5BFAABR8GUAMGH1ACBAEfVlcBVgEHUVQFBgQCHGFgZnRmdHFwYHl1a2Zjd3QcYXdzYHF6FEJCRg9QQF1FQVcUQkJcD1BAXUVBVxRBQVtWDwdIAEZUXQBYWlEC7AgICAgIDBAY7KAwoKBgQBAQIFEE9Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=1ab98280-ac02-11ec-b55f-eb63ab9a4a7f&ft=263&seq=2&en=NTA&cs=535ae0ae69b7109a14e18bdd9222ce33a3bdc357f06f7c79d668b5aa500553f8&pc=0430571718209711&sid=1acd78a4-ac02-11ec-86ea-616b6c735963%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B5%F3%A0%84%B5%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B4%F3%A0%84%B1&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=3",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;S;s_sq;SN;_pxff_ddtc;_px3", END_INLINE
    );

    ns_end_transaction("collector_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_10");
    ns_web_url ("collector_10",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_10_url_0_1_1648188771601.body",
        BODY_END
    );

    ns_end_transaction("collector_10", NS_AUTO_STATUS);
    ns_page_think_time(0.083);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("login_img_c4a81e_png");
    ns_web_url ("login_img_c4a81e_png",
        "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/login_img_c4a81e.png",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("login_img_c4a81e_png", NS_AUTO_STATUS);
    ns_page_think_time(18.947);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("authenticate");
    ns_web_url ("authenticate",
        "URL=https://1.rome.api.flipkart.com/api/4/user/authenticate",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("authenticate", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("authenticate_2");
    ns_web_url ("authenticate_2",
        "URL=https://1.rome.api.flipkart.com/api/4/user/authenticate",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;S;SN;_px3;s_sq",
        BODY_BEGIN,
            "{"loginId":"+917683831142","password":"537262"}",
        BODY_END
    );

    ns_end_transaction("authenticate_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_13");
    ns_web_url ("fdp_13",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":null,"ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":400,"iss":false,"lid":"+917683831142","fid":"WEBSITE-LOGIN","t":1648188587891}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_13", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("status_3");
    ns_web_url ("status_3",
        "URL=https://1.rome.api.flipkart.com/api/6/user/signup/status",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;S;s_sq",
        BODY_BEGIN,
            "{"loginId":["+917683831142"],"supportAllStates":true}",
        BODY_END
    );

    ns_end_transaction("status_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_14");
    ns_web_url ("fdp_14",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":null,"ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":null,"iss":true,"lid":["+917683831142"],"fid":"WEBSITE-LOGIN","t":1648188594808}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_14", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("generate_4");
    ns_web_url ("generate_4",
        "URL=https://1.rome.api.flipkart.com/api/7/user/otp/generate",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"loginId":"+917683831142"}",
        BODY_END
    );

    ns_end_transaction("generate_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_15");
    ns_web_url ("fdp_15",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":null,"ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":null,"iss":true,"lid":"+917683831142","fid":"WEBSITE-LOGIN","t":1648188595224}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_15", NS_AUTO_STATUS);
    ns_page_think_time(10.612);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("otp");
    ns_web_url ("otp",
        "URL=https://1.rome.api.flipkart.com/api/1/user/login/otp",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("otp", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("otp_2");
    ns_web_url ("otp_2",
        "URL=https://1.rome.api.flipkart.com/api/1/user/login/otp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"userId":"+917683831142","requestId":"B57821A4F5A14CC583E2299D9163B0762","otp":"846564"}",
        BODY_END
    );

    ns_end_transaction("otp_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_16");
    ns_web_url ("fdp_16",
        "URL=https://1.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":null,"ppt":"hp","ppn":"homepage","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"b7ef5489-bb27-4b14-b063-de3d35cf7460.SRTFTFCBRKGYTQEF.SEARCH","fm":"organic","sqid":"jd30bwq6v40000001648188463307"},"e":[{"en":"LRE","erc":null,"iss":true,"lid":"7683831142","fid":"WEBSITE-LOGIN","t":1648188606318}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_16", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("beacon");
    ns_web_url ("beacon",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector/beacon",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("beacon", NS_AUTO_STATUS);
    ns_page_think_time(0.2);

    ns_start_transaction("pr");
    ns_web_url ("pr",
        "URL=https://www.flipkart.com/clothing-and-accessories/bottomwear/pr?sid=clo,vua&p[]=facets.ideal_for%255B%255D%3DMen&p[]=facets.ideal_for%255B%255D%3Dmen&otracker=categorytree&fm=neo%2Fmerchandising&iid=M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.8HARX8UX7IX5&otracker=hp_rich_navigation_2_2.navigationCard.RICH_NAVIGATION_Fashion~Men%2527s%2BBottom%2BWear_8HARX8UX7IX5&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&cid=8HARX8UX7IX5",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;_px3;s_sq;S;SN",
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Browse.chunk.4eeda8.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fk-logo_f64bb3.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Browse.chunk.15210220.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("pr", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_11");
    ns_web_url ("collector_11",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dUV5dRlpbXFUfU1xWH1NRUVdBQV1AW1dBHVBdRkZdX0VXU0AdQkANQVtWD1FeXR5ER1MUQmlvD1RTUVdGQRxbVldTXm1UXUAXAAcHcBcABwd2FwF2f1dcFEJpbw9UU1FXRkEcW1ZXU15tVF1AFwAHB3AXAAcHdhcBdl9XXBRdRkBTUVlXQA9RU0ZXVV1AS0ZAV1cUVF8PXFddFwB0X1dAUVpTXFZbQVtcVRRbW1YPf20KUAdRUQMCAh8CA1EBHwYGAgAfUARXVh8CBQMLA1YBUFEGVlBtAG0BBQBndgdwanZ0a2Ftf3EcCnpzYGoKZ2oFe2oHFF1GQFNRWVdAD1pCbUBbUVptXFNGEW1VTRltdXG0Ab%5EQAcXFNEW1VTRltdXHFTQFYcYHtxem18c2R7dXNme318bXRTQVpbXVxMf1dcFwAHAAVBFwBwcF1GSRl1fFwBwZVdTQG0KenNgag%7DDNK8pnagV7agcUXUZAU1FZV0ADD1pCDbUBbUVptXFNEW1VTRltdXG1ie3x8d3ZtXFddFwB0X1dAUVpTXFZbQVtcVW18Nc218c2Rtd2pic3x2c3B+d21cU0RbVVNGW11ccVNAVm1RUW0AbX4DbURbV0UfU15eFFFbVg8KenNagagpnag%3EV7agcQHhBiagMCDCwALEAgCHhBiagM%60DAwoEEKAgQZVtcAQAQHhBiagMCBAAAEAgCHhBiagMCAAUAEAgAAgYCHhBiagMCCwUCEAgBBAICHhBiagMCAgsGEAgDBAYKAwoKBAIKBgAB%7DHhBiagMDAgIGEAGKgDBAYKAwoKBAIKBgAEHhBiagMCAAIEEAgQAQpRBVEDAwIfU1ECAB8DA1dRH1BQBgEfBAUAC1MGAgpTAgZU7EB4QYmo7DAgIKChAIVFNeQVdPT28%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=38c7c110-ac02-11ec-bb43-6729a408a04f&ft=263&seq=0&en=NTA&pc=4668054777897109&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=1",
        BODY_END
    );

    ns_end_transaction("collector_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("eventBatch_3");
    ns_web_url ("eventBatch_3",
        "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"events":[{"adunits":[{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS0Dh0iU3EDeJo02o7ZNPrBvi3w6QA1zz9TMhdpB2r9Ypw==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS3Ljx6w1G2s46fvEbn9DdhxsxOue27OID9WHduLDMbZaQ==","bannerId":"215899510"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS2Qlhqrq61QxbOjuGj9EdIpuzAn1p90HxbUvJbMyitppA==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS24+PQPorNsik4rVHuekjPJdCvKy7BoEOXU9APJB38T0w==","bannerId":"215551226"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS1ZBVG5EwuySfWGckdPZCOYJQhjOhLbTUrYPjbe1yyfQg==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS13Nbk6ItfUViJQMKLyeXH8YxsQO2Jb8OwmtZxJjQxtGg==","bannerId":"220569896"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS0o8ADbsMAujMgx1tjNCFlxUJvfMxNB0ubdf/Hz5J0yMw==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS2U6ktRQizDkkutZeFfpTjSauU3KTt4vGGmOKlklOHnOQ==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS0i96X8/0HESCU6Fwql1agPlmP0FLNBNcNgQtlDdHpAKQ==","bannerId":"215899498"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS32LGHTAptdJJpT0Q8T5xxoAu4vealLeY0sFRd6dIGWFQ==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS2RQJb2Omw5INO7gzn0sZuBcLEuvo3fXJgv5dM0gNmOIA==","bannerId":"-1"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS2jyX55dFNAQ3hzf2yy2zwUjLlSobtPBeAgwDOqJZuqUQ==","bannerId":"218187060"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS0P1ABnIZF9IVi/5JoliQ/0UD5spI3AMFqc3emcBCVb2A==","bannerId":"218483740"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS2O574ECjdfnKxG09wenP7Q93J+dORJCp6E9/MoR0CXOQ==","bannerId":"217617519"},{"impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS2OIzAksDS7u0QY8jkrAtVCa7VMiiGQWObuxRyX0Y2vaw==","bannerId":"-1"}],"event":"adBeacon","dontTrack":false,"eventTime":1648188608989,"responseId":"bcdc8e78-74d9-4648-84fb-07767b41231c","id":"bcdc8e78-74d9-4648-84fb-07767b41231c"}],"eventDispatchTime":1648188608989,"dontTrack":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBnBsYS10awhmbGlwa2FydANuZXQAAAEAAQAAKRAAAAAAAABQAAwATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("eventBatch_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_17");
    ns_web_url ("fdp_17",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"9ae6739fxc0000001648188608994","fm":"organic"},"e":[{"en":"PV","ib":false,"id":false,"cat":"MensClothingFormalSmartwear","t":1648188608994}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dxBnb29nbGVhZHNlcnZpY2VzA2NvbQAAAQABAAApEAAAAAAAAEsADABHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DTROG7XEBG6EJHFYF%2CTKPG9AGPYNVWEH9N%2CSRTFTFCBRKGYTQEF%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=browsePage&cd%5Bpcat%5D=clothing-and-accessories,bottomwear&cd%5Bbrand%5D=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kxp0mfk0/trouser/t/c/n/32-kpb-51-killer-original-imaga3hnregbqshj.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kwnv6a80/track-pant/n/y/a/l-av202-avolt-original-imag9agb8vpgyxyy.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kxqg2a80/track-pant/u/0/s/m-solid-men-rama-track-pants-foxter-original-imaga4hhhnfskakh.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kfoapow0-0/jean/i/y/9/36-mss20jn016a-metronaut-original-imafw2g4sfx2rhpg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k12go7k0/jean/j/z/h/32-maw19jn139b-metronaut-original-imafkpgymsykgjfr.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/l0mr7gw0/track-pant/g/f/n/s-combo-tr1-vebnor-original-imagcdcdwqwfrpzh.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k0wqwsw0/jean/u/m/x/32-10064521-roadster-original-imafkhyfdgbggm6f.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ku5ufm80/short/m/7/8/m-dwbshtovg-damensch-original-imag7c8wq3bwn9ze.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kwtkxow0/jean/r/z/c/32-maw21elppjn570-metronaut-original-imag9ersm3htwhhx.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k52s58w0/track-pant/m/a/z/m-1jg-jog-cargo-og-jugular-original-imafnuhcx7mygufs.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ko7idu80/jean/c/0/r/38-dis-heavy-black-01-urbano-fashion-original-imag2q2tqxfxk5kd.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ksez24w0/short/b/t/n/32-kj-sh-9004-killer-original-imag5zpzn36ptyyr.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kly2aa80/trouser/f/1/m/34-kctr-2121-olive-fubar-original-imagyysvgw8pfe3e.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ki7qw7k0/track-pant/q/v/k/xl-tbl-rdjog-j22-tripr-original-imafyfryfnvqdpp6.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=551938319", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=3203203849", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kwnv6a80/track-pant/c/z/x/l-av202-avolt-original-imag9agbgzx6hasg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DTROG7XEBG6EJHFYF%2CTKPG9AGPYNVWEH9N%2CSRTFTFCBRKGYTQEF%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=2157765467", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kzblocw0/track-pant/h/c/j/xxl-mens-track-vebnor-original-imagbcq3krnrgj8r.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/krkyt8w0/track-pant/b/z/s/m-kmtbnz9-black-numero-uno-original-imag5cfbbngjbfgy.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/knan98w0/track-pant/w/v/4/m-solid-navy-black-jogger-combo-trackpants-kizaar-original-imag2yevpadumncd.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kjem3rk0-0/track-pant/q/q/f/l-fc2006-fastcolors-original-imafyzk7x5teggej.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kylvr0w0/jean/i/b/f/32-9271-slm-killer-original-imagasv6f8t4zffz.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&ocp_id=wVw9Yuu0D4m_rtoPg_mlsAY&random=1239158495&sscte=1&crd=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/ksqeky80/jean/y/c/7/32-maw21jn167-metronaut-original-imag68gyzywkhcpu.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kxnl6kw0/jean/9/l/4/32-pjdnpskfl00514-peter-england-original-imaga27ymt4vbyqb.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kwjkuq80/track-pant/x/4/r/3xl-bblwtrdjog-p9-blive-original-imag96vwce6df4ea.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kzd147k0/track-pant/s/t/1/m-av104c-nstripe-blk-navy-avolt-original-imagbeagjgzfwjgt.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=551938319&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=3203203849&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DTROG7XEBG6EJHFYF%2CTKPG9AGPYNVWEH9N%2CSRTFTFCBRKGYTQEF%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%3Bbrand%3D&is_vtc=1&random=2157765467&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwZnb29nbGUCY28CaW4AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/k7usyvk0/track-pant/g/j/r/xxl-cool-ts134-blu-tt-original-imafqybahx69mzhu.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kzygpzk0/track-pant/m/i/a/m-tr1-vebnor-original-imagbugahbmsvsss.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kxhvf680/jean/6/l/b/32-pjdnpstfa21517-peter-england-original-imag9xurzfvnsuwu.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/klgx0280/short/0/4/i/m-dv-short11-vero-lie-original-imagyhbzyhghf6pz.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=1239158495&crd=&is_vtc=1&random=249436226", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/jhs0o7k0/track-pant/g/m/g/l-jog18-fn05-org-alan-jones-original-imaf5q8qpe6screz.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/580/696/kxqg2a80/jean/p/r/i/32-kjb-1662-killer-original-imaga4htreudnsxg.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/100/100/promos/18/07/2019/4aebbd99-7478-411e-aced-265e7722d18d.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/100/100/promos/08/07/2019/7e4ee41e-ce86-43e0-834e-485eaf5a3cfb.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=1239158495&crd=&is_vtc=1&random=249436226&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.flipkart.com/osdd.xml?v=2", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;S", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/promos/new/20150528-140547-favicon-retina.ico", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_17", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_12");
    ns_web_url ("collector_12",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_12_url_0_1_1648188771654.body",
        BODY_END
    );

    ns_end_transaction("collector_12", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("swatch_5");
    ns_web_url ("swatch_5",
        "URL=https://rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("swatch_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_13");
    ns_web_url ("collector_13",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKBgcQHhBWEAhJEGJqAwIKAQIQCAsCAR4QYmoDAwMGAxAIBgcGHhBiagMCBQIHEAgQZktCV3dAQF1ACBJxU1xcXUYSQFdTVhJCQF1CV0BGSxIVAhUSXVQSXEdeXm5cEhISElNGEmFGEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAwEGCwsbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFNdEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAEIAAIGCgUbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFYSGlpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHUBTRFdcHAEcAAAcARxYQQgACAYGBAcbEB4QYmoDAwIABRAIEF9dR0FXXURXQBAeEGJqAwIGAwQQCAABAQEeEGJqAwIFAgoQCBBGQEdXEB4QYmoDAwMKCxAIRkBHVx4QYmoDAgEEBRAIEHZ7ZAhcRlofUVpbXlYaARsMdntkCFxGWh9RWlteVhoDGwx2e2QIXEZaH1FaW15WGgAbDHZ7ZAhcRlofUVGpbXlYaABsMdntkCFxGWh9RWlt%5EeVhoDGwx2e2QIXEZaH1FaW15WGgEbEB4QYmoDAgQAABAIAR4QYmoDAgAFABAIBgEFBx4QYmoDAwICBhAIAwQGCgMKCgQDAgsHBh4QYmoDAgACBBAIEAEKUQVRAwMCH1SNRAgAfAwNXUR9QUAYBHwQFAAtTBgIKUwIGVBAe%3AENO%3EEGJqAwICCgoQCFRTXkFXHhBiagMCAQQCEAgQWkZGQkEIHRE1FRUUcVF5bQllTQEYcUV1fHVFeXUZaW1xVH1NcVh9TUVFXQUFdQFtXQR1QXUZGXV9FV1NAHUJADUFbVg9RXl0eREdTFEJpbw9UU1FXNRkEcW1ZXU15tVF1AFwAHB3AXAAcHdhcBdn9XXBRCaW8PVFNRV0ZBHFtWV1NebVRdQBcABwdwFwAHB3YXAXZfV1wUXUZAU1FZV0APUVNGV1VdQEtGQFdXFFRfD1xXXRcSAdF9XQFFaU1x%7DWW0FbXFUUW1tWD39tClAHUVEDEAgIfAgNRAR8GBgIAH1AEV1YfANgUDCwNWAVBRB_lZQbQBtAQUAZ3YHcGp2dGthbX9xHAp6c2BqCmdqBXtqBxRdRkBTUVlXQA9aQm1AW1FabVxTRFtVU0ZbXVxtAG0AHFxTRFtVU0ZbXVxxU0BWHGB7cXptfHNke3VzZnt9fG10U0FaW11cTH9XXBcABwAFQRcAcHBdRkZdXxc%3AAcGVXU0BtCnpzYGoKZ2oFe2oD%7DHFF1GQFNRWVdAAw9aQm1AW1FabVxTRFtVU0ZbXVxtYnt8fHd2bVxXXRcAdF9XQFFaU1xWW0FbXFVtfHNtfHNkbXdqYnN8dnNwfndtXFNEW1VTRltdXHFTQFZtUVFtAG1+A21EW1dFH1N7eXhRRW1YPCnp7zYGoKZ2oFe2oHEE9Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=38c7c110-ac02-11ec-bb43-6729a408a04f&ft=263&seq=2&en=NTA&cs=5a4206876922c4980933b075833e472c2bc240660d4ee1b2295eee0c824ea3d8&pc=0883481103490807&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B6%F3%A0%84%B0%F3%A0%84%B8%F3%A0%84%B5%F3%A0%84%B4%F3%A0%84%B7&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=3",
        BODY_END
    );

    ns_end_transaction("collector_13", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_6");
    ns_web_url ("swatch_6",
        "URL=https://rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;S;SN;_px3",
        BODY_BEGIN,
            "{"pidLidMap":{"TKPG9AGPYNVWEH9N":"LSTTKPG9AGPYNVWEH9NVEGFSN"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END
    );

    ns_end_transaction("swatch_6", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("swatch_7");
    ns_web_url ("swatch_7",
        "URL=https://2.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABATIEcm9tZQNhcGkIZmxpcGthcnQDY29tAAABAAEAACkQAAAAAAAATAAMAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("swatch_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_8");
    ns_web_url ("swatch_8",
        "URL=https://2.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;S;SN;_px3",
        BODY_BEGIN,
            "{"pidLidMap":{"TKPG9AGPYNVWEH9N":"LSTTKPG9AGPYNVWEH9NVEGFSN"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;s_sq;S;SN;_px3", END_INLINE
    );

    ns_end_transaction("swatch_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_9");
    ns_web_url ("swatch_9",
        "URL=https://2.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;SN;_px3;S",
        BODY_BEGIN,
            "{"pidLidMap":{"JEAFKH589YBU4GYH":"LSTJEAFKH589YBU4GYH292VLM"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END
    );

    ns_end_transaction("swatch_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_10");
    ns_web_url ("swatch_10",
        "URL=https://2.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;SN;_px3;S",
        BODY_BEGIN,
            "{"pidLidMap":{"JEAFGBPWMNBDDXGU":"LSTJEAFGBPWMNBDDXGUMYKKHU"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END
    );

    ns_end_transaction("swatch_10", NS_AUTO_STATUS);
    ns_page_think_time(0.522);

    //Page Auto split for Image Link '' Clicked by User
    ns_start_transaction("fdp_18");
    ns_web_url ("fdp_18",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fdp_18", NS_AUTO_STATUS);
    ns_page_think_time(0.544);

    ns_start_transaction("itmfgps8dzzzu8ft");
    ns_web_url ("itmfgps8dzzzu8ft",
        "URL=https://www.flipkart.com/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft?pid=JEAFGBPWMNBDDXGU&lid=LSTJEAFGBPWMNBDDXGUMYKKHU&marketplace=FLIPKART&store=clo%2Fvua&srno=b_1_13&otracker=browse&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&iid=bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH&ssid=9c1lkfudog0000001648188608609",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;s_sq;SN;_px3;S"
    );

    ns_end_transaction("itmfgps8dzzzu8ft", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_19");
    ns_web_url ("fdp_19",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;SN;_px3;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"9ae6739fxc0000001648188608994","fm":"organic"},"e":[{"en":"EECE","lu":"https://www.flipkart.com/clothing-and-accessories/bottomwear/pr?sid=clo,vua&p[]=facets.ideal_for%255B%255D%3DMen&p[]=facets.ideal_for%255B%255D%3Dmen&otracker=categorytree&fm=neo%2Fmerchandising&iid=M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.8HARX8UX7IX5&otracker=hp_rich_navigation_2_2.navigationCard.RICH_NAVIGATION_Fashion~Men%2527s%2BBottom%2BWear_8HARX8UX7IX5&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&cid=8HARX8UX7IX5","lpn":"Store","ru":"https://www.flipkart.com/clothing-and-accessories/bottomwear/pr?sid=clo,vua&p[]=facets.ideal_for%255B%255D%3DMen&p[]=facets.ideal_for%255B%255D%3Dmen&otracker=categorytree&fm=neo%2Fmerchandising&iid=M_8b5cc100-01c3-4402-b6ed-07191d3bc4db_2_372UD5BXDFYS_MC.8HARX8UX7IX5&otracker=hp_rich_navigation_2_2.navigationCard.RICH_NAVIGATION_Fashion~Men%2527s%2BBottom%2BWear_8HARX8UX7IX5&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_2_L1_view-all&cid=8HARX8UX7IX5","t":1648188609005},{"en":"AppEvents","ev":[{"name":"FCP","value":1620.3950000926852}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Browse","pageUri":"/clothing-and-accessories/bottomwear/pr"},"t":1648188609696},{"en":"AppEvents","ev":[{"name":"TTFB","value":1187.7500000409782}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Browse","pageUri":"/clothing-and-accessories/bottomwear/pr"},"t":1648188610519},{"en":"AppEvents","ev":[{"name":"LCP","value":2779.94}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Browse","pageUri":"/clothing-and-accessories/bottomwear/pr"},"t":1648188612100},{"en":"DCC","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","ct":"productCard","p":"13","t":1648188613536}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app_modules.chunk.94b5e7.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app.chunk.9adf7d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Product.chunk.097bc0.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/runtime.95858ad1.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/vendor.chunk.bf58a224.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_modules.chunk.ac48891f.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_common.chunk.db774788.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app.chunk.b7fa5477.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/jvwpfgw0/jean/a/j/z/28-2312011-roadster-original-imafgps9snhd5fqz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgps92qgvw2u9.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/jvwpfgw0/jean/a/j/z/34-2312011-roadster-original-imafgpsagfsfmhcg.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/jvwpfgw0/jean/a/j/z/34-2312011-roadster-original-imafgpsbrzcnnkqc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/info-basic_6c1a38.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_69e7ec.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/lockin/100/24/images/CCO__PP_2019-07-14.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/112/112/promos/06/04/2020/9c275f1c-a64d-4858-ab3a-27302fbdcbe5.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/100/10/promos/01/12/2018/8aa01ab4-de88-4a46-9d93-5c7f3ebac2df.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_19", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("eventBatch_4");
    ns_web_url ("eventBatch_4",
        "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"events":[{"duration":1402,"maxViewPercentage":100,"viewStartTime":1648188610222,"eventTime":1648188613667,"dontTrack":false,"event":"adView","adUnit":"SUMMARY","pageView":"GRID_VIEW","bannerId":"-1","impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS0Dh0iU3EDeJo02o7ZNPrBvi3w6QA1zz9TMhdpB2r9Ypw==","listingId":"LSTTROG7XEBG6EJHFYFXKB4C0","id":"bcdc8e78-74d9-4648-84fb-07767b41231c","responseId":"bcdc8e78-74d9-4648-84fb-07767b41231c"},{"duration":1402,"maxViewPercentage":100,"viewStartTime":1648188610223,"eventTime":1648188613668,"dontTrack":false,"event":"adView","adUnit":"SUMMARY","pageView":"GRID_VIEW","bannerId":"215899510","impressionId":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS3Ljx6w1G2s46fvEbn9DdhxsxOue27OID9WHduLDMbZaQ==","listingId":"LSTTKPG9AGPYNVWEH9NVEGFSN","id":"bcdc8e78-74d9-4648-84fb-07767b41231c","responseId":"bcdc8e78-74d9-4648-84fb-07767b41231c"}],"eventDispatchTime":1648188615673,"dontTrack":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/NavMenu.chunk.05b448.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/NavMenu.chunk.7e7b9f66.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/raven.3.22.3.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/batman-returns/omni/omni16.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/px/gNtTli3A/init.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Product.chunk.a708467d.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-regular-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-medium-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("eventBatch_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_14");
    ns_web_url ("collector_14",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dQF1TVkFGV0AfQV5bXx9fV1wfVlNAWR9QXkdXH1hXU1xBHUIdW0ZfVFVCQQpWSEhIRwpURg1CW1YPeHdzdHVwYmV%2FfHB2dmp1ZxReW1YPfmFmeHdzdHVwYmV%2FfHB2dmp1Z39reXl6ZxRfU0BZV0ZCXlNRVw90fntieXNgZhRBRl1AVw9RXl0XAHRER1MUQUBcXQ9QbQNtAwEUXUZAU1FZV0APUEBdRUFXFF1GQFNRWVdAAw9aQm1AW1FabVxTRFtVU0ZbXVxtYnt8fHd2bVxXXRcAdF9XQFFaU1G%5ExWW0FbXFVtfS%7DHNtfHNkbXdqYnN8dnNwfndtXFNEW1VTRltdXHFTQFZtUVFtAG1+A21EW1dFH1NeXhRbW1YPUFFWUQpXBQofBDNKQZWCx8GBAYKHwoGVFAfAgUF8BAVQBgMAAQNRHHh3c3R1cGJlf3xwdnZqdWccYXdzYHF6FEFBW1YDNa%3EPC1EDXllUR1ZdVQICAgICAgMEBgoDCgoEAgoEAgsQHhBiagMCDCwALEAgCHhBi%60agMDAwoEEAgQZVtcAQAQHhBiagMCBAAAEAgCHhKBiagMCAAUAEAgABAECHhBiagMC%7DCwUCEAgBBAICHhBiagMCAgsGEAgDBAYKAwoKBAMEAAMDHhBiagMDAgIGEAGgDBAYKAwoKBAKMEAAMGHhBiagMCAAIEEAgQAVYEUFQGUQIfU1ECAB8DA1dRHwtUV1YfVAsBCgpRAFRRCgtUE7B4QYmo7DAgIKChAIVFNeQVdPT28%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=3d6bf4c0-ac02-11ec-9fed-f9388c2fc89f&ft=263&seq=0&en=NTA&pc=9042159297921980&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=1",
        BODY_END
    );

    ns_end_transaction("collector_14", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_20");
    ns_web_url ("fdp_20",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;_px3;S;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"15vtdfyvgg0000001648188616379","iid":"s02tu7ix400000001648188616576","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"OI","fid":"axousgichc0000001648188616576","oids":"PRICING,nb:mp:012d284527,nb:mp:02b5be0710,nb:mp:11a4040a13,nb:mp:014681cd26","t":1648188616576}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Becomm_totalvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=productPage&cd%5Bpcat%5D=clothing-and-accessories,bottomwear,jeans,men-s-jeans,roadster-men-s-jeans&cd%5Bpname%5D=Roadster%20Slim%20Men%20Dark%20Blue%20Jeans&cd%5Bvalue%5D=599&cd%5Bbrand%5D=Roadster", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=ViewContent&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=JEAFGBPWMNBDDXGU", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/768/922/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/374/449/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/374/449/ky7lci80/shirt/c/v/p/m-praw21csh048a-provogue-original-imagahwg7rqumy4h.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/374/449/krayqa80/t-shirt/z/t/a/m-m7ss21pn028a-m7-by-metronaut-original-imag54k2pv8cvzxj.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/kza68i80/shoe/u/j/t/7-a-305-7-srk-black-original-imagbbhkywgmxrec.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/k6b2snk0/cardigan/h/4/g/xl-cd2-seven-rocks-original-imafzshzyusgsgez.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/k186fm80/blazer/u/d/r/xxl-coblz-bl-avaeta-clothing-original-imafkukgnsdg4rff.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=1712705670", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=244087626", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Becomm_totalvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=608623919", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/k2gh30w0/sunglass/k/p/y/medium-waf2019-03-elligator-original-imafhry2uhysjbhd.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=1712705670&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Becomm_totalvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=608623919&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWMNBDDXGU%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=244087626&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_20", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_15");
    ns_web_url ("collector_15",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_15_url_0_1_1648188771688.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;s_sq;S;SN;_px3", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/100/100/promos/18/07/2019/4aebbd99-7478-411e-aced-265e7722d18d.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_15", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_16");
    ns_web_url ("collector_16",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKBgcQHhBWEAhJEGJqAwIKAQIQCAcDBB4QYmoDAwMGAxAIAQcKHhBiagMCBQIHEAgQZktCV3dAQF1ACBJxU1xcXUYSQFdTVhJCQF1CV0BGSxIVAhUSXVQSXEdeXm5cEhISElNGEmFGEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAwEGCwsbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFNdEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAEIAAIGCgUbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFYSGlpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHUBTRFdcHAEcAAAcARxYQQgACAYGBAcbEB4QYmoDAwIABRAIEF9dR0FXXURXQBAeEGJqAwIGAwQQCAEDAgMeEGJqAwIFAgoQCBBGQEdXEB4QYmoDAwMKCxAIRkBHVx4QYmoDAgEEBRAIEHZ7ZAhcRlofUG%5EVpbXlYaARsMdntkCFxGWhS%3A9RWlteVhoDGwx2e2QIXEZaH1FaW15WGgAbDHZ7ZAhcRlofUVpbXlYaARsMdntkCFxGWh9RWlteVhoDGwx2e2QQHhBiagMCBAAAEAgBHhBiagMCAAUAEAgHBQEDHhBiagMDAgIGEAgDBAYKAwoKBAMENOLBgYCHhBiagMCAAIEEAgQAVYEUFQGUQIfU1ECAB8DA1%3EdRHwtUV1YfVAsBCgpRAFRRCgtUEB4QYmoDAgIKChAIVFNeQVceEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhENSrxRXV8dQF1TVkFGV0AfQV5bXx9fV1wfVlNAWR9QXkdXH1hXU1xBHUIdW0ZfVFVCQQpWSEhIRwpURg1CW1YPeHdzdHVDwYmV%2FfHB2dmp1ZxReW1YPf%60mFmeHdzdHVwYmV%2FfHB2dmp1Z39reXl6ZxRfU0BZV0ZCXlNRVw90fntieXNgZhRBRl1AVwG9RXl0XAHRER1MUQUBcXQ9QbQNtAwEUXUZAU1FZV0APUEBpdRUFXFF1GQFNRWVdAAw9aQm1AW1FabVxTRFtVU0ZbXVxtYnt8fHd2bVxXXRcAdF9XQFFaU1xWW0FbXFVtfHNtfHNkbXdqYnN8dnNwfndEtXFNEW1VTRltdXHFTQFZtU%5BVFtAG1+A21EW1dFH1NeXhRbW1YPUFFWUQpXBQofBQZWCx8GBAYKHwoGVFAfAgUFBAVQBgMAAQNRHHh3c3R1cGJlf3xwdnZqdWccYXdzYHF6FEFBW1YPC1EDXllUR1Zd7VQICAgICAgM7EBgoDCgoEAgoEAgsQT09v&appId=PXgNtTli3A&tag=v7.6.2&uuid=3d6bf4c0-ac02-11ec-9fed-f9388c2fc89f&ft=263&seq=2&en=NTA&cs=801f611df2c87d2e0f3be0b4c05a33c768a3004ccfeab351ca6ea523f029f5a9&pc=3829391611898797&sid=3d794cbf-ac02-11ec-9a0d-476971794c69%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B6%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B3%F3%A0%84%B3%F3%A0%84%B9&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=3",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/lockin/671/160/images/CCO__PP_2019-07-14.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/2397/169/promos/01/12/2018/8aa01ab4-de88-4a46-9d93-5c7f3ebac2df.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/1844/1844/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_16", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_21");
    ns_web_url ("fdp_21",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;S;SN;_px3",
        BODY_BEGIN,
            "[{"nc":{"ssid":null,"mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic"},"e":[{"en":"DCI","iid":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS0Dh0iU3EDeJo02o7ZNPrBvi3w6QA1zz9TMhdpB2r9Ypw==","ct":"ProductCard","p":"1","t":1648188613668,"st":1648188610222,"f":true,"pv":100},{"en":"DCI","iid":"en_FU8Dj1nqucqv81OQYcAeypYDUjUqAenhjUFoRUwNqS3Ljx6w1G2s46fvEbn9DdhxsxOue27OID9WHduLDMbZaQ==","ct":"ProductCard","p":"2","t":1648188613668,"st":1648188610223,"f":true,"pv":100},{"en":"DCI","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.SRTFTFCBRKGYTQEF.SEARCH","ct":"ProductCard","p":"3","t":1648188613668,"st":1648188610223,"f":true,"pv":100},{"en":"DCI","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.TKPGA4HHYEJQUTHB.SEARCH","ct":"ProductCard","p":"4","t":1648188613668,"st":1648188610223,"f":true,"pv":100},{"en":"AppEvents","ev":[{"name":"FID","value":18.049999955110252}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Browse","pageUri":"/clothing-and-accessories/bottomwear/pr"},"t":1648188614864}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_21", NS_AUTO_STATUS);
    ns_page_think_time(235.732);

    ns_start_transaction("itmfgps8dzzzu8ft_2");
    ns_web_url ("itmfgps8dzzzu8ft_2",
        "URL=https://www.flipkart.com/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft?pid=JEAFGBPWQ4SHJDFH&lid=LSTJEAFGBPWQ4SHJDFHI2SWOQ&marketplace=FLIPKART&sattr[]=size&st=size&otracker=browse"
    );

    ns_end_transaction("itmfgps8dzzzu8ft_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fetch_3");
    ns_web_url ("fetch_3",
        "URL=https://rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fetch_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_4");
    ns_web_url ("fetch_4",
        "URL=https://rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;S;SN;_px3;s_sq",
        BODY_BEGIN,
            "{"pageUri":"/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft?pid=JEAFGBPWQ4SHJDFH&lid=LSTJEAFGBPWQ4SHJDFHI2SWOQ&marketplace=FLIPKART&sattr[]=size&st=size&otracker=browse","pageContext":{"fetchSeoData":true}}",
        BODY_END
    );

    ns_end_transaction("fetch_4", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fetch_5");
    ns_web_url ("fetch_5",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fetch_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_6");
    ns_web_url ("fetch_6",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;S;SN;_px3;s_sq",
        BODY_BEGIN,
            "{"pageUri":"/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft?pid=JEAFGBPWQ4SHJDFH&lid=LSTJEAFGBPWQ4SHJDFHI2SWOQ&marketplace=FLIPKART&sattr[]=size&st=size&otracker=browse","pageContext":{"fetchSeoData":true}}",
        BODY_END
    );

    ns_end_transaction("fetch_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_22");
    ns_web_url ("fdp_22",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic","pn":"pp","pt":"pp","ppt":"None","ppn":"None","mpid":"FLIPKART","ss":"ProductPageContext"},"e":[{"en":"PSI","fid":"b7ca0ca2-5010-47a0-a0a9-6d295f911202.JEAFGBPWMNBDDXGU","t":1648188616589},{"en":"PWI","fid":"b7ca0ca2-5010-47a0-a0a9-6d295f911202.JEAFGBPWMNBDDXGU","t":1648188616589},{"en":"PV","ib":false,"id":false,"cat":"MensClothingJeans","t":1648188616589},{"en":"AppEvents","ev":[{"name":"FCP","value":2259.734999970533},{"name":"TTFB","value":986.9050000561401}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Product","pageUri":"/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft"},"t":1648188617700},{"en":"AppEvents","ev":[{"name":"LCP","value":3427.525}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Product","pageUri":"/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft"},"t":1648188621754},{"en":"AppEvents","ev":[{"name":"FID","value":3.1850000377744436}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Product","pageUri":"/roadster-slim-men-dark-blue-jeans/p/itmfgps8dzzzu8ft"},"t":1648188626605},{"en":"OI","fid":"b7ca0ca2-5010-47a0-a0a9-6d295f911202.JEAFGBPWMNBDDXGU","oids":"PRICING,nb:mp:012d284527,nb:mp:02b5be0710,nb:mp:11a4040a13,nb:mp:014681cd26","t":1648188634931},{"en":"RI","fid":"b7ca0ca2-5010-47a0-a0a9-6d295f911202.JEAFGBPWMNBDDXGU","rid":"c52e4a03-8ca0-4fb6-9b20-43e4903d74d0","tp":"product","t":1648188634935},{"en":"RI","fid":"b7ca0ca2-5010-47a0-a0a9-6d295f911202.JEAFGBPWMNBDDXGU","rid":"6984b50d-1ca7-4dd8-be74-444215a78da1","tp":"product","t":1648188634935},{"en":"RI","fid":"b7ca0ca2-5010-47a0-a0a9-6d295f911202.JEAFGBPWMNBDDXGU","rid":"884e01b6-391c-4e3f-9ae2-ec406ce02699","tp":"product","t":1648188634935}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Becomm_totalvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=productPage&cd%5Bpcat%5D=clothing-and-accessories,bottomwear,jeans,men-s-jeans,roadster-men-s-jeans&cd%5Bpname%5D=Roadster%20Slim%20Men%20Dark%20Blue%20Jeans&cd%5Bvalue%5D=599&cd%5Bbrand%5D=Roadster", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=ViewContent&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=JEAFGBPWQ4SHJDFH", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/374/449/ktuewsw0/t-shirt/k/o/0/m-mss21cn173a-metronaut-original-imag73qzgrsnpspw.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=3206882301", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=1388352237", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=1388352237&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Bvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=3206882301&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Becomm_totalvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=1100552220", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DJEAFGBPWQ4SHJDFH%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bbrand%3DRoadster%3Becomm_totalvalue%3D599%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=1100552220&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/1844/1844/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_22", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_23");
    ns_web_url ("fdp_23",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic","pn":"pp","pt":"pp","ppt":"None","ppn":"None","mpid":"FLIPKART","ss":"ProductPageContext"},"e":[{"en":"PSI","fid":"4a3d1785-a59c-44aa-a68b-5809777b49d7.JEAFGBPWQ4SHJDFH","t":1648188634948},{"en":"PWI","fid":"4a3d1785-a59c-44aa-a68b-5809777b49d7.JEAFGBPWQ4SHJDFH","t":1648188634949},{"en":"PV","ib":true,"id":false,"cat":"MensClothingJeans","t":1648188634949},{"en":"DCI","iid":"lo_cco:96d08c0d-d1dc-6a2d-0044-85b106e27498.ct:COIN_CONVERSION_OVERLAY;","wk":"22.overlayCard.PROMOTIONS_13","ct":"overlayCard","p":"1","t":1648188636372,"st":1648188627122,"f":true,"pv":100},{"en":"DCI","iid":"3ebcb0df-7d05-4e4f-9b05-6f23d51ff323.JEAFGBPWMNBDDXGU.jean","wk":"31_17","ct":"BrandChatIcon:BrandChatDesktop","p":17,"t":1648188636372,"st":1648188620347,"f":true,"pv":100}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_23", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_17");
    ns_web_url ("collector_17",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_17_url_0_1_1648188771706.body",
        BODY_END
    );

    ns_end_transaction("collector_17", NS_AUTO_STATUS);
    ns_page_think_time(1.395);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("cart");
    ns_web_url ("cart",
        "URL=https://2.rome.api.flipkart.com/api/5/cart",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("cart", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_24");
    ns_web_url ("fdp_24",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic","pn":"pp","pt":"pp","ppt":"None","ppn":"None","mpid":"FLIPKART","ss":"ProductPageContext"},"e":[{"en":"ATCC","iid":"4a3d1785-a59c-44aa-a68b-5809777b49d7.JEAFGBPWQ4SHJDFH","lid":"LSTJEAFGBPWQ4SHJDFHI2SWOQ","ct":"PAGE","t":1648188639825}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=cartPage&cd%5Bpcat%5D=clothing-and-accessories,bottomwear,jeans,men-s-jeans,roadster-men-s-jeans&cd%5Bpname%5D=Roadster%20Slim%20Men%20Dark%20Blue%20Jeans&cd%5Bvalue%5D=599&cd%5Bbrand%5D=Roadster", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=AddToCart&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=JEAFGBPWQ4SHJDFH", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dcart%3Becomm_prodid%3DJEAFGBPWQ4SHJDFH%3Becomm_totalvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE
    );

    ns_end_transaction("fdp_24", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cart_2");
    ns_web_url ("cart_2",
        "URL=https://2.rome.api.flipkart.com/api/5/cart",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;S;s_sq",
        BODY_BEGIN,
            "{"cartContext":{"LSTJEAFGBPWQ4SHJDFHI2SWOQ":{"quantity":1}}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dcart%3Becomm_prodid%3DJEAFGBPWQ4SHJDFH%3Becomm_totalvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=2397512296", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=97518367", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=3015245676", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dcart%3Becomm_prodid%3DJEAFGBPWQ4SHJDFH%3Becomm_totalvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=2397512296&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=97518367&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DJEAFGBPWQ4SHJDFH%3Bvalue%3D599%3Bbrand%3DRoadster%3Bpcat%3Dclothing-and-accessories%2Cbottomwear%2Cjeans%2Cmen-s-jeans%2Croadster-men-s-jeans%3Bpname%3DRoadster%20Slim%20Men%20Dark%20Blue%20Jeans&is_vtc=1&random=3015245676&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("cart_2", NS_AUTO_STATUS);
    ns_page_think_time(246.813);

    ns_start_transaction("viewcart");
    ns_web_url ("viewcart",
        "URL=https://www.flipkart.com/viewcart?otracker=PP_GoToCart",
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/CartPage.chunk.fde5bf.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/CartPage.chunk.0581c7d3.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABEXN0YXRpYy1hc3NldHMtd2ViCGZsaXhjYXJ0A2NvbQAAAQABAAApEAAAAAAAAEUADABBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("viewcart", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_7");
    ns_web_url ("fetch_7",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pageUri":"/viewcart?otracker=PP_GoToCart","pageContext":{"fetchSeoData":true}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/shield_b33c0c.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fetch_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_8");
    ns_web_url ("fetch_8",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pageUri":"/viewcart?otracker=PP_GoToCart","pageContext":{"paginatedFetch":true,"pageNumber":2,"fetchAllPages":false,"paginationContextMap":{}}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/224/224/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fetch_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_25");
    ns_web_url ("fdp_25",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic","pn":"CART_PAGE","pt":"dynamic","ppt":"pp","ppn":"pp","mpid":"FLIPKART","ss":"CartPageContext"},"e":[{"en":"VCE","cartId":"ACCB259435309434FA7AA1FFAB626D2C265S:P:A-0001","cartPrice":{"coinPrice":[{"title":"PRICE","type":"PRICE","value":569},{"title":"COIN_PRICE","type":"PRICE","value":30}],"discount":[{"title":"DISCOUNT","type":"DISCOUNT","value":700}],"finalPrice":599,"platformCharges":[],"preBookPrice":null,"price":1299,"totalAmountSaved":700},"dataCenter":"HYD","groceryTabShown":null,"hyperlocalTabShown":null,"marketplaceData":[{"addressConfidence":null,"items":[{"addedTimestamp":1648188639000,"analyticsInfo":{"category":"MensClothingJeans","subCategory":"MensJean","superCategory":"MensClothingJeans","vertical":"MensJean"},"isAvailable":true,"isServiceable":true,"listingId":"LSTJEAFGBPWQ4SHJDFHI2SWOQ","metadata":{"moqValue":1,"nudges":[{"shown":true,"tagName":"Deal of the Day","type":"time based deal","value":0}],"offersApplied":[{"d2rValue":null,"id":"nb:mp:012d284527","thresholdType":null,"thresholdValue":null,"title":"Sign up for Pay Later and get Rs100 Gift Card","type":"DISCOUNT"},{"d2rValue":null,"id":"nb:mp:02b5be0710","thresholdType":null,"thresholdValue":null,"title":"Flipkart Pay Later- iPad and EGV offer","type":"DISCOUNT"}],"offersAvailable":[{"d2rValue":null,"id":null,"thresholdType":null,"thresholdValue":null,"title":"5% Cashback on Flipkart Axis Bank Card","type":"CONDITIONAL"},{"d2rValue":null,"id":null,"thresholdType":null,"thresholdValue":null,"title":"No Cost EMI on Bajaj Finserv EMI Card","type":"CONDITIONAL"}],"priceDetails":{"coinPrice":[{"title":"PRICE","type":"PRICE","value":569},{"title":"COIN_PRICE","type":"PRICE","value":30}],"discount":[{"title":"DISCOUNT","type":"DISCOUNT","value":700}],"finalPrice":599,"platformCharges":[],"preBookPrice":null,"price":1299},"recommendations":null,"serviceabilityDetails":[{"maxSla":1,"minSla":1,"promiseTime":1648319399000,"shippingCharges":0,"slaType":"REGULAR"}]},"parentListingId":null,"productId":"JEAFGBPWQ4SHJDFH","sellerId":"d591418b408940a0","unitsAdded":1,"unserviceabilityReason":null}],"marketplace":"FLIPKART","movD2R":null,"shippingD2RType":null,"shippingD2RValue":null,"zones":null},{"addressConfidence":null,"items":[],"marketplace":"GROCERY","movD2R":null,"shippingD2RType":null,"shippingD2RValue":null,"zones":null}],"navigationContext":null,"offersApplied":null,"offersAvailable":null,"requestId":"9bf97e4f-73f8-4def-83e1-84a8096732fa","timestamp":null,"t":1648188641127},{"en":"PV","ib":true,"id":false,"cat":"MensClothingJeans","t":1648188641139},{"en":"DCI","iid":"f0b0d3a5-0503-4db0-9058-f81a1185f5df.JEAFGBPWQ4SHJDFH.jean","wk":"31_17","ct":"BrandChatIcon:BrandChatDesktop","p":17,"t":1648188641378,"st":1648188635996,"f":true,"pv":100},{"en":"DCI","iid":"R:as;p:JEAFGBPWQ4SHJDFH;l:LSTJEAFGBPWQ4SHJDFHI2SWOQ;pt:pp;uid:48527c47-ac02-11ec-a7db-8bfa007b5285;.SHTG8U223VX4T36W","ct":"productCard","p":"1","t":1648188641378,"st":1648188639129,"f":true,"pv":95},{"en":"DCI","iid":"R:as;p:JEAFGBPWQ4SHJDFH;l:LSTJEAFGBPWQ4SHJDFHI2SWOQ;pt:pp;uid:48527c47-ac02-11ec-a7db-8bfa007b5285;.TSHGY6SNHCBGBQUB","ct":"productCard","p":"2","t":1648188641378,"st":1648188639129,"f":true,"pv":90}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/224/224/kz065jk0/emergency-light/v/t/4/high-quality-60-high-bright-led-light-with-android-charging-original-imagb3v8h8zdm3q3.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/224/224/kcnp8y80/hair-accessory/n/g/u/satin-hair-scrunchies-hair-ties-hair-band-multicolour-2-satin-original-imaftqhggptzjy2u.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_25", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("autosuggest");
    ns_web_url ("autosuggest",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("autosuggest", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_2");
    ns_web_url ("autosuggest_2",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188648389,"searchBrowseHistory":[]}",
        BODY_END
    );

    ns_end_transaction("autosuggest_2", NS_AUTO_STATUS);
    ns_page_think_time(1.971);

    //Page Auto split for 
    ns_start_transaction("cc6daa23_e09c_40d8_a4e1_ead4");
    ns_web_url ("cc6daa23_e09c_40d8_a4e1_ead4",
        "URL=https://rukminim1.flixcart.com/www/100/100/promos/19/07/2018/cc6daa23-e09c-40d8-a4e1-ead447bf80fa.png?q=90",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("cc6daa23_e09c_40d8_a4e1_ead4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_3");
    ns_web_url ("autosuggest_3",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"l","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188651472,"searchBrowseHistory":[]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/100/100/l0y6qa80/kids-lehenga-choli/v/l/u/4-5-years-gajra-the-fashion-prime-original-imagcmgezfqsknry.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/kbqu4cw0/computer/q/x/r/hp-original-imaftyzachgrav8f.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/kp1imq80/computer/u/b/z/na-gaming-laptop-acer-original-imag3d63hk3h8nm2.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/kfeamq80/lehenga-choli/h/g/h/free-3-4-sleeve-ot-flurry-fab-original-imafvv6k8hpcxy6s.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("autosuggest_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_4");
    ns_web_url ("autosuggest_4",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"li","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188651973,"searchBrowseHistory":[]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/100/100/kkec4280/usb-gadget/o/1/s/usb-charging-finger-touch-cigarette-lighter-essentials-4-you-original-imafzrfsjkbznbes.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/kf1fo280/pocket-lighter/v/h/e/knife-lighter-and-harley-davidson-lighter-cigarette-lighter-jet-original-imafvhh9shhvytgf.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/kvjuufk0/lipstick/b/a/b/23-5-insta-beauty-5-in-1-creamy-lipsticks-liquid-matte-mini-original-imag8f8mkgyuvayd.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/kqttg280/pocket-lighter/b/v/4/usb-charging-lighter-nvirav-original-imag4rg8gyqmuxzu.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("autosuggest_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_5");
    ns_web_url ("autosuggest_5",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"lip","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188652250,"searchBrowseHistory":[]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/100/100/kebpqq80/lip-balm/k/s/u/31-2-baby-lips-cherry-crush-baby-lips-berry-crush-maybelline-new-original-imafvf46hwkb3mz3.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("autosuggest_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_6");
    ns_web_url ("autosuggest_6",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"lips","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188652606,"searchBrowseHistory":[]}",
        BODY_END
    );

    ns_end_transaction("autosuggest_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_7");
    ns_web_url ("autosuggest_7",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"lipst","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188652918,"searchBrowseHistory":[]}",
        BODY_END
    );

    ns_end_transaction("autosuggest_7", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_8");
    ns_web_url ("autosuggest_8",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"lipstic","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188653642,"searchBrowseHistory":[]}",
        BODY_END
    );

    ns_end_transaction("autosuggest_8", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("autosuggest_9");
    ns_web_url ("autosuggest_9",
        "URL=https://2.rome.api.flipkart.com/api/4/discover/autosuggest",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"query":"lipstick","contextUri":"/viewcart?otracker=PP_GoToCart","marketPlaceId":"FLIPKART","types":["QUERY","QUERY_STORE","PRODUCT","RICH","PARTITION"],"rows":10,"zeroPrefixHistory":false,"userTimeStamp":1648188653924,"searchBrowseHistory":[]}",
        BODY_END
    );

    ns_end_transaction("autosuggest_9", NS_AUTO_STATUS);
    ns_page_think_time(260.67);

    ns_start_transaction("search");
    ns_web_url ("search",
        "URL=https://www.flipkart.com/search?q=lipstick&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off"
    );

    ns_end_transaction("search", NS_AUTO_STATUS);
    ns_page_think_time(261.142);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("Browse_chunk_4eeda8_css_2");
    ns_web_url ("Browse_chunk_4eeda8_css_2",
        "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Browse.chunk.4eeda8.css",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:style",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Browse.chunk.15210220.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("Browse_chunk_4eeda8_css_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_26");
    ns_web_url ("fdp_26",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9c1lkfudog0000001648188608609","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic","pn":"CART_PAGE","pt":"dynamic","ppt":"pp","ppn":"pp","mpid":"FLIPKART","ss":"BasePageContext"},"e":[{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"","ua":1,"t":1648188648392},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"li","ua":5,"t":1648188651963},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lip","ua":5,"t":1648188652252},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lips","ua":5,"t":1648188652610},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lipst","ua":5,"t":1648188652922},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lipstic","ua":5,"t":1648188653647},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lipstick","ua":5,"t":1648188653926},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lipstick","ua":4,"t":1648188654478},{"en":"TIE","tp":"search_autosuggest","iid":"15vtdfyvgg0000001648188616379","te":"lipstick","ua":2,"t":1648188654479}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_26", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_9");
    ns_web_url ("fetch_9",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pageUri":"/search?q=lipstick&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off","pageContext":{"fetchSeoData":true,"paginatedFetch":false,"pageNumber":1},"requestContext":{"type":"BROWSE_PAGE","ssid":"j3m79krww00000001648188654598","sqid":"yetinatygw0000001648188654598"}}",
        BODY_END
    );

    ns_end_transaction("fetch_9", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_27");
    ns_web_url ("fdp_27",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"j3m79krww00000001648188654598","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"Search","pn":"CART_PAGE","pt":"dynamic","ppt":"pp","ppn":"pp","mpid":"FLIPKART","ss":"BasePageContext","sqid":"yetinatygw0000001648188654598"},"e":[{"en":"SC","st":"textual","s":"Text","ssid":"j3m79krww00000001648188654598","t":1648188654598},{"en":"S","pat":"SEARCH","sqid":"yetinatygw0000001648188654598","t":1648188654598},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"06366e67-338a-4228-b921-a416d8cc34c4","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"06366e67-338a-4228-b921-a416d8cc34c4","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"04a19dde-55c1-4445-bb6c-46685eacf98b","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"d2a91ad6-b37d-473d-b769-2a67bafc5c2a","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"f30d8016-a648-4e35-844e-180a855a0e25","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"2c2a6d50-10f8-433d-a4db-45c43df9a297","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"27009010-7f7d-4004-a194-cd52da2e465e","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"75fa089b-680e-4f6d-87f0-bff453678317","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"1d519691-e86d-474a-ae05-b5b067e731b8","tp":"product","t":1648188655405},{"en":"RI","fid":"9bf97e4f-73f8-4def-83e1-84a8096732fa","rid":"95dc7bb3-fdcb-4cf2-9964-91dd56dd45f9","tp":"product","t":1648188655405}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_27", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("eventBatch_5");
    ns_web_url ("eventBatch_5",
        "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"events":[{"adunits":[{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZQizYUaQaanLpI0HNGjAN9vQ28a7wiSYlc031yUUoOLw==","bannerId":"-1"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FY2qc96p9dXzULkJopvbzFZ/xrpFL/d+3guYElebWS8ZQ==","bannerId":"-1"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FbW9GfyEz0cvZsc0jkCSDxB8ECyPEUeZAL4NPDAl6qwcQ==","bannerId":"220608619"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZmeP3f0R1uxzXLm/voTgDX9J9gdPFuM/5THSJx9ZhFew==","bannerId":"-1"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FYEh8y+hAXikwOXP3ZTHVPZlNzxbyA4vhaOLb6VKkHuQA==","bannerId":"219482843"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FYaZym7NaAju+LXFuCRplXAP/Xq7UznIJR7NYbbki9sMQ==","bannerId":"219482849"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZ1Bi8ify7E1Fu+LO7Qg9wAmXLWWSFItrUcGelVgcZHdg==","bannerId":"-1"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZvOzC2HL2lOjug/7VWWDdKrCOrZT7pXJ4UWVznG0zPmA==","bannerId":"219482848"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FYA/fXWJg8cY7ObBjazRVOLEeZvuYt3CdXSeZGHOqhbKQ==","bannerId":"217603404"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZ7sUjG4VTGRZjIBFR0fw5KRG13D2V0R/gN7gPWbWgWAg==","bannerId":"219168143"},{"impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZYe+98Vg443BFNoKy0JCOUxKCZE3dbHTBkd159P5X/Aw==","bannerId":"219810997"}],"event":"adBeacon","dontTrack":false,"eventTime":1648188655407,"responseId":"7d2530e4-905e-4abc-80d0-2bd0486b7a16","id":"7d2530e4-905e-4abc-80d0-2bd0486b7a16"}],"eventDispatchTime":1648188655407,"dontTrack":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/612/612/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9qbyjj6a9.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/keaaavk0/lipstick/r/g/n/4-5-cushion-matte-lipstick-lakm-original-imafuzwcrg9bhhg3.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/l0o6nbk0/lipstick/b/q/h/60-12-pc-set-liquid-matte-lipstick-sh-huda-original-imagcdzdktgfdhkg.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kjlrb0w0-0/lipstick/u/n/m/38-ayurvedic-lipstick-shade-sampler-kit-just-herbs-original-imafz4zuyhsfwkd5.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kzblocw0/lipstick/u/y/n/3-black-magic-ph-stick-madness-3g-renee-original-imagbczrk9qwhzta.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kvjuufk0/lipstick/b/a/b/23-5-insta-beauty-5-in-1-creamy-lipsticks-liquid-matte-mini-original-imag8f8mkgyuvayd.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DLSKFM797YFHKHPTH%2CLSKFUYWGG5NUCRGN%2CLSKFSWH6BUYHXWAY%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=browsePage&cd%5Bpcat%5D=beauty-and-grooming,makeup,lips,lipstick&cd%5Bbrand%5D=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.googleadservices.com/pagead/conversion/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/klphn680/lipstick/j/o/q/hd-matte-lipstick-sb-212-05-swiss-beauty-original-imagyryg2fagrwca.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kyq62kw0/lipstick/2/c/f/20-the-daily-pack-professionally-forever-matte-liquid-lip-colour-original-imagawf73mzrcygh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kql8sy80/lipstick/7/m/y/21-sara-s-favorite-sensational-liquid-matte-pack-of-3-touch-of-original-imag4kgnbmynydnw.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kxtaxzk0/lipstick/9/i/e/12-fashion-colour-non-transfer-lipstick-pack-of-12-skinplus-original-imaga6vh4xnpxfzf.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kx7vc7k0/lipstick/e/s/1/mini-bullet-lipstick-set-of-12-shade-a-ronzille-original-imag9pssfnuykkah.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/k0flmkw0/lipstick/w/x/z/5-super-stay-matte-ink-liquid-lipstick-maybelline-new-york-original-imafgxh26ufhgdb5.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kz1lle80/lipstick/l/y/8/16-red-edition-liquid-matte-lipsticks-teayason-original-imagb53ubarps8np.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D&is_vtc=1&random=3645313730", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DLSKFM797YFHKHPTH%2CLSKFUYWGG5NUCRGN%2CLSKFSWH6BUYHXWAY%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D&is_vtc=1&random=1163952915", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D&is_vtc=1&random=2200344951", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/l0igvww0/shopsy-lipstick/e/z/g/16-insta-beauty-super-stay-water-proof-sensational-liquid-matte-original-imagcah7hvajbpga.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/ktuewsw0/lipstick/i/e/z/fab-5-step-lipstick-5-in-1-lipstick-shade-b-ronzille-original-imag73mdesb7tghb.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&ocp_id=71w9YrH6JcmNrtoP9oK0kAQ&random=1784207945&sscte=1&crd=", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/ksdjma80/lipstick/d/t/0/5-velvet-matte-lipstick-seven-seas-original-imag5ykspbcddzsb.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kwpam4w0/lipstick/a/u/b/7-5-fab-5-5-in-1-lipstick-7-5g-renee-original-imag9bjsqq27jqhz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_prodid%3DLSKFM797YFHKHPTH%2CLSKFUYWGG5NUCRGN%2CLSKFSWH6BUYHXWAY%3Becomm_pagetype%3Dsearchresults%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D&is_vtc=1&random=1163952915&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D&is_vtc=1&random=2200344951&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dbrowse%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%3Bbrand%3D&is_vtc=1&random=3645313730&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/klphn680/lipstick/n/y/q/hd-matte-lipstick-sb-212-15-swiss-beauty-original-imagyrygqwebvkwz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kmccosw0/lipstick/7/v/d/3-9-color-sensational-creamy-matte-lipstick-677-noho-amber-3-9gm-original-imagf9puhyefzgyz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kbl4cy80/lipstick/q/m/5/100-matte-velvet-touch-lipstick-burgundy-seven-seas-original-imafswmyrng32cgy.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/l0o6nbk0/lipstick/w/l/l/16-nude-edition-sensational-liquid-matte-lipsticks-teayason-original-imagcekeqmq4zvmc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/jksm4y80/lipstick/r/p/4/3-9-color-sensational-creamy-matte-maybelline-new-york-original-imaf82hqaut9nr4v.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/klphn680/lipstick/n/u/y/hd-matte-lipstick-sb-212-24-swiss-beauty-original-imagyryg7s4dmaga.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=1784207945&crd=&is_vtc=1&random=255334923", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kh0vonk0/lipstick/j/u/m/3-6-9to5-primer-matte-lip-color-mp2-lakme-original-imafx4pzzpquxjbn.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kwxv98w0/lipstick/t/t/f/7-5-fab-5-5-in-1-lipstick-la-otter-original-imag9ghyusndgzgp.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kgzg8sw0/lipstick/c/s/p/7-sensational-liquid-matte-lipstick-03-flush-it-red-7ml-original-imafx3xgmfpggjts.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/k5jxfgw0/lipstick/q/a/6/21-pocket-matte-lipstick-set-of-10-pc-ronzille-original-imafz7mbvxhzapm3.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kbl4cy80/lipstick/g/y/m/100-matte-velvet-touch-lipstick-fuchsia-pink-seven-seas-original-imafswmyjsej3gyf.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/k0e66q80/lipstick/p/g/3/14-4-matte-liquid-lipstick-smudge-proof-with-under-eyecream-mars-original-imafk7evgrvzhfac.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?label=8whdCOP52AMQtaWd5QM&guid=ON&script=0&ctc_id=CAIVAgAAAB0CAAAA&ct_cookie_present=false&random=1784207945&crd=&is_vtc=1&random=255334923&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kyrlifk0/lipstick/x/r/u/2-moisture-matte-longstay-lipstick-for-12-hour-long-stay-plum-original-imagaxh9aws4sqyg.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/km6mxe80/lipstick/v/a/g/3-12hrs-stay-matte-crayon-lipstick-sb-s18-06-swiss-beauty-original-imagf5fgzfdehynh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kz5vwy80/lipstick/r/b/e/3d-matte-lipstick-velvet-smooth-full-coverage-seven-seas-original-imagb8c6ha7fdzbh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/ktlu9ow0/lipstick/w/r/e/15-fab-10-combo-renee-original-imag6wtt8ztfpzj8.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kyrlifk0/lipstick/r/g/c/2-moisture-matte-longstay-lipstick-for-12-hour-long-stay-original-imagaxh9ddzzyh42.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kqv8vww0/lipstick/z/m/0/megalast-liquid-catsuit-matte-lipstick-wet-n-wild-original-imag4sggwxycjf3s.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kngd0nk0/lipstick/v/9/r/10-5-cute-pop-matte-lipstick-waterproof-longlasting-pack-of-3-original-imag24ufzx9yssjp.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kyrlifk0/lipstick/e/3/u/2-moisture-matte-longstay-lipstick-for-12-hour-long-stay-original-imagaxh9gzrhvzkm.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/k3ahbww0/lipstick/d/d/y/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9hxvyqjrb.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/kr2e3680/lipstick/c/p/j/7-sensational-liquid-matte-lipstick-nu07-get-undressed-7-g-original-imag4y2tyskwdqkp.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/612/612/k7w8eq80/lipstick/h/h/z/5-lasche-it-liquid-lipstick-lenphor-original-imafqff7gauwa7pg.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("eventBatch_5", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_11");
    ns_web_url ("swatch_11",
        "URL=https://2.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pidLidMap":{"LSKFM797YFHKHPTH":"LSTLSKFM797YFHKHPTHCBK6CJ"},"pincode":"","snippetContext":{"facetMap":{},"layout":"grid","query":"lipstick","queryType":"categoryOnly","storePath":"g9b/ffi/tv5/una","viewType":"GRID_DEFAULT"}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/www/100/100/promos/08/07/2019/7e4ee41e-ce86-43e0-834e-485eaf5a3cfb.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/20/20/kvvad8w0/lipstick/n/p/e/-original-imag8zdc5urzh9hz.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/20/20/kvvad8w0/lipstick/3/3/y/-original-imag8zdchfhktsqd.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/20/20/kvvad8w0/lipstick/q/c/o/-original-imag8zdcrtkdwkvz.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/20/20/kvvad8w0/lipstick/j/d/1/-original-imag8zdcy2dvy2tc.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/20/20/kvvad8w0/lipstick/q/r/o/-original-imag8zdcgdcfnqvr.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("swatch_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_28");
    ns_web_url ("fdp_28",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"j3m79krww00000001648188654598","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"Search","pn":"sp","pt":"sp","ppt":"dynamic","ppn":"CART_PAGE","mpid":"FLIPKART","ss":"BrowsePageContext","sqid":"yetinatygw0000001648188654598"},"e":[{"en":"PV","ib":true,"id":false,"cat":"Makeup","t":1648188655467},{"en":"DCC","iid":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZQizYUaQaanLpI0HNGjAN9vQ28a7wiSYlc031yUUoOLw==","ct":"productCard","p":"1","t":1648188659783}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_28", NS_AUTO_STATUS);
    ns_page_think_time(0.121);

    ns_start_transaction("itm82829011c7db7");
    ns_web_url ("itm82829011c7db7",
        "URL=https://www.flipkart.com/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7?pid=LSKFM797YFHKHPTH&lid=LSTLSKFM797YFHKHPTHCBK6CJ&marketplace=FLIPKART&q=lipstick&store=g9b%2Fffi%2Ftv5%2Funa&srno=s_1_1&otracker=search&otracker1=search&fm=Search&iid=en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZQizYUaQaanLpI0HNGjAN9vQ28a7wiSYlc031yUUoOLw%3D%3D&ppt=sp&ppn=sp&ssid=j3m79krww00000001648188654598&qH=0f97737ad54537c5",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;SN;_px3;qH;S;s_sq"
    );

    ns_end_transaction("itm82829011c7db7", NS_AUTO_STATUS);
    ns_page_think_time(0.69);

    //Page Auto split for Image Link 'Lakmé Forever Matte Liquid Lip Colour -' Clicked by User
    ns_start_transaction("dns_query_2");
    ns_web_url ("dns_query_2",
        "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwhmbGlwa2FydANjb20AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
        "HEADER=Accept-Language:*"
    );

    ns_end_transaction("dns_query_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("eventBatch_6");
    ns_web_url ("eventBatch_6",
        "URL=https://pla-tk.flipkart.net/mapi/v1/tracker/eventBatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"events":[{"activity":"TAP","event":"adInteraction","widget":"GRID_ITEM","adUnit":"SUMMARY","pageView":"GRID_VIEW","bannerId":"-1","impressionId":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZQizYUaQaanLpI0HNGjAN9vQ28a7wiSYlc031yUUoOLw==","listingId":"LSTLSKFM797YFHKHPTHCBK6CJ","dontTrack":false,"eventTime":1648188659782,"id":"7d2530e4-905e-4abc-80d0-2bd0486b7a16","responseId":"7d2530e4-905e-4abc-80d0-2bd0486b7a16"}],"eventDispatchTime":1648188659782,"dontTrack":false}",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCXJ1a21pbmltMQhmbGl4Y2FydANjb20AAAEAAQAAKRAAAAAAAABNAAwASQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app_modules.chunk.94b5e7.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app.chunk.9adf7d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Product.chunk.097bc0.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/runtime.95858ad1.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/vendor.chunk.bf58a224.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_modules.chunk.ac48891f.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app_common.chunk.db774788.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/app.chunk.b7fa5477.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9qbyjj6a9.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9rjx72rgt.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9fsypnk68.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb94ghfryjr.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/krce64w0/lipstick/i/y/o/ultra-hd-matte-lip-color-revlon-original-imag55hmf9ehy363.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9smvdyaaz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/info-basic_6c1a38.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_69e7ec.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9gvtgnwmh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/7/b/z/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9waycgefe.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9qqy8qrhq.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/416/416/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9qbyjj6a9.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/36/36/promos/30/07/2019/79f48e86-8a93-46ab-b45a-5a12df491941.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/36/36/promos/06/09/2016/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/lockin/100/24/images/CCO__PP_2019-07-14.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/312/312/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9qbyjj6a9.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/312/312/keuagsw0/compact/z/g/h/7-sun-expert-ultra-matte-spf-40-pa-lakme-original-imafvfqqcg6x7hfj.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/312/312/k8ndrww0/moisturizer-cream/r/y/w/50-absolute-perfect-radiance-skin-brightening-day-creme-lakme-original-imafqm9at4agapkb.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/112/112/promos/06/04/2020/9c275f1c-a64d-4858-ab3a-27302fbdcbe5.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/140/140/promos/22/07/2018/28f4feda-10cd-45ea-8eef-8ebc9af02d1e.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202007/blobio-imr-202007_e7729b3cda7742979c7423cbcb0d64b0.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202008/blobio-imr-202008_f75e6cf0e1474d90a1bd784d1db33e30.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-202103/blobio-imr-202103_6f47f7356b7c4349af50b79195bc23c1.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-201912/blobio-imr-201912_9898d3eff251443db425962842129f09.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/140/140/imr-201912/blobio-imr-201912_e5d4e67284ac4bffbff800dcdb3e8e05.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202007/blobio-imr-202007_e7729b3cda7742979c7423cbcb0d64b0.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/shield_b33c0c.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202008/blobio-imr-202008_f75e6cf0e1474d90a1bd784d1db33e30.jpg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/blobio/124/124/imr-202103/blobio-imr-202103_6f47f7356b7c4349af50b79195bc23c1.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/NavMenu.chunk.05b448.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/NavMenu.chunk.7e7b9f66.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/raven.3.22.3.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/batman-returns/omni/omni16.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/px/gNtTli3A/init.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Product.chunk.a708467d.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-regular-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/fonts/roboto-medium-webfont.woff2", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:font", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("eventBatch_6", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_29");
    ns_web_url ("fdp_29",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;s_sq;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"9hll75apq80000001648188662333","iid":"qekeug2of40000001648188662728","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"OI","fid":"5j3e913q6o0000001648188662727","oids":"nb:mp:02610f7b08,PRICING,nb:mp:11a4040a13,nb:mp:012d284527,nb:mp:02b5be0710,nb:mp:01184fe026,nb:mp:02c8535702","t":1648188662727}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBHJvbWUDYXBpCGZsaXBrYXJ0A2NvbQAAAQABAAApEAAAAAAAAE4ADABKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Becomm_totalvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=productPage&cd%5Bpcat%5D=beauty-and-grooming,makeup,lips,lipstick,lakm--lipstick&cd%5Bpname%5D=Lakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&cd%5Bvalue%5D=211&cd%5Bbrand%5D=Lakm%C3%A9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=ViewContent&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=LSKFM797YFHKHPTH", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/kl8ccy80/highlighter/5/l/c/pigmented-baked-blusher-brick-highlighter-face-master-chrome-original-imagyegtchyhzcuz.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/k8ho0i80/nail-polish/r/d/b/33-nail-paint-combo-27-juice-original-imafqhr5gnzmpfq5.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/kpcy5jk0/eye-liner/q/8/z/magneteyes-eyeliner-facescanada-original-imag3hmpduvchzgn.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/jlwmgsw0/lip-gloss/g/h/y/6-matte-lipgloss-12-pcs-kiss-beauty-original-imaf8wsawzzx34yu.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_29", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_30");
    ns_web_url ("fdp_30",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;_px3;S;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"15vtdfyvgg0000001648188616379","iid":"s02tu7ix400000001648188616576","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"RI","fid":"77wr9gkzow0000001648188616580","rid":"c52e4a03-8ca0-4fb6-9b20-43e4903d74d0","tp":"product","t":1648188616580},{"en":"RI","fid":"8g9ap6ksr40000001648188616580","rid":"6984b50d-1ca7-4dd8-be74-444215a78da1","tp":"product","t":1648188616580},{"en":"RI","fid":"vm5dnhooow0000001648188616580","rid":"884e01b6-391c-4e3f-9ae2-ec406ce02699","tp":"product","t":1648188616580}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_30", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_18");
    ns_web_url ("collector_18",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dXlNZXx9UXUBXRFdAH19TRkZXH15bQ0dbVh9eW0IfUV1eXUdAHUIdW0ZfCgAKAAsCAwNRBVZQBQ1CW1YPfmF5dH8FCwVrdHp5emJmehReW1YPfmFmfmF5dH8FCwVrdHp5emJmenFweQRxeBRfU0BZV0ZCXlNRVw90fntieXNgZhRDD15bQkFGW1FZFEFGXUBXD1ULUBcAdFRUWxcAdEZEBxcAdEdcUxRBQFxdD0FtA20DFF1GQFNRWVdAD0FXU0BRWhRdRkBTUVlXQAMPQVdTQFFaFFRfD2FXU0BRWhRbW1YPV1xtUGREX0EKGeQZ9cVt8X1B5Y1h5fHFlC3pUYmdBd2ZacwVbYUBnWFRBVgR0aGNbSGtnU2NTU1x+Qns%5ES%7DCenx1WHN8C0RjAApTDNBUVbYWteUQIBA0tnZ119fkUXAKXYXAXYUQkJGD0FCFEJC8XA9BQhRBQVtWDD1gBXwULWUBFRQICAgINCAgIDBAYKAwoKBAcGBwsKFEN6DwJUCwUFAQVTVgcGBwEFUQcQHhBiagMCCwALEAgCHhaBiagM%3EDAwoEEAgQZVtcAQAQHhBDiagMCBAAAEAg%60CHhBiagMCAAUAEAgBAAAGHhBiaKgMCCwUCEAgBBAICHhBiagMCAgsGEAgDBAYKAwoKBAQACgsFHhBiagMDAgIG%7DGEAgDBAYKAwoKKBAQBAgECHhBiagMCAAIEEAgQBwsBBQRUBgIfU1ECAB8DA1dRHwtXUwEfVFYEBwJWUFAAAwZRE7B4QYmo7DAgIKChAIVFNeQVdPT28%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=59376f40-ac02-11ec-9ea3-fd650dbb214c&ft=263&seq=0&en=NTA&pc=9123925655954700&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=1",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3228501457", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Becomm_totalvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3164182407", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=510556977", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Becomm_totalvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3164182407&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3228501457&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797YFHKHPTH%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D211%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=510556977&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/1664/1664/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9qbyjj6a9.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_18", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_19");
    ns_web_url ("collector_19",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_19_url_0_1_1648188771765.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/lockin/671/160/images/CCO__PP_2019-07-14.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/www/64/64/promos/11/07/2018/9886f3f9-7b63-4057-b5e0-e9897697337b.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/lockin/32/32/images/locked-benefit-icon.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;qH;S;s_sq;SN;_px3", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/416/416/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9p3yamuhk.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_19", NS_AUTO_STATUS);
    ns_page_think_time(273.253);

    ns_start_transaction("itm82829011c7db7_2");
    ns_web_url ("itm82829011c7db7_2",
        "URL=https://www.flipkart.com/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7?pid=LSKFM797QWECHKRX&lid=LSTLSKFM797QWECHKRXKZKS9A&marketplace=FLIPKART&sattr[]=shade&st=shade"
    );

    ns_end_transaction("itm82829011c7db7_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_10");
    ns_web_url ("fetch_10",
        "URL=https://rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;S;s_sq;SN;_px3",
        BODY_BEGIN,
            "{"pageUri":"/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7?pid=LSKFM797QWECHKRX&lid=LSTLSKFM797QWECHKRXKZKS9A&marketplace=FLIPKART&sattr[]=shade&st=shade","pageContext":{"fetchSeoData":true}}",
        BODY_END
    );

    ns_end_transaction("fetch_10", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_11");
    ns_web_url ("fetch_11",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;S;s_sq;SN;_px3",
        BODY_BEGIN,
            "{"pageUri":"/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7?pid=LSKFM797QWECHKRX&lid=LSTLSKFM797QWECHKRXKZKS9A&marketplace=FLIPKART&sattr[]=shade&st=shade","pageContext":{"fetchSeoData":true}}",
        BODY_END
    );

    ns_end_transaction("fetch_11", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_31");
    ns_web_url ("fdp_31",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;s_sq;SN;_px3;S",
        BODY_BEGIN,
            "[{"nc":{"ssid":"j3m79krww00000001648188654598","iid":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZQizYUaQaanLpI0HNGjAN9vQ28a7wiSYlc031yUUoOLw==","fm":"Search","pn":"pp","pt":"pp","ppt":"sp","ppn":"sp","mpid":"FLIPKART","ss":"ProductPageContext"},"e":[{"en":"PSI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","t":1648188662751},{"en":"PWI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","t":1648188662751},{"en":"PV","ib":false,"id":false,"cat":"Makeup","t":1648188662752},{"en":"AppEvents","ev":[{"name":"FCP","value":2045.2550000045449},{"name":"TTFB","value":708.9200000045821}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Product","pageUri":"/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7"},"t":1648188663817},{"en":"AppEvents","ev":[{"name":"LCP","value":2849.805}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Product","pageUri":"/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7"},"t":1648188664970},{"en":"AppEvents","ev":[{"name":"FID","value":7.5400000205263495}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"Product","pageUri":"/lakm-forever-matte-liquid-lip-colour/p/itm82829011c7db7"},"t":1648188667170},{"en":"OI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","oids":"nb:mp:11a4040a13,ns:0220a4ec23,nb:mp:012d284527,nb:mp:02b5be0710,nb:mp:02c8535702","t":1648188667645},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-202007_e7729b3cda7742979c7423cbcb0d64b0.jpg","pid":"LSKFM797QWECHKRX","pos":1,"t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-202008_f75e6cf0e1474d90a1bd784d1db33e30.jpg","pid":"LSKFM797QWECHKRX","pos":2,"t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-202103_6f47f7356b7c4349af50b79195bc23c1.jpeg","pid":"LSKFM797QWECHKRX","pos":3,"t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-201912_9898d3eff251443db425962842129f09.jpg","pid":"LSKFM797QWECHKRX","pos":4,"t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-201912_e5d4e67284ac4bffbff800dcdb3e8e05.jpg","pid":"LSKFM797QWECHKRX","pos":5,"t":1648188667652},{"en":"ITCI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","pid":"LSKFM797QWECHKRX","is":4,"tic":1953,"t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-202007_e7729b3cda7742979c7423cbcb0d64b0.jpg","pid":"LSKFM797QWECHKRX","pos":1,"rid":"c4c9c921-50d5-4581-9092-d7b579a324ba","t":1648188667652},{"en":"ITCI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","pid":"LSKFM797QWECHKRX","is":1,"rid":"c4c9c921-50d5-4581-9092-d7b579a324ba","tic":1,"t":1648188667652},{"en":"RI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","rid":"c4c9c921-50d5-4581-9092-d7b579a324ba","tp":"product","t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-202008_f75e6cf0e1474d90a1bd784d1db33e30.jpg","pid":"LSKFM797QWECHKRX","pos":1,"rid":"1bdd1774-6caf-4ffb-9693-d7433db16df4","t":1648188667652},{"en":"ITCI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","pid":"LSKFM797QWECHKRX","is":1,"rid":"1bdd1774-6caf-4ffb-9693-d7433db16df4","tic":1,"t":1648188667652},{"en":"RI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","rid":"1bdd1774-6caf-4ffb-9693-d7433db16df4","tp":"product","t":1648188667652},{"en":"ITI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","iid":"blobio-imr-202103_6f47f7356b7c4349af50b79195bc23c1.jpeg","pid":"LSKFM797QWECHKRX","pos":1,"rid":"686f8568-2672-4a34-8c7e-d1c9b5408a2a","t":1648188667652},{"en":"ITCI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","pid":"LSKFM797QWECHKRX","is":1,"rid":"686f8568-2672-4a34-8c7e-d1c9b5408a2a","tic":1,"t":1648188667652},{"en":"RI","fid":"fa093d62-febc-49e0-a5e8-e1b6bb9acd24.LSKFM797YFHKHPTH","rid":"686f8568-2672-4a34-8c7e-d1c9b5408a2a","tp":"product","t":1648188667652}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9p3yamuhk.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9tqgdzva4.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9ygqd2yar.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9bmq75jxj.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9zvzweujq.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb92jfhzsug.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Becomm_totalvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=productPage&cd%5Bpcat%5D=beauty-and-grooming,makeup,lips,lipstick,lakm--lipstick&cd%5Bpname%5D=Lakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&cd%5Bvalue%5D=295&cd%5Bbrand%5D=Lakm%C3%A9", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=ViewContent&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=LSKFM797QWECHKRX", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/128/128/k3ahbww0/lipstick/p/t/h/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9ybnznray.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=634795638", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=2543961872", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Becomm_totalvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=2732553037", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/312/312/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9p3yamuhk.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/280/336/jx3kn0w0/eye-liner/9/8/e/3-colossal-eyeliner-maybelline-new-york-original-imafgyrysym4jydy.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dproduct%3Becomm_prodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Becomm_totalvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=2732553037&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=634795638&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dviewitem%3Bprodid%3DLSKFM797QWECHKRX%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bbrand%3DLakm%C3%A9%3Bvalue%3D295%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=2543961872&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/1664/1664/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9p3yamuhk.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_31", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_20");
    ns_web_url ("collector_20",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_20_url_0_1_1648188771793.body",
        BODY_END
    );

    ns_end_transaction("collector_20", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("cart_3");
    ns_web_url ("cart_3",
        "URL=https://2.rome.api.flipkart.com/api/5/cart",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;S;s_sq",
        BODY_BEGIN,
            "{"cartContext":{"LSTLSKFM797QWECHKRXKZKS9A":{"quantity":1}}}",
        BODY_END
    );

    ns_end_transaction("cart_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_32");
    ns_web_url ("fdp_32",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;S;s_sq",
        BODY_BEGIN,
            "[{"nc":{"ssid":"j3m79krww00000001648188654598","iid":"en_bVvms8K4OCiNmbKQjKNCW9HfPUsEThA7iSrUjfsd6FZQizYUaQaanLpI0HNGjAN9vQ28a7wiSYlc031yUUoOLw==","fm":"Search","pn":"pp","pt":"pp","ppt":"sp","ppn":"sp","mpid":"FLIPKART","ss":"ProductPageContext"},"e":[{"en":"PSI","fid":"f2f4a147-2a55-405b-8c6b-20f27807837a.LSKFM797QWECHKRX","t":1648188667656},{"en":"PWI","fid":"f2f4a147-2a55-405b-8c6b-20f27807837a.LSKFM797QWECHKRX","t":1648188667656},{"en":"PV","ib":true,"id":false,"cat":"Makeup","t":1648188667656},{"en":"DCI","iid":"lo_cco:57560330-628e-791f-4d78-f8b27a67df59.ct:COIN_CONVERSION_OVERLAY;","wk":"20.overlayCard.PROMOTIONS_13","ct":"overlayCard","p":"1","t":1648188667706,"st":1648188666429,"f":true,"pv":78},{"en":"DCI","iid":"6bd757a3-db39-4ec9-8a39-8d0a3ecd84d4.LSKFM797YFHKHPTH.lipstick","wk":"32_22","ct":"BrandChatIcon:BrandChatDesktop","p":22,"t":1648188667706,"st":1648188664111,"f":true,"pv":100},{"en":"ATCC","iid":"f2f4a147-2a55-405b-8c6b-20f27807837a.LSKFM797QWECHKRX","lid":"LSTLSKFM797QWECHKRXKZKS9A","ct":"PAGE","t":1648188671162}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_32", NS_AUTO_STATUS);
    ns_page_think_time(0.063);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("tr");
    ns_web_url ("tr",
        "URL=https://www.facebook.com/tr?id=1469476963265313&ev=cartPage&cd%5Bpcat%5D=beauty-and-grooming,makeup,lips,lipstick,lakm--lipstick&cd%5Bpname%5D=Lakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&cd%5Bvalue%5D=295&cd%5Bbrand%5D=Lakm%C3%A9",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://www.facebook.com/tr?id=1469476963265313&ev=AddToCart&cd%5Bcontent_type%5D=product&cd%5Bcontent_ids%5D=LSKFM797QWECHKRX", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DLSKFM797QWECHKRX%3Bvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DLSKFM797QWECHKRX%3Bvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dcart%3Becomm_prodid%3DLSKFM797QWECHKRX%3Becomm_totalvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=IDE", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DLSKFM797QWECHKRX%3Bvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3744600043", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DLSKFM797QWECHKRX%3Bvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=894656920", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1017598645/?value=0&label=5BlXCLOpwQMQtaWd5QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DLSKFM797QWECHKRX%3Bvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3744600043&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1008984980/?value=0&label=eJWICIydxgMQlMeP4QM&guid=ON&script=0&data=pagetype%3Dcart%3Bprodid%3DLSKFM797QWECHKRX%3Bvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=894656920&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.com/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dcart%3Becomm_prodid%3DLSKFM797QWECHKRX%3Becomm_totalvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3597411669", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("tr", NS_AUTO_STATUS);
    ns_page_think_time(278.141);

    ns_start_transaction("viewcart_2");
    ns_web_url ("viewcart_2",
        "URL=https://www.flipkart.com/viewcart?otracker=PP_GoToCart",
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/CartPage.chunk.fde5bf.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/CartPage.chunk.0581c7d3.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.google.co.in/pagead/1p-user-list/1011966879/?guid=ON&script=0&data=ecomm_pagetype%3Dcart%3Becomm_prodid%3DLSKFM797QWECHKRX%3Becomm_totalvalue%3D295%3Bbrand%3DLakm%C3%A9%3Bpcat%3Dbeauty-and-grooming%2Cmakeup%2Clips%2Clipstick%2Clakm--lipstick%3Bpname%3DLakm%C3%A9%20Forever%20Matte%20Liquid%20Lip%20Colour%20-&is_vtc=1&random=3597411669&ipr=y", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("viewcart_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_12");
    ns_web_url ("fetch_12",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pageUri":"/viewcart?otracker=PP_GoToCart","pageContext":{"fetchSeoData":true}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/144/144/ktuewsw0/face-pack/t/o/h/20-berries-rose-sheet-mask-sheet-mask-health-glow-original-imag73k3nvympsdp.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fetch_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fetch_13");
    ns_web_url ("fetch_13",
        "URL=https://2.rome.api.flipkart.com/api/4/page/fetch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pageUri":"/viewcart?otracker=PP_GoToCart","pageContext":{"paginatedFetch":true,"pageNumber":2,"fetchAllPages":false,"paginationContextMap":{}}}",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/www/100/100/promos/18/07/2019/4aebbd99-7478-411e-aced-265e7722d18d.png?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/224/224/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9p3yamuhk.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/224/224/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fetch_13", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_21");
    ns_web_url ("collector_21",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIEBQAQHhBWEAhJEGJqAwILBwIQCBBpbhBmS0JXd0BAXUAIEnFTXFxdRhJAV1NWEkJAXUJXQEZLEhUCFRJdVBJcR15eElNGEmFGElNGEnpmf352XVFHX1dcRhxVV0Z3XldfV1xGQXBLZlNVfFNfVxJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1AR1xGW19XHAsHCgcKU1YDHFhBCAMIAwIFCgYSU0YSXFdFEmJAXV9bQVcSU0YSdEdcUUZbXVwcXRxXElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHVNCQm1RXV9fXVwcUVpHXFkcVlAFBQYFCgocWEEIAwgDBgQCAQMSU0YSfVBYV1FGHFVXRnFdX0JdXFdcRhJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgDCwIHABJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgDCwMAABJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgDBAIHAxJTRhJzQEBTSxxUXUB3U1FaElNGElASU0YScRJTRhJRElNGEkASU0YSQEdcd1xGV0B6XV1ZQRJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgAAAUDBxJTRhJAElNGEkBHXHFaU1xVV3pdXVlBElNGEkESU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIAAABBwYSU0YSQRJTRhJLElNGEnQSU0YSXhJTRhJAElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAUCBwAHElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAUAAgAGElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAUBAAEGElNGEnNAQFNLHFRdQHdTUVoSU0YSSxJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgFAQsGAhJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgFAQQDChJTRhJBElNGEkYcXl1dQnNBS1xRElNGEkoSU0YSbRJTRhJ9UFhXUUYcWRJTRhJ9UFhXUUYcQhJTRhJ9UFhXUUYcRBJTRhJbHFVdZl1xU0BGElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHWJAXVZHUUYcUVpHXFkcUwUCCgYEBVYcWEEIAwgEAgQEAwFuEB5uEGZLQld3QEBdQAgScVNcXF1GEkBXU1YSQkBdQldARksSFQIVEl1UElxHXl4SU0YSYUYSU0YSemZ%2FfnZdUUdfV1xGHFVXRndeV19XXEZBcEtmU1V8U19XElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHUBHXEZbX1ccCwcKBwpTVgMcWEEIAwgDAgsEBhJTRhJcV0USYkBdX1tBVxJTRhJ0R1xRRltdXBxdHFcSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdU0JCbVFdX19dXBxRWkdcWRxWUAUFBgUKChxYQQgDCAMGBAIBAxJTRhJ9UFhXUUYcVVdGcV1fQl1cV1xGElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAMLAgcAElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAMLAwAAElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAMEAgcDElNGEnNAQFNLHFRdQHdTUVoSU0YSUBJTRhJxElNGElESU0YSQBJTRhJAR1x3XEZXQHpdXVlBElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAAABQMHElNGEkASU0YSQEdccVpTXFVXel1dWUESU0YSQRJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgAAAEHBhJTRhJBElNGEksSU0YSdBJTRhJeElNGEkASU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIBQIHAAcSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIBQACAAYSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIBQEAAQYSU0YSc0BAU0scVF1Ad1NRWhJTRhJLElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAUBCwYCElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQgACAUBBAMKElNGEkESU0YSRhxeXV1Cc0FLXFESU0YSShJTRhJtElNGEn1QWFdRRhxZElNGEn1QWFdRRhxCElNGEn1QWFdRRhxEElNGElscVV1mXXFTQEYSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdYkBdVkdRRhxRWkdcWRxTBQIKBgQFVhxYQQgDCAQCBAQDAW4QHm4QZktCV3dAQF1ACBJxU1xcXUYSQFdTVhJCQF1CV0BGSxIVAhUSXVQSXEdeXhJTRhJhRhJTRhJ6Zn9+dl1RR19XXEYcVVdGd15XX1dcRkFwS2ZTVXxTX1cSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdQEdcRltfVxwLBwoHClNWAxGxYQQgDCAMDBgQLElNGElxXRRJiQF1fW0FXElNGEnRHXFFGW11cHF0cVxJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1TQkJtUV1fX11cHFFaR1xZHFZQBQUG%5EBQoKHFhBCAMIAwYEAgEDElNGEn1QWFdRRhxVV0ZxXV9CXVxXXEYSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIAwsCBwASU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIAwsDAAASU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIAwQCBwMSU0YSc0BAU0scVF1Ad1NRWhJTRhJQElNGEnESU0YSURJTRhJAESlNGEkBHXHdcRldAel1dWUESU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFH%3AV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGEHFhBCAAIAAAFAwcSU0YSQBJTRhJAR1xxWlNcVVd6XV1ZQRJTRhJBElNGElpGRkJBCB0dQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11cHVhBHURXXFZdQBxRWkdcWRxQVAcKUwAABhxYQQNgACAAAAQcGElNGEkESU0YSSxJTRhJ0ElNGEl4SU0YSQBJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdOVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgFAgcABxJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgFAAIABhJTRhJaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQH%3EFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1EV1xWXUAcUVpHXFkcUFQHClMAAAYcWEEIAAgFAQABBhJTRhJzQEBTSxxUXUB3U1FaElNGEksSU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIBQEENSLBgISU0YSWkZGQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dRUVFHV5bXFFaQltcHVRZH1FCH0hbXVwdWEEdRFdcVl1AHFFaR1xZHFBUBwpTAAAGHFhBCAAIBQEEAwoSU0YSQRJTRhJGHF5dXUJzQUtcURJTRhJKElNGEm0SU0YSfVBYV1FGHFkSU0YSfVBYV1FGHEISU0YSfVBYV1FGHEQSU0YSWxxVXWZdcVNARhJTRhJaRkZCQQgdHUFGU0ZbUR9TQ8GUFXpORkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1iQF1WR1FGHFFaR1xZHFMFAgoGBAVWHFhBCAMIBAIEBAMBbhBvpEB4QYmoDAgQAABAIBx4QYmoDAgAFABAIAwMLBQMeEGJqAwMCAgYQCAMEBgoDCgoEBQAGAwUeEGJqAwIAAgQQCBAHCwEFBFQGAh9TUQIAHwMDV1EfC1dTAR9UVgQHAlZQUAADBlEQHhBiagMCAgoKEAhUU15BVx4QYmoDAgEEAhAIEFpGRkJBCB0dRUVFHFReW0JZU0BGHFFdXx1eU1lfH1RdQFdEV0AfX1NGRlcfXltDR1tWH15bQh9RXV5dR0AdQh1bRDl8KAAoACwIDA1EFVlAFDUJbVg9+YXl0fwULBWt0enl6YmZ6FF5bVg9+YWZ+YXl0fwULBWt0enl6YmZ6cXB5BHF4FF9TQFlXRkJeU1FXD3R+e2J5c2BmFEMPXltCQUZbUVkUQUZdQFcPVQtQFwB0VFRbFwB0RkQHFwB0R1xTFEFAXF0PQW0DbQMUXUZAU1FZV0APQVdTQFFaFF1GQFNRWVdAAw9BV1NAUVoUVF8PYVdTQFFaFFtbVg9XXG1QZERfQQp5Bn1xW3xfUHljWHl8cWULelKRiZ0F3ZlpzBVthQGdYVEFWBHRoY1tIa2dTY1NTXH5CewJ6fHVYc3wLRGMAClMFRVtha15RAgEDS2dnXX1+RRcBdhcBdhRCQkYPQUIUQkJcD0FC7FEFBW1YPWAFfBQtZQEVFAgICAgICAgMEBgoDCgoEBwYHCwoUQ3oPAlQLBQU7BBVNWBwYHAQVRBxBPT28%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=59376f40-ac02-11ec-9ea3-fd650dbb214c&ft=263&seq=3&en=NTA&cs=37fc7aeb2f324a0dc73ff9df5d89a1d197f0896f8b743ac6942e2b4d2b0f98c6&pc=3886499188898271&sid=5961b8b6-ac02-11ec-88de-734b734b5568%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B6%F3%A0%84%B6%F3%A0%84%B3%F3%A0%84%B1%F3%A0%84%B3%F3%A0%84%B4&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=4",
        BODY_END
    );

    ns_end_transaction("collector_21", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_33");
    ns_web_url ("fdp_33",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/fdp_33_main_url_1_1648188771818.body",
        BODY_END
    );

    ns_end_transaction("fdp_33", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("swatch_12");
    ns_web_url ("swatch_12",
        "URL=https://2.rome.api.flipkart.com/api/4/product/swatch",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "{"pidLidMap":{"TKPFNUGYNCNWGMAZ":"LSTTKPFNUGYNCNWGMAZ3M1AQT"},"pincode":"","snippetContext":{"facetMap":{"facets.ideal_for[]":["Men","men"]},"layout":"grid","query":null,"queryType":null,"storePath":"clo/vua","viewType":"QUICK_VIEW"},"showSuperTitle":true}",
        BODY_END
    );

    ns_end_transaction("swatch_12", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_22");
    ns_web_url ("collector_22",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwMDBgQQHhBWEAhJEGJqAwIFBAMQCGlJEGJqAwMCBAoQCBBfXUdBV19dRFcQHhBiagMCCwsGEAgQRkBHVxAeEGJqAwMCAAcQCBALAgEeBgcGHgABBgUQTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAQMFHAMCAgICBAMCAQcDBwQeEGJqAwILAQIQCAoLCxwEAgICAQQEAAMCCwEKHhBiagMCAQQFEAgAHhBiagMCBwYAEAgBAgQeEGJqAwIBBgQQCAYFCh4QYmoDAgoKBRAIEFZbRBAeEGJqAwIKAQIQCAoKCh4QYmoDAwMGAxAIBgEHHhBiagMCBgMEEAgAAQcGTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAQMFHAMCAgICBAMCAQcDBwQeEGJqAwILAQIQCAQCAxwFCwsLCgUFCwALBAoKHhBiagMCAQQFEAgBHhBiagMCBwYAEAgACwIeEGJqAwIBBgQQCAEGCh4QYmoDAgoKBRAIEFZbRBAeEGJqAwIKAQIQCAoABx4QYmoDAwMGAxAIBgIKHhBiagMCBgMEEAgABgMKTx5JEGJqAwMCBAoQCBBBUUBdXl4QHhBiagMCCwsGEAgQRkBHVxAeEGJqAwIFBwYQCAMcBAICAgICAgABCgYDCgcKHhBiagMCAQoHEAgCHhBiagMCBgMEEAgBAgEHTx5JEGJqAwMCBAoQCBBBUUBdXl4QHhBiagMCCwsGEAgQRkBHVxAeEGJqAwIFBwYQCAcAHhBiagMCAQoHEAgCHhBiagMCBgMEEAgBAwMFTx5JEGJqAwMCBAoQCBBBUUBdXl4QHhBiagMCCwsGEAgQRkBHVxAeEGJqAwIFBwYQCAcHARwHCwsLBQcHCgcLAQUHHhBiagMCAQoHEAgCHhBiagMCBgMEEAgBAwsATx5JEGJqAwMCBAoQCBBfXUdBV19dRFcQHhBiagMCCwsGEAgQRkBHVxAeEGJqAwMCAAcQCBAKAAceBgIKHgAGAwtOBQUBHgYACh4DAQFOBQQEHgYBAB4DAAQQTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIHwYABRwFAgICAwAAAgUCAQMABx4QYmoDAgsBAhAIBAcLHAcDAAcDAAACBQIBAwAeEGJqAwIBBAUQCAYeEGJqAwIHBgAQCAMFBh4QYmoDAgEGBBAIAQYKHhBiagMCCgoFEAgQW19VEB4QYmoDAgoBAhAIBQQHHhBiagMDAwYDEAgDAwUEHhBiagMCBgMEEAgBAAAFTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAAoBHAELCwsLAQoLBAYKBgYeEGJqAwILAQIQCAQDBRwFCwsLCgUFCwALBAoKHhBiagMCAQQFEAgHHhBiagMCBwYAEAgDBQoeEGJqAwIBBgQQCAMLHhBiagMCCgoFEAgQUxAeEGJqAwIKAQIQCAUEBx4QYmoDAwMGAxAIAwEAAh4QYmoDAgYDBBAIAQAFBE8eSRBiagMDAgQKEAgQQVFAXV5eEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMCBQcGEAgLAgIeEGJqAwIBCgcQCAIeEGJqAwIGAwQQCAEBAgJPHkkQYmoDAwIEChAIEEFRQF1eXhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgUHBhAICwIGHAULCwsKBQULAAsECgoeEGJqAwIBCgcQCAIeEGJqAwIGAwQQCAEGCgZPHkkQYmoDAwIEChAIEF9dR0FXXUdGEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMCAwEFEAgfAQMFHAoCAgIDCgEDAgcGBAseEGJqAwILAQIQCAQDBBwACgUHAQQEAAMCCwEKHhBiagMCAQQFEAgEHhBiagMCBwYAEAgABAMeEGJqAwIBBgQQCAEGCh4QYmoDAgoKBRAIEFtfVRAeEGJqAwIKAQIQCAcABh4QYmoDAwMGAxAIAwoLAB4QYmoDAgYDBBAIBgoFBE8eSRBiagMDAgQKEAgQX11HQVdfXURXEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMDAgAHEAgQBwAGHgMKCwAeBgoKAhBPHkkQYmoDAwIEChAIEF9dR0FXVl1FXBAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAwYCHAEHAgICBAMCAQcDBwQAHhBiagMCCwECEAgBAwIcBgoFBwMKAQMCBwYECx4QYmoDAgEEBRAIBR4QYmoDAgcGABAIAAQDHhBiagMCAQYEEAgBBgoeEGJqAwIKCgUQCBBbX1UQHhBiagMCCgECEAgHAAYeEGJqAwMDBgMQCAMKCwAeEGJqAwIGAwQQCAYKCgpPHkkQYmoDAwIEChAIEFRdUUdBEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMCAwEFEAgDBgIcAQcCAgIEAwIBBwMHBAAeEGJqAwILAQIQCAALBB4QYmoDAgEEBRAICh4QYmoDAgcGABAIAAsCHhBiagMCAQYEEAgBBgoeEGJqAwIKCgUQCBBTEB4QYmoDAgYDBBAIBgoLAE8eSRBiagMDAgQKEAgQX11HQVdHQhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAwYCHAEHAgICBAMCAQcDBwQAHhBiagMCCwECEAgBAwIcBgoFBwMKAQMCBwYECx4QYmoDAgEEBRAICh4QYmoDAgcGABAIAAQDHhBiagMCAQYEEAgBBgoeEGJqAwIKCgUQCBBbX1UQHhBiagMCCgECEAgHAAYeEGJqAwMDBgMQCAMKCwAeEGJqAwIGAwQQCAcCCwRPHkkQYmoDAwIEChAIEFFeW1FZEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMCAwEFEAgDBgIcAQcCAgIEAwIBBwMHBAAeEGJqAwILAQIQCAEDAhwGCgUHAwoBAwIHBgQLHhBiagMCAQQFEAgKHhBiagMCBwYAEAgABAMeEGJqAwIBBgQQCAEGCh4QYmoDAgoKBRAIEFtfVRAeEGJqAwIKAQIQCAcABh4QYmoDAwMGAxAIAwoLAB4QYmoDAgUGBBAIEAIQHhBiagMDAwoLEAhGQEdXHhBiagMCBgMEEAgHAwMHTx5JEGJqAwMCBAoQCBBQXkdAEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMCAwEFEAgDAAscCwYLCwsECwYKAAYAAB4QYmoDAgsBAhAIAAsEHhBiagMCAQQFEAgKHhBiagMCBwYAEAgACwIeEGJqAwIBBgQQCAEGCh4QYmoDAgoKBRAIEFMQHhBiagMCBgMEEAgHCgcLTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAwALHAsGCwsLBAsGCgAGAAAeEGJqAwILAQIQCAEDAhwGCgUHAwoBAwIHBgQLHhBiagMCAQQFEAgKHhBiagMCBwYAEAgABAMeEGJqAwIBBgQQCAEGCh4QYmoDAgoKBRAIEFtfVRAeEGJqAwIKAQIQCAcABh4QYmoDAwMGAxAIAwsCAR4QYmoDAgYDBBAIBwoFC08eSRBiagMDAgQKEAgQX11HQVdfXURXEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMDAgAHEAgQBwoEHgADAAEeBAYKAgMQTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAwALHAsGCwsLBAsGCgAGAAAeEGJqAwILAQIQCAAKCh4QYmoDAgEEBRAICx4QYmoDAgcGABAIAQIEHhBiagMCAQYEEAgGBwoeEGJqAwIKCgUQCBBWW0QQHhBiagMCCgECEAgEAgMeEGJqAwMDBgMQCAADAQQeEGJqAwIGAwQQCAQGCgMDTx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIAwALHAsGCwsLBAsGCgAGAAAeEGJqAwILAQIQCAcLARwFCwsLCgUFCwALBAoKHhBiagMCAQQFEAgDAh4QYmoDAgcGABAIAQIEHhBiagMCAQYEEAgGBwoeEGJqAwIKCgUQCBBWW0QQHhBiagMCCgECEAgEAwQeEGJqAwMDBgMQCAADBwQeEGJqAwIGAwQQCAQGCgAETx5JEGJqAwMCBAoQCBBfXUdBV11HRhAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAgMBBRAIBgoAHAUCAgIDAAACBQIBAwAHHhBiagMCCwECEAgEAgMcBQsLCwoFBQsACwQKCh4QYmoDAgEEBRAIAwMeEGJqAwIHBgAQCAALAh4QYmoDAgEGBBAIAwIHHhBiagMCCgoFEAgQVltEEB4QYmoDAgoBAhAIBAAHHhBiagMDAwYDEAgAAwUDHhBiagMCBgMEEAgEBgoBC08eSRBiagMDAgQKEAgQX11HQVddR0YQHhBiagMCCwsGEAgQRkBHVxAeEGJqAwIDAQUQCAcHBBwHHhBiagMCCwECEAgEAwUcBQsLCwoFBQsACwQKCh4QYmoDAgEEBRAIAwAeEGJqAwIHBgAQCAoKHhBiagMCAQYEEAgDBR4QYmoDAgoKBRAIEFZbRBAeEGJqAwIKAQIQCAQACh4QYmoDAwMGAxAIAAMKAR4QYmoDAgYDBBAIBAYKBARPHkkQYmoDAwIEChAIEF9dR0FXX11EVxAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAwIABxAIEAQACh4AAwoBHgQGCgQEEE8eSRBiagMDAgQKEAgQX11HQVddR0YQHhBiagMCCwsGEAgQRkBHVxAeEGJqAwIDAQUQCAYKABwFAgICAwAAAgUCAQMABx4QYmoDAgsBAhAIBAIDHAULCwsKBQULAAsECgoeEGJqAwIBBAUQCAMAHhBiagMCBwYAEAgACwIeEGJqAwIBBgQQCAMCBx4QYmoDAgoKBRAIEFZbRBAeEGJqAwIKAQIQCAQBCx4QYmoDAwMGAxAIAAMFAB4QYmoDAgYDBBAIBAcDAARPHkkQYmoDAwIEChAIEF9dR0FXX11EVxAeEGJqAwILCwYQCBBGQEdXEB4QYmoDAwIABxAIEAQBCx4AAwUAHgQHAwAEEE8eSRBiagMDAgQKEAgQX11HQVddR0YQHhBiagMCCwsGEAgQRkBHVxAeEGJqAwIDAQUQCAcHBBwHHhBiagMCCwECEAgEAwUcBQsLCwoFBQsACwQKCh4QYmoDAgEEBRAIAwAeEGJqAwIHBgAQCAoKHhBiagMCAQYEEAgDBR4QYmoDAgoKBRAIEFZbRBAeEGJqAwIKAQIQCAQKCx4QYmoDAwMGAxAIAAMAAR4QYmoDAgYDBBAIBAcDBgBPHkkQYmoDAwIEChAIEF9dR0FXXUdGEB4QYmoDAgsLBhAIEEZAR1cQHhBiagMCAwEFEAgHAgQcCgsLCwsBCgsEBgoGBh4QYmoDAgsBAhAIBAMFHAULCwsKBQULAAsECgoeEGJqAwIBBAUQCAMBHhBiagMCBwYAEAgDBQoeEGJqAwIBBgQQCAMLHhBiagMCCgoFEGAgQUxAeEGJqAwIKAQIQCAQLCh4QYmoDAwMGAxAIAAMDBh4QYmoDAgYDBBAIBAcDBgtPbx4QYmoDAgoLABAIEAQCbUFXUW1AV0FGEB4QYmoDAgQHBxAIEFpGRkJBCB0dRUVFHFReW0JZU0BGHFFdXx1RXl1GWltcVR9TXFYfU1FRV0FBXUBbV0EdUF1GRl1fRVdTQB1CQA1BW1YPUV5dHkRHUxRCaW8PVFNRV0ZBHFtWV1NebVRdQBcABwdwFwAHB3YXAXZ%2FV1wUQmlvD1RTUVdGQRxbVldTXm1UXUAXAAcHcBcABwd2FwF2X1dcFF1GQFNRWVdAD1FTRldVXUBLRkBXVxRUXw9cV10XAHRfV0BRWlNcVltBW1xVFFtbVg9%2FbQpQB1FRAwICHwIDUQEfBgYCAB9QBFdWHwIFAwsDVgFQUQZWUG0AbQEFAGd2B3BqdnRrYW1%2FcRwKenNgagpnagV7agcUXUZAU1FZV0APWkJtQFtRWm1cU0RbVVNGW11cbQBtABxcU0RbVVNGW11ccVNAVhxge3F6bXxzZHt1c2Z7fXxtdFNBWltdXEx%2FV1%5EwXAAcABUEXAHBwXUZGXV8XAHBlV1NAbQp6c2BqCmdqBXtqBxRdRkBTUVlXQAMPWkJtQFtRWm1cU0RbVVNGW11cbWJ7fHx3dm1cV10XAHRfV0BRWlSNcVltBW1xVbXxzbXxzZG13amJzfHZzcH53bVxTRFtVU0ZbXVxxU0BWbVFRbQBtfgNtR%3AFtXRR9TXl4UUVtWDwp6c2BqCmdqBXtqBxAeEGJqAwIEKCgAQCEkQdntkAQx2e2QDDHZ7ZAAMdntkAAx2e2QDDHZ7ZAEQCAMeEHZ7ZAAMdntkAwx2e2QADHZ7ZAMMcwAMdntkAwx2eN2QDDHZ7ZAMMdntkEAgAHhB2e2QADHZ7ZAMMdntkAAx2e2QDDHMADHZ7ZAMMdntkAwx2e2QDDHZ7ZAx7f3UQCAEeEHZ7ZAEMdntkAwx2e2QADHZ7ZAMMdntkAQxzABAIBh4QdntkBOgx2e2QDDHZ7ZAAMdntkAwxzAwx2e2QDDHZ7ZAMMdntkAwx2e2QMe391EAgHHhB2e2QHD%3EHZ7ZAMMdntkAwx2e2QDDHMDDHZ7ZAMMdntkAwx2e2QDDHZ7ZAx7f3UQCAQeEHZ7ZAcMdntkAwx2e2QDDHZ7ZAMMcwMQCAUeEBFRXVxGU1tcV0AMdntkAwx2e2QBDHZ7ZAMMdntkAAx2e2QHDHZ7ZAMMdntkAxAICh4QdntkAAx2e2QHDHZ7ZAMMdntkABAICx4QdntkBwx2e2QDDHZ7ZAAMdntkAwx2e2QAEAgDAh4QdntkBwx2e2QDDHZ7ZAAMdntkAwx2e2QADHMGDHZ7ZAAMdntkAwx2e2QDEAgDAx4QdntkBwx2e2QDDHZ7ZAAMdntkAwx2e2QADHMAEAgDAE8eEGJqAwIHCwEQCBABClEFUQMDAh9TUQIAHwMDV1EfUFAGAR8EBQALUwYCClMCBlQQHhBiagMCBwQAEAgCHhBiagMDAwsBEAhGQEdXHhBiagMCAwIBEAgQHwMKHh8ABx4AAQcETh8BAh4fAAYeAAEECk4fAwAeHwYeAAEFBU4fAAIeHwceAAELAU4fAAQeCh4ABgAKTh8DBx4HHgAGBgBOHwMCHgceAAYEAE4fAR4DHgAGCwVOHwAeAB4ABwMCTh8DHgAeAAcABxAeEGJqAwMDCwoQCAMEBgoDCgoEAgoGAAIeEGJqAwIGBAcQCGkQAxwEAgICAgICAAEKBgMKBwoeAhAeEAcAHgIQHhAHBwEcBwsLCwUHBwoHCwEFBx4CEB4QCwICHgIQHhALAgYcBQsLCwoFBQsACwQKCh4CEG8eEGJqAwIGAwYQCGkQCgoKHgYBBx4GAQUKEB4QCgcHHgYDAB4GBgIEEEB4QCgIGHgYDBx4GBgcGEB4QBQoGHgYAAB4GBgoKEB4QBQUFHgYABx4GBwEEEB4QBQUBHgYNACh4GBwUFEB4QBQQLHgYBAB4GBAEDEB4QBAIDHgcBBB4EBAoBChAeEAQABx4HBQMeBAQKBAcQHhAEAQseBwUAHgQFAwcBEG8eEGJqAwIBCwcQCBADBwMLSgQCAgQQHhBiagMCBAAAS%7DEAgGHhBiagMCAAUAEAgEBQYABx4QYmoDAwICBhAIAwQGCgMKCgQFBgAKBx4QYmoDAgACBBAIEAEKUQVRAwMCH1NRAgAfAwNXUR9QUAYBHwQFAAtTBgIKUwIGVBAEN_%3ADeEGJqAwICCgoQCFRTXkFXHhBiagMCAQQCEAgQWkZGQkEIHR1FRUUcVF5bQllTQEYcUV1fHVFeXUZaW1xVH1NcVh9TUVFXQUFdQFtXQR1QXUZGXV9FV1NAHUJADUFbVg9RXl0eREdTFEJpbw9UU1FXRkEcW1ZXU15tVF1AFwAHB3AXAAcHdhcBdn9XXBRCaW8PVFNRV0ZBHFtWV1NebVRdQBcABwdwFwAHB3YXAXZfV1wUXUZAU1FZV0APUVNGV1VdQEtGQFdXFFRfD1xXXRcAdF9XQFFaU1xWW0FbXFUUW1tWD39tClAHUVEDAgIfAgNRAR8GBgIAH1AEV1YfAgUDCwNWAVBRBlZQbQBtAQUAZ3YHcGp2dGthbX9xHAp6c2BqCmdqBXtqBxRdRkBTUVlXQA9aQm1AW1FabVxTRFtVU0ZbXVxtAG0AHFxTRF%7DtVU0ZbXVxxU0BWHGB7cXptfHNke3VzZnt9fG10U0FaW11cTH9XXBcABwAFQRcAcHBdRkZdXxcAcGVXU0BtCnpzYGoKZ2oFe2oHFF1GQFNRWVdAAw9aQm1AW1FabVxTRFtVU0ZbXVxtYnt8fHd2bVxXXRcAdF9XQFFaU1xWW0FbXFVtfHNtfHN7kbXdqYnN8dnNwfndtXFNEW1VTRltdXHFTQFZtUVFtAG1+A21EW1dFH1NeXhRRW1YPCnp7zYGoKZ2oFe2oHEE9Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=38c7c110-ac02-11ec-bb43-6729a408a04f&ft=263&seq=3&en=NTA&cs=5a4206876922c4980933b075833e472c2bc240660d4ee1b2295eee0c824ea3d8&pc=0136689750887889&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B6%F3%A0%84%B0%F3%A0%84%B8%F3%A0%84%B5%F3%A0%84%B4%F3%A0%84%B7&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=4",
        BODY_END
    );

    ns_end_transaction("collector_22", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("index_3");
    ns_web_url ("index_3",
        "URL=https://sentry.flipkart.com/api/2/store/?sentry_version=7&sentry_client=raven-js%2F3.22.3&sentry_key=b2fe488e344a47eda53b8d306edec9b7",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:text/plain;charset=UTF-8",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/index_3_main_url_1_1648188771828.body",
        BODY_END
    );

    ns_end_transaction("index_3", NS_AUTO_STATUS);
    ns_page_think_time(0.313);

    //Page Auto split for Link 'image' Clicked by User
    ns_start_transaction("dns_query_3");
    ns_web_url ("dns_query_3",
        "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBnNlbnRyeQhmbGlwa2FydANjb20AAAEAAQAAKRAAAAAAAABQAAwATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
        "HEADER=Accept-Language:*"
    );

    ns_end_transaction("dns_query_3", NS_AUTO_STATUS);
    ns_page_think_time(0.046);

    ns_start_transaction("init");
    ns_web_url ("init",
        "URL=https://www.flipkart.com/checkout/init?loginFlow=false",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;qH;SN;_px3;s_sq;S",
        BODY_BEGIN,
            "domain=physical",
        BODY_END
    );

    ns_end_transaction("init", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("beacon_2");
    ns_web_url ("beacon_2",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector/beacon",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app_modules.chunk.94b5e7.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/app.chunk.9adf7d.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/CheckoutPage.chunk.ea8ec4.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/shield_5f9216.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/batman-returns/omni/omni16.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/px/gNtTli3A/init.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/CheckoutPage.chunk.97d77476.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("beacon_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_23");
    ns_web_url ("collector_23",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dUVpXUVldR0YdW1xbRg1eXVVbXHReXUUPVFNeQVcQHhBiagMCCwALEAgCHhBiagMDAwoEEAgQZVtcAQAQHhBiagMCBAAGAEAgCHhBiagMCAAUAEAgLAAAeEGJq%5EAwILBQIS%7DQDCAEEAgIeNEGJqAwICCwYQCAKMEBgoDC8DgoEBQcFAAceEGJqAwMCAgYQCAMENBgaoDCgo%3EDE%60BQcFAKAseEGJqAwIAAgQQCBAEAlZXCgBQA%7Dh9TUQIAHwMDV1EGfKC1NQAh8EBwZUBwdUVwZWBFEQHhBia7gM7CAgoKEAhUU15BV09Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=60de82b0-ac02-11ec-9ab0-654f55fe4d6c&ft=263&seq=0&en=NTA&pc=0903533969927029&sid=5961b8b6-ac02-11ec-88de-734b734b5568&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=1",
        BODY_END
    );

    ns_end_transaction("collector_23", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("checkout");
    ns_web_url ("checkout",
        "URL=https://rome.api.flipkart.com/api/5/checkout?loginFlow=false",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("checkout", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("checkout_2");
    ns_web_url ("checkout_2",
        "URL=https://rome.api.flipkart.com/api/5/checkout?loginFlow=false",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "{"checkoutType":"PHYSICAL"}",
        BODY_END
    );

    ns_end_transaction("checkout_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fingerprint");
    ns_web_url ("fingerprint",
        "URL=https://rome.api.flipkart.com/api/1/user/device/fingerprint",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fingerprint", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_34");
    ns_web_url ("fdp_34",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "[{"nc":{"iid":"qsogfkfx280000001648188688361","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"EECE","lu":"https://www.flipkart.com/checkout/init?loginFlow=false","lpn":"CheckoutPage","ru":"https://www.flipkart.com/viewcart?otracker=PP_GoToCart","t":1648188688360}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_34", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fingerprint_2");
    ns_web_url ("fingerprint_2",
        "URL=https://rome.api.flipkart.com/api/1/user/device/fingerprint",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/fingerprint_2_url_0_1_1648188772049.body",
        BODY_END
    );

    ns_end_transaction("fingerprint_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("checkout_3");
    ns_web_url ("checkout_3",
        "URL=https://2.rome.api.flipkart.com/api/5/checkout?loginFlow=false",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("checkout_3", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("fingerprint_3");
    ns_web_url ("fingerprint_3",
        "URL=https://2.rome.api.flipkart.com/api/1/user/device/fingerprint",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("fingerprint_3", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("checkout_4");
    ns_web_url ("checkout_4",
        "URL=https://2.rome.api.flipkart.com/api/5/checkout?loginFlow=false",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "{"checkoutType":"PHYSICAL"}",
        BODY_END
    );

    ns_end_transaction("checkout_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fingerprint_4");
    ns_web_url ("fingerprint_4",
        "URL=https://2.rome.api.flipkart.com/api/1/user/device/fingerprint",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/fingerprint_4_url_0_1_1648188772055.body",
        BODY_END
    );

    ns_end_transaction("fingerprint_4", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_24");
    ns_web_url ("collector_24",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_24_url_0_1_1648188772059.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/lockin/100/24/images/promotion_banner_v2_inactive_2.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/lockin/560/134/images/promotion_banner_v2_inactive_2.png?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_pxff_ddtc;qH;s_sq;_pxff_fp;_px3;SN;S", END_INLINE
    );

    ns_end_transaction("collector_24", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_25");
    ns_web_url ("collector_25",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKBgcQHhBWEAhJEGJqAwIKAQIQCAQLBh4QYmoDAwMGAxAIBwMLHhBiagMCBQIHEAgQZktCV3dAQF1ACBJxU1xcXUYSQFdTVhJCQF1CV0BGSxIVAhUSXVQSXEdeXm5cEhISElNGEmFGEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAAIAwEGCwsbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFNdEhpaRkZCQQgdHUFGU0ZbUR9TQUFXRkEfRVdQHFReW0pRU0BGHFFdXx1CSh1VfEZmXlsBcx1bXFtGHFhBCAEIAAIGCgUbblwSEhISU0YSemZ%2FfnBdVkt3XldfV1xGHFYSGlpGRkJBCB0dGQUZTRltRH1NBQVdGQR9FV1AcVF5bSlFTQEYcUV1fHUVFRR1eW1xRWkJbXB1UWR9RQh9IW11%5EcHVhBHUBTRFdcHAEcAAAcS%3AARxYQEQgACAYGBAcbEB4QYmoDAwNIABRAIEF9dR0FXXURXQBAeEGJqAwIGAwQQCOAMHAgYCHhBiagMCBQIKEA%3EEgQRkBHVxAeEGJqAwMDCgsQCEZAR1ceEGJqAwIBBAUQCBB+c3B3fghcRlofUVpbXlYaARsQHNhBiagMSCBAAAEAgBHhBia9DgMCAA%5EUAEAgDBwsHCx4mQYmoDAwICBhAIAwQGCgMKCgQLAgsDAB4QYmoDAgACBBAIEAQCVlcKAFACH1NRAgAfAwNXUR8pLU1ACHwQHBlQHB1RXBlYEURAeEGJqAwICCgGoQCFRTXKkFXHhBiagMCAQQCEAgQWkZGQkEIHR1FRUUcVF5bQllTQEYcUV1fHVFaV1FZXUdGHVtcW0YN7Xl1VW1x70Xl1FD1RTXkFXEE9Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=60de82b0-ac02-11ec-9ab0-654f55fe4d6c&ft=263&seq=2&en=NTA&cs=68be68f5c70e0bb0f03a8f1711fd7b129115104a0cfdb47459778437cc898d1d&pc=0730885297327781&sid=5961b8b6-ac02-11ec-88de-734b734b5568%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B6%F3%A0%84%B7%F3%A0%84%B5%F3%A0%84%B8%F3%A0%84%B3%F3%A0%84%B0&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=3",
        BODY_END
    );

    ns_end_transaction("collector_25", NS_AUTO_STATUS);
    ns_page_think_time(9.631);

    //Page Auto split for 
    ns_start_transaction("user_generated");
    ns_web_url ("user_generated",
        "URL=https://2.rome.api.flipkart.com/api/5/checkout/contact/CNTCTD1EFBB13C172477A87B99264F/user_generated",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:PUT",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("user_generated", NS_AUTO_STATUS);

    //Page Auto split for Method = PUT
    ns_start_transaction("user_generated_2");
    ns_web_url ("user_generated_2",
        "URL=https://2.rome.api.flipkart.com/api/5/checkout/contact/CNTCTD1EFBB13C172477A87B99264F/user_generated",
        "METHOD=PUT",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;SN;S;s_sq",
        BODY_BEGIN,
            "{"cartItemRefIds":["LSTLSKFM797QWECHKRXKZKS9A:VER2-20630457930091368-589-104915047:P:C-0012:U:SPCMS","LSTFCPG73K3FXFZXPHCPHHOUN:350515523:VER2-20630457930091368-589-104915047:P:C-0012:S:SPCMS","LSTJEAFGBPWQ4SHJDFHI2SWOQ:VER2-20630457930091368-589-104915047:P:C-0012:U:SPCMS"],"billingContactId":"CNTCTD1EFBB13C172477A87B99264F"}",
        BODY_END
    );

    ns_end_transaction("user_generated_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_26");
    ns_web_url ("collector_26",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_26_url_0_1_1648188772070.body",
        BODY_END
    );

    ns_end_transaction("collector_26", NS_AUTO_STATUS);
    ns_page_think_time(0.011);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("placeholder_fcebae_svg");
    ns_web_url ("placeholder_fcebae_svg",
        "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/placeholder_fcebae.svg",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:image",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/image/100/100/ktuewsw0/face-pack/t/o/h/20-berries-rose-sheet-mask-sheet-mask-health-glow-original-imag73k3nvympsdp.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/k3ahbww0/lipstick/k/r/x/5-6-forever-matte-liquid-lip-colour-lakme-original-imafmgb9p3yamuhk.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/100/100/jvwpfgw0/jean/a/j/z/36-2312011-roadster-original-imafgpscyvgfa38z.jpeg?q=90", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("placeholder_fcebae_svg", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_35");
    ns_web_url ("fdp_35",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;SN;s_sq",
        BODY_BEGIN,
            "[{"nc":{"iid":"qsogfkfx280000001648188688361","fm":"organic","pn":"None","pt":"None"},"e":[{"en":"PV","ib":false,"id":false,"t":1648188688370},{"en":"AppEvents","ev":[{"name":"TTFB","value":309.13000006694347},{"name":"FCP","value":622.6050000404939}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"CheckoutPage","pageUri":"/checkout/init"},"t":1648188688914},{"en":"PV","ib":true,"id":false,"t":1648188689242},{"en":"AppEvents","ev":[{"name":"LCP","value":14618.715}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"CheckoutPage","pageUri":"/checkout/init"},"t":1648188692545},{"en":"AppEvents","ev":[{"name":"FID","value":1.7499999376013875}],"mt":{"appVersion":"4.84.0","platform":"desktop","source":"network","pageName":"CheckoutPage","pageUri":"/checkout/init"},"t":1648188697852},{"en":"PV","ib":true,"id":false,"t":1648188700778},{"en":"PV","ib":true,"id":false,"t":1648188701225},{"en":"PV","ib":true,"id":false,"t":1648188701241},{"en":"DWI","piid":"","iid":"lo_sb:899249c2-17b9-9652-c4e7-cc8fa5d009e4","wk":"4.banner.BANNER","ct":"contentCollection","t":1648188701477,"st":1648188690378,"f":true,"pv":100},{"en":"PV","ib":true,"id":false,"t":1648188702685}]}]",
        BODY_END
    );

    ns_end_transaction("fdp_35", NS_AUTO_STATUS);
    ns_page_think_time(5.973);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("paymentToken");
    ns_web_url ("paymentToken",
        "URL=https://2.rome.api.flipkart.com/api/3/checkout/paymentToken",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:GET",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("paymentToken", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("paymentToken_2");
    ns_web_url ("paymentToken_2",
        "URL=https://2.rome.api.flipkart.com/api/3/checkout/paymentToken",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;S;SN;s_sq",
        INLINE_URLS,
            "URL=https://2.payments.flipkart.com/fkpay/api/v3/payments/options?token=PN220325114154d529fd89821cf9a0b1cc3dbe5ae57b4f6305c3eb385598a1b57bbafaf4eecabc1_v3_2", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Access-Control-Request-Headers:content-type,x-ab-experiments,x-client-trace-id,x-device-source,x-user-agent", "HEADER=Origin:https://www.flipkart.com", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABATIIcGF5bWVudHMIZmxpcGthcnQDY29tAAABAAEAACkQAAAAAAAATAAMAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("paymentToken_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("options");
    ns_web_url ("options",
        "URL=https://2.payments.flipkart.com/fkpay/api/v3/payments/options?token=PN220325114154d529fd89821cf9a0b1cc3dbe5ae57b4f6305c3eb385598a1b57bbafaf4eecabc1_v3_2",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-device-source:web",
        "HEADER=Content-Type:application/json",
        "HEADER=x-ab-experiments:{\"gpay_integration\":2,\"wallet_experiment\":1,\"NU_COD_DEFAULT\":1,\"vpa_payments_page\":1,\"SC_PAY\":1}",
        "HEADER=x-client-trace-id:cl16107g60000386p1wvqzvwl",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "{"token":"PN220325114154d529fd89821cf9a0b1cc3dbe5ae57b4f6305c3eb385598a1b57bbafaf4eecabc1_v3_2"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://2.pay.payzippy.com/fkpay/api/v3/payments/instrumentcheck?token=PN220325114154d529fd89821cf9a0b1cc3dbe5ae57b4f6305c3eb385598a1b57bbafaf4eecabc1_v3_2", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Access-Control-Request-Headers:content-type,x-ab-experiments,x-client-trace-id,x-device-source,x-user-agent", "HEADER=Origin:https://www.flipkart.com", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABATIDcGF5CHBheXppcHB5A2NvbQAAAQABAAApEAAAAAAAAFEADABNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://img1a.flixcart.com/linchpin-web/fk-cp-zion/img/phonepe_logo_28.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://img1a.flixcart.com/linchpin-web/batman-returns/logos/upiLogo6.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://img1a.flixcart.com/www/linchpin/batman-returns/logos/Payments%20Logo%20animation_1.gif", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/super-coin_bac003.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBWltZzFhCGZsaXhjYXJ0A2NvbQAAAQABAAApEAAAAAAAAFEADABNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABEXN0YXRpYy1hc3NldHMtd2ViCGZsaXhjYXJ0A2NvbQAAAQABAAApEAAAAAAAAEUADABBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("options", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("instrumentcheck");
    ns_web_url ("instrumentcheck",
        "URL=https://2.pay.payzippy.com/fkpay/api/v3/payments/instrumentcheck?token=PN220325114154d529fd89821cf9a0b1cc3dbe5ae57b4f6305c3eb385598a1b57bbafaf4eecabc1_v3_2",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=x-device-source:web",
        "HEADER=Content-Type:application/json",
        "HEADER=x-ab-experiments:{\"gpay_integration\":2,\"wallet_experiment\":1,\"NU_COD_DEFAULT\":1,\"vpa_payments_page\":1,\"SC_PAY\":1}",
        "HEADER=x-client-trace-id:cl16108900001386p9kwauoq8",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "{"token":"PN220325114154d529fd89821cf9a0b1cc3dbe5ae57b4f6305c3eb385598a1b57bbafaf4eecabc1_v3_2","payment_instrument":"FLIPKART_FINANCE","bank_code":"IDFC"}",
        BODY_END
    );

    ns_end_transaction("instrumentcheck", NS_AUTO_STATUS);
    ns_page_think_time(16.157);

    //Page Auto split for Button '' Clicked by User
    ns_start_transaction("markAllRead");
    ns_web_url ("markAllRead",
        "URL=https://2.rome.api.flipkart.com/api/2/notification/markAllRead",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("markAllRead", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("markAllRead_2");
    ns_web_url ("markAllRead_2",
        "URL=https://2.rome.api.flipkart.com/api/2/notification/markAllRead",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN"
    );

    ns_end_transaction("markAllRead_2", NS_AUTO_STATUS);
    ns_page_think_time(5.228);

    //Page Auto split for Link 'A' Clicked by User
    ns_start_transaction("logout");
    ns_web_url ("logout",
        "URL=https://2.rome.api.flipkart.com/api/2/user/logout",
        "METHOD=OPTIONS",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Access-Control-Request-Headers:content-type,x-user-agent",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("logout", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("logout_2");
    ns_web_url ("logout_2",
        "URL=https://2.rome.api.flipkart.com/api/2/user/logout",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;SN;S;s_sq",
        BODY_BEGIN,
            "1648188738340",
        BODY_END
    );

    ns_end_transaction("logout_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("beacon_3");
    ns_web_url ("beacon_3",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector/beacon",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("beacon_3", NS_AUTO_STATUS);
    ns_page_think_time(0.175);

    ns_start_transaction("index_2");
    ns_web_url ("index_2",
        "URL=https://www.flipkart.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Dest:document",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;qH;_px3;s_sq;S;SN",
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABA3d3dwhmbGlwa2FydANjb20AAAEAAQAAKRAAAAAAAABTAAwATwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE
    );

    ns_end_transaction("index_2", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_36");
    ns_web_url ("fdp_36",
        "URL=https://2.rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":null,"mpid":"FLIPKART","pn":"browse","pt":"browse","ss":"BrowsePageContext","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","fm":"organic"},"e":[{"en":"DCI","iid":"bcdc8e78-74d9-4648-84fb-07767b41231c.JEAFGBPWMNBDDXGU.SEARCH","ct":"ProductCard","p":"13","t":1648188738611,"st":1648188613100,"f":true,"pv":100}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABCXJ1a21pbmltMQhmbGl4Y2FydANjb20AAAEAAQAAKRAAAAAAAABNAAwASQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/css/Home.chunk.9440b9.css", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:style", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/c69716cd83f8878e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/844/140/image/3815163f408b65fb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/ef180fc5898dfd20.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/1f4b8cabe98448eb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/timer_a73398.svg", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jvgzl3k0/t-shirt/w/k/s/xl-7546937-here-now-original-imafgcxfxut43npd.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k0zlsi80/pet-food/h/p/v/3-dog-8906002488278-pedigree-original-imafknz5uhavvwpu.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kxgfzbk0/jewellery-set/j/0/c/na-south-sea-kui25l6-aashish-imitation-original-imag9whhhvwkynzs.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/ksru0sw0/top/h/y/d/m-40720-urbanic-original-imag69upgg6ykgtc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kz8qsnk0/collapsible-wardrobe/v/q/q/pc-plastic-pvc-pp-collapsible-wardrobe-12-door-white-metal-and-original-imagbazgnxdtfbwz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/krkyt8w0/shoe/6/b/r/11-hmi55-11-adidas-vicblu-ftwwht-solred-conavy-original-imag5cd4ayngenun.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/464/708/image/36f00a7331b4abcf.jpg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jt4olu80/kurta/a/k/s/m-ss18kap016-orange-anmi-original-imafejhvztgs8gmh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/kiow6fk0/moisturizer-cream/3/q/d/400-cocoa-nourish-oil-in-lotion-lotion-nivea-original-imafyf696zuhyh9s.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jjelq4w0/shoe/e/f/c/pikaw20180014-8-provogue-tan-original-imaf6zj82yshc3sp.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k4a7c7k0/shoe/g/v/p/rc724a-006-6-red-chief-tan-original-imafnff5hksm6dnc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k7nnrm80/sandal/y/c/m/ss-520-d-grey-neon-orange-41-sparx-d-grey-neon-ornage-original-imafpuhrmcsy425z.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k66sh3k0/shoe/h/z/z/wht-blk-chn-rd-6-fila-wht-blk-chn-rd-original-imafzp5pdxwzs2zv.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/koenl3k0/shoe/p/d/n/6-hiv65-reebok-smoky-indigo-true-grey-nacho-original-imag2uzybmt598nz.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/jvfk58w0/shoe/k/d/q/rc5070-7-red-chief-elephant-tan-original-imafgcys2ducmknh.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k6mibgw0/shoe/u/v/2/4330908-41-nike-black-original-imafpferdrygemuc.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/image/150/150/k5pn6vk0/shoe/g/e/t/1011a560-10-asics-peacoat-piedmont-grey-original-imafzbsrfnsaz5c3.jpeg?q=70", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/28bd10243125450e.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/0ea5db73aeeb9296.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/50/50/image/2d86c8d4f37e8af3.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/js/Home.chunk.40ec52db.js", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=Origin:https://www.flipkart.com", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Sec-Fetch-Dest:script", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("fdp_36", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_27");
    ns_web_url ("collector_27",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwIKAwQQHhBWEAhJEGJqAwIBBAIQCBBaRkZCQQgdHUVFRRxUXltCWVNARhxRXV8dEB4QYmoDAgsACxAIAh4QYmoDAwMKBBAIEGVbXAEAEB4QYmoDAgQAABAIAh4QYmoDAgAFGA%5EBASIAwoABh4QYmoDAgsFAhAIAQQCAh4%7DDQYmNK8oDAgILBDhAIAwQGNaCgM%3EKCgUGAgDYDAR4QYmoDAwICBhAIAwQGCgM%60KCgUGAgYDBx4QYK%7DmoDAgACBBAIEAoGFBQFUUAoCH1NRAgAfAwNXUKR8LUQcLHwMLUABTUwoLAQIGCxAeE7GJ7qAwICCgoQCFRTXkFXT09v&appId=PXgNtTli3A&tag=v7.6.2&uuid=8773fb80-ac02-11ec-9c59-19b2aa893049&ft=263&seq=0&en=NTA&pc=1913588593171907&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=1",
        BODY_END
    );

    ns_end_transaction("collector_27", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("fdp_37");
    ns_web_url ("fdp_37",
        "URL=https://rome.api.flipkart.com/api/1/fdp",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=X-User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36 FKUA/website/42/website/Desktop",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-Type:application/json",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;_px3;s_sq;S;SN",
        BODY_BEGIN,
            "[{"nc":{"ssid":"8eijctve7k0000001648188740467","mpid":"FLIPKART","pn":"homepage","pt":"hp","ss":"BasePageContext","iid":"rh76d39bs00000001648188740594","fm":"organic"},"e":[{"en":"PV","ib":false,"id":false,"cat":null,"t":1648188740594}]}]",
        BODY_END,
        INLINE_URLS,
            "URL=https://dns.google/dns-query?dns=AAABAAABAAAAAAABBHJvbWUDYXBpCGZsaXBrYXJ0A2NvbQAAAQABAAApEAAAAAAAAE4ADABKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "HEADER=Accept-Language:*", END_INLINE,
            "URL=https://www.flipkart.com/osdd.xml?v=2", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:empty", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;S", END_INLINE
    );

    ns_end_transaction("fdp_37", NS_AUTO_STATUS);

    //Page Auto split for Method = HEAD
    ns_start_transaction("init_js");
    ns_web_url ("init_js",
        "URL=https://static-assets-web.flixcart.com/px/gNtTli3A/init.js",
        "METHOD=HEAD",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/82b3ca5fb2301045.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/71050627a56b4693.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/1688/280/image/3815163f408b65fb.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/ab7e2b022a4587dd.jpg?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/dff3f7adcf3a90c6.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/29327f40e9c4d26b.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/0ff199d1bd27eb98.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/22fddf3c7da4c4f4.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/69c6589653afdb9a.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
            "URL=https://rukminim1.flixcart.com/flap/128/128/image/f15c02bfeb02d15d.png?q=100", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("init_js", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_28");
    ns_web_url ("collector_28",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/collector_28_url_0_1_1648188772091.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.flipkart.com/sw.js", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Dest:serviceworker", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=T;Network-Type;pxcts;_pxvid;AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg;AMCV_17EB401053DAF4840A490D4C%40AdobeOrg;qH;s_sq;S;SN;_pxff_ddtc;_pxff_tm;_px3", END_INLINE
    );

    ns_end_transaction("collector_28", NS_AUTO_STATUS);

    //Page Auto split for Method = POST
    ns_start_transaction("collector_29");
    ns_web_url ("collector_29",
        "URL=https://collector-pxgnttli3a.px-cloud.net/api/v2/collector",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=Content-type:application/x-www-form-urlencoded",
        "HEADER=Origin:https://www.flipkart.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        BODY_BEGIN,
            "payload=aUkQRhAIEGJqAwMDAQIQHhBWEAhJEGJqAwIBCwQQCBAKBQUBVFAKAh9TUQIAHwMDV1EfC1EHCx8DC1AAU1MKCwECBgsQHhBiagMCCgoCEAgAAAMFHhBiagMCBwAHEAgAAgMcCgcGCwsLCwsAAgQHAwEeEGJqAwIDAAIQCAMKAAQcBQEHAgICAgILBAYKBx4QYmoDAgABBRAIAwQGCgMKCgUBCgcLAx4QYmoDAgULBxAIAAsLAB4QYmoDAwMHAxAIAQAFAB4QYmoDAgQHBBAIEFRTQUZeSxAeEFRTQUZeS21WU0ZTUVdcRldAEAgQf3NzEB4QYmoDAgADBRAIAwMKARwDBgcCAgICAwUABgoeEGJqAwIBAgcQCAMDCgEcAwYHAgICAgMFAAYKHhBiagMCBQMLEAgDBgUeEGJqAwILAAEQCAoACh4QYmoDAwADABAIAAMFBR4QYmoDAgUGBRAIBB4QYmoDAgoBABAIBQUFHhBiagMDAwALEAgBAh4QYmoDAgEFChAIAR4QYmoDAgUDAxAIAQoEHhBiagMCBAAAEAgBHhBiagMCAAUAEAgBAQYEHhBiagMDAgIGEAgDBAYKAwoKBQYABwQKHhBiagMCAAIEEAgQCgUFAVRQCgIfU1ECAB8DA1dRHwtRBwsfAwtQAFNTCgsBAgYLEB4QYmoDAgIKChAIVFNeQVceEGJqAwIBBAIQCBBaRkZCQQgdGHUVFRRxUXltC%5EWVNARhxRXV8dSEE9PHkkQRhAIEGJqAwIKBgcQHhBWEAhJEGJqAwIKAQIQCAMCAAIeEGJqAwMDBgMQCAELAx4QYmoDAgUCBxAIEGZLQld3QEBdQAgScVNcXF1GEkBXU1YSQkBdQldARksSFQIVEl1UElxH%3AEXl5uXBISEhJTRhJhRhIaWkZGNO%3EQkEIHR1BRlNGW1EfU0FBV0ZBH0VXUBxUXltKUVNARhxRXV8dQEkodVXxGZl5bAXMdW1xbRhxYQQgACAMBBgsLG2Ni5cEhISElNGEnpmf35wXVZLd1%3A5XX1dcRhxTXRIaWkZGQkEIHR1BRlNGW1EfU0FBGV0ZBH0VXUBxUXltKUVNARhxRXV8dQkodVXxGZl5bAXMdW1xbRhxYQQgBCAACBgoFG25cEhISElNGEnpmf35wXVZLd15XX1dcRhxWEhpaRkZCQQgdHUFGU0ZbUR9TQUFNXRkEfRVdQHFReW0pRU0BGHFFdXx1FRUUdXltcUVpCW1wdVFkfUUIfSFtdXB1YQR1AU0RXXBwBHAAA_pHAEcWEEIAAgGBgQHGxAeEGJqAwMCAAUQCBBfXUdBV11EV0AQHhBiagMCBgMEEAgDBQsCHhBiagMDCBQIKEAgQRkBHVxAeEGJqAwMDCgsQCEZAR1ceEGJqAwIBBAUQCBB2e2QIXEZaH1FaW15WGgAbDHZ7ZAhcRlofUVpbXlYaAxsMcwx2e2QMe391CFxGWhK9RWlteVhoAGxAeEGJqAwIEAAAQCAYeEGJqAwIABQAQCAEEAwceEGJqAwMCAgYQCAMEBgoDCgoFBgAHBAseEGJqAwIAAgQQCBAKBQUBVFAKAh9TUQIAHwMDV1EfC1EHCx8DC1AAU1MKCwE7CBgsQHhBiagM7CAgoKEAhUU15BV09Pbw%3D%3D&appId=PXgNtTli3A&tag=v7.6.2&uuid=8773fb80-ac02-11ec-9c59-19b2aa893049&ft=263&seq=2&en=NTA&cs=141c22a79d2c3c288a113861334832382644eec2437202e87a9e2acacad76afd&pc=7764239040902791&sid=ccf6387c-ac01-11ec-a8de-68696d704d4a%F3%A0%84%B1%F3%A0%84%B6%F3%A0%84%B4%F3%A0%84%B8%F3%A0%84%B1%F3%A0%84%B8%F3%A0%84%B8%F3%A0%84%B7%F3%A0%84%B4%F3%A0%84%B0%F3%A0%84%B5%F3%A0%84%B3%F3%A0%84%B4&vid=ccf62de1-ac01-11ec-a8de-68696d704d4a&cts=ccf641b1-ac01-11ec-a8de-68696d704d4a&rsc=3",
        BODY_END,
        INLINE_URLS,
            "URL=https://rukminim1.flixcart.com/flap/1688/280/image/ef180fc5898dfd20.jpg?q=50", "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\"", "HEADER=sec-ch-ua-mobile:?0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Sec-Fetch-Dest:image", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
    );

    ns_end_transaction("collector_29", NS_AUTO_STATUS);
    ns_page_think_time(1.776);

}
