--Request 
OPTIONS https://2.rome.api.flipkart.com/api/4/product/swatch
Host: 2.rome.api.flipkart.com
Accept: */*
Access-Control-Request-Method: POST
Access-Control-Request-Headers: content-type,x-user-agent
Origin: https://www.flipkart.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
Sec-Fetch-Dest: empty
Referer: https://www.flipkart.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Server: nginx
Date: Fri, 25 Mar 2022 06:10:11 GMT
Content-Type: application/json;charset=utf-8
Content-Length: 0
Access-Control-Expose-Headers: X-BOT
Access-Control-Allow-Origin: https://www.flipkart.com
Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT
Access-Control-Allow-Credentials: true
Access-Control-Max-Age: 2592000
Access-Control-Allow-Headers: content-type,x-user-agent,X-ACK-RESPONSE,X-PARTNER-CONTEXT
Allow: GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH
----
--Request 
GET https://dns.google/dns-query?dns=AAABAAABAAAAAAABATIEcm9tZQNhcGkIZmxpcGthcnQDY29tAAABAAEAACkQAAAAAAAATAAMAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Host: dns.google
Accept: application/dns-message
Accept-Language: *
User-Agent: Chrome
Accept-Encoding: identity
----
--Response 
HTTP/1.1 200
x-content-type-options: nosniff
strict-transport-security: max-age=31536000; includeSubDomains; preload
access-control-allow-origin: *
date: Fri, 25 Mar 2022 06:10:11 GMT
expires: Fri, 25 Mar 2022 06:10:11 GMT
cache-control: private, max-age=149
content-type: application/dns-message
server: HTTP server (unknown)
content-length: 468
x-xss-protection: 0
x-frame-options: SAMEORIGIN
alt-svc: h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----

