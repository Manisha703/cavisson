--Request 
OPTIONS https://2.rome.api.flipkart.com/api/5/checkout/contact/CNTCTD1EFBB13C172477A87B99264F/user_generated
Host: 2.rome.api.flipkart.com
Accept: */*
Access-Control-Request-Method: PUT
Access-Control-Request-Headers: content-type,x-user-agent
Origin: https://www.flipkart.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-site
Sec-Fetch-Dest: empty
Referer: https://www.flipkart.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
----
--Response 
HTTP/1.1 200 OK
Server: nginx
Date: Fri, 25 Mar 2022 06:11:40 GMT
Content-Type: application/json;charset=utf-8
Content-Length: 0
Access-Control-Expose-Headers: X-BOT
Access-Control-Allow-Origin: https://www.flipkart.com
Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT
Access-Control-Allow-Credentials: true
Access-Control-Max-Age: 2592000
Access-Control-Allow-Headers: content-type,x-user-agent,X-ACK-RESPONSE,X-PARTNER-CONTEXT
Allow: GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH
----

